﻿using Guna.UI2.WinForms;
using STAP_CDC.Acceso;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
            imgSlide.Visible = false;
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        [System.Runtime.InteropServices.DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [System.Runtime.InteropServices.DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wasm, int wparam, int lparam);


        private void moveImageBox(object sender)
        {
            Guna2Button b = (Guna2Button)sender;
            imgSlide.Location = new Point(b.Location.X + 24, b.Location.Y - 25);
            imgSlide.SendToBack();
            imgSlide.Visible = true;
        }

        public Form FormularioActivo = null;
        public void AbrirFormulario(Form f)
        {
            if (FormularioActivo != null)
            {
                FormularioActivo.Close();
            }
            FormularioActivo = f;
            f.TopLevel = false;
            f.FormBorderStyle = FormBorderStyle.None;
            f.Dock = DockStyle.Fill;
            PanelFormulario.Controls.Add(f);
            PanelFormulario.Tag = f;
            f.BringToFront();
            f.Show();
        }

        private void moveSideMark(object sender, EventArgs e)
        {
            moveImageBox(sender);
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }



        private void LblHome_Click(object sender, EventArgs e)
        {
            
            BtnSeguimiento.Checked = false;
            BtnCRUDproyectos.Checked = false;
            BtnCRUDPersonal.Checked = false;
            btnCerrarSesion.Checked = false;
            imgSlide.Visible = false;
            FormularioActivo.Close();
        }



        private void BtnCRUDProyectos_Click(object sender, EventArgs e)
        {
            AbrirFormulario(new CRUD_proyectos.FrmProyectos());
        }

        private void BtnCRUDPersonal_Click(object sender, EventArgs e)
        {
            AbrirFormulario(new CRUD_personal.FrmUsuarios());
        }

        private void btnCRUCRol_Click(object sender, EventArgs e)
        {
            AbrirFormulario(new CRUD_roles.FrmRol());
        }

        private void PanelFormulario_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnReportesProyecto_Click(object sender, EventArgs e)
        {
            AbrirFormulario(new Reportes.FrmReporteProyectos());
		}
        private void BtnSalir_Click(object sender, EventArgs e)
        {


        }

        private void BtnSeguimiento_Click(object sender, EventArgs e)
        {
            AbrirFormulario(new Gestion_proyectos.FrmListaProyectos());
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            //Verificar el tipo de usuario
            if (Global.NivelUsuario == 0) //Es un usuario normal
            {
                //Ocultar las opciones de administración
                //BtnCRUDPersonal.Visible = false;
                btnCRUDRol.Visible = false;
                BtnCRUDproyectos.Visible = false;
                BtnReportesProyecto.Visible = false;
            }
        }

        private void BtnMax_Click(object sender, EventArgs e) {

        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            FrmLogin mFrmLogin = new FrmLogin();
            this.Visible = false;
            this.Close();
            mFrmLogin.ShowDialog();
            
        }
    }
}
