﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using CRUD_roles;

namespace STAP_CDC.CRUD.CRUD_roles
{
    class ControlRoles
    {
        Conexion mConexion;
        public ControlRoles()
        {
            mConexion = new Conexion();
            mConexion.conectar();
        }

        public bool crearRol(Rol mRol)
        {
            string SQL = "insert into rol values (null, '?i', '?n')";
            SQL = SQL.Replace("?i", mRol.identificador);
            SQL = SQL.Replace("?n", mRol.nombreRol);
            return mConexion.ejecutarActualizacion(SQL);
        }

        public bool eliminarRol(string id)
        {
            string SQL = "delete from rol where idRol =?d;";
            SQL = SQL.Replace("?d", id);
            return mConexion.ejecutarActualizacion(SQL);
        }

        public bool modificarRol(Rol mRol, string id)
        {
            string SQL = "update rol set identificador = '?i', nombre_rol = '?n' where idRol =?d;";
            SQL = SQL.Replace("?i", mRol.identificador);
            SQL = SQL.Replace("?n", mRol.nombreRol);
            SQL = SQL.Replace("?d", id);
            return mConexion.ejecutarActualizacion(SQL);
        }

        public DataSet consultarRoles()
        {
            string SQL = "select idRol as 'Código', identificador as 'Identificador', nombre_rol as 'Nombre del rol' from rol;";
            DataSet ListaRoles = mConexion.ejecutarConsulta(SQL);
            return ListaRoles;
        }

        public DataSet buscarRol(string buscado)
        {
            string SQL = "select idRol as 'Código', identificador as 'Identificador', nombre_rol as 'Nombre del rol' from rol where (rol.identificador like '%?b%' || rol.nombre_rol like '%?b%');";
            SQL = SQL.Replace("?b", buscado);
            DataSet ListaRoles = mConexion.ejecutarConsulta(SQL);
            return ListaRoles;
        }

    }
}
