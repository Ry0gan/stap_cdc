﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using CRUD_roles;
using STAP_CDC.CRUD.CRUD_roles;

namespace CRUD_roles
{
    public partial class FrmRol : Form
    {
        ControlRoles mControlRoles;
        Conexion mConexion;
        string nombre;
        string identificador;
        string id;

        public FrmRol()
        {
            InitializeComponent();
            mControlRoles = new ControlRoles();

         
        }
        

        private void FrmRol_Load(object sender, EventArgs e)
        {
            llenarTabla();
        }

        public void llenarTabla()
        {
            try
            {
                DtgRoles.DataSource = mControlRoles.consultarRoles().Tables[0];
                DtgRoles.AutoResizeColumns();
            }
            catch
            {
                error();
            }
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            FrmAddRol mFrmAddRol = new FrmAddRol();
            mFrmAddRol.Text = "Agregar rol";
            mFrmAddRol.ShowDialog();

            if (mFrmAddRol.continuar)
            {
                Rol mRol = new Rol();

                mRol.nombreRol = mFrmAddRol.getNombre();
                mRol.identificador = mFrmAddRol.getIdentificador();

               try
                {
                    if (mControlRoles.crearRol(mRol))
                    {
                        MessageBox.Show("Rol agregado con éxito!");
                        llenarTabla();
                    }
                }
                catch
                {
                    error();
                }
            }
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (DtgRoles.Rows.Count > 0)
            {
                if (MessageBox.Show("¿Seguro que desea eliminar el rol " + DtgRoles.SelectedRows[0].Cells[2].Value.ToString() + "?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        if (mControlRoles.eliminarRol(DtgRoles.SelectedRows[0].Cells[0].Value.ToString()))
                        {
                            MessageBox.Show("Rol eliminado con éxito!");
                            llenarTabla();
                        }
                        else
                        {
                            MessageBox.Show("no");
                        }
                    }
                    catch
                    {
                        error();
                    }
                }
            }
            else
            {
                MessageBox.Show("No existen elementos actualmente, por favor agregue.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void BtnModificar_Click(object sender, EventArgs e)
        {
            if (DtgRoles.Rows.Count > 0)
            {
                id = DtgRoles.SelectedRows[0].Cells[0].Value.ToString();
                identificador = DtgRoles.SelectedRows[0].Cells[1].Value.ToString();
                nombre = DtgRoles.SelectedRows[0].Cells[2].Value.ToString();
               

                FrmAddRol mFrmAddRol = new FrmAddRol();
                mFrmAddRol.Text = "Modificar rol";
                mFrmAddRol.modificar(nombre, identificador);
                mFrmAddRol.ShowDialog();

                if (mFrmAddRol.continuar)
                {
                    Rol mRolNuevo = new Rol();

                    mRolNuevo.nombreRol = mFrmAddRol.getNombre();
                    mRolNuevo.identificador = mFrmAddRol.getIdentificador();

                   try
                    {
                        if (mControlRoles.modificarRol(mRolNuevo, id))
                        {
                            MessageBox.Show("Rol modificado con éxito!");
                            llenarTabla();
                        }
                    }
                    catch
                    {
                        error();
                    }
                }
            }
        }
        private void TxtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                DtgRoles.DataSource = mControlRoles.buscarRol(TxtBuscar.Text).Tables[0];
                DtgRoles.AutoResizeColumns();
            }
            catch
            {
                error();
            }
        }

        public void error()
        {
            MessageBox.Show("ERROR al conectarse a la base de datos. \n\nSi el problema persiste por favor de comuníquese con el adrministrador.","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
