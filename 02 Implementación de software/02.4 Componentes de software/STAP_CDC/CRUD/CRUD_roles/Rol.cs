﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_roles
{
    class Rol
    {
        public string idRol { get; set; }
        public string identificador { get; set; }
        public string nombreRol { get; set; }

        public Rol()
        {
            idRol = "";
            identificador = "";
            nombreRol = "";
        }

    }
}
