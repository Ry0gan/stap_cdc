﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_roles
{
    public partial class FrmAddRol : Form
    {
        public bool continuar;

        public FrmAddRol()
        {
            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            continuar = false;
            Bitmap bmp = new Bitmap(STAP_CDC.Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        public string getNombre()
        {
            return TxtNombre.Text;
        }

        public string getIdentificador()
        {
            return TxtIdentificador.Text;
        }

        public void modificar(string nombre, string identificador) {
            LblAccion.Text = "Modificar " + identificador + " - " + nombre;
            TxtNombre.Text = nombre;
            TxtIdentificador.Text = identificador;
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            if (TxtNombre.Text.Trim() != "")
            {
                if (TxtIdentificador.Text.Trim() != "")
                {
                    continuar = true;
                    this.Close();
                } else
                {
                    Error.Clear();
                    Error.SetError(TxtIdentificador, "Campo identificador no puede quedar vacío");
                    TxtIdentificador.Focus();
                }
            } else
            {
                Error.Clear();
                Error.SetError(TxtNombre, "Campo nombre no puede quedar vacío");
                TxtNombre.Focus();
            }
        }

        private void BtnCanelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAddRol_Load(object sender, EventArgs e)
        {

        }

        private void FrmAddRol_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmAddRol_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
