﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_proyectos
{
    public partial class FrmAltaProyecto : Form
    {
        public FrmAltaProyecto()
        {
            InitializeComponent();
            TxtTitulo.Select();
            Bitmap bmp = new Bitmap(STAP_CDC.Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
           Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
           
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAltaProyecto_FormClosed(object sender, FormClosedEventArgs e) { 

        }

        private void MCFechaInicio_DateChanged(object sender, DateRangeEventArgs e)
        {
            DtpFechaInicio.Value = MCFechaInicio.SelectionStart;
            DtpFechaFin.Value = MCFechaInicio.SelectionEnd;
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            try
            {
                TxtTitulo.Text = TxtTitulo.Text.Trim();
                Proyecto mProyecto = new Proyecto();
                if(String.IsNullOrEmpty(TxtTitulo.Text))
                {
                    MessageBox.Show(this, "El campo Título no puede estar vacío", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtTitulo.Focus();
                }
                else
                {
                    mProyecto.settitulo_proyecto(TxtTitulo.Text);
                    mProyecto.setDetalles(TxtDetalles.Text);

                    if (DateTime.Compare(DtpFechaInicio.Value.Date, DtpFechaFin.Value.Date) > 0)
                    {
                        MessageBox.Show(this, "La fecha final no puede ser mayor a la inicial", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        mProyecto.setInicio_Proyecto(DtpFechaInicio.Text);
                        mProyecto.setFin_Proyecto(DtpFechaFin.Text);
                        ControlProyectos mControl = new ControlProyectos();
                        mControl.agregarProyecto(mProyecto);
                        MessageBox.Show(this, "Proyecto Registrado", "Exito!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FrmProyectos.cambioA = true;
                        this.Close();
                    }                        
                }
                
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString());
                MessageBox.Show(this, "Error", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void guna2Button1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(this, "La fecha final no puede ser mayor a la inicial \n El campo título no puede estar vacío", "Ayuda", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void FrmAltaProyecto_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmAltaProyecto_Enter(object sender, EventArgs e) {
           
        }

        private void FrmAltaProyecto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((TxtTitulo.Text.Length > 0) && (TxtDetalles.Text.Length > 0)
                        && (e.KeyChar == (char)Keys.Enter))
            {
                guna2Button3.PerformClick();
            }
        }
    }
}
