﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CRUD_proyectos
{
    public partial class Proyectos : UserControl
    {
        private string id;
        private string titulo;
        private string detalles;
        private string Fecha_inicio;
        private string Fecha_fin;
        public static string numero = " ";
        public FrmProyectos padre;
        public Proyectos()
        {
            InitializeComponent();
            
        }

        public string ID
        {
            get { return id; }
            set { id = value; LblID.Text = value; }
        }


        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; LblNombre.Text = value; }
        }

        public string Detalles
        {
            get { return detalles; }
            set { detalles = value; LblDetalles.Text = value; }
        }

        public string Fecha
        {
            get { return Fecha_fin; }
            set { Fecha_fin = value; LblFechaFin.Text = value; }
        }

        public string Fecha2
        {
            get { return Fecha_inicio; }
            set { Fecha_inicio = value; LblFechaInicio.Text = value; }
        }

        

        private void guna2CustomGradientPanel5_MouseHover(object sender, EventArgs e)
        {
            PbGif.Image = global::STAP_CDC.Properties.Resources.ESTE_ES_EL_GIF;
            PbGif.SizeMode = PictureBoxSizeMode.StretchImage;
            guna2CustomGradientPanel5.FillColor = Color.Blue;
        }

        private void guna2CustomGradientPanel5_MouseLeave(object sender, EventArgs e)
        {
            PbGif.Image = global::STAP_CDC.Properties.Resources.Stap2;
            guna2CustomGradientPanel5.FillColor = Color.FromArgb(28, 34, 43);
            guna2CustomGradientPanel5.FillColor2 = Color.FromArgb(28, 34, 43);
            guna2CustomGradientPanel5.FillColor3 = Color.FromArgb(28, 34, 43);
            guna2CustomGradientPanel5.FillColor4 = Color.FromArgb(28, 34, 43);
        }

        private void guna2CustomGradientPanel5_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void guna2CustomGradientPanel5_Click(object sender, EventArgs e)
        {
           
        }

        private void guna2CustomGradientPanel5_DoubleClick(object sender, EventArgs e)
        {
          
        }

        private void guna2CustomGradientPanel5_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void MenuModificar_Click(object sender, EventArgs e)
        {
            FrmModificarProyectos mModificar = new FrmModificarProyectos();
            mModificar.ObtenerID(ID);
            mModificar.ShowDialog();
            padre.CargarProyecto();

        }

        private void MenuEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea eliminar el proyecto ?", "ADVERTENCIA", MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes){
                ControlProyectos mControl = new ControlProyectos();
                mControl.EliminarProyecto(ID);
                padre.CargarProyecto();
            }
        }

        private void guna2CustomGradientPanel5_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        CmsOpciones.Show(this, new Point(e.X, e.Y));
                    }
                    break;
            }
        }

        private void MenuEliminar_MouseHover(object sender, EventArgs e)
        {
           
        }

        private void Proyectos_Load(object sender, EventArgs e)
        {
           
        }

        private void PbGif_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void PbGif_MouseLeave(object sender, EventArgs e)
        {
            
        }

        private void LblDetalles_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(LblDetalles, Detalles);
        }

        private void LblNombre_MouseHover(object sender, EventArgs e)
        {
            //LblNombre.Enabled = false;
        }
    }
}
