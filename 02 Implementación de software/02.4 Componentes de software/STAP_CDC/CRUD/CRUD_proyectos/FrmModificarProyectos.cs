﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_proyectos
{
    public partial class FrmModificarProyectos : Form
    {
        private String ID;
        public FrmModificarProyectos()
        {
            InitializeComponent();
            TxtTitulo.Select();
            Bitmap bmp = new Bitmap(STAP_CDC.Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        public void ObtenerID(String ID)
        {
            this.ID = ID;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
           
        
        }

        private void FrmModificarProyectos_Load(object sender, EventArgs e) {
            AsignarDatos(ID);
        }

        public void AsignarDatos(String ID)
        {
            ControlProyectos mControl = new ControlProyectos();
            DataSet Proyecto = mControl.ConsultarP(ID);
            String fecha1 = "";
            String fecha11 = "";
            String fecha2 = "";
            String fecha22 = "";
            foreach (System.Data.DataRow row in Proyecto.Tables[0].Rows)
            {

                TxtTitulo.Text = row["titulo_proyecto"].ToString();
                TxtDetalles.Text = row["detalles"].ToString();
                fecha1 = row["inicio_proyecto"].ToString();
                fecha11 = fecha1.Remove(10);
                DtpFechaInicio.Text = fecha11;
                fecha2 = row["fin_proyecto"].ToString();
                fecha22 = fecha2.Remove(10);
                DtpFechaFin.Text = fecha22;
            }

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {

        }

        private void MCFechaInicio_DateChanged(object sender, DateRangeEventArgs e)
        {
            DtpFechaInicio.Value = MCFechaInicio.SelectionStart;
            DtpFechaFin.Value = MCFechaInicio.SelectionEnd;
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void guna2Button1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(this, "La fecha final no puede ser mayor a la inicial \n El campo título no puede estar vacío", "Ayuda", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void FrmModificarProyectos_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmModificarProyectos_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void guna2Button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                TxtTitulo.Text = TxtTitulo.Text.Trim();
                Proyecto mProyecto = new Proyecto();
                if (String.IsNullOrEmpty(TxtTitulo.Text))
                {
                    MessageBox.Show(this, "El campo Título no puede estar vacío", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtTitulo.Focus();
                }
                else
                {
                    mProyecto.settitulo_proyecto(TxtTitulo.Text);
                    mProyecto.setDetalles(TxtDetalles.Text);

                    if (DateTime.Compare(DtpFechaInicio.Value.Date, DtpFechaFin.Value.Date) > 0)
                    {
                        MessageBox.Show(this, "La fecha final no puede ser mayor a la inicial", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        mProyecto.setInicio_Proyecto(DtpFechaInicio.Text);
                        mProyecto.setFin_Proyecto(DtpFechaFin.Text);
                        ControlProyectos mControl = new ControlProyectos();
                        mControl.ModificarProyecto(mProyecto, ID);
                        MessageBox.Show(this, "Proyecto Modificado", "Exito!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FrmProyectos.cambioM = true;
                        this.Close();
                    }
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString());
                MessageBox.Show(this, "Error", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnSalir_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
