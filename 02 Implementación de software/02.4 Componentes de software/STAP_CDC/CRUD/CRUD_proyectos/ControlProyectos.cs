﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;



namespace CRUD_proyectos
{
    public class ControlProyectos
    {
        Conexion mConexion;
        public ControlProyectos()
        {
            mConexion = new Conexion();
            mConexion.conectar();
        }

        public void agregarProyecto(Proyecto mProyecto)
        {
            String SQL = "insert into proyecto values(null,'?1','?2','?3','?4','?5')";
            SQL = SQL.Replace("?1", mProyecto.getTitulo_proyecto());
            SQL = SQL.Replace("?2", mProyecto.getDetalles());
            SQL = SQL.Replace("?3", mProyecto.getInicio_Proyecto());
            SQL = SQL.Replace("?4", mProyecto.getFin_Proyecto());
            SQL = SQL.Replace("?5", mProyecto.Visibilidad.ToString());
            mConexion.ejecutarActualizacion(SQL);
        }


        public void EliminarProyecto(String ID)
        {
            String SQL = "update proyecto set Visibilidad = 0 where idProyecto =  \"" + ID + "\" ";
            mConexion.ejecutarActualizacion(SQL);
        }

        public void RecuperarProyecto(String ID)
        {
            String SQL = "update proyecto set Visibilidad = 1 where idProyecto =  \"" + ID + "\" ";
            mConexion.ejecutarActualizacion(SQL);
        }

        public DataSet ConsultarProyectos()
        {
            String SQL = "select * from proyecto where Visibilidad = 1";
            DataSet ListaProyectos = mConexion.ejecutarConsulta(SQL);
            return ListaProyectos;
        }


        public DataSet ConsultarProyectosPorUsuario(string idPersonal)
        {
            String SQL = "select distinct proyecto.idProyecto, titulo_proyecto, detalles, inicio_proyecto, fin_proyecto, visibilidad from proyecto inner join equipo_trabajo on proyecto.idProyecto = equipo_trabajo.idProyecto where idPersonal = \""+ idPersonal+ "\" and visibilidad = 1";
            DataSet ListaProyectos = mConexion.ejecutarConsulta(SQL);
            return ListaProyectos;
        }

        public DataSet ConsultarProyectosE()
        {
            String SQL = "select * from proyecto where Visibilidad = 0";
            DataSet ListaProyectos = mConexion.ejecutarConsulta(SQL);
            return ListaProyectos;
        }

        public DataSet ConsultarP(String ID)
        {
            String SQL = "select titulo_proyecto,detalles,inicio_proyecto,fin_proyecto from proyecto where idProyecto =  \"" + ID + "\" AND Visibilidad = 1";
            DataSet ListaProyectos = mConexion.ejecutarConsulta(SQL);
            return ListaProyectos;
        }


        public void ModificarProyecto(Proyecto mProyecto, String ID)
        {
            String SQL = "update proyecto set "
                + "titulo_proyecto='?1',"
                + "detalles='?2',"
                + "inicio_proyecto='?3',"
                + "fin_proyecto = '?4',"
                + "Visibilidad = '?5'"
                + " where idProyecto = \"" + ID + "\"";
            SQL = SQL.Replace("?1", mProyecto.getTitulo_proyecto());
            SQL = SQL.Replace("?2", mProyecto.getDetalles());
            SQL = SQL.Replace("?3", mProyecto.getInicio_Proyecto());
            SQL = SQL.Replace("?4", mProyecto.getFin_Proyecto());
            SQL = SQL.Replace("?5", mProyecto.Visibilidad.ToString());
            mConexion.ejecutarActualizacion(SQL);
        }

    }
}
