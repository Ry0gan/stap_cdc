﻿namespace CRUD_proyectos
{
    partial class FrmProyectos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProyectos));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.BtnRegresar = new Guna.UI2.WinForms.Guna2Button();
            this.PBNProyectos = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.PBNEliminados = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.PBNTotalProyectos = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.BtnEliminados = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.TxtFiltrar = new Guna.UI2.WinForms.Guna2TextBox();
            this.BtnNuevo0 = new Guna.UI2.WinForms.Guna2Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LblDialogoP = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.DtgProyectos = new System.Windows.Forms.DataGridView();
            this.LblTotalProyectos = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LblProyectosEliminados = new System.Windows.Forms.Label();
            this.LblProyectosActivos = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.DtgProyectosE = new System.Windows.Forms.DataGridView();
            this.Panel0 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PbAviso = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PanelProyectos = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2GroupBox2 = new Guna.UI2.WinForms.Guna2GroupBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.guna2GroupBox1 = new Guna.UI2.WinForms.Guna2GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtgProyectos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtgProyectosE)).BeginInit();
            this.Panel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).BeginInit();
            this.PanelProyectos.SuspendLayout();
            this.guna2GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.guna2GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnRegresar
            // 
            this.BtnRegresar.Animated = true;
            this.BtnRegresar.BackColor = System.Drawing.Color.Transparent;
            this.BtnRegresar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(75)))), ((int)(((byte)(32)))));
            this.BtnRegresar.BorderRadius = 15;
            this.BtnRegresar.BorderThickness = 1;
            this.BtnRegresar.CheckedState.Parent = this.BtnRegresar;
            this.BtnRegresar.CustomImages.Parent = this.BtnRegresar;
            this.BtnRegresar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(75)))), ((int)(((byte)(32)))));
            this.BtnRegresar.Font = new System.Drawing.Font("Microsoft Tai Le", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRegresar.ForeColor = System.Drawing.Color.White;
            this.BtnRegresar.HoverState.Parent = this.BtnRegresar;
            this.BtnRegresar.Image = global::STAP_CDC.Properties.Resources.back;
            this.BtnRegresar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnRegresar.Location = new System.Drawing.Point(141, 182);
            this.BtnRegresar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnRegresar.Name = "BtnRegresar";
            this.BtnRegresar.ShadowDecoration.Parent = this.BtnRegresar;
            this.BtnRegresar.Size = new System.Drawing.Size(211, 49);
            this.BtnRegresar.TabIndex = 40;
            this.BtnRegresar.Text = "Regresar";
            this.toolTip1.SetToolTip(this.BtnRegresar, "Regresar");
            this.BtnRegresar.UseTransparentBackground = true;
            this.BtnRegresar.Visible = false;
            this.BtnRegresar.Click += new System.EventHandler(this.BtnRegresar_Click);
            // 
            // PBNProyectos
            // 
            this.PBNProyectos.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBNProyectos.FillColor = System.Drawing.Color.LightGray;
            this.PBNProyectos.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBNProyectos.Location = new System.Drawing.Point(28, 204);
            this.PBNProyectos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBNProyectos.Name = "PBNProyectos";
            this.PBNProyectos.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.PBNProyectos.ProgressColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.PBNProyectos.ShadowDecoration.Parent = this.PBNProyectos;
            this.PBNProyectos.Size = new System.Drawing.Size(301, 28);
            this.PBNProyectos.TabIndex = 10;
            this.PBNProyectos.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolTip1.SetToolTip(this.PBNProyectos, "Cantidad de proyectos activos");
            // 
            // PBNEliminados
            // 
            this.PBNEliminados.FillColor = System.Drawing.Color.LightGray;
            this.PBNEliminados.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBNEliminados.Location = new System.Drawing.Point(29, 300);
            this.PBNEliminados.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBNEliminados.Name = "PBNEliminados";
            this.PBNEliminados.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PBNEliminados.ProgressColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PBNEliminados.ShadowDecoration.Parent = this.PBNEliminados;
            this.PBNEliminados.Size = new System.Drawing.Size(300, 28);
            this.PBNEliminados.TabIndex = 9;
            this.PBNEliminados.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolTip1.SetToolTip(this.PBNEliminados, "Cantidad de proyectos eliminados");
            // 
            // PBNTotalProyectos
            // 
            this.PBNTotalProyectos.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBNTotalProyectos.FillColor = System.Drawing.Color.LightGray;
            this.PBNTotalProyectos.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBNTotalProyectos.Location = new System.Drawing.Point(29, 111);
            this.PBNTotalProyectos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBNTotalProyectos.Name = "PBNTotalProyectos";
            this.PBNTotalProyectos.ProgressColor = System.Drawing.Color.Teal;
            this.PBNTotalProyectos.ProgressColor2 = System.Drawing.Color.Teal;
            this.PBNTotalProyectos.ShadowDecoration.Parent = this.PBNTotalProyectos;
            this.PBNTotalProyectos.Size = new System.Drawing.Size(300, 28);
            this.PBNTotalProyectos.TabIndex = 34;
            this.PBNTotalProyectos.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolTip1.SetToolTip(this.PBNTotalProyectos, "Cantidad de proyectos totales");
            // 
            // guna2Button1
            // 
            this.guna2Button1.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button1.BorderColor = System.Drawing.Color.White;
            this.guna2Button1.BorderRadius = 15;
            this.guna2Button1.BorderThickness = 1;
            this.guna2Button1.CheckedState.Parent = this.guna2Button1;
            this.guna2Button1.CustomImages.Parent = this.guna2Button1;
            this.guna2Button1.FillColor = System.Drawing.Color.White;
            this.guna2Button1.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F);
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.HoverState.Parent = this.guna2Button1;
            this.guna2Button1.Image = ((System.Drawing.Image)(resources.GetObject("guna2Button1.Image")));
            this.guna2Button1.ImageSize = new System.Drawing.Size(40, 40);
            this.guna2Button1.Location = new System.Drawing.Point(444, 345);
            this.guna2Button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.ShadowDecoration.Parent = this.guna2Button1;
            this.guna2Button1.Size = new System.Drawing.Size(61, 59);
            this.guna2Button1.TabIndex = 38;
            this.toolTip1.SetToolTip(this.guna2Button1, "NOTA");
            this.guna2Button1.UseTransparentBackground = true;
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click_1);
            // 
            // BtnEliminados
            // 
            this.BtnEliminados.Animated = true;
            this.BtnEliminados.BackColor = System.Drawing.Color.Transparent;
            this.BtnEliminados.BorderColor = System.Drawing.Color.Gray;
            this.BtnEliminados.BorderRadius = 15;
            this.BtnEliminados.BorderThickness = 1;
            this.BtnEliminados.CheckedState.Parent = this.BtnEliminados;
            this.BtnEliminados.CustomImages.Parent = this.BtnEliminados;
            this.BtnEliminados.FillColor = System.Drawing.Color.Gray;
            this.BtnEliminados.Font = new System.Drawing.Font("Microsoft Tai Le", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEliminados.ForeColor = System.Drawing.Color.White;
            this.BtnEliminados.HoverState.Parent = this.BtnEliminados;
            this.BtnEliminados.Image = global::STAP_CDC.Properties.Resources.documento;
            this.BtnEliminados.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnEliminados.Location = new System.Drawing.Point(141, 182);
            this.BtnEliminados.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnEliminados.Name = "BtnEliminados";
            this.BtnEliminados.ShadowDecoration.Parent = this.BtnEliminados;
            this.BtnEliminados.Size = new System.Drawing.Size(211, 49);
            this.BtnEliminados.TabIndex = 39;
            this.BtnEliminados.Text = "Ver";
            this.toolTip1.SetToolTip(this.BtnEliminados, "Eliminados");
            this.BtnEliminados.UseTransparentBackground = true;
            this.BtnEliminados.Click += new System.EventHandler(this.BtnEliminados_Click);
            // 
            // guna2Button3
            // 
            this.guna2Button3.Animated = true;
            this.guna2Button3.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.guna2Button3.BorderRadius = 15;
            this.guna2Button3.BorderThickness = 1;
            this.guna2Button3.CheckedState.Parent = this.guna2Button3;
            this.guna2Button3.CustomImages.Parent = this.guna2Button3;
            this.guna2Button3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.guna2Button3.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button3.ForeColor = System.Drawing.Color.White;
            this.guna2Button3.HoverState.Parent = this.guna2Button3;
            this.guna2Button3.Image = global::STAP_CDC.Properties.Resources.add;
            this.guna2Button3.ImageSize = new System.Drawing.Size(30, 30);
            this.guna2Button3.Location = new System.Drawing.Point(529, 68);
            this.guna2Button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.ShadowDecoration.Parent = this.guna2Button3;
            this.guna2Button3.Size = new System.Drawing.Size(165, 49);
            this.guna2Button3.TabIndex = 22;
            this.guna2Button3.Text = "Nuevo";
            this.toolTip1.SetToolTip(this.guna2Button3, "Nuevo Proyecto");
            this.guna2Button3.UseTransparentBackground = true;
            this.guna2Button3.Click += new System.EventHandler(this.guna2Button3_Click);
            // 
            // TxtFiltrar
            // 
            this.TxtFiltrar.Animated = true;
            this.TxtFiltrar.BackColor = System.Drawing.Color.White;
            this.TxtFiltrar.BorderRadius = 10;
            this.TxtFiltrar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFiltrar.DefaultText = "";
            this.TxtFiltrar.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtFiltrar.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtFiltrar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.DisabledState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.FocusedState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFiltrar.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.HoverState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Location = new System.Drawing.Point(27, 71);
            this.TxtFiltrar.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtFiltrar.Name = "TxtFiltrar";
            this.TxtFiltrar.PasswordChar = '\0';
            this.TxtFiltrar.PlaceholderText = "Buscar...";
            this.TxtFiltrar.SelectedText = "";
            this.TxtFiltrar.ShadowDecoration.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Size = new System.Drawing.Size(468, 44);
            this.TxtFiltrar.TabIndex = 24;
            this.toolTip1.SetToolTip(this.TxtFiltrar, "Filtrar proyectos");
            this.TxtFiltrar.TextChanged += new System.EventHandler(this.TxtBuscar_TextChanged);
            this.TxtFiltrar.Click += new System.EventHandler(this.TxtFiltrar_Click);
            // 
            // BtnNuevo0
            // 
            this.BtnNuevo0.BackColor = System.Drawing.Color.Transparent;
            this.BtnNuevo0.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo0.BorderRadius = 15;
            this.BtnNuevo0.BorderThickness = 1;
            this.BtnNuevo0.CheckedState.Parent = this.BtnNuevo0;
            this.BtnNuevo0.CustomImages.Parent = this.BtnNuevo0;
            this.BtnNuevo0.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo0.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNuevo0.ForeColor = System.Drawing.Color.White;
            this.BtnNuevo0.HoverState.Parent = this.BtnNuevo0;
            this.BtnNuevo0.Image = global::STAP_CDC.Properties.Resources.add;
            this.BtnNuevo0.ImageSize = new System.Drawing.Size(40, 40);
            this.BtnNuevo0.Location = new System.Drawing.Point(464, 711);
            this.BtnNuevo0.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnNuevo0.Name = "BtnNuevo0";
            this.BtnNuevo0.ShadowDecoration.Parent = this.BtnNuevo0;
            this.BtnNuevo0.Size = new System.Drawing.Size(337, 63);
            this.BtnNuevo0.TabIndex = 23;
            this.BtnNuevo0.Text = "CREAR NUEVO PROYECTO";
            this.toolTip1.SetToolTip(this.BtnNuevo0, "Nuevo Proyecto");
            this.BtnNuevo0.UseTransparentBackground = true;
            this.BtnNuevo0.Click += new System.EventHandler(this.BtnNuevo0_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(584, 578);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(111, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Haga click en el boton para comenzar");
            // 
            // LblDialogoP
            // 
            this.LblDialogoP.AutoSize = true;
            this.LblDialogoP.BackColor = System.Drawing.Color.Transparent;
            this.LblDialogoP.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDialogoP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LblDialogoP.Location = new System.Drawing.Point(20, 14);
            this.LblDialogoP.Name = "LblDialogoP";
            this.LblDialogoP.Size = new System.Drawing.Size(148, 34);
            this.LblDialogoP.TabIndex = 25;
            this.LblDialogoP.Text = "Proyectos";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(27, 123);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(684, 718);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // DtgProyectos
            // 
            this.DtgProyectos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgProyectos.Location = new System.Drawing.Point(451, 14);
            this.DtgProyectos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DtgProyectos.Name = "DtgProyectos";
            this.DtgProyectos.RowHeadersWidth = 51;
            this.DtgProyectos.RowTemplate.Height = 24;
            this.DtgProyectos.Size = new System.Drawing.Size(27, 20);
            this.DtgProyectos.TabIndex = 0;
            this.DtgProyectos.Visible = false;
            // 
            // LblTotalProyectos
            // 
            this.LblTotalProyectos.AutoSize = true;
            this.LblTotalProyectos.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTotalProyectos.Location = new System.Drawing.Point(335, 111);
            this.LblTotalProyectos.Name = "LblTotalProyectos";
            this.LblTotalProyectos.Size = new System.Drawing.Size(83, 31);
            this.LblTotalProyectos.TabIndex = 35;
            this.LblTotalProyectos.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(24, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 23);
            this.label2.TabIndex = 33;
            this.label2.Text = "Total De Proyectos";
            // 
            // LblProyectosEliminados
            // 
            this.LblProyectosEliminados.AutoSize = true;
            this.LblProyectosEliminados.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProyectosEliminados.Location = new System.Drawing.Point(335, 300);
            this.LblProyectosEliminados.Name = "LblProyectosEliminados";
            this.LblProyectosEliminados.Size = new System.Drawing.Size(83, 31);
            this.LblProyectosEliminados.TabIndex = 12;
            this.LblProyectosEliminados.Text = "label3";
            // 
            // LblProyectosActivos
            // 
            this.LblProyectosActivos.AutoSize = true;
            this.LblProyectosActivos.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProyectosActivos.Location = new System.Drawing.Point(335, 204);
            this.LblProyectosActivos.Name = "LblProyectosActivos";
            this.LblProyectosActivos.Size = new System.Drawing.Size(83, 31);
            this.LblProyectosActivos.TabIndex = 11;
            this.LblProyectosActivos.Text = "label3";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.White;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label29.Location = new System.Drawing.Point(23, 274);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(216, 23);
            this.label29.TabIndex = 6;
            this.label29.Text = "Proyectos Eliminados";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(24, 178);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(184, 23);
            this.label24.TabIndex = 7;
            this.label24.Text = "Proyectos Activos";
            // 
            // DtgProyectosE
            // 
            this.DtgProyectosE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgProyectosE.Location = new System.Drawing.Point(484, 12);
            this.DtgProyectosE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DtgProyectosE.Name = "DtgProyectosE";
            this.DtgProyectosE.RowHeadersWidth = 51;
            this.DtgProyectosE.RowTemplate.Height = 24;
            this.DtgProyectosE.Size = new System.Drawing.Size(27, 21);
            this.DtgProyectosE.TabIndex = 33;
            this.DtgProyectosE.Visible = false;
            // 
            // Panel0
            // 
            this.Panel0.Controls.Add(this.label1);
            this.Panel0.Controls.Add(this.label6);
            this.Panel0.Controls.Add(this.PbAviso);
            this.Panel0.Controls.Add(this.label5);
            this.Panel0.Controls.Add(this.pictureBox1);
            this.Panel0.Controls.Add(this.BtnNuevo0);
            this.Panel0.Location = new System.Drawing.Point(0, 0);
            this.Panel0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel0.Name = "Panel0";
            this.Panel0.ShadowDecoration.Parent = this.Panel0;
            this.Panel0.Size = new System.Drawing.Size(1241, 853);
            this.Panel0.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(400, 528);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(440, 29);
            this.label1.TabIndex = 30;
            this.label1.Text = "* Presiona el siguiente botón para comenzar!!";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(377, 382);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(501, 48);
            this.label6.TabIndex = 29;
            this.label6.Text = "No hay proyectos registrados...";
            // 
            // PbAviso
            // 
            this.PbAviso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbAviso.Image = ((System.Drawing.Image)(resources.GetObject("PbAviso.Image")));
            this.PbAviso.Location = new System.Drawing.Point(509, 68);
            this.PbAviso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PbAviso.Name = "PbAviso";
            this.PbAviso.Size = new System.Drawing.Size(217, 193);
            this.PbAviso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbAviso.TabIndex = 28;
            this.PbAviso.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(481, 288);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(282, 57);
            this.label5.TabIndex = 27;
            this.label5.Text = "Lo sentimos :(";
            // 
            // PanelProyectos
            // 
            this.PanelProyectos.Controls.Add(this.guna2GroupBox2);
            this.PanelProyectos.Controls.Add(this.guna2GroupBox1);
            this.PanelProyectos.Controls.Add(this.TxtFiltrar);
            this.PanelProyectos.Controls.Add(this.guna2Button3);
            this.PanelProyectos.Controls.Add(this.DtgProyectosE);
            this.PanelProyectos.Controls.Add(this.DtgProyectos);
            this.PanelProyectos.Controls.Add(this.flowLayoutPanel1);
            this.PanelProyectos.Controls.Add(this.LblDialogoP);
            this.PanelProyectos.Location = new System.Drawing.Point(0, 0);
            this.PanelProyectos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelProyectos.Name = "PanelProyectos";
            this.PanelProyectos.ShadowDecoration.Parent = this.PanelProyectos;
            this.PanelProyectos.Size = new System.Drawing.Size(1241, 853);
            this.PanelProyectos.TabIndex = 35;
            // 
            // guna2GroupBox2
            // 
            this.guna2GroupBox2.Controls.Add(this.pictureBox4);
            this.guna2GroupBox2.Controls.Add(this.BtnEliminados);
            this.guna2GroupBox2.Controls.Add(this.BtnRegresar);
            this.guna2GroupBox2.Controls.Add(this.guna2Button1);
            this.guna2GroupBox2.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold);
            this.guna2GroupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.guna2GroupBox2.Location = new System.Drawing.Point(732, 449);
            this.guna2GroupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2GroupBox2.Name = "guna2GroupBox2";
            this.guna2GroupBox2.ShadowDecoration.Parent = this.guna2GroupBox2;
            this.guna2GroupBox2.Size = new System.Drawing.Size(505, 404);
            this.guna2GroupBox2.TabIndex = 31;
            this.guna2GroupBox2.Text = "        Proyectos Eliminados";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(9, 2);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(47, 43);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 43;
            this.pictureBox4.TabStop = false;
            // 
            // guna2GroupBox1
            // 
            this.guna2GroupBox1.Controls.Add(this.pictureBox3);
            this.guna2GroupBox1.Controls.Add(this.PBNTotalProyectos);
            this.guna2GroupBox1.Controls.Add(this.LblTotalProyectos);
            this.guna2GroupBox1.Controls.Add(this.LblProyectosEliminados);
            this.guna2GroupBox1.Controls.Add(this.LblProyectosActivos);
            this.guna2GroupBox1.Controls.Add(this.label2);
            this.guna2GroupBox1.Controls.Add(this.PBNEliminados);
            this.guna2GroupBox1.Controls.Add(this.PBNProyectos);
            this.guna2GroupBox1.Controls.Add(this.label24);
            this.guna2GroupBox1.Controls.Add(this.label29);
            this.guna2GroupBox1.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold);
            this.guna2GroupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.guna2GroupBox1.Location = new System.Drawing.Point(732, 15);
            this.guna2GroupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2GroupBox1.Name = "guna2GroupBox1";
            this.guna2GroupBox1.ShadowDecoration.Parent = this.guna2GroupBox1;
            this.guna2GroupBox1.Size = new System.Drawing.Size(505, 426);
            this.guna2GroupBox1.TabIndex = 41;
            this.guna2GroupBox1.Text = "        Estadísticas";
            this.guna2GroupBox1.Click += new System.EventHandler(this.guna2GroupBox1_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(9, 2);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(47, 43);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 42;
            this.pictureBox3.TabStop = false;
            // 
            // FrmProyectos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1241, 853);
            this.Controls.Add(this.Panel0);
            this.Controls.Add(this.PanelProyectos);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmProyectos";
            this.Text = "FrmProyectos";
            this.Activated += new System.EventHandler(this.FrmProyectos_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmProyectos_FormClosed);
            this.Load += new System.EventHandler(this.FrmProyectos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtgProyectos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtgProyectosE)).EndInit();
            this.Panel0.ResumeLayout(false);
            this.Panel0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).EndInit();
            this.PanelProyectos.ResumeLayout(false);
            this.PanelProyectos.PerformLayout();
            this.guna2GroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.guna2GroupBox1.ResumeLayout(false);
            this.guna2GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private Guna.UI2.WinForms.Guna2Button BtnRegresar;
        private System.Windows.Forms.Label LblDialogoP;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DataGridView DtgProyectos;
        private System.Windows.Forms.Label LblTotalProyectos;
        private Guna.UI2.WinForms.Guna2ProgressBar PBNTotalProyectos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LblProyectosEliminados;
        private System.Windows.Forms.Label LblProyectosActivos;
        private Guna.UI2.WinForms.Guna2ProgressBar PBNEliminados;
        private Guna.UI2.WinForms.Guna2ProgressBar PBNProyectos;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridView DtgProyectosE;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private Guna.UI2.WinForms.Guna2Button BtnEliminados;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2TextBox TxtFiltrar;
        private Guna.UI2.WinForms.Guna2GradientPanel Panel0;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox PbAviso;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2Button BtnNuevo0;
        private Guna.UI2.WinForms.Guna2GradientPanel PanelProyectos;
        private Guna.UI2.WinForms.Guna2GroupBox guna2GroupBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private Guna.UI2.WinForms.Guna2GroupBox guna2GroupBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}