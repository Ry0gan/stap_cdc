﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD_proyectos
{
    public partial class FrmProyectos : Form
    {
        private bool comprobacion = false;
        private bool comprobacionPanel = false;
        static public bool cambioA = false;
        static public bool cambioM = false;
        static public bool cambioE = false;
        Conexion mConexion;
        public FrmProyectos()
        {
            InitializeComponent();
            mConexion = new Conexion();
            mConexion.conectar();
        }

        private void BtnAlta_Click(object sender, EventArgs e)
        {
            FrmAltaProyecto mFrmAlta = new FrmAltaProyecto();
            mFrmAlta.ShowDialog();
            this.CargarProyecto();

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void CargarProyecto()
        {
            try
            {
                ControlProyectos mControl = new ControlProyectos();
                DtgProyectos.DataSource = mControl.ConsultarProyectos().Tables[0];
                DtgProyectosE.DataSource = mControl.ConsultarProyectosE().Tables[0];
                int activos = -1;
                int eliminados = -1;
                int proyectosT = 0;
                foreach (DataGridViewRow item in DtgProyectos.Rows)
                {
                    activos++;
                }
                foreach (DataGridViewRow item in DtgProyectosE.Rows)
                {
                    eliminados++;
                }
                proyectosT = activos + eliminados;
                if ((activos == 0) && (eliminados == 0))
                {
                    PanelProyectos.Visible = false;
                    Panel0.Visible = true;
                    comprobacionPanel = true;
                }
                else
                {
                    PanelProyectos.Visible = true;
                    Panel0.Visible = false;
                    comprobacionPanel = false;
                }
                PBNTotalProyectos.Maximum = proyectosT;
                PBNTotalProyectos.Value = proyectosT;
                PBNProyectos.Maximum = proyectosT;
                PBNEliminados.Maximum = proyectosT;
                PBNProyectos.Value = activos;
                PBNEliminados.Value = eliminados;
                LblTotalProyectos.Text = proyectosT.ToString();
                LblProyectosActivos.Text = activos.ToString();
                LblProyectosEliminados.Text = eliminados.ToString();
                if(comprobacion == true)
                {
                    GenerarProyectos();
                }
                else
                {
                    GenerarProyectosE();
                    BtnEliminados.Visible = false;
                    BtnRegresar.Visible = true;
                    LblDialogoP.Text = "Proyectos Eliminados";
                }
                
            }
            catch
            {
                MessageBox.Show("Error al conectar con la base de datos");
                this.Close();
            }

        }


        private void FrmProyectos_Load(object sender, EventArgs e)
        {
            comprobacion = true;
            CargarProyecto();
        }

        private void GenerarProyectos()
        {
            int x = DtgProyectos.Rows.Count - 1;
            int i = 0;
            int rows = 0;
            string fechafin = " ";
            string fechainicio = " ";
            flowLayoutPanel1.Controls.Clear();
            Proyectos[] listproducto = new Proyectos[x];
            while (x != 0)
            {
                listproducto[i] = new Proyectos();
                listproducto[i].Titulo = DtgProyectos.Rows[rows].Cells[1].Value.ToString();
                listproducto[i].Detalles = DtgProyectos.Rows[rows].Cells[2].Value.ToString();
                listproducto[i].padre = this;
                string fecha2 = DtgProyectos.Rows[rows].Cells[3].Value.ToString();
                string fecha = DtgProyectos.Rows[rows].Cells[4].Value.ToString();
                listproducto[i].ID = DtgProyectos.Rows[rows].Cells[0].Value.ToString();
                if ((fecha.Length > 0) && (fecha.Length > 0))
                {
                    fechafin = fecha.Remove(10);
                    fechainicio = fecha2.Remove(10);
                }
                listproducto[i].Fecha2 = fechainicio;
                listproducto[i].Fecha = fechafin;
                if (flowLayoutPanel1.Controls.Count < 0)
                {
                    flowLayoutPanel1.Controls.Clear();
                }
                else
                {
                    flowLayoutPanel1.Controls.Add(listproducto[i]);
                    i++;
                    rows++;
                    x--;
                }
            }
        }


        private void GenerarProyectosE()
        {
            int x = DtgProyectosE.Rows.Count - 1;
            int i = 0;
            int rows = 0;
            string fechafin = " ";
            string fechainicio = " ";
            flowLayoutPanel1.Controls.Clear();
            ProyectosEliminados[] listproducto = new ProyectosEliminados[x];
            while (x != 0)
            {
                listproducto[i] = new ProyectosEliminados();
                listproducto[i].Titulo = DtgProyectosE.Rows[rows].Cells[1].Value.ToString();
                listproducto[i].Detalles = DtgProyectosE.Rows[rows].Cells[2].Value.ToString();
                listproducto[i].padre = this;
                string fecha2 = DtgProyectosE.Rows[rows].Cells[3].Value.ToString();
                string fecha = DtgProyectosE.Rows[rows].Cells[4].Value.ToString();
                listproducto[i].ID = DtgProyectosE.Rows[rows].Cells[0].Value.ToString();
                if ((fecha.Length > 0) && (fecha.Length > 0))
                {
                    fechafin = fecha.Remove(10);
                    fechainicio = fecha2.Remove(10);
                }
                listproducto[i].Fecha2 = fechainicio;
                listproducto[i].Fecha = fechafin;
                if (flowLayoutPanel1.Controls.Count < 0)
                {
                    flowLayoutPanel1.Controls.Clear();
                }
                else
                {
                    flowLayoutPanel1.Controls.Add(listproducto[i]);
                    i++;
                    rows++;
                    x--;
                }
            }
        }

       

        private void FrmProyectos_FormClosed(object sender, FormClosedEventArgs e){
            
        }

        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void FrmProyectos_Activated(object sender, EventArgs e)
        {
 
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            FrmAltaProyecto mFrmAlta = new FrmAltaProyecto();
            mFrmAlta.ShowDialog();
            this.CargarProyecto();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        { 
            String SQL = "select * from proyecto" +
                " where Visibilidad = 1 AND  (fin_proyecto LIKE ('" + TxtFiltrar.Text + "%') or titulo_proyecto LIKE ('" + TxtFiltrar.Text + "%'))";
            String SQL2 = "select * from proyecto" +
                " where Visibilidad = 0 AND  (fin_proyecto LIKE ('" + TxtFiltrar.Text + "%') or titulo_proyecto LIKE ('" + TxtFiltrar.Text + "%'))";
            if(comprobacion == true)
            {
                DataSet ListaProyectos = mConexion.ejecutarConsulta(SQL);
                DtgProyectos.DataSource = ListaProyectos.Tables[0];
                flowLayoutPanel1.Controls.Clear();
                GenerarProyectos();
            }
            else
            {
                DataSet ListaProyectos = mConexion.ejecutarConsulta(SQL2);
                DtgProyectosE.DataSource = ListaProyectos.Tables[0];
                flowLayoutPanel1.Controls.Clear();
                GenerarProyectosE();
            }
            
        }

        private void TxtFiltrar_Click(object sender, EventArgs e)
        {
            TxtFiltrar.Text = "";
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void guna2Separator1_Click(object sender, EventArgs e)
        {

        }

        private void BtnEliminados_Click(object sender, EventArgs e)
        {
            string eliminarcomprobar = LblProyectosEliminados.Text;
            int eliminadoscomprobar = int.Parse(eliminarcomprobar);
            if (eliminadoscomprobar > 0)
            {
                comprobacion = false;
                CargarProyecto();
            }
            else
            {
                MessageBox.Show(this, "No existen proyectos eliminados", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            
        }

        private void BtnRegresar_Click(object sender, EventArgs e)
        {
            comprobacion = true;
            CargarProyecto();
            BtnRegresar.Visible = false;
            BtnEliminados.Visible = true;
            LblDialogoP.Text = "Proyectos";
        }

        private void BtnNuevo0_Click(object sender, EventArgs e)
        {
            FrmAltaProyecto mFrmAlta = new FrmAltaProyecto();
            mFrmAlta.ShowDialog();
            this.CargarProyecto();
        }

        private void guna2Button1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(this, "Para eliminar o modificar un proyecto, debe de dar click IZQUIERDO sobre este mismo", "Ayuda", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void guna2GroupBox1_Click(object sender, EventArgs e) {

        }
    }
}
