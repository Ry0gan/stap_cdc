﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_proyectos
{
   public class Proyecto
    {
        private int idProyecto;
        private string titulo_proyecto;
        private string detalles;
        private string inicio_proyecto;
        private string fin_proyecto;
        public int Visibilidad = 1;


        public void setidProyecto(int idProyecto)
        {
            this.idProyecto = idProyecto;
        }

        public int getidProyecto()
        {
            return idProyecto;
        }

        public void setVisibilidad(int Visibilidad)
        {
            this.Visibilidad = Visibilidad;
        }

        public int getVisibilidad()
        {
            return Visibilidad;
        }

        public void settitulo_proyecto(string titulo_proyecto)
        {
            this.titulo_proyecto = titulo_proyecto;
        }

        public string getTitulo_proyecto()
        {
            return titulo_proyecto;
        }

        public void setDetalles(string detalles)
        {
            this.detalles = detalles;
        }

        public string getDetalles()
        {
            return detalles;
        }

        public void setInicio_Proyecto(string inicio_proyecto)
        {
            this.inicio_proyecto = inicio_proyecto;
        }

        public string getInicio_Proyecto()
        {
            return inicio_proyecto;
        }

        public void setFin_Proyecto(string fin_proyecto)
        {
            this.fin_proyecto = fin_proyecto;
        }

        public string getFin_Proyecto()
        {
            return fin_proyecto;
        }
    }
}
