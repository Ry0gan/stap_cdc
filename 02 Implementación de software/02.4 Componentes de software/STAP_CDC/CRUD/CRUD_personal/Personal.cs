﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_personal
{
    public class Personal
    {
        private int idPersonal;
        private string nombre;
        private string iniciales;


        public int IDPersonal
        {
            get { return idPersonal; }
            set { idPersonal = value; }
        }
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public String Iniciales
        {
            get { return iniciales; }
            set { iniciales = value; }
        }
    }
}
