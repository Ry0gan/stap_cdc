﻿using CRUD_personal;
using STAP_CDC.Acceso;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.CRUD.CRUD_personal {
    public partial class FrmAddUser : Form {

        public bool correcto;

        public FrmAddUser() {
            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            correcto = false;
            TxtPass.PasswordChar = '\u25CF';
            TxtPassConfirm.PasswordChar = '\u25CF';
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
            TxtNombre.Focus();
        }

        public string  getNombre(){ 
            return TxtNombre.Text; 
        }
        public string getIniciales() {
            return TxtIniciales.Text;
        }
        public string getContrasena() { 
            return TxtPassConfirm.Text; 
        }

        private string tipo;

        public string getTipo() {
            if (RdbUNormal.Checked) {
                tipo = "0";
                return tipo;
            }
            if (RdbUAdmin.Checked) {
                tipo = "1";
                return tipo;
            }
            return null;
        }

        private void TxtPassConfirm_KeyUp(object sender, KeyEventArgs e) {
            if (TxtPass.Text == TxtPassConfirm.Text) {
                EProvider.Clear();
                AProvider.SetError(TxtPassConfirm, "Correcto");
            } else {
                AProvider.Clear();
                EProvider.SetError(TxtPassConfirm, "Incorrecta");
            }
        }

        private void TxtNombre_Leave(object sender, EventArgs e) {
            String iniciales = "";

            try {
                String[] nombreCompleto = TxtNombre.Text.Split(' ');
                foreach (String palabra in nombreCompleto) {
                    iniciales += palabra[0];
                }
                TxtIniciales.Text = iniciales.ToUpper();
            } catch {

            }
        }

        private void BtnNewPass_CheckedChanged(object sender, EventArgs e) {
            switch (BtnNewPass.Checked) {
                case true:
                    TxtPass.PasswordChar = '\u0000';
                    TxtPassConfirm.PasswordChar = '\u0000';
                    break;

                case false:
                    TxtPass.PasswordChar = '\u25CF';
                    TxtPassConfirm.PasswordChar = '\u25CF';
                    break;
            }
        }

        private void guna2CustomRadioButton2_CheckedChanged(object sender, EventArgs e) {
            tipo = "0";
        }

        private void RdbUAdmin_CheckedChanged(object sender, EventArgs e) {
            tipo = "1";
        }

        private void BtnGuardar_Click(object sender, EventArgs e) {
            if (TxtNombre.Text.Trim() != "") {
                if (TxtIniciales.Text.Trim() != "") {
                    if (TxtPass.Text.Trim() != "") {
                        if (TxtPassConfirm.Text.Trim() != "") {
                            if (TxtPass.Text == TxtPassConfirm.Text) { 
                               if (new ControlPersonal().iniciales(TxtIniciales.Text.Trim().ToUpper())) {
                                    correcto = false;
                                    MessageBox.Show(TxtIniciales.Text.Trim().ToUpper() + " ya está registrado, por favor intente con otras iniciales.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                    TxtIniciales.Focus();
                                } else {
                                    correcto = true;
                                    this.Close();
                                }
                            }
                        } else {
                            EProvider.Clear();
                            EProvider.SetError(TxtPassConfirm, "Tiene que confirmar su contraseña");
                        }
                    } else {
                        EProvider.Clear();
                        EProvider.SetError(TxtPass, "La contraseña no puede quedar vacío");
                    }
                } else {
                    EProvider.Clear();
                    EProvider.SetError(TxtIniciales, "El campo iniciales no puede quedar vacío");
                }
                
            } else {
                EProvider.Clear();
                EProvider.SetError(TxtNombre, "El campo Nombre no puede quedar vacío");
            }
        }

        private void FrmAddUser_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void BtnCanelar_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
