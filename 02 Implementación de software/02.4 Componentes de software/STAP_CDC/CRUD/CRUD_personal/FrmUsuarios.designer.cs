﻿namespace CRUD_personal
{
    partial class FrmUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsuarios));
            this.label2 = new System.Windows.Forms.Label();
            this.TxtBuscar = new Guna.UI2.WinForms.Guna2TextBox();
            this.DgvUsuarios = new Guna.UI2.WinForms.Guna2DataGridView();
            this.BtnModificar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnAgregar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnEliminar = new Guna.UI2.WinForms.Guna2Button();
            this.panel0 = new System.Windows.Forms.Panel();
            this.LblRol = new System.Windows.Forms.Label();
            this.LblNombre = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnGuardar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnPassNueva = new Guna.UI2.WinForms.Guna2Button();
            this.BtnPassAnterior = new Guna.UI2.WinForms.Guna2Button();
            this.Lbl3 = new System.Windows.Forms.Label();
            this.TxtPassConfirm = new Guna.UI2.WinForms.Guna2TextBox();
            this.Lbl2 = new System.Windows.Forms.Label();
            this.TxtPassAnterior = new Guna.UI2.WinForms.Guna2TextBox();
            this.TxtPassNueva = new Guna.UI2.WinForms.Guna2TextBox();
            this.Lbl1 = new System.Windows.Forms.Label();
            this.EProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.AProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DgvUsuarios)).BeginInit();
            this.panel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(17, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 26);
            this.label2.TabIndex = 26;
            this.label2.Text = "Usuarios";
            // 
            // TxtBuscar
            // 
            this.TxtBuscar.Animated = true;
            this.TxtBuscar.BackColor = System.Drawing.Color.White;
            this.TxtBuscar.BorderRadius = 10;
            this.TxtBuscar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtBuscar.DefaultText = "";
            this.TxtBuscar.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtBuscar.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtBuscar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtBuscar.DisabledState.Parent = this.TxtBuscar;
            this.TxtBuscar.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtBuscar.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtBuscar.FocusedState.Parent = this.TxtBuscar;
            this.TxtBuscar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBuscar.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtBuscar.HoverState.Parent = this.TxtBuscar;
            this.TxtBuscar.Location = new System.Drawing.Point(22, 64);
            this.TxtBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.TxtBuscar.Name = "TxtBuscar";
            this.TxtBuscar.PasswordChar = '\0';
            this.TxtBuscar.PlaceholderText = "Buscar...";
            this.TxtBuscar.SelectedText = "";
            this.TxtBuscar.ShadowDecoration.Parent = this.TxtBuscar;
            this.TxtBuscar.Size = new System.Drawing.Size(332, 36);
            this.TxtBuscar.TabIndex = 29;
            this.TxtBuscar.TextChanged += new System.EventHandler(this.TxtBuscar_TextChanged);
            this.TxtBuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtBuscar_KeyPress);
            // 
            // DgvUsuarios
            // 
            this.DgvUsuarios.AllowUserToAddRows = false;
            this.DgvUsuarios.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.DgvUsuarios.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvUsuarios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.DgvUsuarios.BackgroundColor = System.Drawing.Color.White;
            this.DgvUsuarios.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DgvUsuarios.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.DgvUsuarios.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvUsuarios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DgvUsuarios.ColumnHeadersHeight = 40;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DgvUsuarios.DefaultCellStyle = dataGridViewCellStyle3;
            this.DgvUsuarios.EnableHeadersVisualStyles = false;
            this.DgvUsuarios.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DgvUsuarios.Location = new System.Drawing.Point(22, 126);
            this.DgvUsuarios.MultiSelect = false;
            this.DgvUsuarios.Name = "DgvUsuarios";
            this.DgvUsuarios.ReadOnly = true;
            this.DgvUsuarios.RowHeadersVisible = false;
            this.DgvUsuarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvUsuarios.Size = new System.Drawing.Size(471, 424);
            this.DgvUsuarios.TabIndex = 39;
            this.DgvUsuarios.Theme = Guna.UI2.WinForms.Enums.DataGridViewPresetThemes.Default;
            this.DgvUsuarios.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.DgvUsuarios.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.DgvUsuarios.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.DgvUsuarios.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.DgvUsuarios.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.DgvUsuarios.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.DgvUsuarios.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DgvUsuarios.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.DgvUsuarios.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DgvUsuarios.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.DgvUsuarios.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.DgvUsuarios.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.DgvUsuarios.ThemeStyle.HeaderStyle.Height = 40;
            this.DgvUsuarios.ThemeStyle.ReadOnly = true;
            this.DgvUsuarios.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.DgvUsuarios.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.DgvUsuarios.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.DgvUsuarios.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.DgvUsuarios.ThemeStyle.RowsStyle.Height = 22;
            this.DgvUsuarios.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DgvUsuarios.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // BtnModificar
            // 
            this.BtnModificar.Animated = true;
            this.BtnModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtnModificar.BorderColor = System.Drawing.Color.DarkViolet;
            this.BtnModificar.BorderRadius = 15;
            this.BtnModificar.BorderThickness = 1;
            this.BtnModificar.CheckedState.Parent = this.BtnModificar;
            this.BtnModificar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnModificar.CustomImages.Parent = this.BtnModificar;
            this.BtnModificar.FillColor = System.Drawing.Color.DarkViolet;
            this.BtnModificar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnModificar.ForeColor = System.Drawing.Color.White;
            this.BtnModificar.HoverState.Parent = this.BtnModificar;
            this.BtnModificar.Image = global::STAP_CDC.Properties.Resources.edit;
            this.BtnModificar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnModificar.Location = new System.Drawing.Point(91, 600);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.ShadowDecoration.Parent = this.BtnModificar;
            this.BtnModificar.Size = new System.Drawing.Size(145, 40);
            this.BtnModificar.TabIndex = 28;
            this.BtnModificar.Text = "Modificar";
            this.BtnModificar.UseTransparentBackground = true;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // BtnAgregar
            // 
            this.BtnAgregar.Animated = true;
            this.BtnAgregar.BackColor = System.Drawing.Color.Transparent;
            this.BtnAgregar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnAgregar.BorderRadius = 15;
            this.BtnAgregar.BorderThickness = 1;
            this.BtnAgregar.CheckedState.Parent = this.BtnAgregar;
            this.BtnAgregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnAgregar.CustomImages.Parent = this.BtnAgregar;
            this.BtnAgregar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnAgregar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAgregar.ForeColor = System.Drawing.Color.White;
            this.BtnAgregar.HoverState.Parent = this.BtnAgregar;
            this.BtnAgregar.Image = global::STAP_CDC.Properties.Resources.add;
            this.BtnAgregar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnAgregar.Location = new System.Drawing.Point(372, 64);
            this.BtnAgregar.Name = "BtnAgregar";
            this.BtnAgregar.ShadowDecoration.Parent = this.BtnAgregar;
            this.BtnAgregar.Size = new System.Drawing.Size(131, 40);
            this.BtnAgregar.TabIndex = 23;
            this.BtnAgregar.Text = "Agregar";
            this.BtnAgregar.UseTransparentBackground = true;
            this.BtnAgregar.Click += new System.EventHandler(this.BtnAgregar_Click);
            // 
            // BtnEliminar
            // 
            this.BtnEliminar.Animated = true;
            this.BtnEliminar.BackColor = System.Drawing.Color.Transparent;
            this.BtnEliminar.BorderColor = System.Drawing.Color.Firebrick;
            this.BtnEliminar.BorderRadius = 15;
            this.BtnEliminar.BorderThickness = 1;
            this.BtnEliminar.CheckedState.Parent = this.BtnEliminar;
            this.BtnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnEliminar.CustomImages.Parent = this.BtnEliminar;
            this.BtnEliminar.FillColor = System.Drawing.Color.Firebrick;
            this.BtnEliminar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEliminar.ForeColor = System.Drawing.Color.White;
            this.BtnEliminar.HoverState.Parent = this.BtnEliminar;
            this.BtnEliminar.Image = global::STAP_CDC.Properties.Resources.cancel;
            this.BtnEliminar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnEliminar.Location = new System.Drawing.Point(306, 600);
            this.BtnEliminar.Name = "BtnEliminar";
            this.BtnEliminar.ShadowDecoration.Parent = this.BtnEliminar;
            this.BtnEliminar.Size = new System.Drawing.Size(145, 40);
            this.BtnEliminar.TabIndex = 24;
            this.BtnEliminar.Text = "Eliminar";
            this.BtnEliminar.UseTransparentBackground = true;
            this.BtnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // panel0
            // 
            this.panel0.Controls.Add(this.LblRol);
            this.panel0.Controls.Add(this.LblNombre);
            this.panel0.Controls.Add(this.pictureBox1);
            this.panel0.Controls.Add(this.BtnGuardar);
            this.panel0.Controls.Add(this.BtnPassNueva);
            this.panel0.Controls.Add(this.BtnPassAnterior);
            this.panel0.Controls.Add(this.Lbl3);
            this.panel0.Controls.Add(this.TxtPassConfirm);
            this.panel0.Controls.Add(this.Lbl2);
            this.panel0.Controls.Add(this.TxtPassAnterior);
            this.panel0.Controls.Add(this.TxtPassNueva);
            this.panel0.Controls.Add(this.Lbl1);
            this.panel0.Location = new System.Drawing.Point(0, 0);
            this.panel0.Name = "panel0";
            this.panel0.Size = new System.Drawing.Size(931, 693);
            this.panel0.TabIndex = 40;
            // 
            // LblRol
            // 
            this.LblRol.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblRol.AutoSize = true;
            this.LblRol.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14F);
            this.LblRol.ForeColor = System.Drawing.Color.Black;
            this.LblRol.Location = new System.Drawing.Point(173, 138);
            this.LblRol.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblRol.Name = "LblRol";
            this.LblRol.Size = new System.Drawing.Size(0, 23);
            this.LblRol.TabIndex = 94;
            // 
            // LblNombre
            // 
            this.LblNombre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LblNombre.AutoSize = true;
            this.LblNombre.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 28F);
            this.LblNombre.ForeColor = System.Drawing.Color.Black;
            this.LblNombre.Location = new System.Drawing.Point(165, 64);
            this.LblNombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblNombre.Name = "LblNombre";
            this.LblNombre.Size = new System.Drawing.Size(0, 46);
            this.LblNombre.TabIndex = 93;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(41, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(119, 112);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 92;
            this.pictureBox1.TabStop = false;
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnGuardar.Animated = true;
            this.BtnGuardar.BackColor = System.Drawing.Color.Transparent;
            this.BtnGuardar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.BorderRadius = 15;
            this.BtnGuardar.BorderThickness = 1;
            this.BtnGuardar.CheckedState.Parent = this.BtnGuardar;
            this.BtnGuardar.CustomImages.Parent = this.BtnGuardar;
            this.BtnGuardar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F);
            this.BtnGuardar.ForeColor = System.Drawing.Color.White;
            this.BtnGuardar.HoverState.Parent = this.BtnGuardar;
            this.BtnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("BtnGuardar.Image")));
            this.BtnGuardar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnGuardar.Location = new System.Drawing.Point(434, 521);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.ShadowDecoration.Parent = this.BtnGuardar;
            this.BtnGuardar.Size = new System.Drawing.Size(146, 40);
            this.BtnGuardar.TabIndex = 91;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseTransparentBackground = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnPassNueva
            // 
            this.BtnPassNueva.BackColor = System.Drawing.Color.White;
            this.BtnPassNueva.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnPassNueva.BorderRadius = 5;
            this.BtnPassNueva.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.ToogleButton;
            this.BtnPassNueva.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("BtnPassNueva.CheckedState.Image")));
            this.BtnPassNueva.CheckedState.Parent = this.BtnPassNueva;
            this.BtnPassNueva.CustomImages.Parent = this.BtnPassNueva;
            this.BtnPassNueva.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnPassNueva.ForeColor = System.Drawing.Color.White;
            this.BtnPassNueva.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.HoverState.Parent = this.BtnPassNueva;
            this.BtnPassNueva.Image = ((System.Drawing.Image)(resources.GetObject("BtnPassNueva.Image")));
            this.BtnPassNueva.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnPassNueva.Location = new System.Drawing.Point(710, 369);
            this.BtnPassNueva.Name = "BtnPassNueva";
            this.BtnPassNueva.PressedColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.ShadowDecoration.Color = System.Drawing.Color.LightGray;
            this.BtnPassNueva.ShadowDecoration.Depth = 10;
            this.BtnPassNueva.ShadowDecoration.Parent = this.BtnPassNueva;
            this.BtnPassNueva.Size = new System.Drawing.Size(41, 28);
            this.BtnPassNueva.TabIndex = 90;
            this.BtnPassNueva.Tag = "";
            this.BtnPassNueva.CheckedChanged += new System.EventHandler(this.BtnPassNueva_CheckedChanged);
            // 
            // BtnPassAnterior
            // 
            this.BtnPassAnterior.BackColor = System.Drawing.Color.White;
            this.BtnPassAnterior.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnPassAnterior.BorderRadius = 5;
            this.BtnPassAnterior.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.ToogleButton;
            this.BtnPassAnterior.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassAnterior.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("BtnPassAnterior.CheckedState.Image")));
            this.BtnPassAnterior.CheckedState.Parent = this.BtnPassAnterior;
            this.BtnPassAnterior.CustomImages.Parent = this.BtnPassAnterior;
            this.BtnPassAnterior.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassAnterior.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnPassAnterior.ForeColor = System.Drawing.Color.White;
            this.BtnPassAnterior.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassAnterior.HoverState.Parent = this.BtnPassAnterior;
            this.BtnPassAnterior.Image = ((System.Drawing.Image)(resources.GetObject("BtnPassAnterior.Image")));
            this.BtnPassAnterior.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnPassAnterior.Location = new System.Drawing.Point(709, 321);
            this.BtnPassAnterior.Name = "BtnPassAnterior";
            this.BtnPassAnterior.PressedColor = System.Drawing.Color.Transparent;
            this.BtnPassAnterior.ShadowDecoration.Color = System.Drawing.Color.LightGray;
            this.BtnPassAnterior.ShadowDecoration.Depth = 10;
            this.BtnPassAnterior.ShadowDecoration.Parent = this.BtnPassAnterior;
            this.BtnPassAnterior.Size = new System.Drawing.Size(41, 28);
            this.BtnPassAnterior.TabIndex = 89;
            this.BtnPassAnterior.Tag = "";
            this.BtnPassAnterior.CheckedChanged += new System.EventHandler(this.BtnPassAnterior_CheckedChanged);
            // 
            // Lbl3
            // 
            this.Lbl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl3.AutoSize = true;
            this.Lbl3.BackColor = System.Drawing.Color.White;
            this.Lbl3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl3.Location = new System.Drawing.Point(223, 424);
            this.Lbl3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lbl3.Name = "Lbl3";
            this.Lbl3.Size = new System.Drawing.Size(169, 20);
            this.Lbl3.TabIndex = 88;
            this.Lbl3.Text = "Confirmar contraseña";
            // 
            // TxtPassConfirm
            // 
            this.TxtPassConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPassConfirm.Animated = true;
            this.TxtPassConfirm.BackColor = System.Drawing.Color.White;
            this.TxtPassConfirm.BorderRadius = 10;
            this.TxtPassConfirm.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPassConfirm.DefaultText = "";
            this.TxtPassConfirm.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPassConfirm.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPassConfirm.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassConfirm.DisabledState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassConfirm.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassConfirm.FocusedState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPassConfirm.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassConfirm.HoverState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Location = new System.Drawing.Point(408, 412);
            this.TxtPassConfirm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPassConfirm.Name = "TxtPassConfirm";
            this.TxtPassConfirm.PasswordChar = '\0';
            this.TxtPassConfirm.PlaceholderText = "";
            this.TxtPassConfirm.SelectedText = "";
            this.TxtPassConfirm.ShadowDecoration.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Size = new System.Drawing.Size(275, 36);
            this.TxtPassConfirm.TabIndex = 85;
            this.TxtPassConfirm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtPassConfirm_KeyUp);
            // 
            // Lbl2
            // 
            this.Lbl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl2.AutoSize = true;
            this.Lbl2.BackColor = System.Drawing.Color.White;
            this.Lbl2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl2.Location = new System.Drawing.Point(245, 374);
            this.Lbl2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lbl2.Name = "Lbl2";
            this.Lbl2.Size = new System.Drawing.Size(147, 20);
            this.Lbl2.TabIndex = 86;
            this.Lbl2.Text = "Nueva contraseña";
            // 
            // TxtPassAnterior
            // 
            this.TxtPassAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPassAnterior.Animated = true;
            this.TxtPassAnterior.BackColor = System.Drawing.Color.White;
            this.TxtPassAnterior.BorderRadius = 10;
            this.TxtPassAnterior.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPassAnterior.DefaultText = "";
            this.TxtPassAnterior.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPassAnterior.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPassAnterior.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassAnterior.DisabledState.Parent = this.TxtPassAnterior;
            this.TxtPassAnterior.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassAnterior.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassAnterior.FocusedState.Parent = this.TxtPassAnterior;
            this.TxtPassAnterior.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPassAnterior.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassAnterior.HoverState.Parent = this.TxtPassAnterior;
            this.TxtPassAnterior.Location = new System.Drawing.Point(407, 318);
            this.TxtPassAnterior.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPassAnterior.Name = "TxtPassAnterior";
            this.TxtPassAnterior.PasswordChar = '\0';
            this.TxtPassAnterior.PlaceholderText = "";
            this.TxtPassAnterior.SelectedText = "";
            this.TxtPassAnterior.ShadowDecoration.Parent = this.TxtPassAnterior;
            this.TxtPassAnterior.Size = new System.Drawing.Size(276, 36);
            this.TxtPassAnterior.TabIndex = 83;
            // 
            // TxtPassNueva
            // 
            this.TxtPassNueva.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPassNueva.Animated = true;
            this.TxtPassNueva.BackColor = System.Drawing.Color.White;
            this.TxtPassNueva.BorderRadius = 10;
            this.TxtPassNueva.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPassNueva.DefaultText = "";
            this.TxtPassNueva.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPassNueva.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPassNueva.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassNueva.DisabledState.Parent = this.TxtPassNueva;
            this.TxtPassNueva.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassNueva.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassNueva.FocusedState.Parent = this.TxtPassNueva;
            this.TxtPassNueva.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPassNueva.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassNueva.HoverState.Parent = this.TxtPassNueva;
            this.TxtPassNueva.Location = new System.Drawing.Point(407, 365);
            this.TxtPassNueva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPassNueva.Name = "TxtPassNueva";
            this.TxtPassNueva.PasswordChar = '\0';
            this.TxtPassNueva.PlaceholderText = "";
            this.TxtPassNueva.SelectedText = "";
            this.TxtPassNueva.ShadowDecoration.Parent = this.TxtPassNueva;
            this.TxtPassNueva.Size = new System.Drawing.Size(276, 36);
            this.TxtPassNueva.TabIndex = 84;
            // 
            // Lbl1
            // 
            this.Lbl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl1.AutoSize = true;
            this.Lbl1.BackColor = System.Drawing.Color.White;
            this.Lbl1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl1.Location = new System.Drawing.Point(236, 324);
            this.Lbl1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lbl1.Name = "Lbl1";
            this.Lbl1.Size = new System.Drawing.Size(156, 20);
            this.Lbl1.TabIndex = 87;
            this.Lbl1.Text = "Contraseña anterior";
            // 
            // EProvider
            // 
            this.EProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.EProvider.ContainerControl = this;
            this.EProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("EProvider.Icon")));
            // 
            // AProvider
            // 
            this.AProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.AProvider.ContainerControl = this;
            this.AProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("AProvider.Icon")));
            // 
            // FrmUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(931, 693);
            this.Controls.Add(this.panel0);
            this.Controls.Add(this.DgvUsuarios);
            this.Controls.Add(this.BtnAgregar);
            this.Controls.Add(this.TxtBuscar);
            this.Controls.Add(this.BtnModificar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BtnEliminar);
            this.Name = "FrmUsuarios";
            this.Text = "Personal";
            this.Load += new System.EventHandler(this.FrmUsuarios_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgvUsuarios)).EndInit();
            this.panel0.ResumeLayout(false);
            this.panel0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI2.WinForms.Guna2Button BtnAgregar;
        private Guna.UI2.WinForms.Guna2Button BtnEliminar;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2Button BtnModificar;
        private Guna.UI2.WinForms.Guna2TextBox TxtBuscar;
        public Guna.UI2.WinForms.Guna2DataGridView DgvUsuarios;
        private System.Windows.Forms.Panel panel0;
        private Guna.UI2.WinForms.Guna2Button BtnPassNueva;
        private Guna.UI2.WinForms.Guna2Button BtnPassAnterior;
        private System.Windows.Forms.Label Lbl3;
        private Guna.UI2.WinForms.Guna2TextBox TxtPassConfirm;
        private System.Windows.Forms.Label Lbl2;
        private Guna.UI2.WinForms.Guna2TextBox TxtPassAnterior;
        private Guna.UI2.WinForms.Guna2TextBox TxtPassNueva;
        private System.Windows.Forms.Label Lbl1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2Button BtnGuardar;
        private System.Windows.Forms.Label LblRol;
        private System.Windows.Forms.Label LblNombre;
        private System.Windows.Forms.ErrorProvider EProvider;
        private System.Windows.Forms.ErrorProvider AProvider;
    }
}