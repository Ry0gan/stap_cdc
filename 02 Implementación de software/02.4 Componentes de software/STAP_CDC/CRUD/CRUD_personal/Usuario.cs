﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STAP_CDC.CRUD.CRUD_personal
{
    public class Usuario
    {
        public string contrasena { get; set; }
        public string tipo { get; set; }
        
        public Usuario()
        {
            contrasena = "";
            tipo = "";
        }
    }
}
