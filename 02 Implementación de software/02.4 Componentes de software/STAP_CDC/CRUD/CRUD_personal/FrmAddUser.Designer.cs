﻿namespace STAP_CDC.CRUD.CRUD_personal {
    partial class FrmAddUser {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAddUser));
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.LblAccion = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtPassConfirm = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPass = new Guna.UI2.WinForms.Guna2TextBox();
            this.BtnGuardar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnCanelar = new Guna.UI2.WinForms.Guna2Button();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtNombre = new Guna.UI2.WinForms.Guna2TextBox();
            this.TxtIniciales = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.EProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.AProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.BtnNewPass = new Guna.UI2.WinForms.Guna2Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RdbUNormal = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.RdbUAdmin = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.gunaLabel1 = new Guna.UI.WinForms.GunaLabel();
            this.gunaLabel2 = new Guna.UI.WinForms.GunaLabel();
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.CustomIconSize = 9F;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(520, 0);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox2.TabIndex = 8;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(567, 0);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox1.TabIndex = 9;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // LblAccion
            // 
            this.LblAccion.AutoSize = true;
            this.LblAccion.BackColor = System.Drawing.Color.White;
            this.LblAccion.Font = new System.Drawing.Font("Microsoft Tai Le", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LblAccion.Location = new System.Drawing.Point(11, 33);
            this.LblAccion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblAccion.Name = "LblAccion";
            this.LblAccion.Size = new System.Drawing.Size(385, 40);
            this.LblAccion.TabIndex = 70;
            this.LblAccion.Text = "Agregar un nuevo usuario";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(38, 288);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(173, 20);
            this.label4.TabIndex = 92;
            this.label4.Text = "Comfirmar contraseña";
            // 
            // TxtPassConfirm
            // 
            this.TxtPassConfirm.Animated = true;
            this.TxtPassConfirm.BackColor = System.Drawing.Color.White;
            this.TxtPassConfirm.BorderRadius = 10;
            this.TxtPassConfirm.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPassConfirm.DefaultText = "";
            this.TxtPassConfirm.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPassConfirm.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPassConfirm.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassConfirm.DisabledState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassConfirm.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassConfirm.FocusedState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPassConfirm.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassConfirm.HoverState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Location = new System.Drawing.Point(226, 280);
            this.TxtPassConfirm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPassConfirm.Name = "TxtPassConfirm";
            this.TxtPassConfirm.PasswordChar = '\0';
            this.TxtPassConfirm.PlaceholderText = "";
            this.TxtPassConfirm.SelectedText = "";
            this.TxtPassConfirm.ShadowDecoration.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Size = new System.Drawing.Size(275, 36);
            this.TxtPassConfirm.TabIndex = 3;
            this.TxtPassConfirm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtPassConfirm_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(116, 239);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 20);
            this.label2.TabIndex = 89;
            this.label2.Text = "Contraseña";
            // 
            // TxtPass
            // 
            this.TxtPass.Animated = true;
            this.TxtPass.BackColor = System.Drawing.Color.White;
            this.TxtPass.BorderRadius = 10;
            this.TxtPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPass.DefaultText = "";
            this.TxtPass.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPass.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPass.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPass.DisabledState.Parent = this.TxtPass;
            this.TxtPass.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPass.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPass.FocusedState.Parent = this.TxtPass;
            this.TxtPass.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPass.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPass.HoverState.Parent = this.TxtPass;
            this.TxtPass.Location = new System.Drawing.Point(227, 230);
            this.TxtPass.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPass.Name = "TxtPass";
            this.TxtPass.PasswordChar = '\0';
            this.TxtPass.PlaceholderText = "";
            this.TxtPass.SelectedText = "";
            this.TxtPass.ShadowDecoration.Parent = this.TxtPass;
            this.TxtPass.Size = new System.Drawing.Size(276, 36);
            this.TxtPass.TabIndex = 2;
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Animated = true;
            this.BtnGuardar.BackColor = System.Drawing.Color.Transparent;
            this.BtnGuardar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.BorderRadius = 15;
            this.BtnGuardar.BorderThickness = 1;
            this.BtnGuardar.CheckedState.Parent = this.BtnGuardar;
            this.BtnGuardar.CustomImages.Parent = this.BtnGuardar;
            this.BtnGuardar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F);
            this.BtnGuardar.ForeColor = System.Drawing.Color.White;
            this.BtnGuardar.HoverState.Parent = this.BtnGuardar;
            this.BtnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("BtnGuardar.Image")));
            this.BtnGuardar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnGuardar.Location = new System.Drawing.Point(167, 434);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.ShadowDecoration.Parent = this.BtnGuardar;
            this.BtnGuardar.Size = new System.Drawing.Size(124, 40);
            this.BtnGuardar.TabIndex = 7;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseTransparentBackground = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnCanelar
            // 
            this.BtnCanelar.Animated = true;
            this.BtnCanelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCanelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.BorderRadius = 15;
            this.BtnCanelar.BorderThickness = 1;
            this.BtnCanelar.CheckedState.Parent = this.BtnCanelar;
            this.BtnCanelar.CustomImages.Parent = this.BtnCanelar;
            this.BtnCanelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCanelar.ForeColor = System.Drawing.Color.White;
            this.BtnCanelar.HoverState.Parent = this.BtnCanelar;
            this.BtnCanelar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCanelar.Image")));
            this.BtnCanelar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnCanelar.Location = new System.Drawing.Point(326, 434);
            this.BtnCanelar.Name = "BtnCanelar";
            this.BtnCanelar.ShadowDecoration.Parent = this.BtnCanelar;
            this.BtnCanelar.Size = new System.Drawing.Size(124, 40);
            this.BtnCanelar.TabIndex = 8;
            this.BtnCanelar.Text = "Cancelar";
            this.BtnCanelar.UseTransparentBackground = true;
            this.BtnCanelar.Click += new System.EventHandler(this.BtnCanelar_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(143, 188);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 20);
            this.label24.TabIndex = 83;
            this.label24.Text = "Iniciales";
            // 
            // TxtNombre
            // 
            this.TxtNombre.Animated = true;
            this.TxtNombre.BackColor = System.Drawing.Color.White;
            this.TxtNombre.BorderRadius = 10;
            this.TxtNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtNombre.DefaultText = "";
            this.TxtNombre.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtNombre.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtNombre.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNombre.DisabledState.Parent = this.TxtNombre;
            this.TxtNombre.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNombre.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNombre.FocusedState.Parent = this.TxtNombre;
            this.TxtNombre.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNombre.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNombre.HoverState.Parent = this.TxtNombre;
            this.TxtNombre.Location = new System.Drawing.Point(226, 130);
            this.TxtNombre.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.PasswordChar = '\0';
            this.TxtNombre.PlaceholderText = "";
            this.TxtNombre.SelectedText = "";
            this.TxtNombre.ShadowDecoration.Parent = this.TxtNombre;
            this.TxtNombre.Size = new System.Drawing.Size(276, 36);
            this.TxtNombre.TabIndex = 0;
            this.TxtNombre.Leave += new System.EventHandler(this.TxtNombre_Leave);
            // 
            // TxtIniciales
            // 
            this.TxtIniciales.Animated = true;
            this.TxtIniciales.BackColor = System.Drawing.Color.White;
            this.TxtIniciales.BorderRadius = 10;
            this.TxtIniciales.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtIniciales.DefaultText = "";
            this.TxtIniciales.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtIniciales.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtIniciales.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIniciales.DisabledState.Parent = this.TxtIniciales;
            this.TxtIniciales.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIniciales.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIniciales.FocusedState.Parent = this.TxtIniciales;
            this.TxtIniciales.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIniciales.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIniciales.HoverState.Parent = this.TxtIniciales;
            this.TxtIniciales.Location = new System.Drawing.Point(226, 180);
            this.TxtIniciales.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtIniciales.Name = "TxtIniciales";
            this.TxtIniciales.PasswordChar = '\0';
            this.TxtIniciales.PlaceholderText = "";
            this.TxtIniciales.SelectedText = "";
            this.TxtIniciales.ShadowDecoration.Parent = this.TxtIniciales;
            this.TxtIniciales.Size = new System.Drawing.Size(134, 36);
            this.TxtIniciales.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(141, 138);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 84;
            this.label1.Text = "Nombre";
            // 
            // EProvider
            // 
            this.EProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.EProvider.ContainerControl = this;
            this.EProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("EProvider.Icon")));
            // 
            // AProvider
            // 
            this.AProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.AProvider.ContainerControl = this;
            this.AProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("AProvider.Icon")));
            // 
            // BtnNewPass
            // 
            this.BtnNewPass.BackColor = System.Drawing.Color.White;
            this.BtnNewPass.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnNewPass.BorderRadius = 5;
            this.BtnNewPass.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.ToogleButton;
            this.BtnNewPass.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("BtnNewPass.CheckedState.Image")));
            this.BtnNewPass.CheckedState.Parent = this.BtnNewPass;
            this.BtnNewPass.CustomImages.Parent = this.BtnNewPass;
            this.BtnNewPass.FillColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnNewPass.ForeColor = System.Drawing.Color.White;
            this.BtnNewPass.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.HoverState.Parent = this.BtnNewPass;
            this.BtnNewPass.Image = ((System.Drawing.Image)(resources.GetObject("BtnNewPass.Image")));
            this.BtnNewPass.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnNewPass.Location = new System.Drawing.Point(505, 235);
            this.BtnNewPass.Name = "BtnNewPass";
            this.BtnNewPass.PressedColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.ShadowDecoration.Color = System.Drawing.Color.LightGray;
            this.BtnNewPass.ShadowDecoration.Depth = 10;
            this.BtnNewPass.ShadowDecoration.Parent = this.BtnNewPass;
            this.BtnNewPass.Size = new System.Drawing.Size(41, 28);
            this.BtnNewPass.TabIndex = 93;
            this.BtnNewPass.Tag = "";
            this.BtnNewPass.CheckedChanged += new System.EventHandler(this.BtnNewPass_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RdbUNormal);
            this.groupBox1.Controls.Add(this.RdbUAdmin);
            this.groupBox1.Controls.Add(this.gunaLabel1);
            this.groupBox1.Controls.Add(this.gunaLabel2);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.groupBox1.Location = new System.Drawing.Point(167, 338);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 74);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo de usuario";
            // 
            // RdbUNormal
            // 
            this.RdbUNormal.Checked = true;
            this.RdbUNormal.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUNormal.CheckedState.BorderThickness = 0;
            this.RdbUNormal.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUNormal.CheckedState.InnerColor = System.Drawing.Color.White;
            this.RdbUNormal.CheckedState.Parent = this.RdbUNormal;
            this.RdbUNormal.Location = new System.Drawing.Point(17, 38);
            this.RdbUNormal.Name = "RdbUNormal";
            this.RdbUNormal.ShadowDecoration.Parent = this.RdbUNormal;
            this.RdbUNormal.Size = new System.Drawing.Size(20, 20);
            this.RdbUNormal.TabIndex = 5;
            this.RdbUNormal.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.RdbUNormal.UncheckedState.BorderThickness = 2;
            this.RdbUNormal.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.RdbUNormal.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.RdbUNormal.UncheckedState.Parent = this.RdbUNormal;
            this.RdbUNormal.CheckedChanged += new System.EventHandler(this.guna2CustomRadioButton2_CheckedChanged);
            // 
            // RdbUAdmin
            // 
            this.RdbUAdmin.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUAdmin.CheckedState.BorderThickness = 0;
            this.RdbUAdmin.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUAdmin.CheckedState.InnerColor = System.Drawing.Color.White;
            this.RdbUAdmin.CheckedState.Parent = this.RdbUAdmin;
            this.RdbUAdmin.Location = new System.Drawing.Point(183, 38);
            this.RdbUAdmin.Name = "RdbUAdmin";
            this.RdbUAdmin.ShadowDecoration.Parent = this.RdbUAdmin;
            this.RdbUAdmin.Size = new System.Drawing.Size(20, 20);
            this.RdbUAdmin.TabIndex = 6;
            this.RdbUAdmin.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.RdbUAdmin.UncheckedState.BorderThickness = 2;
            this.RdbUAdmin.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.RdbUAdmin.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.RdbUAdmin.UncheckedState.Parent = this.RdbUAdmin;
            this.RdbUAdmin.CheckedChanged += new System.EventHandler(this.RdbUAdmin_CheckedChanged);
            // 
            // gunaLabel1
            // 
            this.gunaLabel1.AutoSize = true;
            this.gunaLabel1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gunaLabel1.Location = new System.Drawing.Point(209, 43);
            this.gunaLabel1.Name = "gunaLabel1";
            this.gunaLabel1.Size = new System.Drawing.Size(83, 15);
            this.gunaLabel1.TabIndex = 88;
            this.gunaLabel1.Text = "Administrador";
            // 
            // gunaLabel2
            // 
            this.gunaLabel2.AutoSize = true;
            this.gunaLabel2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gunaLabel2.Location = new System.Drawing.Point(43, 43);
            this.gunaLabel2.Name = "gunaLabel2";
            this.gunaLabel2.Size = new System.Drawing.Size(90, 15);
            this.gunaLabel2.TabIndex = 89;
            this.gunaLabel2.Text = "Usuario Normal";
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this;
            // 
            // FrmAddUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(613, 507);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BtnNewPass);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtPassConfirm);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtPass);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.BtnCanelar);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.TxtIniciales);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.guna2ControlBox2);
            this.Controls.Add(this.guna2ControlBox1);
            this.Controls.Add(this.LblAccion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmAddUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Usuario";
            this.Leave += new System.EventHandler(this.FrmAddUser_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2TextBox TxtPassConfirm;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox TxtPass;
        private Guna.UI2.WinForms.Guna2Button BtnGuardar;
        private Guna.UI2.WinForms.Guna2Button BtnCanelar;
        private System.Windows.Forms.Label label24;
        private Guna.UI2.WinForms.Guna2TextBox TxtNombre;
        private Guna.UI2.WinForms.Guna2TextBox TxtIniciales;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblAccion;
        private System.Windows.Forms.ErrorProvider EProvider;
        private System.Windows.Forms.ErrorProvider AProvider;
        private Guna.UI2.WinForms.Guna2Button BtnNewPass;
        private System.Windows.Forms.GroupBox groupBox1;
        private Guna.UI2.WinForms.Guna2CustomRadioButton RdbUNormal;
        private Guna.UI2.WinForms.Guna2CustomRadioButton RdbUAdmin;
        private Guna.UI.WinForms.GunaLabel gunaLabel1;
        private Guna.UI.WinForms.GunaLabel gunaLabel2;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
    }
}