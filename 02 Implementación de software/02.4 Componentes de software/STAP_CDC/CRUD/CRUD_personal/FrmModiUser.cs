﻿using CRUD_personal;
using STAP_CDC.Acceso;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.CRUD.CRUD_personal
{
    public partial class FrmModiUser : Form
    {

        public bool continuar;
        private string nombre;
        private string iniciales;
        private string tipo;
        ControlPersonal mControlPersonal;

        public FrmModiUser()
        {
            InitializeComponent();
            mControlPersonal = new ControlPersonal();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            continuar = false;
            TxtPassNueva.PasswordChar = '\u25CF';
            TxtPassConfirm.PasswordChar = '\u25CF';
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
            TxtNombre.Focus();


        }
        public string idPersonal { get; set; }
        public string _nombre
        {
            get { return TxtNombre.Text; }
            set { nombre = value; TxtNombre.Text = nombre; }
        }





        public string _iniciales
        {
            get { return TxtIniciales.Text; }
            set { iniciales = value; TxtIniciales.Text = iniciales; }
        }

        public string _tipo
        {
            get { return tipo; }
            set
            {
                tipo = value;
                switch (tipo)
                {
                    case "0":
                        RdbUNormal.Checked = true;
                        RdbUAdmin.Checked = false;
                        break;
                    case "1":
                        RdbUNormal.Checked = false;
                        RdbUAdmin.Checked = true;
                        break;
                }
            }
        }

        private string contrasenaNueva;

        public string _contrasenaNueva
        {
            get { return contrasenaNueva; }
            set { contrasenaNueva = value; }
        }



        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (TxtNombre.Text.Trim() != "")
            {
                if (TxtIniciales.Text.Trim() != "")
                {
                    if (SwitchPass.Checked)
                    {

                        if (TxtPassNueva.Text.Trim() != "")
                        {
                            if (TxtPassConfirm.Text.Trim() != "")
                            {
                                if (TxtPassNueva.Text == TxtPassConfirm.Text)
                                {
                                    verificar();

                                }
                            }
                            else
                            {
                                EProvider.Clear();
                                EProvider.SetError(TxtPassConfirm, "Tiene que confirmar su contraseña");
                            }
                        }
                        else
                        {
                            EProvider.Clear();
                            EProvider.SetError(TxtPassNueva, "La contraseña no puede quedar vacío");
                        }

                    }
                    else
                    {
                        verificar();
                    }
                }
                else
                {
                    EProvider.Clear();
                    EProvider.SetError(TxtIniciales, "El campo iniciales no puede quedar vacío");
                }

            }
            else
            {
                EProvider.Clear();
                EProvider.SetError(TxtNombre, "El campo Nombre no puede quedar vacío");
            }
        }

        public void verificar()
        {
            

            if (!(TxtNombre.Text.Trim().ToUpper() == this.nombre) || !(TxtIniciales.Text.Trim().ToUpper() == this.iniciales))
            {
                if (mControlPersonal.iniciales(TxtIniciales.Text.Trim().ToUpper()))
                {
                    continuar = false;
                    MessageBox.Show(TxtIniciales.Text.Trim().ToUpper() + " ya está registrado, por favor intente con otras iniciales.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    TxtIniciales.Focus();
                }
                else
                {
                    continuar = true;
                    this.Close();
                }
            }
            else
            {
                continuar = true;
                this.Close();
            }
        }

        private void TxtPassConfirm_KeyUp(object sender, KeyEventArgs e)
        {
            if (TxtPassNueva.Text == TxtPassConfirm.Text)
            {
                EProvider.Clear();
                AProvider.SetError(TxtPassConfirm, "Correcto");
                _contrasenaNueva = TxtPassConfirm.Text;
            }
            else
            {
                AProvider.Clear();
                EProvider.SetError(TxtPassConfirm, "Incorrecta");
            }
        }

        private void TxtNombre_Leave(object sender, EventArgs e)
        {
            String iniciales = "";

            try
            {
                String[] nombreCompleto = TxtNombre.Text.Split(' ');
                foreach (String palabra in nombreCompleto)
                {
                    iniciales += palabra[0];
                }
                TxtIniciales.Text = iniciales.ToUpper();
            }
            catch
            {

            }
        }

        private void BtnPassAnterior_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BtnPassNueva_CheckedChanged(object sender, EventArgs e)
        {
            switch (BtnPassNueva.Checked)
            {
                case true:
                    TxtPassNueva.PasswordChar = '\u0000';
                    TxtPassConfirm.PasswordChar = '\u0000';
                    break;

                case false:
                    TxtPassNueva.PasswordChar = '\u25CF';
                    TxtPassConfirm.PasswordChar = '\u25CF';
                    break;
            }
        }

        private void RdbUNormal_CheckedChanged(object sender, EventArgs e)
        {
            tipo = "0";
        }

        private void RdbUAdmin_CheckedChanged(object sender, EventArgs e)
        {
            tipo = "1";
        }

        private void FrmModiUser_Leave(object sender, EventArgs e)
        {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void SwitchPass_CheckedChanged(object sender, EventArgs e)
        {
            switch (SwitchPass.Checked)
            {
                case false:

                    Lbl2.Enabled = false;
                    Lbl3.Enabled = false;

                    TxtPassNueva.Clear();
                    TxtPassConfirm.Clear();

                    TxtPassNueva.Enabled = false;
                    TxtPassConfirm.Enabled = false;

                    BtnPassNueva.Enabled = false;
                    break;
                case true:

                    Lbl2.Enabled = true;
                    Lbl3.Enabled = true;

                    TxtPassNueva.Enabled = true;
                    TxtPassConfirm.Enabled = true;

                    BtnPassNueva.Enabled = true;

                    break;

            }
        }

        private void BtnCanelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtPassAnterior_Leave(object sender, EventArgs e)
        {
            EProvider.Clear();
        }

        private void FrmModiUser_Load(object sender, EventArgs e)
        {

        }
    }
}
