﻿namespace STAP_CDC.CRUD.CRUD_personal {
    partial class FrmModiUser {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmModiUser));
            this.BtnGuardar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnCanelar = new Guna.UI2.WinForms.Guna2Button();
            this.LblAccion = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtNombre = new Guna.UI2.WinForms.Guna2TextBox();
            this.TxtIniciales = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.Lbl2 = new System.Windows.Forms.Label();
            this.TxtPassNueva = new Guna.UI2.WinForms.Guna2TextBox();
            this.Lbl3 = new System.Windows.Forms.Label();
            this.TxtPassConfirm = new Guna.UI2.WinForms.Guna2TextBox();
            this.EProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.AProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.BtnPassNueva = new Guna.UI2.WinForms.Guna2Button();
            this.gunaLabel1 = new Guna.UI.WinForms.GunaLabel();
            this.gunaLabel2 = new Guna.UI.WinForms.GunaLabel();
            this.RdbUAdmin = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.RdbUNormal = new Guna.UI2.WinForms.Guna2CustomRadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SwitchPass = new Guna.UI.WinForms.GunaGoogleSwitch();
            this.label5 = new System.Windows.Forms.Label();
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Animated = true;
            this.BtnGuardar.BackColor = System.Drawing.Color.Transparent;
            this.BtnGuardar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.BorderRadius = 15;
            this.BtnGuardar.BorderThickness = 1;
            this.BtnGuardar.CheckedState.Parent = this.BtnGuardar;
            this.BtnGuardar.CustomImages.Parent = this.BtnGuardar;
            this.BtnGuardar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F);
            this.BtnGuardar.ForeColor = System.Drawing.Color.White;
            this.BtnGuardar.HoverState.Parent = this.BtnGuardar;
            this.BtnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("BtnGuardar.Image")));
            this.BtnGuardar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnGuardar.Location = new System.Drawing.Point(189, 498);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.ShadowDecoration.Parent = this.BtnGuardar;
            this.BtnGuardar.Size = new System.Drawing.Size(124, 40);
            this.BtnGuardar.TabIndex = 8;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseTransparentBackground = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnCanelar
            // 
            this.BtnCanelar.Animated = true;
            this.BtnCanelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCanelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.BorderRadius = 15;
            this.BtnCanelar.BorderThickness = 1;
            this.BtnCanelar.CheckedState.Parent = this.BtnCanelar;
            this.BtnCanelar.CustomImages.Parent = this.BtnCanelar;
            this.BtnCanelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCanelar.ForeColor = System.Drawing.Color.White;
            this.BtnCanelar.HoverState.Parent = this.BtnCanelar;
            this.BtnCanelar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCanelar.Image")));
            this.BtnCanelar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnCanelar.Location = new System.Drawing.Point(348, 498);
            this.BtnCanelar.Name = "BtnCanelar";
            this.BtnCanelar.ShadowDecoration.Parent = this.BtnCanelar;
            this.BtnCanelar.Size = new System.Drawing.Size(124, 40);
            this.BtnCanelar.TabIndex = 9;
            this.BtnCanelar.Text = "Cancelar";
            this.BtnCanelar.UseTransparentBackground = true;
            this.BtnCanelar.Click += new System.EventHandler(this.BtnCanelar_Click);
            // 
            // LblAccion
            // 
            this.LblAccion.AutoSize = true;
            this.LblAccion.BackColor = System.Drawing.Color.White;
            this.LblAccion.Font = new System.Drawing.Font("Microsoft Tai Le", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LblAccion.Location = new System.Drawing.Point(20, 37);
            this.LblAccion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblAccion.Name = "LblAccion";
            this.LblAccion.Size = new System.Drawing.Size(152, 40);
            this.LblAccion.TabIndex = 70;
            this.LblAccion.Text = "Modificar";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(150, 174);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 20);
            this.label24.TabIndex = 68;
            this.label24.Text = "Iniciales";
            // 
            // TxtNombre
            // 
            this.TxtNombre.Animated = true;
            this.TxtNombre.BackColor = System.Drawing.Color.White;
            this.TxtNombre.BorderRadius = 10;
            this.TxtNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtNombre.DefaultText = "";
            this.TxtNombre.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtNombre.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtNombre.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNombre.DisabledState.Parent = this.TxtNombre;
            this.TxtNombre.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNombre.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNombre.FocusedState.Parent = this.TxtNombre;
            this.TxtNombre.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNombre.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNombre.HoverState.Parent = this.TxtNombre;
            this.TxtNombre.Location = new System.Drawing.Point(233, 116);
            this.TxtNombre.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.PasswordChar = '\0';
            this.TxtNombre.PlaceholderText = "";
            this.TxtNombre.SelectedText = "";
            this.TxtNombre.ShadowDecoration.Parent = this.TxtNombre;
            this.TxtNombre.Size = new System.Drawing.Size(276, 36);
            this.TxtNombre.TabIndex = 0;
            this.TxtNombre.Leave += new System.EventHandler(this.TxtNombre_Leave);
            // 
            // TxtIniciales
            // 
            this.TxtIniciales.Animated = true;
            this.TxtIniciales.BackColor = System.Drawing.Color.White;
            this.TxtIniciales.BorderRadius = 10;
            this.TxtIniciales.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtIniciales.DefaultText = "";
            this.TxtIniciales.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtIniciales.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtIniciales.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIniciales.DisabledState.Parent = this.TxtIniciales;
            this.TxtIniciales.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIniciales.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIniciales.FocusedState.Parent = this.TxtIniciales;
            this.TxtIniciales.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIniciales.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIniciales.HoverState.Parent = this.TxtIniciales;
            this.TxtIniciales.Location = new System.Drawing.Point(233, 166);
            this.TxtIniciales.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtIniciales.Name = "TxtIniciales";
            this.TxtIniciales.PasswordChar = '\0';
            this.TxtIniciales.PlaceholderText = "";
            this.TxtIniciales.SelectedText = "";
            this.TxtIniciales.ShadowDecoration.Parent = this.TxtIniciales;
            this.TxtIniciales.Size = new System.Drawing.Size(134, 36);
            this.TxtIniciales.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(148, 124);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 69;
            this.label1.Text = "Nombre";
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(551, 1);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox2.TabIndex = 10;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(598, 1);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox1.TabIndex = 11;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // Lbl2
            // 
            this.Lbl2.AutoSize = true;
            this.Lbl2.BackColor = System.Drawing.Color.White;
            this.Lbl2.Enabled = false;
            this.Lbl2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl2.Location = new System.Drawing.Point(72, 275);
            this.Lbl2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lbl2.Name = "Lbl2";
            this.Lbl2.Size = new System.Drawing.Size(147, 20);
            this.Lbl2.TabIndex = 77;
            this.Lbl2.Text = "Nueva contraseña";
            // 
            // TxtPassNueva
            // 
            this.TxtPassNueva.Animated = true;
            this.TxtPassNueva.BackColor = System.Drawing.Color.White;
            this.TxtPassNueva.BorderRadius = 10;
            this.TxtPassNueva.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPassNueva.DefaultText = "";
            this.TxtPassNueva.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPassNueva.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPassNueva.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassNueva.DisabledState.Parent = this.TxtPassNueva;
            this.TxtPassNueva.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassNueva.Enabled = false;
            this.TxtPassNueva.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassNueva.FocusedState.Parent = this.TxtPassNueva;
            this.TxtPassNueva.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPassNueva.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassNueva.HoverState.Parent = this.TxtPassNueva;
            this.TxtPassNueva.Location = new System.Drawing.Point(234, 266);
            this.TxtPassNueva.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPassNueva.Name = "TxtPassNueva";
            this.TxtPassNueva.PasswordChar = '\0';
            this.TxtPassNueva.PlaceholderText = "";
            this.TxtPassNueva.SelectedText = "";
            this.TxtPassNueva.ShadowDecoration.Parent = this.TxtPassNueva;
            this.TxtPassNueva.Size = new System.Drawing.Size(276, 36);
            this.TxtPassNueva.TabIndex = 4;
            // 
            // Lbl3
            // 
            this.Lbl3.AutoSize = true;
            this.Lbl3.BackColor = System.Drawing.Color.White;
            this.Lbl3.Enabled = false;
            this.Lbl3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Lbl3.Location = new System.Drawing.Point(46, 325);
            this.Lbl3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lbl3.Name = "Lbl3";
            this.Lbl3.Size = new System.Drawing.Size(173, 20);
            this.Lbl3.TabIndex = 80;
            this.Lbl3.Text = "Comfirmar contraseña";
            // 
            // TxtPassConfirm
            // 
            this.TxtPassConfirm.Animated = true;
            this.TxtPassConfirm.BackColor = System.Drawing.Color.White;
            this.TxtPassConfirm.BorderRadius = 10;
            this.TxtPassConfirm.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPassConfirm.DefaultText = "";
            this.TxtPassConfirm.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPassConfirm.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPassConfirm.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassConfirm.DisabledState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPassConfirm.Enabled = false;
            this.TxtPassConfirm.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassConfirm.FocusedState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPassConfirm.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPassConfirm.HoverState.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Location = new System.Drawing.Point(235, 313);
            this.TxtPassConfirm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPassConfirm.Name = "TxtPassConfirm";
            this.TxtPassConfirm.PasswordChar = '\0';
            this.TxtPassConfirm.PlaceholderText = "";
            this.TxtPassConfirm.SelectedText = "";
            this.TxtPassConfirm.ShadowDecoration.Parent = this.TxtPassConfirm;
            this.TxtPassConfirm.Size = new System.Drawing.Size(275, 36);
            this.TxtPassConfirm.TabIndex = 5;
            this.TxtPassConfirm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtPassConfirm_KeyUp);
            // 
            // EProvider
            // 
            this.EProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.EProvider.ContainerControl = this;
            this.EProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("EProvider.Icon")));
            // 
            // AProvider
            // 
            this.AProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.AProvider.ContainerControl = this;
            this.AProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("AProvider.Icon")));
            // 
            // BtnPassNueva
            // 
            this.BtnPassNueva.BackColor = System.Drawing.Color.White;
            this.BtnPassNueva.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnPassNueva.BorderRadius = 5;
            this.BtnPassNueva.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.ToogleButton;
            this.BtnPassNueva.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("BtnPassNueva.CheckedState.Image")));
            this.BtnPassNueva.CheckedState.Parent = this.BtnPassNueva;
            this.BtnPassNueva.CustomImages.Parent = this.BtnPassNueva;
            this.BtnPassNueva.Enabled = false;
            this.BtnPassNueva.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnPassNueva.ForeColor = System.Drawing.Color.White;
            this.BtnPassNueva.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.HoverState.Parent = this.BtnPassNueva;
            this.BtnPassNueva.Image = ((System.Drawing.Image)(resources.GetObject("BtnPassNueva.Image")));
            this.BtnPassNueva.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnPassNueva.Location = new System.Drawing.Point(512, 271);
            this.BtnPassNueva.Name = "BtnPassNueva";
            this.BtnPassNueva.PressedColor = System.Drawing.Color.Transparent;
            this.BtnPassNueva.ShadowDecoration.Color = System.Drawing.Color.LightGray;
            this.BtnPassNueva.ShadowDecoration.Depth = 10;
            this.BtnPassNueva.ShadowDecoration.Parent = this.BtnPassNueva;
            this.BtnPassNueva.Size = new System.Drawing.Size(41, 28);
            this.BtnPassNueva.TabIndex = 82;
            this.BtnPassNueva.Tag = "";
            this.BtnPassNueva.CheckedChanged += new System.EventHandler(this.BtnPassNueva_CheckedChanged);
            // 
            // gunaLabel1
            // 
            this.gunaLabel1.AutoSize = true;
            this.gunaLabel1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gunaLabel1.Location = new System.Drawing.Point(205, 40);
            this.gunaLabel1.Name = "gunaLabel1";
            this.gunaLabel1.Size = new System.Drawing.Size(83, 15);
            this.gunaLabel1.TabIndex = 88;
            this.gunaLabel1.Text = "Administrador";
            // 
            // gunaLabel2
            // 
            this.gunaLabel2.AutoSize = true;
            this.gunaLabel2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.gunaLabel2.Location = new System.Drawing.Point(39, 41);
            this.gunaLabel2.Name = "gunaLabel2";
            this.gunaLabel2.Size = new System.Drawing.Size(90, 15);
            this.gunaLabel2.TabIndex = 89;
            this.gunaLabel2.Text = "Usuario Normal";
            // 
            // RdbUAdmin
            // 
            this.RdbUAdmin.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUAdmin.CheckedState.BorderThickness = 0;
            this.RdbUAdmin.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUAdmin.CheckedState.InnerColor = System.Drawing.Color.White;
            this.RdbUAdmin.CheckedState.Parent = this.RdbUAdmin;
            this.RdbUAdmin.Location = new System.Drawing.Point(183, 38);
            this.RdbUAdmin.Name = "RdbUAdmin";
            this.RdbUAdmin.ShadowDecoration.Parent = this.RdbUAdmin;
            this.RdbUAdmin.Size = new System.Drawing.Size(20, 20);
            this.RdbUAdmin.TabIndex = 7;
            this.RdbUAdmin.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.RdbUAdmin.UncheckedState.BorderThickness = 2;
            this.RdbUAdmin.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.RdbUAdmin.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.RdbUAdmin.UncheckedState.Parent = this.RdbUAdmin;
            this.RdbUAdmin.CheckedChanged += new System.EventHandler(this.RdbUAdmin_CheckedChanged);
            // 
            // RdbUNormal
            // 
            this.RdbUNormal.Checked = true;
            this.RdbUNormal.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUNormal.CheckedState.BorderThickness = 0;
            this.RdbUNormal.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.RdbUNormal.CheckedState.InnerColor = System.Drawing.Color.White;
            this.RdbUNormal.CheckedState.Parent = this.RdbUNormal;
            this.RdbUNormal.Location = new System.Drawing.Point(17, 38);
            this.RdbUNormal.Name = "RdbUNormal";
            this.RdbUNormal.ShadowDecoration.Parent = this.RdbUNormal;
            this.RdbUNormal.Size = new System.Drawing.Size(20, 20);
            this.RdbUNormal.TabIndex = 6;
            this.RdbUNormal.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.RdbUNormal.UncheckedState.BorderThickness = 2;
            this.RdbUNormal.UncheckedState.FillColor = System.Drawing.Color.Transparent;
            this.RdbUNormal.UncheckedState.InnerColor = System.Drawing.Color.Transparent;
            this.RdbUNormal.UncheckedState.Parent = this.RdbUNormal;
            this.RdbUNormal.CheckedChanged += new System.EventHandler(this.RdbUNormal_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RdbUNormal);
            this.groupBox1.Controls.Add(this.RdbUAdmin);
            this.groupBox1.Controls.Add(this.gunaLabel1);
            this.groupBox1.Controls.Add(this.gunaLabel2);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.groupBox1.Location = new System.Drawing.Point(180, 383);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 74);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo de usuario";
            // 
            // SwitchPass
            // 
            this.SwitchPass.BaseColor = System.Drawing.SystemColors.Control;
            this.SwitchPass.CheckedOffColor = System.Drawing.Color.DarkGray;
            this.SwitchPass.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.SwitchPass.FillColor = System.Drawing.Color.White;
            this.SwitchPass.Location = new System.Drawing.Point(235, 227);
            this.SwitchPass.Name = "SwitchPass";
            this.SwitchPass.Size = new System.Drawing.Size(38, 20);
            this.SwitchPass.TabIndex = 2;
            this.SwitchPass.CheckedChanged += new System.EventHandler(this.SwitchPass_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(63, 228);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 19);
            this.label5.TabIndex = 94;
            this.label5.Text = "Cambiar contraseña";
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this;
            // 
            // FrmModiUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(646, 615);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.SwitchPass);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BtnPassNueva);
            this.Controls.Add(this.Lbl3);
            this.Controls.Add(this.TxtPassConfirm);
            this.Controls.Add(this.Lbl2);
            this.Controls.Add(this.TxtPassNueva);
            this.Controls.Add(this.guna2ControlBox2);
            this.Controls.Add(this.guna2ControlBox1);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.BtnCanelar);
            this.Controls.Add(this.LblAccion);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.TxtIniciales);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmModiUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar Usuario";
            this.Load += new System.EventHandler(this.FrmModiUser_Load);
            this.Leave += new System.EventHandler(this.FrmModiUser_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI2.WinForms.Guna2Button BtnGuardar;
        private Guna.UI2.WinForms.Guna2Button BtnCanelar;
        private System.Windows.Forms.Label label24;
        private Guna.UI2.WinForms.Guna2TextBox TxtNombre;
        private Guna.UI2.WinForms.Guna2TextBox TxtIniciales;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Label Lbl3;
        private Guna.UI2.WinForms.Guna2TextBox TxtPassConfirm;
        private System.Windows.Forms.Label Lbl2;
        private Guna.UI2.WinForms.Guna2TextBox TxtPassNueva;
        private System.Windows.Forms.ErrorProvider EProvider;
        private System.Windows.Forms.ErrorProvider AProvider;
        private Guna.UI2.WinForms.Guna2Button BtnPassNueva;
        private System.Windows.Forms.GroupBox groupBox1;
        private Guna.UI2.WinForms.Guna2CustomRadioButton RdbUNormal;
        private Guna.UI2.WinForms.Guna2CustomRadioButton RdbUAdmin;
        private Guna.UI.WinForms.GunaLabel gunaLabel1;
        private Guna.UI.WinForms.GunaLabel gunaLabel2;
        public System.Windows.Forms.Label LblAccion;
        private System.Windows.Forms.Label label5;
        private Guna.UI.WinForms.GunaGoogleSwitch SwitchPass;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
    }
}