﻿
using STAP_CDC.Acceso;
using STAP_CDC.CRUD.CRUD_personal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace CRUD_personal
{
    public partial class FrmUsuarios : Form
    {
        ControlPersonal mControlPersonal;
        DataSet data;
        public FrmUsuarios()
        {
            InitializeComponent();
            mControlPersonal = new ControlPersonal();
            if (Global.NivelUsuario == 1) {
                panel0.Visible = false;
            } else {
                TxtPassAnterior.PasswordChar = '\u25CF';
                TxtPassNueva.PasswordChar = '\u25CF';
                TxtPassConfirm.PasswordChar = '\u25CF';
                try {
                    LblNombre.Text = mControlPersonal.nombreUsuario(Global.idPersonal.ToString());
                } catch { }
                try {
                    LblRol.Text = mControlPersonal.rolUsuario(Global.idPersonal.ToString());
                    
                } catch { }
            }
        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            actualizarTabla();
        }
        public void actualizarTabla()
        {
            ControlPersonal mControlUsuarios = new ControlPersonal();
            
            DgvUsuarios.DataSource = mControlUsuarios.consultarUsuarios().Tables[0];

            DgvUsuarios.Columns["nombre_completo"].HeaderText = "Nombre completo";

            DgvUsuarios.Columns["iniciales"].HeaderText = "Iniciales";

            DgvUsuarios.Columns["idPersonal"].Visible = false;

        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            FrmAddUser mFrmAdd = new FrmAddUser();
            mFrmAdd.ShowDialog();


            if (mFrmAdd.correcto) {
                try {
                    Usuario mUsuario = new Usuario();
                    Personal mPersonal = new Personal();

                    mPersonal.Nombre = mFrmAdd.getNombre();
                    mPersonal.Iniciales = mFrmAdd.getIniciales();

                    mUsuario.contrasena = mFrmAdd.getContrasena();
                    mUsuario.tipo = mFrmAdd.getTipo();

                    ControlPersonal mControlPersonal = new ControlPersonal();

                    mControlPersonal.agregarUsuarioYPass(mPersonal, mUsuario);

                    MessageBox.Show("Nuevo miembro: " + mPersonal.Nombre + " registrado", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    actualizarTabla();

                } catch (Exception err) {
                    MessageBox.Show("Error: " + err.Message + " al guardar el usuario");
                }
            }
        }


        private void BtnModificar_Click(object sender, EventArgs e)
        {
            if (DgvUsuarios.SelectedRows.Count != -1) {
                ControlPersonal mControlPersonal = new ControlPersonal();
                FrmModiUser mFrmMod = new FrmModiUser();
                mFrmMod._nombre = DgvUsuarios.SelectedRows[0].Cells[1].Value.ToString();
                mFrmMod._iniciales = DgvUsuarios.SelectedRows[0].Cells[2].Value.ToString();
                mFrmMod.idPersonal = DgvUsuarios.SelectedRows[0].Cells[0].Value.ToString();
                mFrmMod.LblAccion.Text = "Modificar " +  mFrmMod._iniciales + " - " + mFrmMod._nombre;
                mFrmMod._tipo = mControlPersonal.tipo(DgvUsuarios.SelectedRows[0].Cells[0].Value.ToString());
                mFrmMod.ShowDialog();

                if (mFrmMod.continuar) {
                    try {
                        Personal mPersonal = new Personal();
                        Usuario mUsuario = new Usuario();

                        mPersonal.IDPersonal = int.Parse(DgvUsuarios.SelectedRows[0].Cells[0].Value.ToString());
                        mPersonal.Nombre = mFrmMod._nombre;
                        mPersonal.Iniciales = mFrmMod._iniciales.ToUpper();

                        mUsuario.contrasena = mFrmMod._contrasenaNueva;
                        mUsuario.tipo = mFrmMod._tipo;

                        mControlPersonal.modificarUsuario(mPersonal, mUsuario);
                       
                        MessageBox.Show("Miembro actulizado a: " + mPersonal.Nombre, "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        actualizarTabla();
                    } catch {

                    }

                } 
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
          
        }

        private void BtnActualizar_Click_1(object sender, EventArgs e)
        {
         
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("¿Realmente desea eliminar el usuario: "+ DgvUsuarios.SelectedRows[0].Cells[1].Value.ToString() +"?", "Eliminar usuario", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.OK)
            {
                try
                {
                    int id = int.Parse(DgvUsuarios.SelectedRows[0].Cells[0].Value.ToString());
                    ControlPersonal mControlPersonal = new ControlPersonal();
                    mControlPersonal.eliminarUsuario(id);
                    MessageBox.Show("Mimbro eliminado", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    actualizarTabla();
                }
                catch (Exception err)
                {
                    MessageBox.Show("Error: " + err.Message + " al eliminar el usuario");
                }
            }
            
            
        }

        private void TxtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            
            DgvUsuarios.DataSource = mControlPersonal.consultarUsuarios(TxtBuscar.Text).Tables[0];

        }

        private void DgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e) {
            if (TxtPassAnterior.Text.Trim() != "") {
                if (TxtPassNueva.Text.Trim() != "") {
                    if (TxtPassConfirm.Text.Trim() != "") {
                        if (TxtPassNueva.Text == TxtPassConfirm.Text) {
                            ControlLogin mControlLogin = new ControlLogin();
                            if (mControlLogin.comprobarUsuario(data.Tables[0].Rows[0]["Iniciales"].ToString(), TxtPassAnterior.Text)) {
                                Personal mPersonal = new Personal();
                                Usuario mUsuario = new Usuario();

                                mPersonal.IDPersonal = Global.idPersonal;
                                mPersonal.Nombre = data.Tables[0].Rows[0]["Nombre"].ToString();
                                mPersonal.Iniciales = data.Tables[0].Rows[0]["Iniciales"].ToString();

                                mUsuario.contrasena = TxtPassConfirm.Text;
                                mUsuario.tipo = "0";

                                mControlPersonal.modificarUsuario(mPersonal, mUsuario);

                                
                                MessageBox.Show(mPersonal.Nombre + " tu contraseña ha actualizada con éxito.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                TxtPassAnterior.Clear();
                                TxtPassNueva.Clear();
                                TxtPassConfirm.Clear();
                                EProvider.Clear();
                                AProvider.Clear();

                            } else {
                                MessageBox.Show("La contraseña anterior es incorrecta, inténte de nuevo por favor.", "ERROR!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                TxtPassAnterior.Focus();
                            }
                        }
                    } else {
                        EProvider.Clear();
                        EProvider.SetError(TxtPassConfirm, "Tiene que confirmar su contraseña");
                    }
                } else {
                    EProvider.Clear();
                    EProvider.SetError(TxtPassNueva, "La contraseña no puede quedar vacío");
                }
            } else {
                EProvider.Clear();
                EProvider.SetError(TxtPassAnterior, "La contraseña no puede quedar vacío");
            }
        }

        private void BtnPassAnterior_CheckedChanged(object sender, EventArgs e) {
            switch (BtnPassAnterior.Checked) {
                case true:
                    TxtPassAnterior.PasswordChar = '\u0000';
                    break;

                case false:
                    TxtPassAnterior.PasswordChar = '\u25CF';
                    break;
            }
        }

        private void BtnPassNueva_CheckedChanged(object sender, EventArgs e) {
            switch (BtnPassNueva.Checked) {
                case true:
                    TxtPassNueva.PasswordChar = '\u0000';
                    TxtPassConfirm.PasswordChar = '\u0000';
                    break;

                case false:
                    TxtPassNueva.PasswordChar = '\u25CF';
                    TxtPassConfirm.PasswordChar = '\u25CF';
                    break;
            }
        }

        private void TxtPassConfirm_KeyUp(object sender, KeyEventArgs e) {
            if (TxtPassNueva.Text == TxtPassConfirm.Text) {
                EProvider.Clear();
                AProvider.SetError(TxtPassConfirm, "Correcto");
            } else {
                AProvider.Clear();
                EProvider.SetError(TxtPassConfirm, "Incorrecta");
            }
        }
    }
}
