﻿using AccesoDatos;
using STAP_CDC.CRUD.CRUD_personal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_personal
{
    public class ControlPersonal
    {
        private Conexion mConexion;
        public ControlPersonal()
        {
            mConexion = new Conexion();
            mConexion.conectar();
        }

        public void agregarUsuario(Personal mUsuario)
        {
            String SQL = "insert into personal values (null,'?1','?2','?3')";
            SQL = SQL.Replace("?1", mUsuario.Iniciales);
            SQL = SQL.Replace("?2", mUsuario.Nombre);
            SQL = SQL.Replace("?3", "1");
            mConexion.ejecutarActualizacion(SQL);
        }
        public DataSet consultarUsuarios(string texto = "")
        {
            String SQL = "select idpersonal,nombre_completo,iniciales from Personal WHERE (nombre_completo like \'%" + texto + "%\' or iniciales like \'%" + texto + "%\') and visibilidad = 1";
            DataSet ListaUsuarios = mConexion.ejecutarConsulta(SQL);
            return ListaUsuarios;
        }
        public void modificarUsuario(Personal mPersonal, Usuario mUsuario)
        {
            String SQL = "update Personal set nombre_completo='?1',iniciales='?2' where idpersonal='?3';";
            SQL = SQL.Replace("?1", mPersonal.Nombre);
            SQL = SQL.Replace("?2", mPersonal.Iniciales);
            SQL = SQL.Replace("?3", mPersonal.IDPersonal.ToString());
            mConexion.ejecutarActualizacion(SQL);

            string SQLU = "";
            if (mUsuario.contrasena != null) {
                SQLU = "update usuario set contrasena = '?c', tipo = ?t where usuario.idPersonal = ?i;";
                SQLU = SQLU.Replace("?c", mUsuario.contrasena);
                SQLU = SQLU.Replace("?t", mUsuario.tipo);
                SQLU = SQLU.Replace("?i", mPersonal.IDPersonal.ToString());
                
            } else {
                SQLU = "update usuario set tipo = ?t where usuario.idPersonal = ?i;";
                SQLU = SQLU.Replace("?c", mUsuario.contrasena);
                SQLU = SQLU.Replace("?t", mUsuario.tipo);
                SQLU = SQLU.Replace("?i", mPersonal.IDPersonal.ToString());
               
            }
            mConexion.ejecutarActualizacion(SQLU);

        }
        public void eliminarUsuario(int ID)
        {
            String SQL = "update Personal set visibilidad = 0 where idpersonal='?1'";
            SQL = SQL.Replace("?1", ID.ToString());
            mConexion.ejecutarActualizacion(SQL);
        }

        public bool agregarUsuarioYPass(Personal mPersonal, Usuario mUsuario) {
            agregarUsuario(mPersonal);

            String SQL = "insert into Usuario values (null,'?c','?t',(select max(idPersonal) from personal));";
            SQL = SQL.Replace("?c", mUsuario.contrasena);
            SQL = SQL.Replace("?t", mUsuario.tipo);
            return mConexion.ejecutarActualizacion(SQL);
        }
        public string tipo(string id) {
            String SQL = "select tipo from usuario where idPersonal = ?i;";
            SQL = SQL.Replace("?i", id);
       
            return mConexion.ejecutarConsulta(SQL).Tables[0].Rows[0]["tipo"].ToString(); 
        }

        public bool iniciales(string iniciales) {
            String SQL = "select personal.iniciales from personal where iniciales = '?i' and visibilidad = 1;";
            SQL = SQL.Replace("?i", iniciales);

            return mConexion.ejecutarConsulta(SQL).Tables[0].Rows.Count > 0;
        }

        public bool iniciales(string iniciales, string nombre) {
            String SQL = "select personal.iniciales from personal where iniciales = '?i' and nombre = '?n' and visibilidad = 1;";
            SQL = SQL.Replace("?i", iniciales);
            SQL = SQL.Replace("?n", nombre);


            return mConexion.ejecutarConsulta(SQL).Tables[0].Rows.Count > 0;
        }

        public string rolUsuario(string id) {
            string SQL = "select " +
                "group_concat(rol.identificador, ' - ',rol.nombre_rol separator ', ') as 'Rol' " +
                "from personal inner join equipo_trabajo on personal.idPersonal = equipo_trabajo.idPersonal inner join rol on equipo_trabajo.idRol = rol.idRol " +
                "where personal.idPersonal = ?i group by personal.idPersonal;";
            SQL = SQL.Replace("?i", id);
           

            return mConexion.ejecutarConsulta(SQL).Tables[0].Rows[0]["Rol"].ToString();
        }
        public string nombreUsuario(string id) {
            string SQL = "select group_concat(personal.iniciales, '-', personal.nombre_completo separator '') as 'Nombre' from personal where personal.idPersonal = ?i;";
            SQL = SQL.Replace("?i", id);


            return mConexion.ejecutarConsulta(SQL).Tables[0].Rows[0]["Nombre"].ToString();
        }
    }
}
