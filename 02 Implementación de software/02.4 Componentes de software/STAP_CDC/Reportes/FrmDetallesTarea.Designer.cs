﻿namespace Reportes
{
    partial class FrmDetallesTarea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDetallesTarea));
            this.label24 = new System.Windows.Forms.Label();
            this.TxtTarea = new Guna.UI2.WinForms.Guna2TextBox();
            this.TxtInicioPlan = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtFinPlan = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtDuracionPlan = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtDuracionReal = new Guna.UI2.WinForms.Guna2TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtFinReal = new Guna.UI2.WinForms.Guna2TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtInicioReal = new Guna.UI2.WinForms.Guna2TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnCancelar = new Guna.UI2.WinForms.Guna2Button();
            this.label8 = new System.Windows.Forms.Label();
            this.LstResponsables = new System.Windows.Forms.ListBox();
            this.ChEstado = new Guna.UI2.WinForms.Guna2Chip();
            this.ChRetrasada = new Guna.UI2.WinForms.Guna2Chip();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.SuspendLayout();
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(43, 106);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(130, 24);
            this.label24.TabIndex = 42;
            this.label24.Text = "Inicio planeado:";
            // 
            // TxtTarea
            // 
            this.TxtTarea.Animated = true;
            this.TxtTarea.BackColor = System.Drawing.Color.White;
            this.TxtTarea.BorderRadius = 10;
            this.TxtTarea.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtTarea.DefaultText = "";
            this.TxtTarea.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtTarea.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtTarea.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtTarea.DisabledState.Parent = this.TxtTarea;
            this.TxtTarea.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtTarea.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtTarea.FocusedState.Parent = this.TxtTarea;
            this.TxtTarea.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTarea.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtTarea.HoverState.Parent = this.TxtTarea;
            this.TxtTarea.Location = new System.Drawing.Point(180, 50);
            this.TxtTarea.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtTarea.Name = "TxtTarea";
            this.TxtTarea.PasswordChar = '\0';
            this.TxtTarea.PlaceholderText = "";
            this.TxtTarea.ReadOnly = true;
            this.TxtTarea.SelectedText = "";
            this.TxtTarea.ShadowDecoration.Parent = this.TxtTarea;
            this.TxtTarea.Size = new System.Drawing.Size(449, 36);
            this.TxtTarea.TabIndex = 40;
            // 
            // TxtInicioPlan
            // 
            this.TxtInicioPlan.Animated = true;
            this.TxtInicioPlan.BackColor = System.Drawing.Color.White;
            this.TxtInicioPlan.BorderRadius = 10;
            this.TxtInicioPlan.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtInicioPlan.DefaultText = "";
            this.TxtInicioPlan.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtInicioPlan.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtInicioPlan.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtInicioPlan.DisabledState.Parent = this.TxtInicioPlan;
            this.TxtInicioPlan.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtInicioPlan.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtInicioPlan.FocusedState.Parent = this.TxtInicioPlan;
            this.TxtInicioPlan.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInicioPlan.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtInicioPlan.HoverState.Parent = this.TxtInicioPlan;
            this.TxtInicioPlan.Location = new System.Drawing.Point(181, 94);
            this.TxtInicioPlan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtInicioPlan.Name = "TxtInicioPlan";
            this.TxtInicioPlan.PasswordChar = '\0';
            this.TxtInicioPlan.PlaceholderText = "";
            this.TxtInicioPlan.ReadOnly = true;
            this.TxtInicioPlan.SelectedText = "";
            this.TxtInicioPlan.ShadowDecoration.Parent = this.TxtInicioPlan;
            this.TxtInicioPlan.Size = new System.Drawing.Size(448, 36);
            this.TxtInicioPlan.TabIndex = 41;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(120, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 20);
            this.label1.TabIndex = 43;
            this.label1.Text = "Tarea:";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(65, 146);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 20);
            this.label2.TabIndex = 45;
            this.label2.Text = "Fin planeado:";
            // 
            // TxtFinPlan
            // 
            this.TxtFinPlan.Animated = true;
            this.TxtFinPlan.BackColor = System.Drawing.Color.White;
            this.TxtFinPlan.BorderRadius = 10;
            this.TxtFinPlan.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFinPlan.DefaultText = "";
            this.TxtFinPlan.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtFinPlan.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtFinPlan.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFinPlan.DisabledState.Parent = this.TxtFinPlan;
            this.TxtFinPlan.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFinPlan.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFinPlan.FocusedState.Parent = this.TxtFinPlan;
            this.TxtFinPlan.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinPlan.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFinPlan.HoverState.Parent = this.TxtFinPlan;
            this.TxtFinPlan.Location = new System.Drawing.Point(179, 138);
            this.TxtFinPlan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtFinPlan.Name = "TxtFinPlan";
            this.TxtFinPlan.PasswordChar = '\0';
            this.TxtFinPlan.PlaceholderText = "";
            this.TxtFinPlan.ReadOnly = true;
            this.TxtFinPlan.SelectedText = "";
            this.TxtFinPlan.ShadowDecoration.Parent = this.TxtFinPlan;
            this.TxtFinPlan.Size = new System.Drawing.Size(448, 36);
            this.TxtFinPlan.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(17, 188);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 25);
            this.label3.TabIndex = 47;
            this.label3.Text = "Duración planeada:";
            // 
            // TxtDuracionPlan
            // 
            this.TxtDuracionPlan.Animated = true;
            this.TxtDuracionPlan.BackColor = System.Drawing.Color.White;
            this.TxtDuracionPlan.BorderRadius = 10;
            this.TxtDuracionPlan.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtDuracionPlan.DefaultText = "";
            this.TxtDuracionPlan.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtDuracionPlan.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtDuracionPlan.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtDuracionPlan.DisabledState.Parent = this.TxtDuracionPlan;
            this.TxtDuracionPlan.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtDuracionPlan.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtDuracionPlan.FocusedState.Parent = this.TxtDuracionPlan;
            this.TxtDuracionPlan.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDuracionPlan.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtDuracionPlan.HoverState.Parent = this.TxtDuracionPlan;
            this.TxtDuracionPlan.Location = new System.Drawing.Point(180, 182);
            this.TxtDuracionPlan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtDuracionPlan.Name = "TxtDuracionPlan";
            this.TxtDuracionPlan.PasswordChar = '\0';
            this.TxtDuracionPlan.PlaceholderText = "";
            this.TxtDuracionPlan.ReadOnly = true;
            this.TxtDuracionPlan.SelectedText = "";
            this.TxtDuracionPlan.ShadowDecoration.Parent = this.TxtDuracionPlan;
            this.TxtDuracionPlan.Size = new System.Drawing.Size(448, 36);
            this.TxtDuracionPlan.TabIndex = 46;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(61, 316);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 36);
            this.label4.TabIndex = 53;
            this.label4.Text = "Duración real:";
            // 
            // TxtDuracionReal
            // 
            this.TxtDuracionReal.Animated = true;
            this.TxtDuracionReal.BackColor = System.Drawing.Color.White;
            this.TxtDuracionReal.BorderRadius = 10;
            this.TxtDuracionReal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtDuracionReal.DefaultText = "";
            this.TxtDuracionReal.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtDuracionReal.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtDuracionReal.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtDuracionReal.DisabledState.Parent = this.TxtDuracionReal;
            this.TxtDuracionReal.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtDuracionReal.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtDuracionReal.FocusedState.Parent = this.TxtDuracionReal;
            this.TxtDuracionReal.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDuracionReal.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtDuracionReal.HoverState.Parent = this.TxtDuracionReal;
            this.TxtDuracionReal.Location = new System.Drawing.Point(180, 314);
            this.TxtDuracionReal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtDuracionReal.Name = "TxtDuracionReal";
            this.TxtDuracionReal.PasswordChar = '\0';
            this.TxtDuracionReal.PlaceholderText = "";
            this.TxtDuracionReal.ReadOnly = true;
            this.TxtDuracionReal.SelectedText = "";
            this.TxtDuracionReal.ShadowDecoration.Parent = this.TxtDuracionReal;
            this.TxtDuracionReal.Size = new System.Drawing.Size(448, 36);
            this.TxtDuracionReal.TabIndex = 52;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(108, 279);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 20);
            this.label5.TabIndex = 51;
            this.label5.Text = "Fin real:";
            // 
            // TxtFinReal
            // 
            this.TxtFinReal.Animated = true;
            this.TxtFinReal.BackColor = System.Drawing.Color.White;
            this.TxtFinReal.BorderRadius = 10;
            this.TxtFinReal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFinReal.DefaultText = "";
            this.TxtFinReal.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtFinReal.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtFinReal.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFinReal.DisabledState.Parent = this.TxtFinReal;
            this.TxtFinReal.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFinReal.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFinReal.FocusedState.Parent = this.TxtFinReal;
            this.TxtFinReal.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFinReal.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFinReal.HoverState.Parent = this.TxtFinReal;
            this.TxtFinReal.Location = new System.Drawing.Point(180, 270);
            this.TxtFinReal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtFinReal.Name = "TxtFinReal";
            this.TxtFinReal.PasswordChar = '\0';
            this.TxtFinReal.PlaceholderText = "";
            this.TxtFinReal.ReadOnly = true;
            this.TxtFinReal.SelectedText = "";
            this.TxtFinReal.ShadowDecoration.Parent = this.TxtFinReal;
            this.TxtFinReal.Size = new System.Drawing.Size(448, 36);
            this.TxtFinReal.TabIndex = 50;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(88, 234);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 20);
            this.label6.TabIndex = 49;
            this.label6.Text = "Inicio real:";
            // 
            // TxtInicioReal
            // 
            this.TxtInicioReal.Animated = true;
            this.TxtInicioReal.BackColor = System.Drawing.Color.White;
            this.TxtInicioReal.BorderRadius = 10;
            this.TxtInicioReal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtInicioReal.DefaultText = "";
            this.TxtInicioReal.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtInicioReal.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtInicioReal.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtInicioReal.DisabledState.Parent = this.TxtInicioReal;
            this.TxtInicioReal.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtInicioReal.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtInicioReal.FocusedState.Parent = this.TxtInicioReal;
            this.TxtInicioReal.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInicioReal.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtInicioReal.HoverState.Parent = this.TxtInicioReal;
            this.TxtInicioReal.Location = new System.Drawing.Point(180, 226);
            this.TxtInicioReal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtInicioReal.Name = "TxtInicioReal";
            this.TxtInicioReal.PasswordChar = '\0';
            this.TxtInicioReal.PlaceholderText = "";
            this.TxtInicioReal.ReadOnly = true;
            this.TxtInicioReal.SelectedText = "";
            this.TxtInicioReal.ShadowDecoration.Parent = this.TxtInicioReal;
            this.TxtInicioReal.Size = new System.Drawing.Size(448, 36);
            this.TxtInicioReal.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(104, 362);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 55;
            this.label7.Text = "Estatus:";
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Animated = true;
            this.BtnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCancelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCancelar.BorderRadius = 15;
            this.BtnCancelar.BorderThickness = 1;
            this.BtnCancelar.CheckedState.Parent = this.BtnCancelar;
            this.BtnCancelar.CustomImages.Parent = this.BtnCancelar;
            this.BtnCancelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCancelar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.ForeColor = System.Drawing.Color.White;
            this.BtnCancelar.HoverState.Parent = this.BtnCancelar;
            this.BtnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancelar.Image")));
            this.BtnCancelar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnCancelar.Location = new System.Drawing.Point(553, 477);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.ShadowDecoration.Parent = this.BtnCancelar;
            this.BtnCancelar.Size = new System.Drawing.Size(124, 40);
            this.BtnCancelar.TabIndex = 58;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseTransparentBackground = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(61, 402);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 20);
            this.label8.TabIndex = 57;
            this.label8.Text = "Responsables:";
            // 
            // LstResponsables
            // 
            this.LstResponsables.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F);
            this.LstResponsables.FormattingEnabled = true;
            this.LstResponsables.ItemHeight = 19;
            this.LstResponsables.Location = new System.Drawing.Point(181, 399);
            this.LstResponsables.Name = "LstResponsables";
            this.LstResponsables.Size = new System.Drawing.Size(303, 118);
            this.LstResponsables.TabIndex = 59;
            // 
            // ChEstado
            // 
            this.ChEstado.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.ChEstado.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F);
            this.ChEstado.ForeColor = System.Drawing.Color.White;
            this.ChEstado.Location = new System.Drawing.Point(180, 360);
            this.ChEstado.Name = "ChEstado";
            this.ChEstado.ShadowDecoration.Parent = this.ChEstado;
            this.ChEstado.Size = new System.Drawing.Size(179, 28);
            this.ChEstado.TabIndex = 60;
            this.ChEstado.Text = "---";
            this.ChEstado.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ChEstado.TextTransform = Guna.UI2.WinForms.Enums.TextTransform.UpperCase;
            // 
            // ChRetrasada
            // 
            this.ChRetrasada.FillColor = System.Drawing.Color.Firebrick;
            this.ChRetrasada.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F);
            this.ChRetrasada.ForeColor = System.Drawing.Color.White;
            this.ChRetrasada.Location = new System.Drawing.Point(365, 360);
            this.ChRetrasada.Name = "ChRetrasada";
            this.ChRetrasada.ShadowDecoration.Parent = this.ChRetrasada;
            this.ChRetrasada.Size = new System.Drawing.Size(177, 28);
            this.ChRetrasada.TabIndex = 61;
            this.ChRetrasada.Text = "Retrasada";
            this.ChRetrasada.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.ChRetrasada.TextTransform = Guna.UI2.WinForms.Enums.TextTransform.UpperCase;
            this.ChRetrasada.Visible = false;
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(611, 0);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox2.TabIndex = 67;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(658, 0);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox1.TabIndex = 66;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this;
            // 
            // FrmDetallesTarea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(704, 545);
            this.Controls.Add(this.guna2ControlBox2);
            this.Controls.Add(this.guna2ControlBox1);
            this.Controls.Add(this.ChRetrasada);
            this.Controls.Add(this.ChEstado);
            this.Controls.Add(this.LstResponsables);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtDuracionReal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TxtFinReal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtInicioReal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtDuracionPlan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtFinPlan);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.TxtTarea);
            this.Controls.Add(this.TxtInicioPlan);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDetallesTarea";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmDetallesTarea";
            this.Load += new System.EventHandler(this.FrmDetallesTarea_Load);
            this.Enter += new System.EventHandler(this.FrmDetallesTarea_Enter);
            this.Leave += new System.EventHandler(this.FrmDetallesTarea_Leave);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label24;
        private Guna.UI2.WinForms.Guna2TextBox TxtTarea;
        private Guna.UI2.WinForms.Guna2TextBox TxtInicioPlan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox TxtFinPlan;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TextBox TxtDuracionPlan;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2TextBox TxtDuracionReal;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2TextBox TxtFinReal;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2TextBox TxtInicioReal;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2Button BtnCancelar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox LstResponsables;
        private Guna.UI2.WinForms.Guna2Chip ChEstado;
        private Guna.UI2.WinForms.Guna2Chip ChRetrasada;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
    }
}