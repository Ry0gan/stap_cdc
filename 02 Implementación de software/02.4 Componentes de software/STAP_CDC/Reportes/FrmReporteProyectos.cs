﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reportes
{
    public partial class FrmReporteProyectos : Form
    {
        private ControlReportes mControl;
        Dictionary<int, Proyecto> DiccionarioProyectos = new Dictionary<int, Proyecto>();

        public FrmReporteProyectos()
        {
            InitializeComponent();
          
            mControl = new ControlReportes();

        }


        private void FrmProyectos_Load(object sender, EventArgs e)
        {

            DiccionarioProyectos = mControl.obtenerProyectos();
            if (DiccionarioProyectos != null)
            {
                if (DiccionarioProyectos.Count == 0)
                    PanelEmpty.Visible = true;
                else
                    mostrarListaProyectos("");
            }

        }

        private void mostrarListaProyectos(String filtro)
        {
            int terminados = 0;
            int progreso = 0;
            int pendientes = 0;
            int retrasados;
            int total;
            float porcentaje = 0;

            LayoutProyectos.Controls.Clear();
            foreach (KeyValuePair<int, Proyecto> elemento in DiccionarioProyectos)
            {
                Proyecto mProyecto = elemento.Value;

                ReporteProyectos mElementoReporte = new ReporteProyectos();
                mElementoReporte.IdProyecto = mProyecto.IdProyecto;
                mElementoReporte.Titulo = mProyecto.Titulo_proyecto;
                mElementoReporte.FechaInicio = mProyecto.Inicio_proyecto;
                mElementoReporte.FechaFin = mProyecto.Fin_proyecto;
                if (mProyecto.Duracion_total > 0)
                    porcentaje = (float)mProyecto.Duracion_avance / (float)mProyecto.Duracion_total * 100f;
                else
                    porcentaje = 0;
                mElementoReporte.Porcentaje = (int)porcentaje;
                switch (mElementoReporte.Porcentaje)
                {
                    case 0:
                        pendientes++;
                        break;
                    case 100:
                        terminados++;
                        break;
                    default:
                        progreso++;
                        break;
                }
                //Si esta filtrado solo mostrar los buscados
                if (mProyecto.Titulo_proyecto.ToUpper().Contains(filtro.ToUpper()))
                {
                    LayoutProyectos.Controls.Add(mElementoReporte);
                }
            }

            total = DiccionarioProyectos.Count;
            retrasados = mControl.obtenerCantidadProyectosRetrasados();

            lblPendientes.Text = pendientes.ToString();
            lblProgreso.Text = progreso.ToString();
            lblRetrasados.Text = retrasados.ToString();
            lblTerminados.Text = terminados.ToString();
            PBprogreso.Value = (int)((float)progreso / (float)total * 100f);
            PBterminados.Value = (int)((float)terminados / (float)total * 100f);
            PBpendientes.Value = (int)((float)pendientes / (float)total * 100f);
            PBretrasados.Value = (int)((float)retrasados / (float)total * 100f);

        }


        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            if (DiccionarioProyectos != null)
            {
                mostrarListaProyectos(TxtFiltrar.Text);
            }
        }

        private void PanelEmpty_Leave(object sender, EventArgs e) {
        }

        private void FrmReporteProyectos_Enter(object sender, EventArgs e) {
          
        }

        private void FrmReporteProyectos_Leave(object sender, EventArgs e) {
         
        }

        private void guna2GroupBox1_Click(object sender, EventArgs e) {

        }
    }
}
