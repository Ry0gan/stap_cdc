﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reportes
{
    public partial class FrmDetallesTarea : Form
    {
        private ControlReportes mControl;
        public FrmDetallesTarea()
        {
            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            mControl = new ControlReportes();
            Bitmap bmp = new Bitmap(STAP_CDC.Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void cargarValores(int idTarea)
        {
            DataSet dataTarea = mControl.obtenerDetallesTarea(idTarea);
            foreach (DataRow fila in dataTarea.Tables[0].Rows)
            {
                TxtTarea.Text = Convert.ToString(fila["descripcion"]);
                TxtInicioPlan.Text = Convert.ToString(fila["inicio_plan"]);
                TxtFinPlan.Text = Convert.ToString(fila["fin_plan"]);
                TxtDuracionPlan.Text = Convert.ToString(fila["Duracion_plan"]) + " día(s)";
                TxtInicioReal.Text = aplicarNoDisponible(Convert.ToString(fila["inicio_real"]));
                TxtFinReal.Text = aplicarNoDisponible(Convert.ToString(fila["fin_real"]));
                TxtDuracionReal.Text = aplicarNoDisponible(Convert.ToString(fila["Duracion_real"]));

                //Mostrar el estado
                String estado = Convert.ToString(fila["Estado"]);
                if (estado.ToUpper().Contains("TERMINADA"))
                {
                    ChEstado.Text = "Terminada";
                    ChEstado.FillColor = Color.FromArgb(62, 164, 80);
                }
                if (estado.ToUpper().Contains("PENDIENTE"))
                {
                    ChEstado.Text = "Pendiente";
                    ChEstado.FillColor = Color.Gray;
                }
                if (estado.ToUpper().Contains("EN PROGRESO"))
                {
                    ChEstado.Text = "En progreso";
                    ChEstado.FillColor = Color.FromArgb(216,75,32);
                }
                ChRetrasada.Visible = estado.ToUpper().Contains("RETRASADA");
            }
            //Obtener los responsables
            DataSet dataResponsables = mControl.obtenerResponsablesTarea(idTarea);
            LstResponsables.Items.Clear();
            foreach (DataRow fila in dataResponsables.Tables[0].Rows)
            {
                String responsable = "("+Convert.ToString(fila["iniciales"])+") "+ Convert.ToString(fila["nombre_completo"]);
                LstResponsables.Items.Add(responsable);
            }

        }

        private String aplicarNoDisponible(String text)
        {
            if (!text.Equals(""))
            {
                return text;
            }
            else
            {
                return "No disponible";
            }
        }

        private void FrmDetallesTarea_Load(object sender, EventArgs e)
        {
            
        }

        private void FrmDetallesTarea_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmDetallesTarea_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
