﻿namespace Reportes
{
    partial class ReporteProyectos
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReporteProyectos));
            this.PanelContenido = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFin = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LblInicio = new System.Windows.Forms.Label();
            this.Icon = new System.Windows.Forms.PictureBox();
            this.LblNombre = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.Grafica = new Guna.UI2.WinForms.Guna2CircleProgressBar();
            this.PanelContenido.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Icon)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelContenido
            // 
            this.PanelContenido.BorderRadius = 20;
            this.PanelContenido.Controls.Add(this.label1);
            this.PanelContenido.Controls.Add(this.lblFin);
            this.PanelContenido.Controls.Add(this.label3);
            this.PanelContenido.Controls.Add(this.label2);
            this.PanelContenido.Controls.Add(this.LblInicio);
            this.PanelContenido.Controls.Add(this.Icon);
            this.PanelContenido.Controls.Add(this.LblNombre);
            this.PanelContenido.Controls.Add(this.lblStatus);
            this.PanelContenido.Controls.Add(this.Grafica);
            this.PanelContenido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContenido.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.PanelContenido.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.PanelContenido.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.PanelContenido.FillColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.PanelContenido.Location = new System.Drawing.Point(0, 0);
            this.PanelContenido.Name = "PanelContenido";
            this.PanelContenido.ShadowDecoration.Parent = this.PanelContenido;
            this.PanelContenido.Size = new System.Drawing.Size(500, 160);
            this.PanelContenido.TabIndex = 9;
            this.PanelContenido.Leave += new System.EventHandler(this.PanelContenido_Leave);
            this.PanelContenido.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PanelContenido_MouseClick);
            this.PanelContenido.MouseLeave += new System.EventHandler(this.PanelContenido_MouseLeave);
            this.PanelContenido.MouseHover += new System.EventHandler(this.PanelContenido_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(83, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 21);
            this.label1.TabIndex = 13;
            this.label1.Text = "Fin:";
            // 
            // lblFin
            // 
            this.lblFin.AutoSize = true;
            this.lblFin.BackColor = System.Drawing.Color.Transparent;
            this.lblFin.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold);
            this.lblFin.ForeColor = System.Drawing.Color.White;
            this.lblFin.Location = new System.Drawing.Point(123, 113);
            this.lblFin.Name = "lblFin";
            this.lblFin.Size = new System.Drawing.Size(41, 21);
            this.lblFin.TabIndex = 12;
            this.lblFin.Text = "N/A";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(83, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 21);
            this.label3.TabIndex = 11;
            this.label3.Text = "Inicio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 12F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(83, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 21);
            this.label2.TabIndex = 10;
            this.label2.Text = "Título:";
            // 
            // LblInicio
            // 
            this.LblInicio.AutoSize = true;
            this.LblInicio.BackColor = System.Drawing.Color.Transparent;
            this.LblInicio.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold);
            this.LblInicio.ForeColor = System.Drawing.Color.White;
            this.LblInicio.Location = new System.Drawing.Point(139, 76);
            this.LblInicio.Name = "LblInicio";
            this.LblInicio.Size = new System.Drawing.Size(41, 21);
            this.LblInicio.TabIndex = 8;
            this.LblInicio.Text = "N/A";
            // 
            // Icon
            // 
            this.Icon.BackColor = System.Drawing.Color.Transparent;
            this.Icon.Image = ((System.Drawing.Image)(resources.GetObject("Icon.Image")));
            this.Icon.Location = new System.Drawing.Point(18, 13);
            this.Icon.Name = "Icon";
            this.Icon.Size = new System.Drawing.Size(56, 56);
            this.Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Icon.TabIndex = 7;
            this.Icon.TabStop = false;
            this.Icon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Icon_MouseClick);
            // 
            // LblNombre
            // 
            this.LblNombre.BackColor = System.Drawing.Color.Transparent;
            this.LblNombre.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold);
            this.LblNombre.ForeColor = System.Drawing.Color.White;
            this.LblNombre.Location = new System.Drawing.Point(141, 11);
            this.LblNombre.Name = "LblNombre";
            this.LblNombre.Size = new System.Drawing.Size(220, 56);
            this.LblNombre.TabIndex = 2;
            this.LblNombre.Text = "N/A";
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.Color.White;
            this.lblStatus.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Location = new System.Drawing.Point(375, 124);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(100, 19);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "--";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Grafica
            // 
            this.Grafica.BackColor = System.Drawing.Color.Transparent;
            this.Grafica.FillThickness = 10;
            this.Grafica.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Grafica.ForeColor = System.Drawing.Color.White;
            this.Grafica.Location = new System.Drawing.Point(375, 11);
            this.Grafica.Name = "Grafica";
            this.Grafica.ProgressColor = System.Drawing.Color.Green;
            this.Grafica.ProgressColor2 = System.Drawing.Color.Yellow;
            this.Grafica.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Triangle;
            this.Grafica.ProgressThickness = 10;
            this.Grafica.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.Grafica.ShadowDecoration.Parent = this.Grafica;
            this.Grafica.ShowPercentage = true;
            this.Grafica.Size = new System.Drawing.Size(100, 100);
            this.Grafica.TabIndex = 5;
            this.Grafica.Value = 100;
            // 
            // ReporteProyectos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PanelContenido);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ReporteProyectos";
            this.Size = new System.Drawing.Size(500, 160);
            this.PanelContenido.ResumeLayout(false);
            this.PanelContenido.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Icon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2CustomGradientPanel PanelContenido;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LblInicio;
        private System.Windows.Forms.PictureBox Icon;
        private System.Windows.Forms.Label LblNombre;
        private System.Windows.Forms.Label lblStatus;
        private Guna.UI2.WinForms.Guna2CircleProgressBar Grafica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFin;
    }
}
