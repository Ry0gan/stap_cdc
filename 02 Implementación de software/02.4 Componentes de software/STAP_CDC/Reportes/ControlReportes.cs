﻿using System;
using System.Collections.Generic;
using System.Data;
using AccesoDatos;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reportes
{
    public class ControlReportes
    {
        Conexion mConexion;
        public ControlReportes()
        {
            mConexion = new Conexion();
            mConexion.conectar();
        }



        public DataSet consultarProyectos()
        {
            String SQL = "select * from proyecto where Visibilidad = 1 order by fin_proyecto";
            DataSet ListaProyectos = mConexion.ejecutarConsulta(SQL);
            return ListaProyectos;
        }

        public DataSet consultarDuracionTotalProyecto()
        {
            String SQL = "select P.idProyecto as ID, sum(datediff(fin_plan,inicio_plan)+1) as duracion" +
                "  from proyecto P inner join tarea T on P.idProyecto = T.idProyecto" +
                 " where visibilidad=1" +
                " group by P.idProyecto";
            DataSet ListaDuraciones = mConexion.ejecutarConsulta(SQL);
            return ListaDuraciones;
        }

        public DataSet consultarDuracionAvanceProyecto()
        {
            String SQL = "select P.idProyecto as ID, sum(datediff(fin_plan,inicio_plan)+1) as duracion" +
                " from proyecto P left join tarea T on P.idProyecto = T.idProyecto" +
                " where not isnull(fin_real) and visibilidad=1" +
                " group by P.idProyecto";
            DataSet ListaDuracionesAvance = mConexion.ejecutarConsulta(SQL);
            return ListaDuracionesAvance;
        }
        /// <summary>
        /// Obtener los proyectos que tienen una o más tareas retrasadas
        /// </summary>
        /// <returns>conteo - Cantidad de proyectos con retraso</returns>
        public int obtenerCantidadProyectosRetrasados()
        {
            int conteo = 0;
            String SQL = "select distinct(P.idProyecto) from tarea T inner join proyecto P on T.idProyecto=P.idProyecto " +
                " where ( (isnull(inicio_real) and inicio_plan<now())  or  (inicio_plan < inicio_real)  or  (fin_plan < fin_real) ) " +
                " and visibilidad=1";
            DataSet ListaProyectosRetrasados = mConexion.ejecutarConsulta(SQL);
            conteo = ListaProyectosRetrasados.Tables[0].Rows.Count;
            return conteo;
        }

        /// <summary>
        /// Obtiene los datos de un proyecto con sus duraciones planeadas y reales
        /// </summary>
        /// <returns>Diccionario con los datos del proyecto</returns>
        public Dictionary<int, Proyecto> obtenerProyectos()
        {
            try
            {
                Dictionary<int, Proyecto> DiccionarioProyectos = new Dictionary<int, Proyecto>();
                //Obtener los datos generales del proyecto
                DataSet dataProyectos = this.consultarProyectos();
                
                //Formar los proyectos
                foreach (DataRow fila in dataProyectos.Tables[0].Rows)
                {
                    Proyecto mProyecto = new Proyecto();
                    mProyecto.IdProyecto = Convert.ToInt32(fila["idProyecto"]);
                    mProyecto.Titulo_proyecto = Convert.ToString(fila["titulo_proyecto"]);
                    mProyecto.Inicio_proyecto = Convert.ToString(fila["inicio_proyecto"]);
                    mProyecto.Fin_proyecto = Convert.ToString(fila["fin_proyecto"]);
                    mProyecto.Duracion_avance = 0;
                    mProyecto.Duracion_total = 0;
                    mProyecto.Porcentaje = 0;
                    DiccionarioProyectos.Add(mProyecto.IdProyecto, mProyecto);
                }

                //Obtener la duracion de los avances del proyecto
                DataSet dataAvance = this.consultarDuracionAvanceProyecto();
                //Actualizar la duracion de avance
                foreach (DataRow fila in dataAvance.Tables[0].Rows)
                {
                    int IdProyecto = Convert.ToInt32(fila["ID"]);
                    Proyecto mProyecto;
                    if (DiccionarioProyectos.ContainsKey(IdProyecto))
                    {
                        DiccionarioProyectos.TryGetValue(IdProyecto, out mProyecto);
                        mProyecto.Duracion_avance = Convert.ToInt32(fila["duracion"]);
                    }
                }
                //Obtener la duracion TOTAL del proyecto
                DataSet dataDuracionTotal = this.consultarDuracionTotalProyecto();
                //Actualizar la duracion de avance
                foreach (DataRow fila in dataDuracionTotal.Tables[0].Rows)
                {
                    int IdProyecto = Convert.ToInt32(fila["ID"]);
                    Proyecto mProyecto;
                    if (DiccionarioProyectos.ContainsKey(IdProyecto))
                    {
                        DiccionarioProyectos.TryGetValue(IdProyecto, out mProyecto);
                        mProyecto.Duracion_total = Convert.ToInt32(fila["duracion"]);
                    }
                }
                return DiccionarioProyectos;
            }
            catch
            {
                return null;
            }

        }


        public DataSet obtenerListaTareas(int idProyecto)
        {
            String SQL = "select idTarea, descripcion as 'Descripción', inicio_plan as 'Inicio planeado', fin_plan as 'Fin planeado'," +
                " inicio_real as 'Inicio real', fin_real as 'Fin real'," +
                " concat(IF(not isnull(fin_real),\"Terminada\",IF(isnull(inicio_real),    \"Pendiente\",     \"En progreso\"))," +
                " if ((isnull(inicio_real) and inicio_plan<now())or(inicio_plan < inicio_real)or(fin_plan < fin_real),  \",Retrasada\",   \"\")) as 'Estado'" +
                " from tarea" +
                " where idProyecto='"+idProyecto+"'";
            DataSet ListaTareas = mConexion.ejecutarConsulta(SQL);
            return ListaTareas;
        }

        public DataSet obtenerDetallesTarea(int idTarea)
        {
            String SQL = "select idTarea, descripcion, inicio_plan , fin_plan ," +
                " datediff(fin_plan, inicio_plan) + 1 as 'Duracion_plan', " +
                " inicio_real, fin_real," +
                " datediff(fin_real, inicio_real) + 1 as 'Duracion_real'," +
                " concat(IF(not isnull(fin_real),\"TERMINADA\",IF(isnull(inicio_real),    \"PENDIENTE\",     \"EN PROGRESO\"))," +
                " if ((isnull(inicio_real) and inicio_plan<now()) or (inicio_plan < inicio_real) or (fin_plan < fin_real),  \",RETRASADA\",   \"\")) as 'Estado'" +
                " from tarea" +
                " where idTarea='" + idTarea + "'";
            DataSet ListaTareas = mConexion.ejecutarConsulta(SQL);
            return ListaTareas;
        }

        /// <summary>
        /// Obtiene los datos simples de un proyecto
        /// </summary>
        /// <param name="idProyecto"></param>
        /// <returns>Datos de proyecto</returns>
        public DataSet obtenerDatosProyecto(int idProyecto)
        {
            String SQL = "select * from proyecto where idProyecto='" + idProyecto + "'";
            DataSet Proyecto = mConexion.ejecutarConsulta(SQL);
            return Proyecto;
        }

        public DataSet obtenerResponsablesTarea(int idTarea)
        {
            String SQL = "select iniciales, nombre_completo" +
                " from personal P inner join tarea_personal TP on TP.idPersonal=P.idPersonal" +
                " where TP.idTarea='" + idTarea + "'";
            DataSet ListaResponsables = mConexion.ejecutarConsulta(SQL);
            return ListaResponsables;
        }
    }
}
