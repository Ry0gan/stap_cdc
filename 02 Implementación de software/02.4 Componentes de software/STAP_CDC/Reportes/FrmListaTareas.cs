﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reportes
{
    public partial class FrmListaTareas : Form
    {
        private ControlReportes mControl;
        int idProyecto = -1;
        int idTarea = -1;
        public FrmListaTareas()
        {
            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            mControl = new ControlReportes();
            Bitmap bmp = new Bitmap(STAP_CDC.Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());

        }


        public void setIdProyecto(int id)
        {
            this.idProyecto = id;
        }

        private void FrmListaTareas_Load(object sender, EventArgs e)
        {
            DataSet dataProyecto = mControl.obtenerDatosProyecto(this.idProyecto);
            foreach (DataRow fila in dataProyecto.Tables[0].Rows)
            {
                lblTitulo.Text = Convert.ToString(fila["titulo_proyecto"]);
            }
            DataSet dataTareas = mControl.obtenerListaTareas(this.idProyecto);
            
            DtgTareas.DataSource = dataTareas.Tables[0];
            DtgTareas.Columns["idTarea"].Visible = false;
            if (dataTareas.Tables[0].Rows.Count == 0)
            {
                PanelEmpty.Visible = true;
                BtnDetalleTarea.Visible = false;
            }
            else
            {

                //Obtener estadísticas
                int progreso = 0;
                int terminadas = 0;
                int retrasadas = 0;

                //Recorrer el dataset y contabilidad segun su estado
                foreach (DataRow fila in dataTareas.Tables[0].Rows)
                {
                    String estado = Convert.ToString(fila["Estado"]);

                    if (estado.ToUpper().Contains("TERMINADA"))
                    {
                        terminadas++;
                    }
                    else if (estado.ToUpper().Contains("EN PROGRESO"))
                    {
                        progreso++;
                    }
                    if (estado.ToUpper().Contains("RETRASADA"))
                    {
                        retrasadas++;
                    }
                }
                int total = dataTareas.Tables[0].Rows.Count;

                lblProgreso.Text = progreso.ToString();
                lblRetrasados.Text = retrasadas.ToString();
                lblTerminados.Text = terminadas.ToString();
                PBprogreso.Value = (int)((float)progreso / (float)total * 100f);
                PBterminados.Value = (int)((float)terminadas / (float)total * 100f);
                PBretrasados.Value = (int)((float)retrasadas / (float)total * 100f);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnDetalleTarea_Click(object sender, EventArgs e)
        {
            if (idTarea != -1)
            {
                //Obtener el ID de la tarea seleccionada
                FrmDetallesTarea mForm = new FrmDetallesTarea();
                mForm.cargarValores(idTarea);
                mForm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Seleccione una tarea de la tabla para mostrar sus detalles.","Información",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DtgTareas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void DtgTareas_Click(object sender, EventArgs e)
        {
            int sel = DtgTareas.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (sel == 1)
            {
                idTarea = Convert.ToInt32(DtgTareas.SelectedRows[0].Cells[0].Value);
            }
        }

        private void FrmListaTareas_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmListaTareas_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
