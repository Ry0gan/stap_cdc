﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reportes
{
    public class Proyecto
    {
        public int IdProyecto { get; set; }
        public string Titulo_proyecto { get; set; }
        public string Inicio_proyecto { get; set; }
        public string Fin_proyecto { get; set; }
        public int Porcentaje { get; set; }
        public int Duracion_total { get; set; }
        public int Duracion_avance { get; set; }
    }
}
