﻿namespace Reportes
{
    partial class FrmReporteProyectos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReporteProyectos));
            this.toolTipReportes = new System.Windows.Forms.ToolTip(this.components);
            this.TxtFiltrar = new Guna.UI2.WinForms.Guna2TextBox();
            this.PBprogreso = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.PBpendientes = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.PBterminados = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.PBretrasados = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.LayoutProyectos = new System.Windows.Forms.FlowLayoutPanel();
            this.LblDialogoP = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblProgreso = new System.Windows.Forms.Label();
            this.lblPendientes = new System.Windows.Forms.Label();
            this.lblTerminados = new System.Windows.Forms.Label();
            this.lblRetrasados = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PanelEmpty = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.PbAviso = new System.Windows.Forms.PictureBox();
            this.guna2GroupBox1 = new Guna.UI2.WinForms.Guna2GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.PanelEmpty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).BeginInit();
            this.guna2GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtFiltrar
            // 
            this.TxtFiltrar.BackColor = System.Drawing.Color.White;
            this.TxtFiltrar.BorderRadius = 10;
            this.TxtFiltrar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFiltrar.DefaultText = "";
            this.TxtFiltrar.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtFiltrar.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtFiltrar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.DisabledState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.FocusedState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFiltrar.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.HoverState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Location = new System.Drawing.Point(13, 81);
            this.TxtFiltrar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtFiltrar.Name = "TxtFiltrar";
            this.TxtFiltrar.PasswordChar = '\0';
            this.TxtFiltrar.PlaceholderText = "Buscar...";
            this.TxtFiltrar.SelectedText = "";
            this.TxtFiltrar.ShadowDecoration.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Size = new System.Drawing.Size(384, 36);
            this.TxtFiltrar.TabIndex = 27;
            this.toolTipReportes.SetToolTip(this.TxtFiltrar, "Filtrar proyectos");
            this.TxtFiltrar.TextChanged += new System.EventHandler(this.TxtFiltrar_TextChanged);
            // 
            // PBprogreso
            // 
            this.PBprogreso.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBprogreso.FillColor = System.Drawing.Color.LightGray;
            this.PBprogreso.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBprogreso.Location = new System.Drawing.Point(23, 106);
            this.PBprogreso.Margin = new System.Windows.Forms.Padding(2);
            this.PBprogreso.Name = "PBprogreso";
            this.PBprogreso.ProgressColor = System.Drawing.Color.Teal;
            this.PBprogreso.ProgressColor2 = System.Drawing.Color.MidnightBlue;
            this.PBprogreso.ShadowDecoration.Parent = this.PBprogreso;
            this.PBprogreso.Size = new System.Drawing.Size(225, 23);
            this.PBprogreso.TabIndex = 35;
            this.PBprogreso.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolTipReportes.SetToolTip(this.PBprogreso, "Cantidad de proyectos en progreso");
            this.PBprogreso.Value = 50;
            // 
            // PBpendientes
            // 
            this.PBpendientes.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBpendientes.FillColor = System.Drawing.Color.LightGray;
            this.PBpendientes.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBpendientes.Location = new System.Drawing.Point(23, 171);
            this.PBpendientes.Margin = new System.Windows.Forms.Padding(2);
            this.PBpendientes.Name = "PBpendientes";
            this.PBpendientes.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.PBpendientes.ProgressColor2 = System.Drawing.Color.Fuchsia;
            this.PBpendientes.ShadowDecoration.Parent = this.PBpendientes;
            this.PBpendientes.Size = new System.Drawing.Size(225, 23);
            this.PBpendientes.TabIndex = 41;
            this.PBpendientes.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolTipReportes.SetToolTip(this.PBpendientes, "Cantidad de proyectos pendientes");
            this.PBpendientes.Value = 50;
            // 
            // PBterminados
            // 
            this.PBterminados.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBterminados.FillColor = System.Drawing.Color.LightGray;
            this.PBterminados.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBterminados.Location = new System.Drawing.Point(23, 234);
            this.PBterminados.Margin = new System.Windows.Forms.Padding(2);
            this.PBterminados.Name = "PBterminados";
            this.PBterminados.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.PBterminados.ProgressColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PBterminados.ShadowDecoration.Parent = this.PBterminados;
            this.PBterminados.Size = new System.Drawing.Size(225, 23);
            this.PBterminados.TabIndex = 43;
            this.PBterminados.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolTipReportes.SetToolTip(this.PBterminados, "Cantidad de proyectos totales");
            this.PBterminados.Value = 50;
            // 
            // PBretrasados
            // 
            this.PBretrasados.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBretrasados.FillColor = System.Drawing.Color.LightGray;
            this.PBretrasados.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBretrasados.Location = new System.Drawing.Point(23, 297);
            this.PBretrasados.Margin = new System.Windows.Forms.Padding(2);
            this.PBretrasados.Name = "PBretrasados";
            this.PBretrasados.ProgressColor = System.Drawing.Color.Yellow;
            this.PBretrasados.ProgressColor2 = System.Drawing.Color.Maroon;
            this.PBretrasados.ShadowDecoration.Parent = this.PBretrasados;
            this.PBretrasados.Size = new System.Drawing.Size(225, 23);
            this.PBretrasados.TabIndex = 45;
            this.PBretrasados.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.toolTipReportes.SetToolTip(this.PBretrasados, "Cantidad de proyectos retrasados");
            this.PBretrasados.Value = 50;
            // 
            // LayoutProyectos
            // 
            this.LayoutProyectos.AutoScroll = true;
            this.LayoutProyectos.Location = new System.Drawing.Point(13, 133);
            this.LayoutProyectos.Margin = new System.Windows.Forms.Padding(2);
            this.LayoutProyectos.Name = "LayoutProyectos";
            this.LayoutProyectos.Size = new System.Drawing.Size(538, 555);
            this.LayoutProyectos.TabIndex = 26;
            // 
            // LblDialogoP
            // 
            this.LblDialogoP.AutoSize = true;
            this.LblDialogoP.BackColor = System.Drawing.Color.Transparent;
            this.LblDialogoP.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Bold);
            this.LblDialogoP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LblDialogoP.Location = new System.Drawing.Point(11, 19);
            this.LblDialogoP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblDialogoP.Name = "LblDialogoP";
            this.LblDialogoP.Size = new System.Drawing.Size(222, 27);
            this.LblDialogoP.TabIndex = 28;
            this.LblDialogoP.Text = "Reporte de Proyectos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(23, 84);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 19);
            this.label1.TabIndex = 40;
            this.label1.Text = "En progreso";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(23, 149);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 19);
            this.label2.TabIndex = 42;
            this.label2.Text = "Pendientes";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(23, 212);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 19);
            this.label3.TabIndex = 44;
            this.label3.Text = "Terminados";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(23, 275);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 19);
            this.label4.TabIndex = 46;
            this.label4.Text = "Retrasados";
            // 
            // lblProgreso
            // 
            this.lblProgreso.AutoSize = true;
            this.lblProgreso.BackColor = System.Drawing.Color.White;
            this.lblProgreso.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, System.Drawing.FontStyle.Bold);
            this.lblProgreso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblProgreso.Location = new System.Drawing.Point(275, 106);
            this.lblProgreso.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProgreso.Name = "lblProgreso";
            this.lblProgreso.Size = new System.Drawing.Size(21, 23);
            this.lblProgreso.TabIndex = 47;
            this.lblProgreso.Text = "0";
            // 
            // lblPendientes
            // 
            this.lblPendientes.AutoSize = true;
            this.lblPendientes.BackColor = System.Drawing.Color.White;
            this.lblPendientes.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, System.Drawing.FontStyle.Bold);
            this.lblPendientes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblPendientes.Location = new System.Drawing.Point(275, 171);
            this.lblPendientes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPendientes.Name = "lblPendientes";
            this.lblPendientes.Size = new System.Drawing.Size(21, 23);
            this.lblPendientes.TabIndex = 48;
            this.lblPendientes.Text = "0";
            // 
            // lblTerminados
            // 
            this.lblTerminados.AutoSize = true;
            this.lblTerminados.BackColor = System.Drawing.Color.White;
            this.lblTerminados.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, System.Drawing.FontStyle.Bold);
            this.lblTerminados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTerminados.Location = new System.Drawing.Point(275, 234);
            this.lblTerminados.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTerminados.Name = "lblTerminados";
            this.lblTerminados.Size = new System.Drawing.Size(21, 23);
            this.lblTerminados.TabIndex = 49;
            this.lblTerminados.Text = "0";
            // 
            // lblRetrasados
            // 
            this.lblRetrasados.AutoSize = true;
            this.lblRetrasados.BackColor = System.Drawing.Color.White;
            this.lblRetrasados.Font = new System.Drawing.Font("Microsoft Tai Le", 13.8F, System.Drawing.FontStyle.Bold);
            this.lblRetrasados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblRetrasados.Location = new System.Drawing.Point(275, 297);
            this.lblRetrasados.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRetrasados.Name = "lblRetrasados";
            this.lblRetrasados.Size = new System.Drawing.Size(21, 23);
            this.lblRetrasados.TabIndex = 50;
            this.lblRetrasados.Text = "0";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(379, 363);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 45);
            this.label5.TabIndex = 24;
            this.label5.Text = "Lo sentimos :(";
            // 
            // PanelEmpty
            // 
            this.PanelEmpty.BorderColor = System.Drawing.Color.Black;
            this.PanelEmpty.Controls.Add(this.label6);
            this.PanelEmpty.Controls.Add(this.PbAviso);
            this.PanelEmpty.Controls.Add(this.label5);
            this.PanelEmpty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelEmpty.Location = new System.Drawing.Point(0, 0);
            this.PanelEmpty.Margin = new System.Windows.Forms.Padding(2);
            this.PanelEmpty.Name = "PanelEmpty";
            this.PanelEmpty.ShadowDecoration.Parent = this.PanelEmpty;
            this.PanelEmpty.Size = new System.Drawing.Size(944, 699);
            this.PanelEmpty.TabIndex = 37;
            this.PanelEmpty.Visible = false;
            this.PanelEmpty.Leave += new System.EventHandler(this.PanelEmpty_Leave);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(301, 439);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(402, 39);
            this.label6.TabIndex = 26;
            this.label6.Text = "No hay proyectos registrados...";
            // 
            // PbAviso
            // 
            this.PbAviso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbAviso.Image = ((System.Drawing.Image)(resources.GetObject("PbAviso.Image")));
            this.PbAviso.Location = new System.Drawing.Point(414, 187);
            this.PbAviso.Name = "PbAviso";
            this.PbAviso.Size = new System.Drawing.Size(137, 133);
            this.PbAviso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbAviso.TabIndex = 25;
            this.PbAviso.TabStop = false;
            // 
            // guna2GroupBox1
            // 
            this.guna2GroupBox1.Controls.Add(this.lblRetrasados);
            this.guna2GroupBox1.Controls.Add(this.pictureBox3);
            this.guna2GroupBox1.Controls.Add(this.lblTerminados);
            this.guna2GroupBox1.Controls.Add(this.PBretrasados);
            this.guna2GroupBox1.Controls.Add(this.lblPendientes);
            this.guna2GroupBox1.Controls.Add(this.PBprogreso);
            this.guna2GroupBox1.Controls.Add(this.lblProgreso);
            this.guna2GroupBox1.Controls.Add(this.label1);
            this.guna2GroupBox1.Controls.Add(this.label4);
            this.guna2GroupBox1.Controls.Add(this.PBpendientes);
            this.guna2GroupBox1.Controls.Add(this.label2);
            this.guna2GroupBox1.Controls.Add(this.label3);
            this.guna2GroupBox1.Controls.Add(this.PBterminados);
            this.guna2GroupBox1.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold);
            this.guna2GroupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.guna2GroupBox1.Location = new System.Drawing.Point(557, 133);
            this.guna2GroupBox1.Name = "guna2GroupBox1";
            this.guna2GroupBox1.ShadowDecoration.Parent = this.guna2GroupBox1;
            this.guna2GroupBox1.Size = new System.Drawing.Size(379, 390);
            this.guna2GroupBox1.TabIndex = 42;
            this.guna2GroupBox1.Text = "        Estadísticas";
            this.guna2GroupBox1.Click += new System.EventHandler(this.guna2GroupBox1_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(7, 2);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(35, 35);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 42;
            this.pictureBox3.TabStop = false;
            // 
            // FrmReporteProyectos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(944, 699);
            this.Controls.Add(this.PanelEmpty);
            this.Controls.Add(this.TxtFiltrar);
            this.Controls.Add(this.LayoutProyectos);
            this.Controls.Add(this.LblDialogoP);
            this.Controls.Add(this.guna2GroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmReporteProyectos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmProyectos";
            this.Load += new System.EventHandler(this.FrmProyectos_Load);
            this.Enter += new System.EventHandler(this.FrmReporteProyectos_Enter);
            this.Leave += new System.EventHandler(this.FrmReporteProyectos_Leave);
            this.PanelEmpty.ResumeLayout(false);
            this.PanelEmpty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).EndInit();
            this.guna2GroupBox1.ResumeLayout(false);
            this.guna2GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTipReportes;
        private Guna.UI2.WinForms.Guna2TextBox TxtFiltrar;
        private System.Windows.Forms.FlowLayoutPanel LayoutProyectos;
        private System.Windows.Forms.Label LblDialogoP;
        private Guna.UI2.WinForms.Guna2ProgressBar PBprogreso;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2ProgressBar PBpendientes;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2ProgressBar PBterminados;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2ProgressBar PBretrasados;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblProgreso;
        private System.Windows.Forms.Label lblPendientes;
        private System.Windows.Forms.Label lblTerminados;
        private System.Windows.Forms.Label lblRetrasados;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2GradientPanel PanelEmpty;
        private System.Windows.Forms.PictureBox PbAviso;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2GroupBox guna2GroupBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}