﻿namespace Reportes
{
    partial class FrmListaTareas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmListaTareas));
            this.DtgTareas = new Guna.UI2.WinForms.Guna2DataGridView();
            this.BtnCancelar = new Guna.UI2.WinForms.Guna2Button();
            this.lblRetrasados = new System.Windows.Forms.Label();
            this.lblTerminados = new System.Windows.Forms.Label();
            this.lblProgreso = new System.Windows.Forms.Label();
            this.PBretrasados = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.PBterminados = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.PBprogreso = new Guna.UI2.WinForms.Guna2ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.BtnDetalleTarea = new Guna.UI2.WinForms.Guna2Button();
            this.PanelEmpty = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.PbAviso = new System.Windows.Forms.PictureBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2GroupBox1 = new Guna.UI2.WinForms.Guna2GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DtgTareas)).BeginInit();
            this.PanelEmpty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).BeginInit();
            this.guna2GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // DtgTareas
            // 
            this.DtgTareas.AllowUserToAddRows = false;
            this.DtgTareas.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.DtgTareas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DtgTareas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DtgTareas.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.DtgTareas.BackgroundColor = System.Drawing.Color.White;
            this.DtgTareas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DtgTareas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.DtgTareas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DtgTareas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DtgTareas.ColumnHeadersHeight = 40;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DtgTareas.DefaultCellStyle = dataGridViewCellStyle3;
            this.DtgTareas.EnableHeadersVisualStyles = false;
            this.DtgTareas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DtgTareas.Location = new System.Drawing.Point(16, 70);
            this.DtgTareas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DtgTareas.MultiSelect = false;
            this.DtgTareas.Name = "DtgTareas";
            this.DtgTareas.ReadOnly = true;
            this.DtgTareas.RowHeadersVisible = false;
            this.DtgTareas.RowHeadersWidth = 51;
            this.DtgTareas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtgTareas.Size = new System.Drawing.Size(1227, 388);
            this.DtgTareas.TabIndex = 34;
            this.DtgTareas.Theme = Guna.UI2.WinForms.Enums.DataGridViewPresetThemes.Default;
            this.DtgTareas.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.DtgTareas.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.DtgTareas.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.DtgTareas.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.DtgTareas.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.DtgTareas.ThemeStyle.BackColor = System.Drawing.Color.White;
            this.DtgTareas.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DtgTareas.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.DtgTareas.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DtgTareas.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.DtgTareas.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.DtgTareas.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.DtgTareas.ThemeStyle.HeaderStyle.Height = 40;
            this.DtgTareas.ThemeStyle.ReadOnly = true;
            this.DtgTareas.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.DtgTareas.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.DtgTareas.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.DtgTareas.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.DtgTareas.ThemeStyle.RowsStyle.Height = 22;
            this.DtgTareas.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DtgTareas.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.DtgTareas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgTareas_CellContentClick);
            this.DtgTareas.Click += new System.EventHandler(this.DtgTareas_Click);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Animated = true;
            this.BtnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCancelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCancelar.BorderRadius = 15;
            this.BtnCancelar.BorderThickness = 1;
            this.BtnCancelar.CheckedState.Parent = this.BtnCancelar;
            this.BtnCancelar.CustomImages.Parent = this.BtnCancelar;
            this.BtnCancelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCancelar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.ForeColor = System.Drawing.Color.White;
            this.BtnCancelar.HoverState.Parent = this.BtnCancelar;
            this.BtnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCancelar.Image")));
            this.BtnCancelar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnCancelar.Location = new System.Drawing.Point(1000, 502);
            this.BtnCancelar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.ShadowDecoration.Parent = this.BtnCancelar;
            this.BtnCancelar.Size = new System.Drawing.Size(165, 49);
            this.BtnCancelar.TabIndex = 43;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseTransparentBackground = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // lblRetrasados
            // 
            this.lblRetrasados.AutoSize = true;
            this.lblRetrasados.BackColor = System.Drawing.Color.White;
            this.lblRetrasados.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRetrasados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblRetrasados.Location = new System.Drawing.Point(351, 209);
            this.lblRetrasados.Name = "lblRetrasados";
            this.lblRetrasados.Size = new System.Drawing.Size(21, 22);
            this.lblRetrasados.TabIndex = 50;
            this.lblRetrasados.Text = "0";
            // 
            // lblTerminados
            // 
            this.lblTerminados.AutoSize = true;
            this.lblTerminados.BackColor = System.Drawing.Color.White;
            this.lblTerminados.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTerminados.Location = new System.Drawing.Point(351, 149);
            this.lblTerminados.Name = "lblTerminados";
            this.lblTerminados.Size = new System.Drawing.Size(21, 22);
            this.lblTerminados.TabIndex = 49;
            this.lblTerminados.Text = "0";
            // 
            // lblProgreso
            // 
            this.lblProgreso.AutoSize = true;
            this.lblProgreso.BackColor = System.Drawing.Color.White;
            this.lblProgreso.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgreso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblProgreso.Location = new System.Drawing.Point(351, 85);
            this.lblProgreso.Name = "lblProgreso";
            this.lblProgreso.Size = new System.Drawing.Size(21, 22);
            this.lblProgreso.TabIndex = 47;
            this.lblProgreso.Text = "0";
            // 
            // PBretrasados
            // 
            this.PBretrasados.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBretrasados.FillColor = System.Drawing.Color.LightGray;
            this.PBretrasados.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBretrasados.Location = new System.Drawing.Point(27, 209);
            this.PBretrasados.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBretrasados.Name = "PBretrasados";
            this.PBretrasados.ProgressColor = System.Drawing.Color.Yellow;
            this.PBretrasados.ProgressColor2 = System.Drawing.Color.Maroon;
            this.PBretrasados.ShadowDecoration.Parent = this.PBretrasados;
            this.PBretrasados.Size = new System.Drawing.Size(319, 28);
            this.PBretrasados.TabIndex = 45;
            this.PBretrasados.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // PBterminados
            // 
            this.PBterminados.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBterminados.FillColor = System.Drawing.Color.LightGray;
            this.PBterminados.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBterminados.Location = new System.Drawing.Point(27, 148);
            this.PBterminados.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBterminados.Name = "PBterminados";
            this.PBterminados.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.PBterminados.ProgressColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.PBterminados.ShadowDecoration.Parent = this.PBterminados;
            this.PBterminados.Size = new System.Drawing.Size(319, 28);
            this.PBterminados.TabIndex = 43;
            this.PBterminados.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // PBprogreso
            // 
            this.PBprogreso.BackColor = System.Drawing.SystemColors.WindowText;
            this.PBprogreso.FillColor = System.Drawing.Color.LightGray;
            this.PBprogreso.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.PBprogreso.Location = new System.Drawing.Point(27, 82);
            this.PBprogreso.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBprogreso.Name = "PBprogreso";
            this.PBprogreso.ProgressColor = System.Drawing.Color.Teal;
            this.PBprogreso.ProgressColor2 = System.Drawing.Color.MidnightBlue;
            this.PBprogreso.ShadowDecoration.Parent = this.PBprogreso;
            this.PBprogreso.Size = new System.Drawing.Size(319, 28);
            this.PBprogreso.TabIndex = 35;
            this.PBprogreso.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(23, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 22);
            this.label2.TabIndex = 45;
            this.label2.Text = "Proyecto:";
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.BackColor = System.Drawing.Color.White;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(135, 25);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(67, 34);
            this.lblTitulo.TabIndex = 46;
            this.lblTitulo.Text = "N/A";
            // 
            // BtnDetalleTarea
            // 
            this.BtnDetalleTarea.Animated = true;
            this.BtnDetalleTarea.BackColor = System.Drawing.Color.Transparent;
            this.BtnDetalleTarea.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnDetalleTarea.BorderRadius = 15;
            this.BtnDetalleTarea.BorderThickness = 1;
            this.BtnDetalleTarea.CheckedState.Parent = this.BtnDetalleTarea;
            this.BtnDetalleTarea.CustomImages.Parent = this.BtnDetalleTarea;
            this.BtnDetalleTarea.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnDetalleTarea.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F);
            this.BtnDetalleTarea.ForeColor = System.Drawing.Color.White;
            this.BtnDetalleTarea.HoverState.Parent = this.BtnDetalleTarea;
            this.BtnDetalleTarea.Image = ((System.Drawing.Image)(resources.GetObject("BtnDetalleTarea.Image")));
            this.BtnDetalleTarea.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnDetalleTarea.Location = new System.Drawing.Point(615, 502);
            this.BtnDetalleTarea.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnDetalleTarea.Name = "BtnDetalleTarea";
            this.BtnDetalleTarea.ShadowDecoration.Parent = this.BtnDetalleTarea;
            this.BtnDetalleTarea.Size = new System.Drawing.Size(295, 49);
            this.BtnDetalleTarea.TabIndex = 47;
            this.BtnDetalleTarea.Text = "Ver detalles de tarea";
            this.BtnDetalleTarea.UseTransparentBackground = true;
            this.BtnDetalleTarea.Click += new System.EventHandler(this.BtnDetalleTarea_Click);
            // 
            // PanelEmpty
            // 
            this.PanelEmpty.Controls.Add(this.label6);
            this.PanelEmpty.Controls.Add(this.PbAviso);
            this.PanelEmpty.Location = new System.Drawing.Point(16, 70);
            this.PanelEmpty.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelEmpty.Name = "PanelEmpty";
            this.PanelEmpty.ShadowDecoration.Parent = this.PanelEmpty;
            this.PanelEmpty.Size = new System.Drawing.Size(1227, 388);
            this.PanelEmpty.TabIndex = 48;
            this.PanelEmpty.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(360, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(455, 48);
            this.label6.TabIndex = 30;
            this.label6.Text = "No hay tareas registradas :(";
            // 
            // PbAviso
            // 
            this.PbAviso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbAviso.Image = ((System.Drawing.Image)(resources.GetObject("PbAviso.Image")));
            this.PbAviso.Location = new System.Drawing.Point(516, 42);
            this.PbAviso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PbAviso.Name = "PbAviso";
            this.PbAviso.Size = new System.Drawing.Size(159, 137);
            this.PbAviso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbAviso.TabIndex = 25;
            this.PbAviso.TabStop = false;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(1135, 0);
            this.guna2ControlBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox2.TabIndex = 53;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(1197, 0);
            this.guna2ControlBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox1.TabIndex = 52;
            // 
            // guna2GroupBox1
            // 
            this.guna2GroupBox1.Controls.Add(this.lblRetrasados);
            this.guna2GroupBox1.Controls.Add(this.pictureBox3);
            this.guna2GroupBox1.Controls.Add(this.lblTerminados);
            this.guna2GroupBox1.Controls.Add(this.PBretrasados);
            this.guna2GroupBox1.Controls.Add(this.label9);
            this.guna2GroupBox1.Controls.Add(this.lblProgreso);
            this.guna2GroupBox1.Controls.Add(this.label10);
            this.guna2GroupBox1.Controls.Add(this.label12);
            this.guna2GroupBox1.Controls.Add(this.PBterminados);
            this.guna2GroupBox1.Controls.Add(this.PBprogreso);
            this.guna2GroupBox1.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold);
            this.guna2GroupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.guna2GroupBox1.Location = new System.Drawing.Point(28, 465);
            this.guna2GroupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2GroupBox1.Name = "guna2GroupBox1";
            this.guna2GroupBox1.ShadowDecoration.Parent = this.guna2GroupBox1;
            this.guna2GroupBox1.Size = new System.Drawing.Size(484, 256);
            this.guna2GroupBox1.TabIndex = 54;
            this.guna2GroupBox1.Text = "        Estadísticas de Tareas";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(9, 2);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(47, 43);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 42;
            this.pictureBox3.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(21, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 23);
            this.label9.TabIndex = 40;
            this.label9.Text = "En progreso";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(21, 183);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 23);
            this.label10.TabIndex = 46;
            this.label10.Text = "Retrasados";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(21, 118);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 23);
            this.label12.TabIndex = 44;
            this.label12.Text = "Terminados";
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this;
            // 
            // FrmListaTareas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1259, 736);
            this.Controls.Add(this.guna2GroupBox1);
            this.Controls.Add(this.guna2ControlBox2);
            this.Controls.Add(this.guna2ControlBox1);
            this.Controls.Add(this.PanelEmpty);
            this.Controls.Add(this.BtnDetalleTarea);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.DtgTareas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmListaTareas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmTareas";
            this.Load += new System.EventHandler(this.FrmListaTareas_Load);
            this.Enter += new System.EventHandler(this.FrmListaTareas_Enter);
            this.Leave += new System.EventHandler(this.FrmListaTareas_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.DtgTareas)).EndInit();
            this.PanelEmpty.ResumeLayout(false);
            this.PanelEmpty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).EndInit();
            this.guna2GroupBox1.ResumeLayout(false);
            this.guna2GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2DataGridView DtgTareas;
        private Guna.UI2.WinForms.Guna2Button BtnCancelar;
        private System.Windows.Forms.Label lblRetrasados;
        private System.Windows.Forms.Label lblTerminados;
        private System.Windows.Forms.Label lblProgreso;
        private Guna.UI2.WinForms.Guna2ProgressBar PBretrasados;
        private Guna.UI2.WinForms.Guna2ProgressBar PBterminados;
        private Guna.UI2.WinForms.Guna2ProgressBar PBprogreso;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTitulo;
        private Guna.UI2.WinForms.Guna2Button BtnDetalleTarea;
        private Guna.UI2.WinForms.Guna2GradientPanel PanelEmpty;
        private System.Windows.Forms.PictureBox PbAviso;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2GroupBox guna2GroupBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
    }
}