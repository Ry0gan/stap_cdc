﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reportes
{
    public partial class ReporteProyectos : UserControl
    {
        public int IdProyecto { get; set; }
        private string titulo;
        private string fecha_inicio;
        private string fecha_fin;
        private int porcentaje;
        public ReporteProyectos()
        {
            InitializeComponent();
        }

        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; LblNombre.Text = value; }
        }

        public string FechaInicio
        {
            get { return fecha_inicio; }
            set { fecha_inicio = value; LblInicio.Text = value.Substring(0,10); }
        }
        public string FechaFin
        {
            get { return fecha_fin; }
            set { fecha_fin = value; lblFin.Text = value.Substring(0, 10); }
        }
        public int Porcentaje
        {
            get { return porcentaje; }
            set
            {
                porcentaje = value; 
                Grafica.Value = value;
                switch (porcentaje)
                {
                    case 0:
                        lblStatus.BackColor = Color.Gray;
                        lblStatus.Text = "Pendiente";
                        break;
                    case 100:
                        lblStatus.BackColor = Color.Green;
                        lblStatus.Text = "Terminado";
                        break;
                    default:
                        lblStatus.BackColor = Color.Yellow;
                        lblStatus.Text = "En progreso";
                        break;
                }
            }
        }

        private void PanelContenido_MouseClick(object sender, MouseEventArgs e)
        {
            FrmListaTareas mForm = new FrmListaTareas();
            mForm.setIdProyecto(this.IdProyecto);
            mForm.ShowDialog();
        }

        private void Icon_MouseClick(object sender, MouseEventArgs e)
        {
            FrmListaTareas mForm = new FrmListaTareas();
            mForm.setIdProyecto(this.IdProyecto);
            mForm.ShowDialog();
        }

        private void PanelContenido_MouseHover(object sender, EventArgs e) {
            PanelContenido.FillColor = Color.Blue;
        }

        private void PanelContenido_Leave(object sender, EventArgs e) {
            
        }

        private void PanelContenido_MouseLeave(object sender, EventArgs e) {
            PanelContenido.FillColor = Color.FromArgb(28, 34, 43);
            PanelContenido.FillColor2 = Color.FromArgb(28, 34, 43);
            PanelContenido.FillColor3 = Color.FromArgb(28, 34, 43);
            PanelContenido.FillColor4 = Color.FromArgb(28, 34, 43);
        }
    }
}
