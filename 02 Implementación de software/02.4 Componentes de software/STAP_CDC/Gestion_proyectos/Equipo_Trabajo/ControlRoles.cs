﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;

namespace STAP_CDC.Gestion_proyectos.Equipo_Trabajo
{
    class ControlRoles
    {

        Conexion mConexion;
        public ControlRoles()
        {
            mConexion = new Conexion();
            mConexion.conectar();
        }

        public DataSet consultarRoles(string texto = "")
        {
            String SQL = "select identificador,nombre_rol from rol WHERE (identificador like \'%" + texto + "%\' or nombre_rol like \'%" + texto + "%\')";
            DataSet Listarol = mConexion.ejecutarConsulta(SQL);
            return Listarol;
        }
        public DataSet ConsultarIdPersonal(String Iniciales)
        {
            String SQL = "select idPersonal from personal where iniciales = \"" + Iniciales + "\"";
            DataSet ListaID = mConexion.ejecutarConsulta(SQL);
            return ListaID;
        }

        public DataSet ConsultarIdRol(String Identificador)
        {
            String SQL = "select idRol from rol where identificador = \"" + Identificador + "\"";
            DataSet ListaID = mConexion.ejecutarConsulta(SQL);
            return ListaID;
        }
        //Corregir idProyecto cuando se integre
        public DataSet consultarEquipo(string ID)
        {
            String SQL = "select distinct nombre_completo, iniciales, identificador from personal left join equipo_trabajo on personal.idPersonal = equipo_trabajo.idPersonal left join rol on rol.idRol = equipo_trabajo.idRol where idProyecto = \"" + ID + "\"";
            DataSet ListaEquipo = mConexion.ejecutarConsulta(SQL);
            return ListaEquipo;
        }


        public void ActualizarRol(String IDRol, String IDPersonal, string ID)
        {
            String SQL = "update equipo_trabajo set "
                + "idRol='?1'"
                + " where idPersonal = \"" + IDPersonal + "\" and idProyecto = \"" + ID + "\"";
            SQL = SQL.Replace("?1", IDRol);
            mConexion.ejecutarActualizacion(SQL);
        }


        public void EliminarRol(String IDPersonal, string idR,string ID)
        {
            String SQL = "update equipo_trabajo set "
                + "idRol= null"
                + " where idPersonal = \"" + IDPersonal + "\" and idRol = \"" + idR + "\" and idProyecto = \"" + ID + "\"";
            mConexion.ejecutarActualizacion(SQL);
        }


        public void EliminarUsuarioDeEquipo(String IDPersonal, string idR,string ID)
        {
            String SQL = "delete equipo_trabajo from equipo_trabajo  where idPersonal = \"" + IDPersonal + "\" and idRol = \"" + idR + "\" and idProyecto = \"" + ID + "\"";
            mConexion.ejecutarActualizacion(SQL);
        }
    }
}
