﻿namespace STAP_CDC.Gestion_proyectos.Equipo_Trabajo
{
    partial class FrmAsignarRol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAsignarRol));
            this.PanelTabla = new System.Windows.Forms.FlowLayoutPanel();
            this.DgvEquipo = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtFiltrar = new Guna.UI2.WinForms.Guna2TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DgvRoles = new System.Windows.Forms.DataGridView();
            this.BtnQuitar = new Guna.UI2.WinForms.Guna2Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BtnSalir = new Guna.UI2.WinForms.Guna2Button();
            this.Panel0 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PbAviso = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnNuevo0 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2ControlBox3 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox4 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.PanelTabla.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvEquipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.Panel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelTabla
            // 
            this.PanelTabla.Controls.Add(this.DgvEquipo);
            this.PanelTabla.Location = new System.Drawing.Point(488, 96);
            this.PanelTabla.Margin = new System.Windows.Forms.Padding(2);
            this.PanelTabla.Name = "PanelTabla";
            this.PanelTabla.Size = new System.Drawing.Size(298, 363);
            this.PanelTabla.TabIndex = 72;
            // 
            // DgvEquipo
            // 
            this.DgvEquipo.AllowUserToAddRows = false;
            this.DgvEquipo.AllowUserToDeleteRows = false;
            this.DgvEquipo.AllowUserToResizeColumns = false;
            this.DgvEquipo.AllowUserToResizeRows = false;
            this.DgvEquipo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvEquipo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DgvEquipo.BackgroundColor = System.Drawing.Color.White;
            this.DgvEquipo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DgvEquipo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvEquipo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvEquipo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvEquipo.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvEquipo.DefaultCellStyle = dataGridViewCellStyle2;
            this.DgvEquipo.EnableHeadersVisualStyles = false;
            this.DgvEquipo.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DgvEquipo.Location = new System.Drawing.Point(3, 3);
            this.DgvEquipo.MultiSelect = false;
            this.DgvEquipo.Name = "DgvEquipo";
            this.DgvEquipo.ReadOnly = true;
            this.DgvEquipo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DgvEquipo.RowHeadersVisible = false;
            this.DgvEquipo.RowHeadersWidth = 60;
            this.DgvEquipo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvEquipo.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.DgvEquipo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvEquipo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvEquipo.Size = new System.Drawing.Size(294, 347);
            this.DgvEquipo.TabIndex = 56;
            this.DgvEquipo.DragDrop += new System.Windows.Forms.DragEventHandler(this.DgvEquipo_DragDrop);
            this.DgvEquipo.DragEnter += new System.Windows.Forms.DragEventHandler(this.DgvEquipo_DragEnter);
            this.DgvEquipo.DragOver += new System.Windows.Forms.DragEventHandler(this.DgvEquipo_DragOver);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(423, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(207, 26);
            this.label1.TabIndex = 67;
            this.label1.Text = "Equipo de Trabajo";
            // 
            // TxtFiltrar
            // 
            this.TxtFiltrar.Animated = true;
            this.TxtFiltrar.BackColor = System.Drawing.Color.White;
            this.TxtFiltrar.BorderRadius = 10;
            this.TxtFiltrar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFiltrar.DefaultText = "";
            this.TxtFiltrar.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtFiltrar.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtFiltrar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.DisabledState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.FocusedState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFiltrar.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.HoverState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Location = new System.Drawing.Point(32, 53);
            this.TxtFiltrar.Margin = new System.Windows.Forms.Padding(4);
            this.TxtFiltrar.Name = "TxtFiltrar";
            this.TxtFiltrar.PasswordChar = '\0';
            this.TxtFiltrar.PlaceholderText = "Buscar...";
            this.TxtFiltrar.SelectedText = "";
            this.TxtFiltrar.ShadowDecoration.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Size = new System.Drawing.Size(233, 36);
            this.TxtFiltrar.TabIndex = 66;
            this.TxtFiltrar.TextChanged += new System.EventHandler(this.TxtFiltrar_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(33, 7);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 26);
            this.label5.TabIndex = 65;
            this.label5.Text = "Roles";
            // 
            // DgvRoles
            // 
            this.DgvRoles.AllowUserToAddRows = false;
            this.DgvRoles.AllowUserToDeleteRows = false;
            this.DgvRoles.AllowUserToResizeColumns = false;
            this.DgvRoles.AllowUserToResizeRows = false;
            this.DgvRoles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvRoles.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DgvRoles.BackgroundColor = System.Drawing.Color.White;
            this.DgvRoles.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DgvRoles.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvRoles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DgvRoles.ColumnHeadersHeight = 40;
            this.DgvRoles.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvRoles.DefaultCellStyle = dataGridViewCellStyle5;
            this.DgvRoles.EnableHeadersVisualStyles = false;
            this.DgvRoles.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DgvRoles.Location = new System.Drawing.Point(22, 96);
            this.DgvRoles.MultiSelect = false;
            this.DgvRoles.Name = "DgvRoles";
            this.DgvRoles.ReadOnly = true;
            this.DgvRoles.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DgvRoles.RowHeadersVisible = false;
            this.DgvRoles.RowHeadersWidth = 60;
            this.DgvRoles.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvRoles.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DgvRoles.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvRoles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvRoles.Size = new System.Drawing.Size(292, 363);
            this.DgvRoles.TabIndex = 64;
            this.DgvRoles.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DgvRoles_MouseDown);
            this.DgvRoles.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DgvRoles_MouseMove);
            // 
            // BtnQuitar
            // 
            this.BtnQuitar.Animated = true;
            this.BtnQuitar.BackColor = System.Drawing.Color.Transparent;
            this.BtnQuitar.BorderColor = System.Drawing.Color.Firebrick;
            this.BtnQuitar.BorderRadius = 15;
            this.BtnQuitar.BorderThickness = 1;
            this.BtnQuitar.CheckedState.Parent = this.BtnQuitar;
            this.BtnQuitar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnQuitar.CustomImages.Parent = this.BtnQuitar;
            this.BtnQuitar.FillColor = System.Drawing.Color.Firebrick;
            this.BtnQuitar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQuitar.ForeColor = System.Drawing.Color.White;
            this.BtnQuitar.HoverState.Parent = this.BtnQuitar;
            this.BtnQuitar.Image = global::STAP_CDC.Properties.Resources.minus1;
            this.BtnQuitar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnQuitar.Location = new System.Drawing.Point(579, 502);
            this.BtnQuitar.Name = "BtnQuitar";
            this.BtnQuitar.ShadowDecoration.Parent = this.BtnQuitar;
            this.BtnQuitar.Size = new System.Drawing.Size(94, 39);
            this.BtnQuitar.TabIndex = 71;
            this.BtnQuitar.Text = "Quitar";
            this.BtnQuitar.UseTransparentBackground = true;
            this.BtnQuitar.Click += new System.EventHandler(this.BtnQuitar_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::STAP_CDC.Properties.Resources.arrastrar;
            this.pictureBox2.Location = new System.Drawing.Point(336, 210);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(129, 121);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 70;
            this.pictureBox2.TabStop = false;
            // 
            // BtnSalir
            // 
            this.BtnSalir.Animated = true;
            this.BtnSalir.BackColor = System.Drawing.Color.Transparent;
            this.BtnSalir.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnSalir.BorderRadius = 15;
            this.BtnSalir.BorderThickness = 1;
            this.BtnSalir.CheckedState.Parent = this.BtnSalir;
            this.BtnSalir.CustomImages.Parent = this.BtnSalir;
            this.BtnSalir.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnSalir.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSalir.ForeColor = System.Drawing.Color.White;
            this.BtnSalir.HoverState.Parent = this.BtnSalir;
            this.BtnSalir.Image = global::STAP_CDC.Properties.Resources.cancel;
            this.BtnSalir.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnSalir.Location = new System.Drawing.Point(220, 502);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.ShadowDecoration.Parent = this.BtnSalir;
            this.BtnSalir.Size = new System.Drawing.Size(94, 39);
            this.BtnSalir.TabIndex = 68;
            this.BtnSalir.Text = "Salir";
            this.BtnSalir.UseTransparentBackground = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // Panel0
            // 
            this.Panel0.Controls.Add(this.guna2ControlBox2);
            this.Panel0.Controls.Add(this.guna2ControlBox1);
            this.Panel0.Controls.Add(this.label7);
            this.Panel0.Controls.Add(this.label8);
            this.Panel0.Controls.Add(this.PbAviso);
            this.Panel0.Controls.Add(this.pictureBox1);
            this.Panel0.Controls.Add(this.BtnNuevo0);
            this.Panel0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel0.Location = new System.Drawing.Point(0, 0);
            this.Panel0.Margin = new System.Windows.Forms.Padding(2);
            this.Panel0.Name = "Panel0";
            this.Panel0.ShadowDecoration.Parent = this.Panel0;
            this.Panel0.Size = new System.Drawing.Size(824, 576);
            this.Panel0.TabIndex = 73;
            this.Panel0.Leave += new System.EventHandler(this.Panel0_Leave);
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(732, 0);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox2.TabIndex = 57;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(779, 0);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox1.TabIndex = 56;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(246, 356);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(342, 23);
            this.label7.TabIndex = 39;
            this.label7.Text = "* Presiona el siguiente botón para crear uno!!";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 20F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(134, 217);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(554, 33);
            this.label8.TabIndex = 38;
            this.label8.Text = "No existe un equipo de trabajo en este proyecto :( ...";
            // 
            // PbAviso
            // 
            this.PbAviso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbAviso.Image = ((System.Drawing.Image)(resources.GetObject("PbAviso.Image")));
            this.PbAviso.Location = new System.Drawing.Point(336, 24);
            this.PbAviso.Name = "PbAviso";
            this.PbAviso.Size = new System.Drawing.Size(163, 157);
            this.PbAviso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbAviso.TabIndex = 37;
            this.PbAviso.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(375, 407);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(83, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // BtnNuevo0
            // 
            this.BtnNuevo0.BackColor = System.Drawing.Color.Transparent;
            this.BtnNuevo0.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo0.BorderRadius = 15;
            this.BtnNuevo0.BorderThickness = 1;
            this.BtnNuevo0.CheckedState.Parent = this.BtnNuevo0;
            this.BtnNuevo0.CustomImages.Parent = this.BtnNuevo0;
            this.BtnNuevo0.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo0.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNuevo0.ForeColor = System.Drawing.Color.White;
            this.BtnNuevo0.HoverState.Parent = this.BtnNuevo0;
            this.BtnNuevo0.Image = global::STAP_CDC.Properties.Resources.add;
            this.BtnNuevo0.ImageSize = new System.Drawing.Size(40, 40);
            this.BtnNuevo0.Location = new System.Drawing.Point(272, 489);
            this.BtnNuevo0.Name = "BtnNuevo0";
            this.BtnNuevo0.ShadowDecoration.Parent = this.BtnNuevo0;
            this.BtnNuevo0.Size = new System.Drawing.Size(286, 40);
            this.BtnNuevo0.TabIndex = 23;
            this.BtnNuevo0.Text = "CREAR EQUIPO DE TRABAJO";
            this.BtnNuevo0.UseTransparentBackground = true;
            this.BtnNuevo0.Click += new System.EventHandler(this.BtnNuevo0_Click);
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2ControlBox3
            // 
            this.guna2ControlBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox3.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox3.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox3.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox3.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox3.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox3.HoverState.Parent = this.guna2ControlBox3;
            this.guna2ControlBox3.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox3.Location = new System.Drawing.Point(732, 1);
            this.guna2ControlBox3.Name = "guna2ControlBox3";
            this.guna2ControlBox3.ShadowDecoration.Parent = this.guna2ControlBox3;
            this.guna2ControlBox3.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox3.TabIndex = 75;
            // 
            // guna2ControlBox4
            // 
            this.guna2ControlBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox4.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox4.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox4.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox4.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox4.HoverState.Parent = this.guna2ControlBox4;
            this.guna2ControlBox4.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox4.Location = new System.Drawing.Point(779, 1);
            this.guna2ControlBox4.Name = "guna2ControlBox4";
            this.guna2ControlBox4.ShadowDecoration.Parent = this.guna2ControlBox4;
            this.guna2ControlBox4.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox4.TabIndex = 74;
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this;
            // 
            // FrmAsignarRol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(824, 576);
            this.Controls.Add(this.guna2ControlBox3);
            this.Controls.Add(this.guna2ControlBox4);
            this.Controls.Add(this.Panel0);
            this.Controls.Add(this.PanelTabla);
            this.Controls.Add(this.BtnQuitar);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.BtnSalir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtFiltrar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DgvRoles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmAsignarRol";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asignar Roles";
            this.Load += new System.EventHandler(this.FrmAsignarRol_Load);
            this.Enter += new System.EventHandler(this.FrmAsignarRol_Enter);
            this.Leave += new System.EventHandler(this.FrmAsignarRol_Leave);
            this.PanelTabla.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvEquipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DgvRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.Panel0.ResumeLayout(false);
            this.Panel0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel PanelTabla;
        private System.Windows.Forms.DataGridView DgvEquipo;
        private Guna.UI2.WinForms.Guna2Button BtnQuitar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Guna.UI2.WinForms.Guna2Button BtnSalir;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox TxtFiltrar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView DgvRoles;
        private Guna.UI2.WinForms.Guna2GradientPanel Panel0;
        private Guna.UI2.WinForms.Guna2Button BtnNuevo0;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox PbAviso;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox3;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox4;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
    }
}