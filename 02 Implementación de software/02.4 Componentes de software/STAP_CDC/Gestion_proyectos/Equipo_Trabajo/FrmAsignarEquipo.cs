﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.Gestion_proyectos.Equipo_Trabajo
{
    public partial class FrmAsignarEquipo : Form
    {
        private Rectangle dragBoxFromMouseDown;

        private int rowIndexFrom;
        private string ID;
        private int rowIndexTo;
        ControlEquipo mControlEquipo;
        public FrmAsignarEquipo()
        {
            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            mControlEquipo = new ControlEquipo();
            Panel0.Visible = false;
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        private void FrmAsignarEquipo_Load(object sender, EventArgs e)
        {
            cargarPrincipal();
        }


        public void cargarPrincipal()
        {
            DataSet comprobar = mControlEquipo.consultarUsuarios();
            if (comprobar.Tables[0].Rows.Count > 0)
            {
                Panel0.Visible = false;
                cargarUsuarios();
                cargarEquipo();
                this.DgvEquipo.AllowDrop = true;
                DgvEquipo.ClearSelection();
                DgvUsuarios.ClearSelection();
            }
            else
            {
                Panel0.Visible = true;
            }
        }

        public void cargarUsuarios()
        { 
            DgvUsuarios.DataSource = mControlEquipo.consultarUsuarios().Tables[0];

            DgvUsuarios.Columns["nombre_completo"].HeaderText = "Nombre completo";

            DgvUsuarios.Columns["iniciales"].HeaderText = "Iniciales";

            DgvUsuarios.Columns["idPersonal"].Visible = false;
        }


        public void cargarEquipo()
        {
            DataSet consultarE = mControlEquipo.consultarEquipo(this.ID);
            if(consultarE.Tables[0].Rows.Count > 0)
            {

                DgvEquipo.DataSource = mControlEquipo.consultarEquipo(this.ID).Tables[0];

                DgvEquipo.Columns["nombre_completo"].HeaderText = "Nombre completo";

                DgvEquipo.Columns["iniciales"].HeaderText = "Iniciales";
                PanelTabla.Visible = true;
                PanelImagen.Visible = false;
            }
            else
            {
                PanelTabla.Visible = false;
                PanelImagen.Visible = true;
            }
            
        }

        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            DgvUsuarios.DataSource = mControlEquipo.consultarUsuarios(TxtFiltrar.Text).Tables[0];
        }

        private void DgvEquipo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DgvUsuarios_MouseDown(object sender, MouseEventArgs e)
        {
            rowIndexFrom = DgvUsuarios.HitTest(e.X, e.Y).RowIndex;

            if (rowIndexFrom != -1)
            {
                Size dragSize = SystemInformation.DragSize;

                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),

                    e.Y - (dragSize.Height / 2)),

                    dragSize);
            }
            else
            {
                dragBoxFromMouseDown = Rectangle.Empty;
            }
                
        }

        private void DgvUsuarios_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            { 
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {                   
                    DragDropEffects dropEffect = DgvUsuarios.DoDragDrop(DgvUsuarios.Rows[rowIndexFrom],DragDropEffects.Move);
                }
            }
        }

        private void DgvEquipo_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void DgvEquipo_DragDrop(object sender, DragEventArgs e)
        {
            
                Point clientPoint = DgvEquipo.PointToClient(new Point(e.X, e.Y));

                 rowIndexTo = DgvEquipo.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
                if (e.Effect == DragDropEffects.Move)

                {
                foreach (DataGridViewRow rowToMove in DgvUsuarios.SelectedRows)

                {
                    DataTable dt = this.DgvEquipo.DataSource as DataTable;


                    DataRow dr = dt.NewRow();

                    dr[0] = rowToMove.Cells[1].Value.ToString();

                    dr[1] = rowToMove.Cells[2].Value.ToString();

                    if (DgvEquipo.RowCount > 0)
                    {
                        bool existe = false;
                        for (int i = 0; i < DgvEquipo.RowCount; i++)
                        {
                            if (DgvEquipo.Rows[i].Cells[1].Value.ToString() == dr[1].ToString())
                            {
                                MessageBox.Show(this, "El usuario ya se encuentra en el equipo de trabajo", "WARNING!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                existe = true;
                                break;
                            }
                        }
                        if (existe == false)
                        {
                            if (rowIndexTo == -1)
                            {
                                MessageBox.Show(this, "Arrastre el usuario dentro de la tabla", "WARNING!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {

                                string idP = "";
                                DataSet ID = mControlEquipo.ConsultarIdPersonal(dr[1].ToString());
                                foreach (System.Data.DataRow id in ID.Tables[0].Rows)
                                {
                                    idP = id[0].ToString();
                                }
                                mControlEquipo.agregarEquipo(idP,this.ID);
                                dt.Rows.InsertAt(dr, rowIndexTo);
                            }

                        }
                    }

                }
            } 

        }

        private void BtnQuitar_Click(object sender, EventArgs e)
        {

            if (DgvEquipo.SelectedRows.Count == 0)
            {
                MessageBox.Show(this, "Seleccione un renglon de la tabla de equipo de trabajo", "WARNING!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int selected = DgvEquipo.SelectedRows[0].Index;
                string IDP = "";
                DataSet idPersonal = mControlEquipo.ConsultarIdPersonal(DgvEquipo.Rows[selected].Cells[1].Value.ToString());
                foreach (System.Data.DataRow row in idPersonal.Tables[0].Rows)
                {
                    IDP = row[0].ToString();
                }
                if (MessageBox.Show("¿Desea eliminar el usuario \"" + "" + DgvEquipo.Rows[selected].Cells[0].Value.ToString() + "" + "\"  del equipo ?\n Se perderán los roles y las tareas que tiene asignado el usuario", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    mControlEquipo.EliminarUsuarioDeEquipo(IDP,this.ID);
                    mControlEquipo.EliminarTareas_personal(IDP,this.ID);
                    MessageBox.Show(this, "Usuario Eliminado Correctamente Del Grupo", "Exito!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.cargarEquipo();
                }
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_DragDrop(object sender, DragEventArgs e)
        {
           
        }

        public void obtenerID(string ID)
        {
            this.ID = ID;
        }

        private void PanelImagen_DragDrop(object sender, DragEventArgs e)
        {
            foreach (DataGridViewRow rowToMove in DgvUsuarios.SelectedRows)

            {
                string idP = "";
                DataSet ID = mControlEquipo.ConsultarIdPersonal(rowToMove.Cells[2].Value.ToString());
                foreach (System.Data.DataRow id in ID.Tables[0].Rows)
                {
                    idP = id[0].ToString();
                }
                mControlEquipo.agregarEquipo(idP,this.ID);
                MessageBox.Show("Usuario \"" + "" + rowToMove.Cells[1].Value.ToString() + "" + "\" registrado al equipo de trabajo");
                PanelImagen.Visible = false;
                PanelTabla.Visible = true;
                this.cargarEquipo();
            }
        }

        private void pictureBox1_DragOver(object sender, DragEventArgs e)
        {
            
        }

        private void PanelImagen_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void BtnNuevo0_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Panel0_Leave(object sender, EventArgs e) {
          
        }

        private void Panel0_Enter(object sender, EventArgs e) {

        }

        private void FrmAsignarEquipo_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmAsignarEquipo_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
