﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;

namespace STAP_CDC.Gestion_proyectos.Equipo_Trabajo
{
    class ControlEquipo
    {
        Conexion mConexion;
        public ControlEquipo()
        {
            mConexion = new Conexion();
            mConexion.conectar();
        }

        public DataSet consultarUsuarios(string texto = "")
        {
            String SQL = "select idpersonal,nombre_completo,iniciales from Personal WHERE (nombre_completo like \'%" + texto + "%\' or iniciales like \'%" + texto + "%\') and visibilidad = 1";
            DataSet ListaUsuarios = mConexion.ejecutarConsulta(SQL);
            return ListaUsuarios;
        }
        public DataSet consultarEquipo(string ID)
        {
            String SQL = "select distinct nombre_completo, iniciales from personal inner join equipo_trabajo on personal.idPersonal = equipo_trabajo.idPersonal where idProyecto = \"" + ID + "\"";
            DataSet ListaUsuarios = mConexion.ejecutarConsulta(SQL);
            return ListaUsuarios;
        }

        public DataSet consultarEquipoIniciales(string ID)
        {
            String SQL = "select distinct iniciales from personal inner join equipo_trabajo on personal.idPersonal = equipo_trabajo.idPersonal where idProyecto = \"" + ID + "\"";
            DataSet ListaUsuarios = mConexion.ejecutarConsulta(SQL);
            return ListaUsuarios;
        }


        public void EliminarUsuarioDeEquipo(String ID,String IDP)
        {
            String SQL = "delete equipo_trabajo from equipo_trabajo where idPersonal =  \"" + ID + "\" AND idProyecto = \"" + IDP + "\"  ";
            mConexion.ejecutarActualizacion(SQL);
        }


        public void EliminarTareas_personal(String IDpersonal,string ID)
        {
            String SQL = "delete tarea_personal from tarea_personal inner join tarea on tarea_personal.idTarea = tarea.idTarea where idPersonal = \"" + IDpersonal + "\" and idProyecto = \"" + ID + "\"";
            mConexion.ejecutarActualizacion(SQL);
        }

        public DataSet ConsultarIdPersonal(String Iniciales)
        {
            String SQL = "select idPersonal from personal where iniciales = \"" + Iniciales + "\"";
            DataSet ListaID = mConexion.ejecutarConsulta(SQL);
            return ListaID;
        }

        public void agregarEquipo(string idPersonal,string ID)
        {
            String SQL = "insert into equipo_trabajo values(null,'?1','?2',null)";
            SQL = SQL.Replace("?1", ID);
            SQL = SQL.Replace("?2", idPersonal);
            mConexion.ejecutarActualizacion(SQL);
        }


        public void agregarEquipo2(string idPersonal,string idRol,string ID)
        {
            String SQL = "insert into equipo_trabajo values(null,'?1','?2','?3')";
            SQL = SQL.Replace("?1", ID);
            SQL = SQL.Replace("?2", idPersonal);
            SQL = SQL.Replace("?3", idRol);
            mConexion.ejecutarActualizacion(SQL);
        }
    }
}
