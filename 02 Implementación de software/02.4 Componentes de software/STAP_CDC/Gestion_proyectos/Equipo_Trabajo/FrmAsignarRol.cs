﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.Gestion_proyectos.Equipo_Trabajo
{
    public partial class FrmAsignarRol : Form
    {
        private Rectangle dragBoxFromMouseDown;
        private object valueFromMouseDown;
        private int rowIndexFrom;
        private int rowIndexTo;
        private string columnDragName;
        private string ID;
        ControlRoles mControlRoles;
        public FrmAsignarRol()
        {
            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            mControlRoles = new ControlRoles();
            this.DgvEquipo.AllowDrop = true;
            Panel0.Visible = false;
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        private void FrmAsignarRol_Load(object sender, EventArgs e)
        {
            cargarPrincipal();
        }

        public void cargarPrincipal()
        {
            DataSet consultarE = mControlRoles.consultarEquipo(this.ID);
            if(consultarE.Tables[0].Rows.Count > 0)
            {
                Panel0.Visible = false;
                cargarRoles();
                cargarEquipo();
                DgvEquipo.ClearSelection();
            }
            else
            {
                Panel0.Visible = true;
            }
           
        }

        public void cargarRoles()
        {
            DgvRoles.DataSource = mControlRoles.consultarRoles().Tables[0];

            DgvRoles.Columns["identificador"].HeaderText = "Identificador";

            DgvRoles.Columns["nombre_rol"].HeaderText = "Nombre";
        }

        public void cargarEquipo()
        {
            DataSet consultarE = mControlRoles.consultarEquipo(this.ID);
            DgvEquipo.DataSource = mControlRoles.consultarEquipo(this.ID).Tables[0];
            DgvEquipo.Columns["nombre_completo"].HeaderText = "Nombre completo";
            DgvEquipo.Columns["iniciales"].HeaderText = "Iniciales";
            DgvEquipo.Columns["Identificador"].HeaderText = "Identificador";
            PanelTabla.Visible = true;
        }

        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            DgvRoles.DataSource = mControlRoles.consultarRoles(TxtFiltrar.Text).Tables[0];
        }

        private void DgvRoles_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView.HitTestInfo info = DgvRoles.HitTest(e.X, e.Y);
            Object value = null;
            if (info.RowIndex != -1)
            {
               value = DgvRoles.Rows[info.RowIndex].Cells[info.ColumnIndex].Value;
            }
            
            if (info.RowIndex != -1 && info.ColumnIndex != -1)
            {
                if(info.ColumnIndex == 1)
                {
                    value = DgvRoles.Rows[info.RowIndex].Cells[info.ColumnIndex-1].Value;
                }
                
                if (value != null)
                {
                    this.DoDragDrop(value, DragDropEffects.Copy);
                }
            }
            else
            {
                MessageBox.Show(this, "Seleccione un renglon para arrastrar", "WARNING!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DgvRoles_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    DragDropEffects dropEffect = DgvRoles.DoDragDrop(DgvRoles
                        .Rows[rowIndexFrom], DragDropEffects.Copy);
                }
            }
        }

        private void DgvEquipo_DragOver(object sender, DragEventArgs e)
        {
           // e.Effect = DragDropEffects.Move;
        }

        public void ObtenerID(string ID)
        {
            this.ID = ID;
        }
        private void DgvEquipo_DragDrop(object sender, DragEventArgs e)
        {
            Point p = DgvEquipo.PointToClient(new Point(e.X, e.Y));
            DataGridView.HitTestInfo info = DgvEquipo.HitTest(p.X, p.Y);
            Object value = (Object)e.Data.GetData(typeof(string));
            
            if (info.RowIndex != -1 && info.ColumnIndex != -1)
            {

                string idP = "";
                string idR = "";
                DataSet idPersonal = mControlRoles.ConsultarIdPersonal(DgvEquipo.Rows[info.RowIndex].Cells[1].Value.ToString());
                foreach (System.Data.DataRow id in idPersonal.Tables[0].Rows)
                {
                    idP = id[0].ToString();
                }
                string dato = DgvEquipo.Rows[info.RowIndex].Cells[2].Value.ToString();
                string nuevo = "";
                if(value.ToString() != dato)
                {
                    if (dato.Length > 0)
                    {

                        DataTable dt = this.DgvEquipo.DataSource as DataTable;


                        DataRow dr = dt.NewRow();

                        dr[0] = DgvEquipo.Rows[info.RowIndex].Cells[0].Value;

                        dr[1] = DgvEquipo.Rows[info.RowIndex].Cells[1].Value;

                        dr[2] = value;

                        DataSet idRol = mControlRoles.ConsultarIdRol(value.ToString());
                        foreach (System.Data.DataRow Identi in idRol.Tables[0].Rows)
                        {
                            idR = Identi[0].ToString();
                        }
                        ControlEquipo mControlE = new ControlEquipo();
                        mControlE.agregarEquipo2(idP, idR,this.ID);
                        dt.Rows.InsertAt(dr, info.RowIndex + 1);

                    }
                    else
                    {
                        DataSet idRol = mControlRoles.ConsultarIdRol(value.ToString());
                        foreach (System.Data.DataRow Identi in idRol.Tables[0].Rows)
                        {
                            idR = Identi[0].ToString();
                        }
                        if (info.ColumnIndex == 0)
                        {
                            DgvEquipo.Rows[info.RowIndex].Cells[info.ColumnIndex + 2].Value = value;
                        }
                        else if (info.ColumnIndex == 1)
                        {
                            DgvEquipo.Rows[info.RowIndex].Cells[info.ColumnIndex + 1].Value = value;
                        }
                        else
                        {
                            DgvEquipo.Rows[info.RowIndex].Cells[info.ColumnIndex].Value = value;
                        }

                        mControlRoles.ActualizarRol(idR, idP,this.ID);
                    }
                }
                else
                {
                    MessageBox.Show(this, "El usuario ya tiene este rol", "WARNING!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                

            }
            else
            {
                MessageBox.Show(this, "Arrastre hasta el usuario que le quiera asignar el rol", "WARNING!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
         }

        private void DgvEquipo_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void BtnQuitar_Click(object sender, EventArgs e)
        {
            if (DgvEquipo.SelectedRows.Count == 0)
            {
                MessageBox.Show(this, "Seleccione un renglon de la tabla de equipo de trabajo", "WARNING!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int contador = 0;
                string idR = "";
                int selected = DgvEquipo.SelectedRows[0].Index;
                string IDP = "";
                DataSet idPersonal = mControlRoles.ConsultarIdPersonal(DgvEquipo.Rows[selected].Cells[1].Value.ToString());
                foreach (System.Data.DataRow row in idPersonal.Tables[0].Rows)
                {
                    IDP = row[0].ToString();
                }

                DataSet idRol = mControlRoles.ConsultarIdRol(DgvEquipo.Rows[selected].Cells[2].Value.ToString());
                foreach (System.Data.DataRow Identi in idRol.Tables[0].Rows)
                {
                    idR = Identi[0].ToString();
                }
                foreach (DataGridViewRow row in DgvEquipo.Rows)
                {
                    
                    if(row.Cells["Iniciales"].Value.ToString() == DgvEquipo.Rows[selected].Cells[1].Value.ToString())
                    {
                        contador++;
                    }
                }
                if(contador > 1)
                {
                    mControlRoles.EliminarUsuarioDeEquipo(IDP, idR,this.ID);
                }
                else
                {
                    mControlRoles.EliminarRol(IDP, idR,this.ID);
                }
                this.cargarEquipo();
                DgvEquipo.ClearSelection();
            }
        }

        private void BtnNuevo0_Click(object sender, EventArgs e)
        {
            FrmAsignarEquipo frmAsignarEquipo = new FrmAsignarEquipo();
            frmAsignarEquipo.obtenerID(this.ID);
            frmAsignarEquipo.ShowDialog();
            this.cargarPrincipal();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Panel0_Leave(object sender, EventArgs e) {

        }

        private void FrmAsignarRol_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmAsignarRol_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
