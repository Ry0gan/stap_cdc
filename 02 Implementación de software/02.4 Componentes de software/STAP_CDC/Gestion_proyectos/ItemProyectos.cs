﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using STAP_CDC.Seguimiento_tareas;
using STAP_CDC.CRUD.CRUD_tareas;
using STAP_CDC.Gestion_proyectos.Equipo_Trabajo;

namespace Gestion_proyectos
{
    public partial class ItemProyectos : UserControl
    {
        private string id;
        private string titulo;
        private string detalles;
        private string Fecha_inicio;
        private string Fecha_fin;
        public static string numero = " ";
        //public FrmProyectos padre;
        public ItemProyectos()
        {
            InitializeComponent();
            if (STAP_CDC.Acceso.Global.NivelUsuario == 0)
            {
                MenuGestionar.Visible = false;
            }
        }

        public string ID
        {
            get { return id; }
            set { id = value; LblID.Text = value; }
        }


        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; LblNombre.Text = value; }
        }

        public string Detalles
        {
            get { return detalles; }
            set { detalles = value; LblDetalles.Text = value; }
        }

        public string FechaFin
        {
            get { return Fecha_fin; }
            set { Fecha_fin = value; LblFechaFin.Text = value; }
        }

        public string FechaInicio
        {
            get { return Fecha_inicio; }
            set { Fecha_inicio = value; LblFechaInicio.Text = value; }
        }

        

        private void guna2CustomGradientPanel5_MouseHover(object sender, EventArgs e)
        {
            PbGif.Image = global::STAP_CDC.Properties.Resources.ESTE_ES_EL_GIF;
            PbGif.SizeMode = PictureBoxSizeMode.StretchImage;
            guna2CustomGradientPanel5.FillColor = Color.Blue;
        }

        private void guna2CustomGradientPanel5_MouseLeave(object sender, EventArgs e)
        {
            PbGif.Image = global::STAP_CDC.Properties.Resources.Stap2;
            guna2CustomGradientPanel5.FillColor = Color.FromArgb(28, 34, 43);
            guna2CustomGradientPanel5.FillColor2 = Color.FromArgb(28, 34, 43);
            guna2CustomGradientPanel5.FillColor3 = Color.FromArgb(28, 34, 43);
            guna2CustomGradientPanel5.FillColor4 = Color.FromArgb(28, 34, 43);
        }




        private void guna2CustomGradientPanel5_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    {
                        CmsOpciones.Show(this, new Point(e.X, e.Y));
                    }
                    break;
            }
        }

       

        private void LblDetalles_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(LblDetalles, Detalles);
        }

        private void MenuSeguimiento_Click(object sender, EventArgs e)
        {
            String idPersonal = STAP_CDC.Acceso.Global.idPersonal.ToString();
            FrmAvance mFrmAvance = new FrmAvance(idPersonal, id);
            mFrmAvance.ShowDialog();

        }

        private void gestionarTareasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTareas mfrmTareas = new FrmTareas();
            mfrmTareas.obtenerID(this.ID);
            mfrmTareas.ShowDialog();
        }

        private void asignarRolesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAsignarRol frmAsignarRol = new FrmAsignarRol();
            frmAsignarRol.ObtenerID(this.ID);
            frmAsignarRol.ShowDialog();
        }

        private void asignarEquipoDeTrabajoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAsignarEquipo frmAsignarEquipo = new FrmAsignarEquipo();
            frmAsignarEquipo.obtenerID(this.ID);
            frmAsignarEquipo.ShowDialog();
        }
    }
}
