﻿namespace Gestion_proyectos
{
    partial class ItemProyectos
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemProyectos));
            this.guna2CustomGradientPanel5 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.PbGif = new System.Windows.Forms.PictureBox();
            this.LblDetalles = new System.Windows.Forms.Label();
            this.LblFechaInicio = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LblID = new System.Windows.Forms.Label();
            this.LblFechaFin = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LblNombre = new System.Windows.Forms.Label();
            this.CmsOpciones = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuSeguimiento = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuGestionar = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarTareasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarEquipoDeTrabajoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignarRolesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.guna2CustomGradientPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbGif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.CmsOpciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2CustomGradientPanel5
            // 
            this.guna2CustomGradientPanel5.BorderRadius = 20;
            this.guna2CustomGradientPanel5.Controls.Add(this.PbGif);
            this.guna2CustomGradientPanel5.Controls.Add(this.LblDetalles);
            this.guna2CustomGradientPanel5.Controls.Add(this.LblFechaInicio);
            this.guna2CustomGradientPanel5.Controls.Add(this.label4);
            this.guna2CustomGradientPanel5.Controls.Add(this.label1);
            this.guna2CustomGradientPanel5.Controls.Add(this.label3);
            this.guna2CustomGradientPanel5.Controls.Add(this.label2);
            this.guna2CustomGradientPanel5.Controls.Add(this.LblID);
            this.guna2CustomGradientPanel5.Controls.Add(this.LblFechaFin);
            this.guna2CustomGradientPanel5.Controls.Add(this.pictureBox1);
            this.guna2CustomGradientPanel5.Controls.Add(this.LblNombre);
            this.guna2CustomGradientPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2CustomGradientPanel5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.guna2CustomGradientPanel5.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.guna2CustomGradientPanel5.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.guna2CustomGradientPanel5.FillColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.guna2CustomGradientPanel5.Location = new System.Drawing.Point(0, 0);
            this.guna2CustomGradientPanel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2CustomGradientPanel5.Name = "guna2CustomGradientPanel5";
            this.guna2CustomGradientPanel5.ShadowDecoration.Parent = this.guna2CustomGradientPanel5;
            this.guna2CustomGradientPanel5.Size = new System.Drawing.Size(573, 236);
            this.guna2CustomGradientPanel5.TabIndex = 8;
            this.guna2CustomGradientPanel5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.guna2CustomGradientPanel5_MouseClick);
            this.guna2CustomGradientPanel5.MouseLeave += new System.EventHandler(this.guna2CustomGradientPanel5_MouseLeave);
            this.guna2CustomGradientPanel5.MouseHover += new System.EventHandler(this.guna2CustomGradientPanel5_MouseHover);
            // 
            // PbGif
            // 
            this.PbGif.Image = global::STAP_CDC.Properties.Resources.Stap2;
            this.PbGif.Location = new System.Drawing.Point(364, -4);
            this.PbGif.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PbGif.Name = "PbGif";
            this.PbGif.Size = new System.Drawing.Size(221, 82);
            this.PbGif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbGif.TabIndex = 38;
            this.PbGif.TabStop = false;
            // 
            // LblDetalles
            // 
            this.LblDetalles.AutoSize = true;
            this.LblDetalles.BackColor = System.Drawing.Color.Transparent;
            this.LblDetalles.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDetalles.ForeColor = System.Drawing.Color.White;
            this.LblDetalles.Location = new System.Drawing.Point(101, 128);
            this.LblDetalles.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblDetalles.Name = "LblDetalles";
            this.LblDetalles.Size = new System.Drawing.Size(89, 26);
            this.LblDetalles.TabIndex = 37;
            this.LblDetalles.Text = "Detalles";
            // 
            // LblFechaInicio
            // 
            this.LblFechaInicio.AutoSize = true;
            this.LblFechaInicio.BackColor = System.Drawing.Color.Transparent;
            this.LblFechaInicio.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFechaInicio.ForeColor = System.Drawing.Color.White;
            this.LblFechaInicio.Location = new System.Drawing.Point(101, 166);
            this.LblFechaInicio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblFechaInicio.Name = "LblFechaInicio";
            this.LblFechaInicio.Size = new System.Drawing.Size(124, 26);
            this.LblFechaInicio.TabIndex = 36;
            this.LblFechaInicio.Text = "Fecha Inicio";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(16, 166);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 25);
            this.label4.TabIndex = 35;
            this.label4.Text = "Inicia el:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(16, 128);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 25);
            this.label1.TabIndex = 34;
            this.label1.Text = "Detalles:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(16, 206);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 25);
            this.label3.TabIndex = 33;
            this.label3.Text = "Finaliza el:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(16, 90);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 25);
            this.label2.TabIndex = 32;
            this.label2.Text = "Título:";
            // 
            // LblID
            // 
            this.LblID.AutoSize = true;
            this.LblID.BackColor = System.Drawing.Color.Transparent;
            this.LblID.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblID.ForeColor = System.Drawing.Color.White;
            this.LblID.Location = new System.Drawing.Point(-3, 103);
            this.LblID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblID.Name = "LblID";
            this.LblID.Size = new System.Drawing.Size(30, 23);
            this.LblID.TabIndex = 31;
            this.LblID.Text = "ID";
            this.LblID.Visible = false;
            // 
            // LblFechaFin
            // 
            this.LblFechaFin.AutoSize = true;
            this.LblFechaFin.BackColor = System.Drawing.Color.Transparent;
            this.LblFechaFin.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFechaFin.ForeColor = System.Drawing.Color.White;
            this.LblFechaFin.Location = new System.Drawing.Point(119, 206);
            this.LblFechaFin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblFechaFin.Name = "LblFechaFin";
            this.LblFechaFin.Size = new System.Drawing.Size(98, 26);
            this.LblFechaFin.TabIndex = 30;
            this.LblFechaFin.Text = "Fecha fin";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(15, 20);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 47);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // LblNombre
            // 
            this.LblNombre.AutoSize = true;
            this.LblNombre.BackColor = System.Drawing.Color.Transparent;
            this.LblNombre.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNombre.ForeColor = System.Drawing.Color.White;
            this.LblNombre.Location = new System.Drawing.Point(81, 90);
            this.LblNombre.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblNombre.Name = "LblNombre";
            this.LblNombre.Size = new System.Drawing.Size(179, 26);
            this.LblNombre.TabIndex = 28;
            this.LblNombre.Text = "Nombre proyecto";
            // 
            // CmsOpciones
            // 
            this.CmsOpciones.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.CmsOpciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuSeguimiento,
            this.MenuGestionar});
            this.CmsOpciones.Name = "CmsOpciones";
            this.CmsOpciones.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.CmsOpciones.Size = new System.Drawing.Size(245, 64);
            // 
            // MenuSeguimiento
            // 
            this.MenuSeguimiento.BackColor = System.Drawing.Color.DarkViolet;
            this.MenuSeguimiento.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuSeguimiento.ForeColor = System.Drawing.Color.White;
            this.MenuSeguimiento.Image = global::STAP_CDC.Properties.Resources.hora;
            this.MenuSeguimiento.Name = "MenuSeguimiento";
            this.MenuSeguimiento.Size = new System.Drawing.Size(244, 30);
            this.MenuSeguimiento.Text = "Seguimiento";
            this.MenuSeguimiento.Click += new System.EventHandler(this.MenuSeguimiento_Click);
            // 
            // MenuGestionar
            // 
            this.MenuGestionar.BackColor = System.Drawing.Color.Firebrick;
            this.MenuGestionar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionarTareasToolStripMenuItem,
            this.asignarEquipoDeTrabajoToolStripMenuItem,
            this.asignarRolesToolStripMenuItem});
            this.MenuGestionar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuGestionar.ForeColor = System.Drawing.Color.White;
            this.MenuGestionar.Image = global::STAP_CDC.Properties.Resources.engranaje;
            this.MenuGestionar.Name = "MenuGestionar";
            this.MenuGestionar.ShowShortcutKeys = false;
            this.MenuGestionar.Size = new System.Drawing.Size(244, 30);
            this.MenuGestionar.Text = "Gestionar proyecto";
            // 
            // gestionarTareasToolStripMenuItem
            // 
            this.gestionarTareasToolStripMenuItem.Name = "gestionarTareasToolStripMenuItem";
            this.gestionarTareasToolStripMenuItem.Size = new System.Drawing.Size(326, 30);
            this.gestionarTareasToolStripMenuItem.Text = "Gestionar tareas";
            this.gestionarTareasToolStripMenuItem.Click += new System.EventHandler(this.gestionarTareasToolStripMenuItem_Click);
            // 
            // asignarEquipoDeTrabajoToolStripMenuItem
            // 
            this.asignarEquipoDeTrabajoToolStripMenuItem.Name = "asignarEquipoDeTrabajoToolStripMenuItem";
            this.asignarEquipoDeTrabajoToolStripMenuItem.Size = new System.Drawing.Size(326, 30);
            this.asignarEquipoDeTrabajoToolStripMenuItem.Text = "Asignar equipo de trabajo";
            this.asignarEquipoDeTrabajoToolStripMenuItem.Click += new System.EventHandler(this.asignarEquipoDeTrabajoToolStripMenuItem_Click);
            // 
            // asignarRolesToolStripMenuItem
            // 
            this.asignarRolesToolStripMenuItem.Name = "asignarRolesToolStripMenuItem";
            this.asignarRolesToolStripMenuItem.Size = new System.Drawing.Size(326, 30);
            this.asignarRolesToolStripMenuItem.Text = "Asignar roles";
            this.asignarRolesToolStripMenuItem.Click += new System.EventHandler(this.asignarRolesToolStripMenuItem_Click);
            // 
            // ItemProyectos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.guna2CustomGradientPanel5);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ItemProyectos";
            this.Size = new System.Drawing.Size(573, 236);
            this.guna2CustomGradientPanel5.ResumeLayout(false);
            this.guna2CustomGradientPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbGif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.CmsOpciones.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel5;
        private System.Windows.Forms.ContextMenuStrip CmsOpciones;
        private System.Windows.Forms.ToolStripMenuItem MenuSeguimiento;
        private System.Windows.Forms.ToolStripMenuItem MenuGestionar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem gestionarTareasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarEquipoDeTrabajoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignarRolesToolStripMenuItem;
        private System.Windows.Forms.Label LblDetalles;
        private System.Windows.Forms.Label LblFechaInicio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label LblID;
        private System.Windows.Forms.Label LblFechaFin;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LblNombre;
        private System.Windows.Forms.PictureBox PbGif;
    }
}
