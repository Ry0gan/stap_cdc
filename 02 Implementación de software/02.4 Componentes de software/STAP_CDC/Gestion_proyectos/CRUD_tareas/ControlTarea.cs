﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace STAP_CDC.CRUD.CRUD_tareas
{
    class ControlTarea
    {

        Conexion mConexion;
        public ControlTarea()
        {
            mConexion = new Conexion();
            mConexion.conectar();
        }

        public void agregarTarea(Tarea mTarea, string idProyecto)
        {
            String SQL = "insert into tarea (idProyecto, descripcion, inicio_plan, fin_plan) values ('?1','?2','?3','?4')";
            SQL = SQL.Replace("?1", idProyecto);
            SQL = SQL.Replace("?2", mTarea.getDescripcion());
            SQL = SQL.Replace("?3", mTarea.getInicio_Plan());
            SQL = SQL.Replace("?4", mTarea.getFin_Plan());
            //Las fechas reales serán manipuladas desde el seguimiento de tareas
            //SQL = SQL.Replace("?5", mTarea.getInicio_Real());
            //SQL = SQL.Replace("?6", mTarea.getFin_Real());
            //SQL = SQL.Replace("?7", "null");
            mConexion.ejecutarActualizacion(SQL);
        }

        public void agregarResponsables(string idTarea, string idPersonal)
        {
            String SQL = "insert into tarea_personal values('?1','?2')";
            SQL = SQL.Replace("?1", idTarea);
            SQL = SQL.Replace("?2", idPersonal);
            mConexion.ejecutarActualizacion(SQL);
        }


        public DataSet consultarUsuarios(string ID)
        {
            String SQL = "select distinct nombre_completo,iniciales from Personal inner join equipo_trabajo on personal.idPersonal = equipo_trabajo.idPersonal WHERE idProyecto =   \"" + ID + "\"  and visibilidad = 1";
            DataSet ListaUsuarios = mConexion.ejecutarConsulta(SQL);
            return ListaUsuarios;
        }

        public DataSet ConsultarIdTarea()
        {
            String SQL = "select max(idTarea) from tarea";
            DataSet ListaID = mConexion.ejecutarConsulta(SQL);
            return ListaID;
        }

        public void ModificarTarea(Tarea mTarea, String ID)
        {
            String SQL = "update tarea set "
                + "descripcion='?1',"
                + "inicio_plan='?2',"
                + "fin_plan='?3'"
                + " where idTarea = \"" + ID + "\"";
            SQL = SQL.Replace("?1", mTarea.getDescripcion());
            SQL = SQL.Replace("?2", mTarea.getInicio_Plan());
            SQL = SQL.Replace("?3", mTarea.getFin_Plan());
            mConexion.ejecutarActualizacion(SQL);
        }

        public void EliminarTarea_personal(String ID)
        {
            String SQL = "delete tarea_personal from tarea_personal where idTarea =  \"" + ID + "\" ";
            mConexion.ejecutarActualizacion(SQL);
        }

        public void EliminarTarea(String ID)
        {
            String SQL = "delete tarea from tarea where idTarea =  \"" + ID + "\" ";
            mConexion.ejecutarActualizacion(SQL);
        }

        public void ModificarTarea_personal(String ID, String IDPersonal)
        {
            String SQL = "update tarea_personal set "
                + "idPersonal='?1'"
                + " where idTarea = \"" + ID + "\"";
            SQL = SQL.Replace("?1", IDPersonal);
            mConexion.ejecutarActualizacion(SQL);
        }

        public DataSet ConsultarTarea(string ID)
        {
            String SQL = "select descripcion, inicio_plan,fin_plan from tarea where idTarea = \"" + ID + "\"";
            DataSet Tarea = mConexion.ejecutarConsulta(SQL);
            return Tarea;
        }

        public DataSet ConsultarListBox(string ID)
        {
            String SQL = "select idPersonal from tarea_personal where idTarea = \"" + ID + "\"";
            DataSet Tarea = mConexion.ejecutarConsulta(SQL);
            return Tarea;
        }

        public DataSet ConsultarPersonal(String ID)
        {
            String SQL = "select iniciales from personal where idPersonal = \"" + ID + "\"";
            DataSet ListaID = mConexion.ejecutarConsulta(SQL);
            return ListaID;
        }

        public DataSet ConsultarIdPersonal(String Iniciales)
        {
            String SQL = "select idPersonal from personal where iniciales = \"" + Iniciales + "\"";
            DataSet ListaID = mConexion.ejecutarConsulta(SQL);
            return ListaID;
        }
        //Corregir idProyecto cuando se integre
        public DataSet FiltrarTareas(string idProyecto, string texto = "")
        {
            String SQL = "select idtarea,descripcion, inicio_plan,fin_plan from tarea where (descripcion like \'%" + texto + "%\' or inicio_plan like \'%" + texto + "%\') and idProyecto = \"" + idProyecto + "\"";
            DataSet ListaUsuarios = mConexion.ejecutarConsulta(SQL);
            return ListaUsuarios;
        }

        public DataSet consultarUsuarios2(string ID, string texto = "")
        {
            String SQL = "select distinct nombre_completo,iniciales from Personal inner join equipo_trabajo on personal.idPersonal = equipo_trabajo.idPersonal WHERE idProyecto = \"" + ID + "\" AND (nombre_completo like \'%" + texto + "%\' or iniciales like \'%" + texto + "%\') and visibilidad = 1";
            DataSet ListaUsuarios = mConexion.ejecutarConsulta(SQL);
            return ListaUsuarios;
        }
        public DataSet ConsultarTareas(string idProyecto)
        {
            String SQL = "select idTarea, descripcion, inicio_plan,fin_plan from tarea where idProyecto = \"" + idProyecto + "\" ";
            DataSet ListaTareas = mConexion.ejecutarConsulta(SQL);
            return ListaTareas;
        }
        public DataSet ConsultarTareas(string idProyecto, string idPersonal)
        {
            String SQL = "select personal.iniciales, tarea.descripcion,tarea.inicio_real,tarea.fin_real,tarea.inicio_plan,tarea.fin_plan, usuario.tipo, tarea.idTarea, (select DATEDIFF(inicio_plan,inicio_real)),(select DATEDIFF(fin_plan,fin_real)),(select DATEDIFF(inicio_plan,now())),(select DATEDIFF(fin_plan,now())),tarea.revisado " +
                        "from tarea_personal " +
                        "inner join personal on tarea_personal.idPersonal = personal.idPersonal " +
                        "inner join tarea on tarea_personal.idTarea = tarea.idTarea " +
                        "inner join usuario on Personal.idPersonal = Usuario.idPersonal " +
                        "where personal.idPersonal = '?1' and tarea.idProyecto = '?2'; ";
            SQL = SQL.Replace("?1", idPersonal);
            SQL = SQL.Replace("?2", idProyecto);
            DataSet ListaTareas = mConexion.ejecutarConsulta(SQL);
            return ListaTareas;
        }
        public DataSet ConsultarTareasAdministrador(string idProyecto)
        {
            String SQL = "select personal.iniciales, tarea.descripcion,tarea.inicio_real,tarea.fin_real,tarea.inicio_plan,tarea.fin_plan, usuario.tipo, tarea.idTarea, (select DATEDIFF(inicio_plan,inicio_real)),(select DATEDIFF(fin_plan,fin_real)),(select DATEDIFF(inicio_plan,now())),(select DATEDIFF(fin_plan,now())),tarea.revisado " +
                        "from tarea_personal " +
                        "inner join personal on tarea_personal.idPersonal = personal.idPersonal " +
                        "inner join tarea on tarea_personal.idTarea = tarea.idTarea " +
                        "inner join usuario on Personal.idPersonal = Usuario.idPersonal " +
                        "where tarea.idProyecto = '?2'; ";

            SQL = SQL.Replace("?2", idProyecto);
            DataSet ListaTareas = mConexion.ejecutarConsulta(SQL);
            return ListaTareas;
        }

        public string[] obtenerFechas(string idTarea)
        {
            String[] fechas = new String[2];
            String SQL = "select inicio_real,fin_real from tarea where idTarea = '" + idTarea + "'";
            DataSet resultado = mConexion.ejecutarConsulta(SQL);
            foreach (DataRow fila in resultado.Tables[0].Rows)
            {
                fechas[0] = Convert.ToString(fila["inicio_real"]);
                fechas[1] = Convert.ToString(fila["fin_real"]);
            }
            return fechas;
        }

        public void modificarFechas(string id, string Inicio = "null", string Fin = "null", string revisado = "0")
        {
            String SQL = "update tarea set inicio_real = '?1', fin_real = '?2',revisado = '?4' where idtarea = '?3'; ";
            //Sustituir con "null" sin apostrofes
            if (Inicio.Contains("null"))
            {
                SQL = SQL.Replace("'?1'", "null");
            }
            if (Fin.Contains("null"))
            {
                SQL = SQL.Replace("'?2'", "null");
            }
            SQL = SQL.Replace("?1", Inicio);
            SQL = SQL.Replace("?2", Fin);
            SQL = SQL.Replace("?3", id);
            SQL = SQL.Replace("?4", revisado);
            mConexion.ejecutarActualizacion(SQL);

        }
        public void modificarFechaFinal(string id, string Fecha)
        {
            //Preveer la asignacion de la fecha de inicio en caso de no tenerla
            String SQL = "update tarea set inicio_real=IF(isnull(inicio_real), date(now()), inicio_real ), " +
                " fin_real = '?1', revisado = 0 where idtarea = '?3'; ";
            SQL = SQL.Replace("?1", Fecha);
            SQL = SQL.Replace("?3", id);
            mConexion.ejecutarActualizacion(SQL);
        }
        public void agregarRevisado(string id)
        {
            //Preveer la asignacion de la fecha de inicio y fin en caso de no tenerla
            String SQL = "update tarea set inicio_real=IF(isnull(inicio_real), date(now()), inicio_real ), " +
                " fin_real=IF(isnull(fin_real), date(now()), fin_real )," +
                " revisado = 1 where idtarea = '?3'; ";
            SQL = SQL.Replace("?3", id);
            mConexion.ejecutarActualizacion(SQL);
        }
        //public DataSet ConsultarTareaRevisadas(string idProyecto)
        //{
        //    String SQL = "select personal.iniciales, tarea.descripcion,tarea.inicio_real,tarea.fin_real,tarea.inicio_plan,tarea.fin_plan, usuario.tipo, tarea.idTarea, (select DATEDIFF(inicio_plan,inicio_real)),(select DATEDIFF(fin_plan,fin_real)),(select DATEDIFF(inicio_plan,now())),(select DATEDIFF(fin_plan,now())), tarea.revisado " +
        //                "from tarea_personal " +
        //                "inner join personal on tarea_personal.idPersonal = personal.idPersonal " +
        //                "inner join tarea on tarea_personal.idTarea = tarea.idTarea " +
        //                "inner join usuario on Personal.idPersonal = Usuario.idPersonal " +
        //                "where tarea.idProyecto = '?2'; ";

        //    SQL = SQL.Replace("?2", idProyecto);
        //    DataSet ListaTareas = mConexion.ejecutarConsulta(SQL);
        //    return ListaTareas;
        //}
    }
}
