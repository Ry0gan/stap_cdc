﻿namespace STAP_CDC.CRUD.CRUD_tareas
{
    partial class FrmTareas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTareas));
            this.PanelPrincipal = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2ControlBox3 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox4 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.BtnCancelar = new Guna.UI2.WinForms.Guna2Button();
            this.DgvTareas = new System.Windows.Forms.DataGridView();
            this.BtnModificar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnEliminar = new Guna.UI2.WinForms.Guna2Button();
            this.TxtFiltrar = new Guna.UI2.WinForms.Guna2TextBox();
            this.BtnNuevo = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.Panel0 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.PbAviso = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BtnNuevo0 = new Guna.UI2.WinForms.Guna2Button();
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.PanelPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvTareas)).BeginInit();
            this.Panel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelPrincipal
            // 
            this.PanelPrincipal.Controls.Add(this.guna2ControlBox3);
            this.PanelPrincipal.Controls.Add(this.guna2ControlBox4);
            this.PanelPrincipal.Controls.Add(this.BtnCancelar);
            this.PanelPrincipal.Controls.Add(this.DgvTareas);
            this.PanelPrincipal.Controls.Add(this.BtnModificar);
            this.PanelPrincipal.Controls.Add(this.BtnEliminar);
            this.PanelPrincipal.Controls.Add(this.TxtFiltrar);
            this.PanelPrincipal.Controls.Add(this.BtnNuevo);
            this.PanelPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelPrincipal.Location = new System.Drawing.Point(0, 0);
            this.PanelPrincipal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelPrincipal.Name = "PanelPrincipal";
            this.PanelPrincipal.ShadowDecoration.Parent = this.PanelPrincipal;
            this.PanelPrincipal.Size = new System.Drawing.Size(849, 534);
            this.PanelPrincipal.TabIndex = 37;
            // 
            // guna2ControlBox3
            // 
            this.guna2ControlBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox3.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox3.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox3.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox3.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox3.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox3.HoverState.Parent = this.guna2ControlBox3;
            this.guna2ControlBox3.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox3.Location = new System.Drawing.Point(723, 0);
            this.guna2ControlBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox3.Name = "guna2ControlBox3";
            this.guna2ControlBox3.ShadowDecoration.Parent = this.guna2ControlBox3;
            this.guna2ControlBox3.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox3.TabIndex = 57;
            // 
            // guna2ControlBox4
            // 
            this.guna2ControlBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox4.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox4.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox4.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox4.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox4.HoverState.Parent = this.guna2ControlBox4;
            this.guna2ControlBox4.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox4.Location = new System.Drawing.Point(785, 0);
            this.guna2ControlBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox4.Name = "guna2ControlBox4";
            this.guna2ControlBox4.ShadowDecoration.Parent = this.guna2ControlBox4;
            this.guna2ControlBox4.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox4.TabIndex = 56;
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Animated = true;
            this.BtnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCancelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(75)))), ((int)(((byte)(32)))));
            this.BtnCancelar.BorderRadius = 15;
            this.BtnCancelar.BorderThickness = 1;
            this.BtnCancelar.CheckedState.Parent = this.BtnCancelar;
            this.BtnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnCancelar.CustomImages.Parent = this.BtnCancelar;
            this.BtnCancelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(75)))), ((int)(((byte)(32)))));
            this.BtnCancelar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.ForeColor = System.Drawing.Color.White;
            this.BtnCancelar.HoverState.Parent = this.BtnCancelar;
            this.BtnCancelar.Image = global::STAP_CDC.Properties.Resources.back;
            this.BtnCancelar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnCancelar.Location = new System.Drawing.Point(621, 460);
            this.BtnCancelar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.ShadowDecoration.Parent = this.BtnCancelar;
            this.BtnCancelar.Size = new System.Drawing.Size(184, 49);
            this.BtnCancelar.TabIndex = 38;
            this.BtnCancelar.Text = "Regresar";
            this.BtnCancelar.UseTransparentBackground = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click_1);
            // 
            // DgvTareas
            // 
            this.DgvTareas.AllowUserToAddRows = false;
            this.DgvTareas.AllowUserToDeleteRows = false;
            this.DgvTareas.AllowUserToResizeColumns = false;
            this.DgvTareas.AllowUserToResizeRows = false;
            this.DgvTareas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DgvTareas.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DgvTareas.BackgroundColor = System.Drawing.Color.White;
            this.DgvTareas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DgvTareas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvTareas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DgvTareas.ColumnHeadersHeight = 40;
            this.DgvTareas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DgvTareas.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvTareas.DefaultCellStyle = dataGridViewCellStyle5;
            this.DgvTareas.EnableHeadersVisualStyles = false;
            this.DgvTareas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.DgvTareas.Location = new System.Drawing.Point(16, 139);
            this.DgvTareas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DgvTareas.MultiSelect = false;
            this.DgvTareas.Name = "DgvTareas";
            this.DgvTareas.ReadOnly = true;
            this.DgvTareas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DgvTareas.RowHeadersVisible = false;
            this.DgvTareas.RowHeadersWidth = 60;
            this.DgvTareas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvTareas.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DgvTareas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvTareas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvTareas.Size = new System.Drawing.Size(804, 314);
            this.DgvTareas.TabIndex = 37;
            // 
            // BtnModificar
            // 
            this.BtnModificar.Animated = true;
            this.BtnModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtnModificar.BorderColor = System.Drawing.Color.DarkViolet;
            this.BtnModificar.BorderRadius = 15;
            this.BtnModificar.BorderThickness = 1;
            this.BtnModificar.CheckedState.Parent = this.BtnModificar;
            this.BtnModificar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnModificar.CustomImages.Parent = this.BtnModificar;
            this.BtnModificar.FillColor = System.Drawing.Color.DarkViolet;
            this.BtnModificar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnModificar.ForeColor = System.Drawing.Color.White;
            this.BtnModificar.HoverState.Parent = this.BtnModificar;
            this.BtnModificar.Image = global::STAP_CDC.Properties.Resources.edit;
            this.BtnModificar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnModificar.Location = new System.Drawing.Point(83, 460);
            this.BtnModificar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.ShadowDecoration.Parent = this.BtnModificar;
            this.BtnModificar.Size = new System.Drawing.Size(193, 49);
            this.BtnModificar.TabIndex = 36;
            this.BtnModificar.Text = "Modificar";
            this.BtnModificar.UseTransparentBackground = true;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click_1);
            // 
            // BtnEliminar
            // 
            this.BtnEliminar.Animated = true;
            this.BtnEliminar.BackColor = System.Drawing.Color.Transparent;
            this.BtnEliminar.BorderColor = System.Drawing.Color.Firebrick;
            this.BtnEliminar.BorderRadius = 15;
            this.BtnEliminar.BorderThickness = 1;
            this.BtnEliminar.CheckedState.Parent = this.BtnEliminar;
            this.BtnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnEliminar.CustomImages.Parent = this.BtnEliminar;
            this.BtnEliminar.FillColor = System.Drawing.Color.Firebrick;
            this.BtnEliminar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEliminar.ForeColor = System.Drawing.Color.White;
            this.BtnEliminar.HoverState.Parent = this.BtnEliminar;
            this.BtnEliminar.Image = global::STAP_CDC.Properties.Resources.cancel;
            this.BtnEliminar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnEliminar.Location = new System.Drawing.Point(349, 460);
            this.BtnEliminar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnEliminar.Name = "BtnEliminar";
            this.BtnEliminar.ShadowDecoration.Parent = this.BtnEliminar;
            this.BtnEliminar.Size = new System.Drawing.Size(193, 49);
            this.BtnEliminar.TabIndex = 35;
            this.BtnEliminar.Text = "Eliminar";
            this.BtnEliminar.UseTransparentBackground = true;
            this.BtnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click_1);
            // 
            // TxtFiltrar
            // 
            this.TxtFiltrar.Animated = true;
            this.TxtFiltrar.BackColor = System.Drawing.Color.White;
            this.TxtFiltrar.BorderRadius = 10;
            this.TxtFiltrar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFiltrar.DefaultText = "";
            this.TxtFiltrar.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtFiltrar.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtFiltrar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.DisabledState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.FocusedState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFiltrar.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.HoverState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Location = new System.Drawing.Point(21, 53);
            this.TxtFiltrar.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtFiltrar.Name = "TxtFiltrar";
            this.TxtFiltrar.PasswordChar = '\0';
            this.TxtFiltrar.PlaceholderText = "Buscar...";
            this.TxtFiltrar.SelectedText = "";
            this.TxtFiltrar.ShadowDecoration.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Size = new System.Drawing.Size(468, 44);
            this.TxtFiltrar.TabIndex = 34;
            this.TxtFiltrar.TextChanged += new System.EventHandler(this.TxtFiltrar_TextChanged_1);
            // 
            // BtnNuevo
            // 
            this.BtnNuevo.Animated = true;
            this.BtnNuevo.BackColor = System.Drawing.Color.Transparent;
            this.BtnNuevo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo.BorderRadius = 15;
            this.BtnNuevo.BorderThickness = 1;
            this.BtnNuevo.CheckedState.Parent = this.BtnNuevo;
            this.BtnNuevo.CustomImages.Parent = this.BtnNuevo;
            this.BtnNuevo.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNuevo.ForeColor = System.Drawing.Color.White;
            this.BtnNuevo.HoverState.Parent = this.BtnNuevo;
            this.BtnNuevo.Image = global::STAP_CDC.Properties.Resources.add;
            this.BtnNuevo.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnNuevo.Location = new System.Drawing.Point(499, 53);
            this.BtnNuevo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnNuevo.Name = "BtnNuevo";
            this.BtnNuevo.ShadowDecoration.Parent = this.BtnNuevo;
            this.BtnNuevo.Size = new System.Drawing.Size(165, 49);
            this.BtnNuevo.TabIndex = 33;
            this.BtnNuevo.Text = "Nuevo";
            this.BtnNuevo.UseTransparentBackground = true;
            this.BtnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click_1);
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // Panel0
            // 
            this.Panel0.Controls.Add(this.guna2ControlBox2);
            this.Panel0.Controls.Add(this.guna2ControlBox1);
            this.Panel0.Controls.Add(this.label7);
            this.Panel0.Controls.Add(this.label8);
            this.Panel0.Controls.Add(this.PbAviso);
            this.Panel0.Controls.Add(this.pictureBox2);
            this.Panel0.Controls.Add(this.BtnNuevo0);
            this.Panel0.Location = new System.Drawing.Point(0, 0);
            this.Panel0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Panel0.Name = "Panel0";
            this.Panel0.ShadowDecoration.Parent = this.Panel0;
            this.Panel0.Size = new System.Drawing.Size(849, 534);
            this.Panel0.TabIndex = 41;
            this.Panel0.Leave += new System.EventHandler(this.Panel0_Leave);
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(727, 0);
            this.guna2ControlBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox2.TabIndex = 55;
            this.guna2ControlBox2.Click += new System.EventHandler(this.guna2ControlBox2_Click);
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(789, 0);
            this.guna2ControlBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox1.TabIndex = 54;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(208, 331);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(439, 29);
            this.label7.TabIndex = 43;
            this.label7.Text = "* Presiona el siguiente botón para crear una!!";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 20F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(121, 235);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(564, 41);
            this.label8.TabIndex = 42;
            this.label8.Text = "No existen tareas para este proyecto :( ...";
            // 
            // PbAviso
            // 
            this.PbAviso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbAviso.Image = ((System.Drawing.Image)(resources.GetObject("PbAviso.Image")));
            this.PbAviso.Location = new System.Drawing.Point(320, 25);
            this.PbAviso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PbAviso.Name = "PbAviso";
            this.PbAviso.Size = new System.Drawing.Size(217, 193);
            this.PbAviso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbAviso.TabIndex = 41;
            this.PbAviso.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(380, 377);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(111, 81);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // BtnNuevo0
            // 
            this.BtnNuevo0.BackColor = System.Drawing.Color.Transparent;
            this.BtnNuevo0.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo0.BorderRadius = 15;
            this.BtnNuevo0.BorderThickness = 1;
            this.BtnNuevo0.CheckedState.Parent = this.BtnNuevo0;
            this.BtnNuevo0.CustomImages.Parent = this.BtnNuevo0;
            this.BtnNuevo0.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(164)))), ((int)(((byte)(80)))));
            this.BtnNuevo0.Font = new System.Drawing.Font("Microsoft Tai Le", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNuevo0.ForeColor = System.Drawing.Color.White;
            this.BtnNuevo0.HoverState.Parent = this.BtnNuevo0;
            this.BtnNuevo0.Image = global::STAP_CDC.Properties.Resources.add;
            this.BtnNuevo0.ImageSize = new System.Drawing.Size(40, 40);
            this.BtnNuevo0.Location = new System.Drawing.Point(245, 470);
            this.BtnNuevo0.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnNuevo0.Name = "BtnNuevo0";
            this.BtnNuevo0.ShadowDecoration.Parent = this.BtnNuevo0;
            this.BtnNuevo0.Size = new System.Drawing.Size(381, 49);
            this.BtnNuevo0.TabIndex = 23;
            this.BtnNuevo0.Text = "CREAR NUEVA TAREA";
            this.BtnNuevo0.UseTransparentBackground = true;
            this.BtnNuevo0.Click += new System.EventHandler(this.BtnNuevo0_Click_1);
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this.PanelPrincipal;
            // 
            // guna2DragControl1
            // 
            this.guna2DragControl1.TargetControl = this.Panel0;
            // 
            // FrmTareas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(849, 534);
            this.Controls.Add(this.Panel0);
            this.Controls.Add(this.PanelPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmTareas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestionar Tareas";
            this.Load += new System.EventHandler(this.FrmTareas_Load);
            this.Enter += new System.EventHandler(this.FrmTareas_Enter);
            this.Leave += new System.EventHandler(this.FrmTareas_Leave);
            this.PanelPrincipal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DgvTareas)).EndInit();
            this.Panel0.ResumeLayout(false);
            this.Panel0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2GradientPanel PanelPrincipal;
        private Guna.UI2.WinForms.Guna2Button BtnCancelar;
        private System.Windows.Forms.DataGridView DgvTareas;
        private Guna.UI2.WinForms.Guna2Button BtnModificar;
        private Guna.UI2.WinForms.Guna2Button BtnEliminar;
        private Guna.UI2.WinForms.Guna2TextBox TxtFiltrar;
        private Guna.UI2.WinForms.Guna2Button BtnNuevo;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox3;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox4;
        private Guna.UI2.WinForms.Guna2GradientPanel Panel0;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox PbAviso;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Guna.UI2.WinForms.Guna2Button BtnNuevo0;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
    }
}