﻿using STAP_CDC.Gestion_proyectos.Equipo_Trabajo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.CRUD.CRUD_tareas
{
    public partial class FrmModificarTarea : Form
    {
        private string ID;
        private string IDP;
        ControlTarea mControlTarea;
        public FrmModificarTarea()
        {
            InitializeComponent();
           Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            mControlTarea = new ControlTarea();
            TxtDescripcion.Select();
            Panel0.Visible = false;
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        public void obtenerID(String ID)
        {
            this.ID = ID;
        }

        public void obtenerIDP(String IDP)
        {
            this.IDP = IDP;
        }
        public void AsignarDatos(String ID)
        {
            DataSet Tarea = mControlTarea.ConsultarTarea(ID);
            String fecha1 = "";
            String fecha11 = "";
            String fecha2 = "";
            String fecha22 = "";
            foreach (System.Data.DataRow row in Tarea.Tables[0].Rows)
            {
                TxtDescripcion.Text = row["descripcion"].ToString();

                fecha1 = row["inicio_plan"].ToString();
                fecha11 = fecha1.Remove(10);
                DtpFechaInicioPlaneado.Text = fecha11;
                fecha2 = row["fin_plan"].ToString();
                fecha22 = fecha2.Remove(10);
                DtpFechaFinPlaneado.Text = fecha22;
            }

            DataSet idPersonal = mControlTarea.ConsultarListBox(ID);
            int x = 0;
            foreach (System.Data.DataRow row in idPersonal.Tables[0].Rows)
            {

                DataSet iniciales = mControlTarea.ConsultarPersonal(row[0].ToString());
                foreach (System.Data.DataRow row2 in iniciales.Tables[0].Rows)
                {
                    LbResponsables.Items.Add(row2[0].ToString());
                }
            }

        }

        public void TablaUsuarios()
        {  
            DataSet Comprobar = mControlTarea.consultarUsuarios(this.IDP);
            if (Comprobar.Tables[0].Rows.Count > 0)
            {
                Panel0.Visible = false;
                DgvUsuarios.DataSource = mControlTarea.consultarUsuarios(this.IDP).Tables[0];

                DgvUsuarios.Columns["nombre_completo"].HeaderText = "Nombre completo";

                DgvUsuarios.Columns["iniciales"].HeaderText = "Iniciales";
            }
            else
            {
                Panel0.Visible = true;
            }
        }

        private void FrmModificarTarea_Load(object sender, EventArgs e)
        {
            cargarPrincipal();
        }


        public void cargarPrincipal()
        {
            LbResponsables.Items.Clear();
            AsignarDatos(ID);
            TablaUsuarios();
        }
        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            if (DgvUsuarios.CurrentRow != null)
            {
                foreach (DataGridViewRow drv in DgvUsuarios.SelectedRows)
                {
                    if (!LbResponsables.Items.Contains(drv.Cells[1].Value.ToString()))
                    {
                        LbResponsables.Items.Add(drv.Cells[1].Value.ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Seleccione un renglon", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            DgvUsuarios.ClearSelection();
        }

        private void BtnQuitar_Click(object sender, EventArgs e)
        {
            if (LbResponsables.SelectedIndex != -1)
            {
                ArrayList ListaObjetosE = new ArrayList();
                foreach (object obj in LbResponsables.SelectedItems)
                {
                    ListaObjetosE.Add(obj);
                }
                foreach (object obj in ListaObjetosE.ToArray())
                {
                    LbResponsables.Items.Remove(obj);
                }
            }
            else
            {
                MessageBox.Show(this, "Seleccione un responsable para quitarlo", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            DgvUsuarios.DataSource = mControlTarea.consultarUsuarios2(this.IDP,TxtFiltrar.Text).Tables[0];
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (LbResponsables.Items.Count > 0)
            {
                TxtDescripcion.Text = TxtDescripcion.Text.Trim();
                if (String.IsNullOrEmpty(TxtDescripcion.Text))
                {
                    MessageBox.Show(this, "El campo Tarea no puede estar vacío", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtDescripcion.Focus();
                }
                else
                {
                    Tarea mTarea = new Tarea();
                    mTarea.setDescripcion(TxtDescripcion.Text);

                    if (DateTime.Compare(DtpFechaInicioPlaneado.Value.Date, DtpFechaFinPlaneado.Value.Date) > 0)
                    {
                        MessageBox.Show(this, "La fecha inicial no puede ser mayor a la final", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        mTarea.setInicio_plan(DtpFechaInicioPlaneado.Text);
                        mTarea.setFin_Plan(DtpFechaFinPlaneado.Text);
                        mControlTarea.ModificarTarea(mTarea,this.ID);
                        MessageBox.Show(this, "Tarea Modificada", "Exito!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        string IDP;

                        mControlTarea.EliminarTarea_personal(this.ID);

                        for (int i = 0; i < LbResponsables.Items.Count; i++)
                        {
                            IDP = "";
                            LbResponsables.SelectedIndex = i;
                            DataSet idP = mControlTarea.ConsultarIdPersonal(LbResponsables.Text);
                            foreach (System.Data.DataRow row in idP.Tables[0].Rows)
                            {
                                IDP = row[0].ToString();
                            }
                            mControlTarea.agregarResponsables(this.ID, IDP);
                        }

                        this.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Agregue al menos un responsable", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnNuevo0_Click(object sender, EventArgs e)
        {
            FrmAsignarEquipo frmAsignarEquipo = new FrmAsignarEquipo();
            frmAsignarEquipo.obtenerID(this.IDP);
            frmAsignarEquipo.ShowDialog();
            this.cargarPrincipal();
        }

        private void Panel0_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmModificarTarea_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
