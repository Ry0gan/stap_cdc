﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.CRUD.CRUD_tareas
{
    public partial class FrmTareas : Form
    {
        ControlTarea mControlTarea;
        private string ID;
        public FrmTareas()
        {
            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            mControlTarea = new ControlTarea();
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        
        private void FrmTareas_Load(object sender, EventArgs e)
        {
            CargarPrincipal();
        }


        public void CargarPrincipal()
        {
            DataSet comprobar = mControlTarea.ConsultarTareas(this.ID);
            if(comprobar.Tables[0].Rows.Count > 0)
            {
                Panel0.Visible = false;
                PanelPrincipal.Visible = true;
                DgvTareas.DataSource = mControlTarea.ConsultarTareas(this.ID).Tables[0];

                DgvTareas.Columns["idTarea"].Visible = false;
                DgvTareas.Columns["descripcion"].HeaderText = "Descripción";

                DgvTareas.Columns["inicio_plan"].HeaderText = "Inicio_Plan";

                DgvTareas.Columns["fin_plan"].HeaderText = "Fin_Plan";

            }
            else
            {
                PanelPrincipal.Visible = false;
                Panel0.Visible = true;
            }
           
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            
        }


        public void obtenerID(string ID)
        {
            this.ID = ID;
        }
        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
          
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            
        }

        private void BtnNuevo0_Click(object sender, EventArgs e)
        {
            FrmAltaTarea mFrmAltaTarea = new FrmAltaTarea();
            mFrmAltaTarea.ObtenerID(this.ID);
            mFrmAltaTarea.ShowDialog();
            this.CargarPrincipal();
        }

        private void PanelPrincipal_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnNuevo_Click_1(object sender, EventArgs e)
        {
            FrmAltaTarea mFrmAltaTarea = new FrmAltaTarea();
            mFrmAltaTarea.ObtenerID(this.ID);
            mFrmAltaTarea.ShowDialog();
            this.CargarPrincipal();
        }

        private void BtnModificar_Click_1(object sender, EventArgs e)
        {
            if (DgvTareas.SelectedRows.Count == -1)
            {
                MessageBox.Show("Seleccione un renglon");
            }
            else
            {
                int selected = DgvTareas.SelectedRows[0].Index;
                FrmModificarTarea mFrmModificar = new FrmModificarTarea();
                mFrmModificar.obtenerID(DgvTareas.Rows[selected].Cells[0].Value.ToString());
                mFrmModificar.obtenerIDP(this.ID);
                mFrmModificar.ShowDialog();
                this.CargarPrincipal();
            }
        }

        private void TxtFiltrar_TextChanged_1(object sender, EventArgs e)
        {
            DgvTareas.DataSource = mControlTarea.FiltrarTareas(this.ID,TxtFiltrar.Text).Tables[0];
            DgvTareas.Columns["idTarea"].Visible = false;
            DgvTareas.Columns["descripcion"].HeaderText = "Descripción";

            DgvTareas.Columns["inicio_plan"].HeaderText = "Inicio_Plan";

            DgvTareas.Columns["fin_plan"].HeaderText = "Fin_Plan";

        }

        private void BtnCancelar_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnEliminar_Click_1(object sender, EventArgs e)
        {
            if (DgvTareas.SelectedRows.Count == -1)
            {
                MessageBox.Show("Seleccione un renglon");
            }
            else
            {
                int selected = DgvTareas.SelectedRows[0].Index;
                string id = DgvTareas.Rows[selected].Cells[0].Value.ToString();
                if (MessageBox.Show("¿Desea eliminar esta tarea ?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    ControlTarea mControl = new ControlTarea();
                    mControl.EliminarTarea(id);
                    MessageBox.Show(this, "Tarea Eliminada Correctamente", "Exito!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.CargarPrincipal();
                }

            }
        }

        private void BtnNuevo0_Click_1(object sender, EventArgs e)
        {
            FrmAltaTarea mFrmAltaTarea = new FrmAltaTarea();
            mFrmAltaTarea.ObtenerID(this.ID);
            mFrmAltaTarea.ShowDialog();
            this.CargarPrincipal();
        }

        private void Panel0_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmTareas_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmTareas_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void guna2ControlBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
