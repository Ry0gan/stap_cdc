﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STAP_CDC.CRUD.CRUD_tareas
{
    class Tarea
    {
        private int idTarea;
        private int idProyecto;
        private string Descripcion;
        private string Inicio_plan;
        private string Fin_plan;
        private string Inicio_Real = "";
        private string Fin_Real = "";


        public void setidProyecto(int idProyecto)
        {
            this.idProyecto = idProyecto;
        }

        public int getidProyecto()
        {
            return idProyecto;
        }


        public void setidTarea(int idTarea)
        {
            this.idTarea = idTarea;
        }

        public int getidTarea()
        {
            return idTarea;
        }


        public void setDescripcion(string Descripcion)
        {
            this.Descripcion = Descripcion;
        }

        public string getDescripcion()
        {
            return Descripcion;
        }


        public void setInicio_plan(string Inicio_Plan)
        {
            this.Inicio_plan = Inicio_Plan;
        }

        public string getInicio_Plan()
        {
            return Inicio_plan;
        }

        public void setFin_Plan(string Fin_plan)
        {
            this.Fin_plan = Fin_plan;
        }

        public string getFin_Plan()
        {
            return Fin_plan;
        }

        public void setInicio_Real(string Inicio_Real)
        {
            this.Inicio_Real = Inicio_Real;
        }

        public string getInicio_Real()
        {
            return Inicio_Real;
        }
        public void setFin_Real(string Fin_Real)
        {
            this.Fin_Real = Fin_Real;
        }

        public string getFin_Real()
        {
            return Fin_Real;
        }

    }
}
