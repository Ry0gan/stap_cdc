﻿using STAP_CDC.Gestion_proyectos.Equipo_Trabajo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.CRUD.CRUD_tareas
{
    public partial class FrmAltaTarea : Form
    {
        ControlTarea mControlTarea;
        private string ID;
        public FrmAltaTarea()
        {
            InitializeComponent();
           Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            mControlTarea = new ControlTarea();
            TxtDescripcion.Select();
            Panel0.Visible = false;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void FrmAltaTarea_Load(object sender, EventArgs e)
        {
            cargarPrincipal();

        }


        public void cargarPrincipal()
        {
            LbResponsables.Items.Clear();
            DataSet Comprobar = mControlTarea.consultarUsuarios(this.ID);
            if(Comprobar.Tables[0].Rows.Count > 0)
            {
                Panel0.Visible = false;
                DgvUsuarios.DataSource = mControlTarea.consultarUsuarios(this.ID).Tables[0];

                DgvUsuarios.Columns["nombre_completo"].HeaderText = "Nombre completo";

                DgvUsuarios.Columns["iniciales"].HeaderText = "Iniciales";
            }
            else
            {
                Panel0.Visible = true;
            }
            
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {

            if (DgvUsuarios.CurrentRow != null)
            {
                foreach (DataGridViewRow drv in DgvUsuarios.SelectedRows)
                {
                    if (!LbResponsables.Items.Contains(drv.Cells[1].Value.ToString()))
                    {
                        LbResponsables.Items.Add(drv.Cells[1].Value.ToString());
                    }                    
                }
            }
            else
            {
                MessageBox.Show(this, "Seleccione un renglon", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            DgvUsuarios.ClearSelection();
        }
        public void ObtenerID(string ID)
        {
            this.ID = ID;
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (LbResponsables.Items.Count > 0)
            {
                TxtDescripcion.Text = TxtDescripcion.Text.Trim();
                if (String.IsNullOrEmpty(TxtDescripcion.Text))
                {
                    MessageBox.Show(this, "El campo Tarea no puede estar vacío", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    TxtDescripcion.Focus();
                }
                else
                {
                    Tarea mTarea = new Tarea();
                    mTarea.setDescripcion(TxtDescripcion.Text);

                    if (DateTime.Compare(DtpFechaInicioPlaneado.Value.Date , DtpFechaFinPlaneado.Value.Date) >0)
                    {
                        MessageBox.Show(this, "La fecha inicial no puede ser mayor a la final", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }else
                    {
                        mTarea.setInicio_plan(DtpFechaInicioPlaneado.Text);
                        mTarea.setFin_Plan(DtpFechaFinPlaneado.Text);
                        mControlTarea.agregarTarea(mTarea,this.ID);
                        MessageBox.Show(this, "Tarea Registrada", "Exito!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DataSet id = mControlTarea.ConsultarIdTarea();
                        string ID = "";

                    string IDP;
                        foreach (System.Data.DataRow row in id.Tables[0].Rows)
                            {
                                ID = row[0].ToString();
                            }

                            for (int i = 0; i < LbResponsables.Items.Count; i++)
                            {
                                 IDP = "";
                                LbResponsables.SelectedIndex = i;
                                DataSet idP = mControlTarea.ConsultarIdPersonal(LbResponsables.Text);
                                foreach (System.Data.DataRow row in idP.Tables[0].Rows)
                                 {
                                    IDP = row[0].ToString();
                                 }
                            mControlTarea.agregarResponsables(ID, IDP);

                        }

                        this.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Agregue al menos un responsable", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void BtnQuitar_Click(object sender, EventArgs e)
        {
            if(LbResponsables.SelectedIndex != -1)
            {
                ArrayList ListaObjetosE = new ArrayList();
                foreach (object obj in LbResponsables.SelectedItems)
                {
                    ListaObjetosE.Add(obj);
                }
                foreach (object obj in ListaObjetosE.ToArray())
                {
                    LbResponsables.Items.Remove(obj);
                }
            }
            else
            {
                MessageBox.Show(this, "Seleccione un responsable para quitarlo", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            DgvUsuarios.DataSource = mControlTarea.consultarUsuarios2(this.ID,TxtFiltrar.Text).Tables[0];
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnNuevo0_Click(object sender, EventArgs e)
        {
            FrmAsignarEquipo frmAsignarEquipo = new FrmAsignarEquipo();
            frmAsignarEquipo.obtenerID(this.ID);
            frmAsignarEquipo.ShowDialog();
            this.cargarPrincipal();
        }

        private void Panel0_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void Panel0_Enter(object sender, EventArgs e) {
           Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmAltaTarea_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
