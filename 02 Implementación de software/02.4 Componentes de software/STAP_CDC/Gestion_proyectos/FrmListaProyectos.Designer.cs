﻿namespace STAP_CDC.Gestion_proyectos
{
    partial class FrmListaProyectos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmListaProyectos));
            this.LayoutProyectos = new System.Windows.Forms.FlowLayoutPanel();
            this.LblDialogoP = new System.Windows.Forms.Label();
            this.TxtFiltrar = new Guna.UI2.WinForms.Guna2TextBox();
            this.PanelEmpty = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.PbAviso = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PanelEmpty2 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PanelEmpty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).BeginInit();
            this.PanelEmpty2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutProyectos
            // 
            this.LayoutProyectos.AutoScroll = true;
            this.LayoutProyectos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LayoutProyectos.Location = new System.Drawing.Point(16, 124);
            this.LayoutProyectos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LayoutProyectos.Name = "LayoutProyectos";
            this.LayoutProyectos.Size = new System.Drawing.Size(1205, 673);
            this.LayoutProyectos.TabIndex = 0;
            // 
            // LblDialogoP
            // 
            this.LblDialogoP.AutoSize = true;
            this.LblDialogoP.BackColor = System.Drawing.Color.Transparent;
            this.LblDialogoP.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Bold);
            this.LblDialogoP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LblDialogoP.Location = new System.Drawing.Point(15, 22);
            this.LblDialogoP.Name = "LblDialogoP";
            this.LblDialogoP.Size = new System.Drawing.Size(143, 34);
            this.LblDialogoP.TabIndex = 30;
            this.LblDialogoP.Text = "Proyectos";
            // 
            // TxtFiltrar
            // 
            this.TxtFiltrar.BackColor = System.Drawing.Color.White;
            this.TxtFiltrar.BorderRadius = 10;
            this.TxtFiltrar.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFiltrar.DefaultText = "";
            this.TxtFiltrar.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtFiltrar.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtFiltrar.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.DisabledState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtFiltrar.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.FocusedState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFiltrar.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtFiltrar.HoverState.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Location = new System.Drawing.Point(16, 71);
            this.TxtFiltrar.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.TxtFiltrar.Name = "TxtFiltrar";
            this.TxtFiltrar.PasswordChar = '\0';
            this.TxtFiltrar.PlaceholderText = "Buscar...";
            this.TxtFiltrar.SelectedText = "";
            this.TxtFiltrar.ShadowDecoration.Parent = this.TxtFiltrar;
            this.TxtFiltrar.Size = new System.Drawing.Size(448, 44);
            this.TxtFiltrar.TabIndex = 29;
            this.TxtFiltrar.TextChanged += new System.EventHandler(this.TxtFiltrar_TextChanged);
            // 
            // PanelEmpty
            // 
            this.PanelEmpty.BorderColor = System.Drawing.Color.Black;
            this.PanelEmpty.Controls.Add(this.label6);
            this.PanelEmpty.Controls.Add(this.PbAviso);
            this.PanelEmpty.Controls.Add(this.label5);
            this.PanelEmpty.Location = new System.Drawing.Point(1, -2);
            this.PanelEmpty.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelEmpty.Name = "PanelEmpty";
            this.PanelEmpty.ShadowDecoration.Parent = this.PanelEmpty;
            this.PanelEmpty.Size = new System.Drawing.Size(1266, 870);
            this.PanelEmpty.TabIndex = 38;
            this.PanelEmpty.Visible = false;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(401, 540);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(501, 48);
            this.label6.TabIndex = 26;
            this.label6.Text = "No hay proyectos registrados...";
            // 
            // PbAviso
            // 
            this.PbAviso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PbAviso.Image = ((System.Drawing.Image)(resources.GetObject("PbAviso.Image")));
            this.PbAviso.Location = new System.Drawing.Point(552, 230);
            this.PbAviso.Margin = new System.Windows.Forms.Padding(4);
            this.PbAviso.Name = "PbAviso";
            this.PbAviso.Size = new System.Drawing.Size(183, 164);
            this.PbAviso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbAviso.TabIndex = 25;
            this.PbAviso.TabStop = false;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(505, 447);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(282, 57);
            this.label5.TabIndex = 24;
            this.label5.Text = "Lo sentimos :(";
            // 
            // PanelEmpty2
            // 
            this.PanelEmpty2.BorderColor = System.Drawing.Color.Black;
            this.PanelEmpty2.Controls.Add(this.label3);
            this.PanelEmpty2.Controls.Add(this.label1);
            this.PanelEmpty2.Controls.Add(this.pictureBox1);
            this.PanelEmpty2.Controls.Add(this.label2);
            this.PanelEmpty2.Location = new System.Drawing.Point(1, 0);
            this.PanelEmpty2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelEmpty2.Name = "PanelEmpty2";
            this.PanelEmpty2.ShadowDecoration.Parent = this.PanelEmpty2;
            this.PanelEmpty2.Size = new System.Drawing.Size(1244, 820);
            this.PanelEmpty2.TabIndex = 40;
            this.PanelEmpty2.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(445, 619);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(466, 48);
            this.label3.TabIndex = 27;
            this.label3.Text = "Contacta a un administrador";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiCondensed", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(401, 540);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(559, 48);
            this.label1.TabIndex = 26;
            this.label1.Text = "No perteneces a ningún proyecto...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(552, 230);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(183, 164);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(505, 447);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(282, 57);
            this.label2.TabIndex = 24;
            this.label2.Text = "Lo sentimos :(";
            // 
            // FrmListaProyectos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1237, 812);
            this.Controls.Add(this.PanelEmpty2);
            this.Controls.Add(this.PanelEmpty);
            this.Controls.Add(this.TxtFiltrar);
            this.Controls.Add(this.LblDialogoP);
            this.Controls.Add(this.LayoutProyectos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmListaProyectos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista de proyectos";
            this.Load += new System.EventHandler(this.FrmListaProyectos_Load);
            this.PanelEmpty.ResumeLayout(false);
            this.PanelEmpty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbAviso)).EndInit();
            this.PanelEmpty2.ResumeLayout(false);
            this.PanelEmpty2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel LayoutProyectos;
        private System.Windows.Forms.Label LblDialogoP;
        private Guna.UI2.WinForms.Guna2TextBox TxtFiltrar;
        private Guna.UI2.WinForms.Guna2GradientPanel PanelEmpty;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox PbAviso;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2GradientPanel PanelEmpty2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
    }
}