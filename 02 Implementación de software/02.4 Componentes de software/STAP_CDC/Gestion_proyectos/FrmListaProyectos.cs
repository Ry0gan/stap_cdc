﻿using CRUD_proyectos;
using Gestion_proyectos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.Gestion_proyectos
{
    public partial class FrmListaProyectos : Form
    {

        public FrmListaProyectos()
        {
            InitializeComponent();
            this.cargarProyectos();
        }

        List<ItemProyectos> listaProyectos = new List<ItemProyectos>();
        public void cargarProyectos()
        {
            ControlProyectos mControl = new ControlProyectos();
            DataSet dataProyectos;
            if (Acceso.Global.NivelUsuario == 0)
            {
                dataProyectos = mControl.ConsultarProyectosPorUsuario(Acceso.Global.idPersonal.ToString());
                if(dataProyectos.Tables[0].Rows.Count > 0)
                {
                    PanelEmpty.Visible = false;
                    PanelEmpty2.Visible = false;
                    foreach (DataRow fila in dataProyectos.Tables[0].Rows)
                    {
                        ItemProyectos item = new ItemProyectos();
                        item.ID = Convert.ToString(fila["idProyecto"]);
                        item.Titulo = Convert.ToString(fila["titulo_proyecto"]);
                        item.Detalles = Convert.ToString(fila["detalles"]);
                        item.FechaInicio = Convert.ToString(fila["inicio_proyecto"]);
                        item.FechaFin = Convert.ToString(fila["fin_proyecto"]);
                        listaProyectos.Add(item);
                    }
                }
                else
                {
                    PanelEmpty2.Visible = true;
                }
            }
            else
            {
                dataProyectos = mControl.ConsultarProyectos();
                if (dataProyectos.Tables[0].Rows.Count > 0)
                {
                    PanelEmpty.Visible = false;
                    PanelEmpty2.Visible = false;
                    foreach (DataRow fila in dataProyectos.Tables[0].Rows)
                    {
                        ItemProyectos item = new ItemProyectos();
                        item.ID = Convert.ToString(fila["idProyecto"]);
                        item.Titulo = Convert.ToString(fila["titulo_proyecto"]);
                        item.Detalles = Convert.ToString(fila["detalles"]);
                        item.FechaInicio = Convert.ToString(fila["inicio_proyecto"]);
                        item.FechaFin = Convert.ToString(fila["fin_proyecto"]);
                        listaProyectos.Add(item);
                    }
                }
                else
                {
                    PanelEmpty.Visible = true;
                }
               
            }
            
        }

        private void FrmListaProyectos_Load(object sender, EventArgs e)
        {
            LayoutProyectos.Controls.Clear();
            foreach(ItemProyectos item in listaProyectos)
            {
                LayoutProyectos.Controls.Add(item);
            }
        }

        private void TxtFiltrar_TextChanged(object sender, EventArgs e)
        {
            LayoutProyectos.Controls.Clear();
            foreach (ItemProyectos item in listaProyectos)
            {
                if (item.Titulo.ToUpper().Contains(TxtFiltrar.Text.ToUpper())){
                    LayoutProyectos.Controls.Add(item);
                }
            }
        }
    }
}
