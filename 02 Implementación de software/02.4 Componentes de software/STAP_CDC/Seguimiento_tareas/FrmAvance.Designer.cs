﻿namespace STAP_CDC.Seguimiento_tareas
{
    partial class FrmAvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAvance));
            this.DgvTareas = new System.Windows.Forms.DataGridView();
            this.PProgreso = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.PTerminados = new System.Windows.Forms.FlowLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.PRevisados = new System.Windows.Forms.FlowLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PPendientes = new System.Windows.Forms.FlowLayoutPanel();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.ScrBarPendientes = new Guna.UI.WinForms.GunaVScrollBar();
            this.ScrBarProgreso = new Guna.UI.WinForms.GunaVScrollBar();
            this.ScrBarTerminados = new Guna.UI.WinForms.GunaVScrollBar();
            this.ScrBarRevisados = new Guna.UI.WinForms.GunaVScrollBar();
            this.guna2VSeparator1 = new Guna.UI2.WinForms.Guna2VSeparator();
            this.guna2VSeparator2 = new Guna.UI2.WinForms.Guna2VSeparator();
            this.guna2VSeparator3 = new Guna.UI2.WinForms.Guna2VSeparator();
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DgvTareas)).BeginInit();
            this.SuspendLayout();
            // 
            // DgvTareas
            // 
            this.DgvTareas.AllowUserToAddRows = false;
            this.DgvTareas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvTareas.Location = new System.Drawing.Point(16, 725);
            this.DgvTareas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DgvTareas.Name = "DgvTareas";
            this.DgvTareas.RowHeadersWidth = 51;
            this.DgvTareas.Size = new System.Drawing.Size(997, 180);
            this.DgvTareas.TabIndex = 0;
            this.DgvTareas.Visible = false;
            // 
            // PProgreso
            // 
            this.PProgreso.AllowDrop = true;
            this.PProgreso.BackColor = System.Drawing.Color.White;
            this.PProgreso.Location = new System.Drawing.Point(328, 114);
            this.PProgreso.Margin = new System.Windows.Forms.Padding(4, 62, 20, 4);
            this.PProgreso.Name = "PProgreso";
            this.PProgreso.Size = new System.Drawing.Size(288, 645);
            this.PProgreso.TabIndex = 2;
            this.PProgreso.DragDrop += new System.Windows.Forms.DragEventHandler(this.PProgreso_DragDrop);
            this.PProgreso.DragOver += new System.Windows.Forms.DragEventHandler(this.PProgreso_DragOver);
            this.PProgreso.Paint += new System.Windows.Forms.PaintEventHandler(this.PProgreso_Paint);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 18F, System.Drawing.FontStyle.Bold);
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(373, 70);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(220, 43);
            this.label2.TabIndex = 1;
            this.label2.Text = "En Progreso";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PTerminados
            // 
            this.PTerminados.AllowDrop = true;
            this.PTerminados.BackColor = System.Drawing.Color.White;
            this.PTerminados.Location = new System.Drawing.Point(649, 114);
            this.PTerminados.Margin = new System.Windows.Forms.Padding(4, 62, 20, 4);
            this.PTerminados.Name = "PTerminados";
            this.PTerminados.Size = new System.Drawing.Size(288, 645);
            this.PTerminados.TabIndex = 3;
            this.PTerminados.DragDrop += new System.Windows.Forms.DragEventHandler(this.PTerminados_DragDrop);
            this.PTerminados.DragOver += new System.Windows.Forms.DragEventHandler(this.PTerminados_DragOver);
            this.PTerminados.Paint += new System.Windows.Forms.PaintEventHandler(this.PTerminados_Paint);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 18F, System.Drawing.FontStyle.Bold);
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label3.Location = new System.Drawing.Point(693, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(216, 43);
            this.label3.TabIndex = 1;
            this.label3.Text = "Terminados";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PRevisados
            // 
            this.PRevisados.BackColor = System.Drawing.Color.White;
            this.PRevisados.Location = new System.Drawing.Point(971, 114);
            this.PRevisados.Margin = new System.Windows.Forms.Padding(4, 62, 0, 4);
            this.PRevisados.Name = "PRevisados";
            this.PRevisados.Size = new System.Drawing.Size(288, 645);
            this.PRevisados.TabIndex = 4;
            this.PRevisados.DragDrop += new System.Windows.Forms.DragEventHandler(this.PRevisados_DragDrop);
            this.PRevisados.DragOver += new System.Windows.Forms.DragEventHandler(this.PRevisados_DragOver);
            this.PRevisados.Paint += new System.Windows.Forms.PaintEventHandler(this.PRevisados_Paint);
            // 
            // label4
            // 
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 18F, System.Drawing.FontStyle.Bold);
            this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(1025, 66);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(197, 43);
            this.label4.TabIndex = 1;
            this.label4.Text = "Revisados";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Bahnschrift SemiBold SemiConden", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(56, 66);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 43);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pendientes";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PPendientes
            // 
            this.PPendientes.AllowDrop = true;
            this.PPendientes.BackColor = System.Drawing.Color.White;
            this.PPendientes.Location = new System.Drawing.Point(5, 114);
            this.PPendientes.Margin = new System.Windows.Forms.Padding(7, 62, 20, 4);
            this.PPendientes.Name = "PPendientes";
            this.PPendientes.Size = new System.Drawing.Size(288, 645);
            this.PPendientes.TabIndex = 1;
            this.PPendientes.DragDrop += new System.Windows.Forms.DragEventHandler(this.PPendientes_DragDrop);
            this.PPendientes.DragOver += new System.Windows.Forms.DragEventHandler(this.PPendientes_DragOver);
            this.PPendientes.Paint += new System.Windows.Forms.PaintEventHandler(this.PPendientes_Paint);
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(1183, 0);
            this.guna2ControlBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox2.TabIndex = 55;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(1245, 0);
            this.guna2ControlBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox1.TabIndex = 54;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // ScrBarPendientes
            // 
            this.ScrBarPendientes.LargeChange = 10;
            this.ScrBarPendientes.Location = new System.Drawing.Point(296, 129);
            this.ScrBarPendientes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ScrBarPendientes.Maximum = 100;
            this.ScrBarPendientes.Name = "ScrBarPendientes";
            this.ScrBarPendientes.ScrollIdleColor = System.Drawing.Color.Silver;
            this.ScrBarPendientes.Size = new System.Drawing.Size(13, 609);
            this.ScrBarPendientes.TabIndex = 56;
            this.ScrBarPendientes.ThumbColor = System.Drawing.Color.DimGray;
            this.ScrBarPendientes.ThumbHoverColor = System.Drawing.Color.Gray;
            this.ScrBarPendientes.ThumbPressedColor = System.Drawing.Color.DarkGray;
            // 
            // ScrBarProgreso
            // 
            this.ScrBarProgreso.LargeChange = 10;
            this.ScrBarProgreso.Location = new System.Drawing.Point(619, 129);
            this.ScrBarProgreso.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ScrBarProgreso.Maximum = 100;
            this.ScrBarProgreso.Name = "ScrBarProgreso";
            this.ScrBarProgreso.ScrollIdleColor = System.Drawing.Color.Silver;
            this.ScrBarProgreso.Size = new System.Drawing.Size(13, 609);
            this.ScrBarProgreso.TabIndex = 57;
            this.ScrBarProgreso.ThumbColor = System.Drawing.Color.DimGray;
            this.ScrBarProgreso.ThumbHoverColor = System.Drawing.Color.Gray;
            this.ScrBarProgreso.ThumbPressedColor = System.Drawing.Color.DarkGray;
            // 
            // ScrBarTerminados
            // 
            this.ScrBarTerminados.LargeChange = 10;
            this.ScrBarTerminados.Location = new System.Drawing.Point(940, 129);
            this.ScrBarTerminados.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ScrBarTerminados.Maximum = 100;
            this.ScrBarTerminados.Name = "ScrBarTerminados";
            this.ScrBarTerminados.ScrollIdleColor = System.Drawing.Color.Silver;
            this.ScrBarTerminados.Size = new System.Drawing.Size(13, 609);
            this.ScrBarTerminados.TabIndex = 58;
            this.ScrBarTerminados.ThumbColor = System.Drawing.Color.DimGray;
            this.ScrBarTerminados.ThumbHoverColor = System.Drawing.Color.Gray;
            this.ScrBarTerminados.ThumbPressedColor = System.Drawing.Color.DarkGray;
            // 
            // ScrBarRevisados
            // 
            this.ScrBarRevisados.LargeChange = 10;
            this.ScrBarRevisados.Location = new System.Drawing.Point(1263, 129);
            this.ScrBarRevisados.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ScrBarRevisados.Maximum = 100;
            this.ScrBarRevisados.Name = "ScrBarRevisados";
            this.ScrBarRevisados.ScrollIdleColor = System.Drawing.Color.Silver;
            this.ScrBarRevisados.Size = new System.Drawing.Size(13, 609);
            this.ScrBarRevisados.TabIndex = 59;
            this.ScrBarRevisados.ThumbColor = System.Drawing.Color.DimGray;
            this.ScrBarRevisados.ThumbHoverColor = System.Drawing.Color.Gray;
            this.ScrBarRevisados.ThumbPressedColor = System.Drawing.Color.DarkGray;
            // 
            // guna2VSeparator1
            // 
            this.guna2VSeparator1.Location = new System.Drawing.Point(312, 114);
            this.guna2VSeparator1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2VSeparator1.Name = "guna2VSeparator1";
            this.guna2VSeparator1.Size = new System.Drawing.Size(13, 682);
            this.guna2VSeparator1.TabIndex = 60;
            // 
            // guna2VSeparator2
            // 
            this.guna2VSeparator2.Location = new System.Drawing.Point(633, 114);
            this.guna2VSeparator2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2VSeparator2.Name = "guna2VSeparator2";
            this.guna2VSeparator2.Size = new System.Drawing.Size(13, 682);
            this.guna2VSeparator2.TabIndex = 61;
            // 
            // guna2VSeparator3
            // 
            this.guna2VSeparator3.Location = new System.Drawing.Point(955, 118);
            this.guna2VSeparator3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.guna2VSeparator3.Name = "guna2VSeparator3";
            this.guna2VSeparator3.Size = new System.Drawing.Size(13, 682);
            this.guna2VSeparator3.TabIndex = 62;
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this;
            // 
            // FrmAvance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1308, 812);
            this.Controls.Add(this.guna2VSeparator3);
            this.Controls.Add(this.guna2VSeparator2);
            this.Controls.Add(this.guna2VSeparator1);
            this.Controls.Add(this.ScrBarRevisados);
            this.Controls.Add(this.ScrBarTerminados);
            this.Controls.Add(this.ScrBarProgreso);
            this.Controls.Add(this.ScrBarPendientes);
            this.Controls.Add(this.guna2ControlBox2);
            this.Controls.Add(this.guna2ControlBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PPendientes);
            this.Controls.Add(this.PProgreso);
            this.Controls.Add(this.PTerminados);
            this.Controls.Add(this.DgvTareas);
            this.Controls.Add(this.PRevisados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmAvance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tareas";
            this.Activated += new System.EventHandler(this.FrmAvance_Activated);
            this.Load += new System.EventHandler(this.FrmAvance_Load);
            this.Enter += new System.EventHandler(this.FrmAvance_Enter);
            this.Leave += new System.EventHandler(this.FrmAvance_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.DgvTareas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DgvTareas;
        private System.Windows.Forms.FlowLayoutPanel PProgreso;
        private System.Windows.Forms.FlowLayoutPanel PTerminados;
        private System.Windows.Forms.FlowLayoutPanel PRevisados;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel PPendientes;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI.WinForms.GunaVScrollBar ScrBarRevisados;
        private Guna.UI.WinForms.GunaVScrollBar ScrBarTerminados;
        private Guna.UI.WinForms.GunaVScrollBar ScrBarProgreso;
        private Guna.UI.WinForms.GunaVScrollBar ScrBarPendientes;
        private Guna.UI2.WinForms.Guna2VSeparator guna2VSeparator3;
        private Guna.UI2.WinForms.Guna2VSeparator guna2VSeparator2;
        private Guna.UI2.WinForms.Guna2VSeparator guna2VSeparator1;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
    }
}