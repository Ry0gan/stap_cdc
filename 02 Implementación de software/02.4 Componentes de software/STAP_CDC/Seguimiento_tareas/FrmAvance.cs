﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using STAP_CDC.CRUD.CRUD_tareas;
using STAP_CDC.Gestion_proyectos.Equipo_Trabajo;

namespace STAP_CDC.Seguimiento_tareas
{
    public partial class FrmAvance : Form
    {
        private string idPersonal;
        private string idProyecto;
        ControlTarea mControlTarea;
        public FrmAvance(string idPersonal, string idProyecto)
        {

            InitializeComponent();
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
            Guna.UI.Lib.ScrollBar.PanelScrollHelper desplazar = new Guna.UI.Lib.ScrollBar.PanelScrollHelper(PPendientes, ScrBarPendientes, true);
            Guna.UI.Lib.ScrollBar.PanelScrollHelper desplazar1 = new Guna.UI.Lib.ScrollBar.PanelScrollHelper(PProgreso, ScrBarProgreso, true);
            Guna.UI.Lib.ScrollBar.PanelScrollHelper desplazar2 = new Guna.UI.Lib.ScrollBar.PanelScrollHelper(PTerminados, ScrBarTerminados, true);
            Guna.UI.Lib.ScrollBar.PanelScrollHelper desplazar3 = new Guna.UI.Lib.ScrollBar.PanelScrollHelper(PRevisados, ScrBarRevisados, true);
            this.idProyecto = idProyecto;
            this.idPersonal = idPersonal;
            mControlTarea = new ControlTarea();
        }

        private void FrmAvance_Load(object sender, EventArgs e)
        {
            DataSet dataTareas;
            if (Acceso.Global.NivelUsuario == 1)//Es administrador
            {
                dataTareas = mControlTarea.ConsultarTareasAdministrador(idProyecto);

            }
            else //Es usuario normal
            {
                dataTareas = mControlTarea.ConsultarTareas(idProyecto, idPersonal);
            }
                
            //Verificar que haya tareas asignadas al proyecto
            if (dataTareas.Tables[0].Rows.Count>0 ) {
                listar();
            }
            else
            {
                MessageBox.Show(this, "No hay tareas registradas para este proyecto", "Advertencia!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }
        public void listar()
        {
            PRevisados.Controls.Clear();
            PPendientes.Controls.Clear();
            PProgreso.Controls.Clear();
            PTerminados.Controls.Clear();
            DgvTareas.DataSource = new DataSet();

            if (Acceso.Global.NivelUsuario == 1)//Es administrador
            { 
                DgvTareas.DataSource = mControlTarea.ConsultarTareasAdministrador(idProyecto).Tables[0];
                label4.Enabled = true;
                PRevisados.AllowDrop = true;
            }
            else
            {
                DgvTareas.DataSource = mControlTarea.ConsultarTareas(idProyecto, idPersonal).Tables[0];
            }


            CuTarea[] lista = new CuTarea[DgvTareas.RowCount];
            for (int i = 0; i < DgvTareas.RowCount; i++)
            {
                lista[i] = new CuTarea();
                lista[i].Personal = comprobar(lista, DgvTareas.Rows[i].Cells[1].Value.ToString(), DgvTareas.Rows[i].Cells[0].Value.ToString());
                lista[i].Descripcion = DgvTareas.Rows[i].Cells[1].Value.ToString();
                lista[i].Inicio = DgvTareas.Rows[i].Cells[4].Value.ToString().Split(' ')[0];
                lista[i].Fin = DgvTareas.Rows[i].Cells[5].Value.ToString().Split(' ')[0];
                lista[i].idTarea = DgvTareas.Rows[i].Cells[7].Value.ToString();
                lista[i].Inicio_real = DgvTareas.Rows[i].Cells[2].Value.ToString().Split(' ')[0];
                lista[i].Fin_real = DgvTareas.Rows[i].Cells[3].Value.ToString().Split(' ')[0];
                lista[i].diferenciaInicios = DgvTareas.Rows[i].Cells[8].Value.ToString() == "" ? "0" : DgvTareas.Rows[i].Cells[8].Value.ToString();
                lista[i].diferenciaFinales = DgvTareas.Rows[i].Cells[9].Value.ToString() == "" ? "0" : DgvTareas.Rows[i].Cells[9].Value.ToString();
                lista[i].diferenciaInicioHoy = DgvTareas.Rows[i].Cells[10].Value.ToString() == "" ? "0" : DgvTareas.Rows[i].Cells[10].Value.ToString();
                lista[i].diferenciaFinHoy = DgvTareas.Rows[i].Cells[11].Value.ToString() == "" ? "0" : DgvTareas.Rows[i].Cells[11].Value.ToString();
                lista[i].revisado = DgvTareas.Rows[i].Cells[12].Value.ToString();

                //Vinculamos el ControlUser con este formulario
                lista[i].padre = this;

                if (DgvTareas.Rows[i].Cells[12].Value.ToString() == "1")
                {
                    PRevisados.Controls.Add(lista[i]);
                }
                else if (DgvTareas.Rows[i].Cells[3].Value.ToString() != "")
                {
                    PTerminados.Controls.Add(lista[i]);
                }
                else if (DgvTareas.Rows[i].Cells[2].Value.ToString() != "")
                {
                    PProgreso.Controls.Add(lista[i]);
                }
                else
                {
                    PPendientes.Controls.Add(lista[i]);
                }
                lista[i].Mensajes();


            }
            actualizarPaneles();
        }

        public void actualizarPaneles()
        {
            foreach (Control c in this.PPendientes.Controls)
            {
                c.MouseDown += new MouseEventHandler(c_MouseDown);

            }
            foreach (Control c in this.PProgreso.Controls)
            {
                c.MouseDown += new MouseEventHandler(c_MouseDown);
            }
            foreach (Control c in this.PTerminados.Controls)
            {
                c.MouseDown += new MouseEventHandler(c_MouseDown);
            }
            foreach (Control c in this.PRevisados.Controls)
            {
                if (DgvTareas.Rows[0].Cells[6].Value.ToString() == "1")
                {
                    c.MouseDown += new MouseEventHandler(c_MouseDown);
                }
            }
            scrollVerticar(PTerminados);
            scrollVerticar(PPendientes);
            scrollVerticar(PRevisados);
            scrollVerticar(PProgreso);
        }
        //quita el scrool orizontal 
        public void scrollVerticar(Panel mPanel)
        {
            mPanel.AutoScroll = false;
            mPanel.HorizontalScroll.Enabled = false;
            mPanel.HorizontalScroll.Visible = false;
            mPanel.HorizontalScroll.Maximum = 0;
            mPanel.AutoScroll = true;



        }

        void c_MouseDown(object sender, MouseEventArgs e)
        {
            CuTarea c = sender as CuTarea;
            c.DoDragDrop(c, DragDropEffects.Move);
        }



        private void PPendientes_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void PPendientes_DragDrop(object sender, DragEventArgs e)
        {
            CuTarea c = e.Data.GetData(e.Data.GetFormats()[0]) as CuTarea;

            if (c != null)
            {
                try
                {
                    mControlTarea.modificarFechas(c.idTarea);
                    String[] fechas = mControlTarea.obtenerFechas(c.idTarea);
                    c.Inicio_real = fechas[0].Split(' ')[0]; 
                    c.Fin_real = fechas[1].Split(' ')[0];
                    c.revisado = "0";
                    this.PPendientes.Controls.Add(c);
                    // listar();
                    actualizarPaneles();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Error: " + error.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
        }

        private void PProgreso_DragDrop(object sender, DragEventArgs e)
        {
            CuTarea c = e.Data.GetData(e.Data.GetFormats()[0]) as CuTarea;

            if (c != null)
            {
                try
                {
                    mControlTarea.modificarFechas(c.idTarea, DateTime.Now.ToString("yyyy/MM/dd"));
                    String[] fechas = mControlTarea.obtenerFechas(c.idTarea);
                    c.Inicio_real = fechas[0].Split(' ')[0]; 
                    c.Fin_real = fechas[1].Split(' ')[0];
                    c.revisado = "0";
                    this.PProgreso.Controls.Add(c);
                    //  listar();
                    actualizarPaneles();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Error: " + error.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void PProgreso_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void PTerminados_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void PTerminados_DragDrop(object sender, DragEventArgs e)
        {
            CuTarea c = e.Data.GetData(e.Data.GetFormats()[0]) as CuTarea;

            if (c != null)
            {
                try
                {
                    mControlTarea.modificarFechaFinal(c.idTarea, DateTime.Now.ToString("yyyy/MM/dd"));
                    String[] fechas = mControlTarea.obtenerFechas(c.idTarea);
                    c.Inicio_real = fechas[0].Split(' ')[0]; 
                    c.Fin_real = fechas[1].Split(' ')[0];
                    c.revisado = "0";
                    this.PTerminados.Controls.Add(c);
                    //listar();
                    actualizarPaneles();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Error: " + error.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void PRevisados_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void PRevisados_DragDrop(object sender, DragEventArgs e)
        {
            CuTarea c = e.Data.GetData(e.Data.GetFormats()[0]) as CuTarea;

            if (c != null)
            {
                try
                {
                    mControlTarea.agregarRevisado(c.idTarea);
                    String[] fechas = mControlTarea.obtenerFechas(c.idTarea);
                    c.Inicio_real = fechas[0].Split(' ')[0]; ;
                    c.Fin_real = fechas[1].Split(' ')[0]; ;
                    c.revisado = "1";
                    this.PRevisados.Controls.Add(c);
                    //listar();
                    actualizarPaneles();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Error: " + error.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void PPendientes_Paint(object sender, PaintEventArgs e)
        {
            //ControlPaint.DrawBorder(e.Graphics, PPendientes.ClientRectangle,
            //Color.White, 1, ButtonBorderStyle.Solid, // left
            //Color.White, 1, ButtonBorderStyle.Solid, // top
            //Color.FromArgb(192,192,192), 1, ButtonBorderStyle.Solid, // right
            //Color.White, 1, ButtonBorderStyle.Solid);// bottom
        }

        private void PProgreso_Paint(object sender, PaintEventArgs e)
        {
            //ControlPaint.DrawBorder(e.Graphics, PPendientes.ClientRectangle,
            //Color.White, 1, ButtonBorderStyle.Solid, // left
            //Color.White, 1, ButtonBorderStyle.Solid, // top
            //Color.FromArgb(192, 192, 192), 1, ButtonBorderStyle.Solid, // right
            //Color.White, 1, ButtonBorderStyle.Solid);// bottom
        }

        private void PTerminados_Paint(object sender, PaintEventArgs e)
        {
            //ControlPaint.DrawBorder(e.Graphics, PPendientes.ClientRectangle,
            //Color.White, 1, ButtonBorderStyle.Solid, // left
            //Color.White, 1, ButtonBorderStyle.Solid, // top
            //Color.FromArgb(192, 192, 192), 1, ButtonBorderStyle.Outset, // right
            //Color.White, 1, ButtonBorderStyle.Solid);// bottomc
        }

        private void PRevisados_Paint(object sender, PaintEventArgs e)
        {
        //    ControlPaint.DrawBorder(e.Graphics, PPendientes.ClientRectangle,
        //    Color.White, 1, ButtonBorderStyle.Solid, // left
        //    Color.White, 1, ButtonBorderStyle.Solid, // top
        //    Color.FromArgb(192, 192, 192), 1, ButtonBorderStyle.Dashed, // right
        //    Color.White, 1, ButtonBorderStyle.Solid);// bottom
        }

        private void FrmAvance_Activated(object sender, EventArgs e)
        {
            //listar();
        }

        public string comprobar(CuTarea[] lista, string nombre, string personal)
        {
            string resultado = "";

            for (int i = 0; i < lista.Count(); i++)
            {
                if (lista[i] != null)
                {
                    if (lista[i].descripcion == nombre && lista[i].personal != personal)
                    {
                        lista[i].Hide();
                    }
                }
            }

            for (int i = 0; i < DgvTareas.RowCount; i++)
            {

                if (DgvTareas.Rows[i].Cells[1].Value.ToString() == nombre)
                {
                    resultado += DgvTareas.Rows[i].Cells[0].Value.ToString() + " ";
                }
            }
            return resultado;

        }

        private void FrmAvance_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmAvance_Enter(object sender, EventArgs e) {
           Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
    }
}
