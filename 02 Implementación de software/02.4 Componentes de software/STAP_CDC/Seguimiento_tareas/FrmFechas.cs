﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using STAP_CDC.CRUD.CRUD_tareas;

namespace STAP_CDC.Seguimiento_tareas
{
    public partial class FrmFechas : Form
    {
        private string idTarea;
        private string revisado;

        public FrmFechas()
        {
            InitializeComponent();
            //Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }
        public void setFechas(string inicio,string fin,string idTarea,string revisado)
        {
            this.idTarea = idTarea;
            this.revisado = revisado;



            if (inicio != "")
            {
                string[] Inicio = inicio.Split('/');
                DtpInicio.Value = new DateTime(int.Parse(Inicio[2]), int.Parse(Inicio[1]), int.Parse(Inicio[0]));
            }
            if (fin != "")
            {
                string[] Fin = fin.Split('/');
                DtpFin.Value = new DateTime(int.Parse(Fin[2]), int.Parse(Fin[1]), int.Parse(Fin[0]));
            }

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            
            
            try
            {
                string inicio = CbInicio.Checked != true ? DtpInicio.Value.Year + "/" + DtpInicio.Value.Month + "/" + DtpInicio.Value.Day : "null";
                string fin = CbFin.Checked != true ? DtpFin.Value.Year + "/" + DtpFin.Value.Month + "/" + DtpFin.Value.Day : "null";

                if (inicio.Equals("null") && !fin.Equals("null"))
                {
                    CbInicio.Checked = false;
                    MessageBox.Show("No debe quitar la fecha de inicio sin quitar la fecha de fin", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    ControlTarea mControlTarea = new ControlTarea();
                    if (inicio.Equals("null") || fin.Equals("null"))
                        revisado = "0";
                    mControlTarea.modificarFechas(idTarea, inicio, fin, revisado);

                    MessageBox.Show("Fechas modificadas", "Completo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Hide();
                }
                
            }
            catch (Exception error)
            {
                MessageBox.Show("Error: " + error.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

       

        private void CbFin_CheckedChanged(object sender, EventArgs e)
        {

            if (CbFin.Checked != true)
            {
                DtpFin.Enabled = true;
            }
            else
            {
                DtpFin.Enabled = false;
            }
        }

        private void BtnCanelar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void CbInicio_CheckedChanged(object sender, EventArgs e)
        {
            
            if (!CbInicio.Checked)
            {
                DtpInicio.Enabled = true;
            }
            else
            {
                DtpInicio.Enabled = false;
            }
            
            
        }

        private void FrmFechas_Leave(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void FrmFechas_Enter(object sender, EventArgs e) {
            Guna.UI.Lib.GraphicsHelper.ShadowForm(this);
        }

        private void DtpFin_ValueChanged(object sender, EventArgs e) {

        }
    }
}
