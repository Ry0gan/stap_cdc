﻿namespace STAP_CDC.Seguimiento_tareas
{
    partial class CuTarea
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LblDescripcion = new System.Windows.Forms.Label();
            this.LblInicioPlan = new System.Windows.Forms.Label();
            this.LblFinPlan = new System.Windows.Forms.Label();
            this.LblPersonal = new System.Windows.Forms.Label();
            this.CmsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.asignarFechasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PbRetrasoInicio = new System.Windows.Forms.PictureBox();
            this.BtnEditar = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TtMensaje = new System.Windows.Forms.ToolTip(this.components);
            this.PbRetrasoFin = new System.Windows.Forms.PictureBox();
            this.LblFinReal = new System.Windows.Forms.Label();
            this.LblInicioReal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CmsMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbRetrasoInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnEditar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbRetrasoFin)).BeginInit();
            this.SuspendLayout();
            // 
            // LblDescripcion
            // 
            this.LblDescripcion.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDescripcion.ForeColor = System.Drawing.Color.White;
            this.LblDescripcion.Location = new System.Drawing.Point(37, 2);
            this.LblDescripcion.Name = "LblDescripcion";
            this.LblDescripcion.Size = new System.Drawing.Size(157, 36);
            this.LblDescripcion.TabIndex = 0;
            this.LblDescripcion.Text = "label1";
            // 
            // LblInicioPlan
            // 
            this.LblInicioPlan.AutoSize = true;
            this.LblInicioPlan.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInicioPlan.ForeColor = System.Drawing.Color.White;
            this.LblInicioPlan.Location = new System.Drawing.Point(35, 41);
            this.LblInicioPlan.Name = "LblInicioPlan";
            this.LblInicioPlan.Size = new System.Drawing.Size(72, 14);
            this.LblInicioPlan.TabIndex = 1;
            this.LblInicioPlan.Text = "LblInicioPlan";
            // 
            // LblFinPlan
            // 
            this.LblFinPlan.AutoSize = true;
            this.LblFinPlan.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFinPlan.ForeColor = System.Drawing.Color.White;
            this.LblFinPlan.Location = new System.Drawing.Point(104, 41);
            this.LblFinPlan.Name = "LblFinPlan";
            this.LblFinPlan.Size = new System.Drawing.Size(60, 14);
            this.LblFinPlan.TabIndex = 2;
            this.LblFinPlan.Text = "LblFinPlan";
            // 
            // LblPersonal
            // 
            this.LblPersonal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.LblPersonal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPersonal.ForeColor = System.Drawing.Color.White;
            this.LblPersonal.Location = new System.Drawing.Point(50, 86);
            this.LblPersonal.Name = "LblPersonal";
            this.LblPersonal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.LblPersonal.Size = new System.Drawing.Size(141, 38);
            this.LblPersonal.TabIndex = 3;
            this.LblPersonal.Text = "label3";
            // 
            // CmsMenu
            // 
            this.CmsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asignarFechasToolStripMenuItem});
            this.CmsMenu.Name = "CmsMenu";
            this.CmsMenu.Size = new System.Drawing.Size(152, 26);
            // 
            // asignarFechasToolStripMenuItem
            // 
            this.asignarFechasToolStripMenuItem.Name = "asignarFechasToolStripMenuItem";
            this.asignarFechasToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.asignarFechasToolStripMenuItem.Text = "Asignar fechas";
            this.asignarFechasToolStripMenuItem.Click += new System.EventHandler(this.asignarFechasToolStripMenuItem_Click);
            // 
            // PbRetrasoInicio
            // 
            this.PbRetrasoInicio.Image = global::STAP_CDC.Properties.Resources.hora;
            this.PbRetrasoInicio.Location = new System.Drawing.Point(169, 39);
            this.PbRetrasoInicio.Name = "PbRetrasoInicio";
            this.PbRetrasoInicio.Size = new System.Drawing.Size(21, 20);
            this.PbRetrasoInicio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbRetrasoInicio.TabIndex = 7;
            this.PbRetrasoInicio.TabStop = false;
            this.PbRetrasoInicio.Visible = false;
            this.PbRetrasoInicio.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbRetraso_MouseClick);
            // 
            // BtnEditar
            // 
            this.BtnEditar.Image = global::STAP_CDC.Properties.Resources.edit;
            this.BtnEditar.Location = new System.Drawing.Point(2, 91);
            this.BtnEditar.Name = "BtnEditar";
            this.BtnEditar.Size = new System.Drawing.Size(30, 30);
            this.BtnEditar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnEditar.TabIndex = 6;
            this.BtnEditar.TabStop = false;
            this.BtnEditar.Click += new System.EventHandler(this.BtnEditar_Click);
            this.BtnEditar.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BtnEditar_MouseClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Image = global::STAP_CDC.Properties.Resources.mover;
            this.pictureBox1.Location = new System.Drawing.Point(3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // PbRetrasoFin
            // 
            this.PbRetrasoFin.Image = global::STAP_CDC.Properties.Resources.hora;
            this.PbRetrasoFin.Location = new System.Drawing.Point(169, 64);
            this.PbRetrasoFin.Name = "PbRetrasoFin";
            this.PbRetrasoFin.Size = new System.Drawing.Size(21, 20);
            this.PbRetrasoFin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PbRetrasoFin.TabIndex = 8;
            this.PbRetrasoFin.TabStop = false;
            this.PbRetrasoFin.Visible = false;
            // 
            // LblFinReal
            // 
            this.LblFinReal.AutoSize = true;
            this.LblFinReal.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFinReal.ForeColor = System.Drawing.Color.White;
            this.LblFinReal.Location = new System.Drawing.Point(104, 64);
            this.LblFinReal.Name = "LblFinReal";
            this.LblFinReal.Size = new System.Drawing.Size(60, 14);
            this.LblFinReal.TabIndex = 10;
            this.LblFinReal.Text = "LblFinReal";
            // 
            // LblInicioReal
            // 
            this.LblInicioReal.AutoSize = true;
            this.LblInicioReal.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblInicioReal.ForeColor = System.Drawing.Color.White;
            this.LblInicioReal.Location = new System.Drawing.Point(36, 64);
            this.LblInicioReal.Name = "LblInicioReal";
            this.LblInicioReal.Size = new System.Drawing.Size(72, 14);
            this.LblInicioReal.TabIndex = 9;
            this.LblInicioReal.Text = "LblInicioReal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Plan:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(4, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Real:";
            // 
            // CuTarea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.ContextMenuStrip = this.CmsMenu;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LblFinReal);
            this.Controls.Add(this.LblInicioReal);
            this.Controls.Add(this.PbRetrasoFin);
            this.Controls.Add(this.PbRetrasoInicio);
            this.Controls.Add(this.BtnEditar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LblPersonal);
            this.Controls.Add(this.LblFinPlan);
            this.Controls.Add(this.LblInicioPlan);
            this.Controls.Add(this.LblDescripcion);
            this.Name = "CuTarea";
            this.Size = new System.Drawing.Size(195, 124);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CuTarea_MouseClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.CuTarea_MouseDoubleClick);
            this.CmsMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PbRetrasoInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnEditar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbRetrasoFin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblDescripcion;
        private System.Windows.Forms.Label LblInicioPlan;
        private System.Windows.Forms.Label LblFinPlan;
        private System.Windows.Forms.Label LblPersonal;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip CmsMenu;
        private System.Windows.Forms.ToolStripMenuItem asignarFechasToolStripMenuItem;
        private System.Windows.Forms.PictureBox BtnEditar;
        private System.Windows.Forms.PictureBox PbRetrasoInicio;
        private System.Windows.Forms.ToolTip TtMensaje;
        private System.Windows.Forms.PictureBox PbRetrasoFin;
        private System.Windows.Forms.Label LblFinReal;
        private System.Windows.Forms.Label LblInicioReal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
