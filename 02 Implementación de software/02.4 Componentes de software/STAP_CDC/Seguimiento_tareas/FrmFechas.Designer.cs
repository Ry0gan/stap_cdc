﻿namespace STAP_CDC.Seguimiento_tareas
{
    partial class FrmFechas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFechas));
            this.DtpInicio = new System.Windows.Forms.DateTimePicker();
            this.DtpFin = new System.Windows.Forms.DateTimePicker();
            this.CbInicio = new System.Windows.Forms.CheckBox();
            this.CbFin = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.DragControl = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.BtnCanelar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnGuardar = new Guna.UI2.WinForms.Guna2Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DtpInicio
            // 
            this.DtpInicio.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpInicio.Location = new System.Drawing.Point(155, 92);
            this.DtpInicio.Name = "DtpInicio";
            this.DtpInicio.Size = new System.Drawing.Size(200, 21);
            this.DtpInicio.TabIndex = 0;
            // 
            // DtpFin
            // 
            this.DtpFin.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F);
            this.DtpFin.Location = new System.Drawing.Point(155, 147);
            this.DtpFin.Name = "DtpFin";
            this.DtpFin.Size = new System.Drawing.Size(200, 21);
            this.DtpFin.TabIndex = 1;
            this.DtpFin.ValueChanged += new System.EventHandler(this.DtpFin_ValueChanged);
            // 
            // CbInicio
            // 
            this.CbInicio.AutoSize = true;
            this.CbInicio.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F);
            this.CbInicio.Location = new System.Drawing.Point(366, 94);
            this.CbInicio.Name = "CbInicio";
            this.CbInicio.Size = new System.Drawing.Size(73, 18);
            this.CbInicio.TabIndex = 2;
            this.CbInicio.Text = "Sin fecha";
            this.CbInicio.UseVisualStyleBackColor = true;
            this.CbInicio.CheckedChanged += new System.EventHandler(this.CbInicio_CheckedChanged);
            // 
            // CbFin
            // 
            this.CbFin.AutoSize = true;
            this.CbFin.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F);
            this.CbFin.Location = new System.Drawing.Point(366, 150);
            this.CbFin.Name = "CbFin";
            this.CbFin.Size = new System.Drawing.Size(73, 18);
            this.CbFin.TabIndex = 3;
            this.CbFin.Text = "Sin fecha";
            this.CbFin.UseVisualStyleBackColor = true;
            this.CbFin.CheckedChanged += new System.EventHandler(this.CbFin_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Tai Le", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 27);
            this.label1.TabIndex = 44;
            this.label1.Text = "Fechas reales";
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(427, 0);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox2.TabIndex = 55;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(474, 0);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(45, 29);
            this.guna2ControlBox1.TabIndex = 54;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 5;
            this.guna2Elipse1.TargetControl = this;
            // 
            // DragControl
            // 
            this.DragControl.TargetControl = this;
            // 
            // BtnCanelar
            // 
            this.BtnCanelar.Animated = true;
            this.BtnCanelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCanelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.BorderRadius = 15;
            this.BtnCanelar.BorderThickness = 1;
            this.BtnCanelar.CheckedState.Parent = this.BtnCanelar;
            this.BtnCanelar.CustomImages.Parent = this.BtnCanelar;
            this.BtnCanelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCanelar.ForeColor = System.Drawing.Color.White;
            this.BtnCanelar.HoverState.Parent = this.BtnCanelar;
            this.BtnCanelar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCanelar.Image")));
            this.BtnCanelar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnCanelar.Location = new System.Drawing.Point(217, 223);
            this.BtnCanelar.Name = "BtnCanelar";
            this.BtnCanelar.ShadowDecoration.Parent = this.BtnCanelar;
            this.BtnCanelar.Size = new System.Drawing.Size(124, 40);
            this.BtnCanelar.TabIndex = 43;
            this.BtnCanelar.Text = "Cancelar";
            this.BtnCanelar.UseTransparentBackground = true;
            this.BtnCanelar.Click += new System.EventHandler(this.BtnCanelar_Click);
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Animated = true;
            this.BtnGuardar.BackColor = System.Drawing.Color.Transparent;
            this.BtnGuardar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.BorderRadius = 15;
            this.BtnGuardar.BorderThickness = 1;
            this.BtnGuardar.CheckedState.Parent = this.BtnGuardar;
            this.BtnGuardar.CustomImages.Parent = this.BtnGuardar;
            this.BtnGuardar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuardar.ForeColor = System.Drawing.Color.White;
            this.BtnGuardar.HoverState.Parent = this.BtnGuardar;
            this.BtnGuardar.Image = global::STAP_CDC.Properties.Resources.floppy_disk;
            this.BtnGuardar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnGuardar.Location = new System.Drawing.Point(59, 223);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.ShadowDecoration.Parent = this.BtnGuardar;
            this.BtnGuardar.Size = new System.Drawing.Size(124, 40);
            this.BtnGuardar.TabIndex = 42;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseTransparentBackground = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.White;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(45, 88);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 24);
            this.label24.TabIndex = 56;
            this.label24.Text = "Fecha inicio";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(64, 145);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 24);
            this.label2.TabIndex = 57;
            this.label2.Text = "Fecha fin";
            // 
            // FrmFechas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(520, 293);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.guna2ControlBox2);
            this.Controls.Add(this.guna2ControlBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnCanelar);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.CbFin);
            this.Controls.Add(this.CbInicio);
            this.Controls.Add(this.DtpFin);
            this.Controls.Add(this.DtpInicio);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmFechas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configurar Fechas";
            this.Enter += new System.EventHandler(this.FrmFechas_Enter);
            this.Leave += new System.EventHandler(this.FrmFechas_Leave);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DtpInicio;
        private System.Windows.Forms.DateTimePicker DtpFin;
        private System.Windows.Forms.CheckBox CbInicio;
        private System.Windows.Forms.CheckBox CbFin;
        private Guna.UI2.WinForms.Guna2Button BtnGuardar;
        private Guna.UI2.WinForms.Guna2Button BtnCanelar;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DragControl DragControl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label24;
    }
}