﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace STAP_CDC.Seguimiento_tareas
{

    public partial class CuTarea : UserControl
    {
        public string descripcion;
        public string inicio;
        public string fin;
        public string personal;
        public string idTarea;
        public string inicio_real;
        public string fin_real;
        public string diferenciaInicios="0";
        public string diferenciaFinales = "0";
        public string diferenciaInicioHoy = "0";
        public string diferenciaFinHoy = "0";
        public string mensajeFin = "";
        public string mensajeInicio = "";
        public string revisado = "";
        //Vinculo con el formulario padre para actualizar la lista de tareas despues de la modificación manual de fechas
        public FrmAvance padre { get; set; }

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
(
        int nLeftRect, // x-coordinate of upper-left corner
        int nTopRect, // y-coordinate of upper-left corner
        int nRightRect, // x-coordinate of lower-right corner
        int nBottomRect, // y-coordinate of lower-right corner
        int nWidthEllipse, // height of ellipse
        int nHeightEllipse // width of ellipse
);
        public CuTarea()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 10, 10)); 
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; LblDescripcion.Text = value; }
        }
        public string Inicio
        {
            get { return inicio; }
            set { inicio = value; LblInicioPlan.Text ="" + value; }
        }
        public string Fin
        {
            get { return fin; }
            set { fin = value; LblFinPlan.Text ="" + value; }
        }
        public string Personal
        {
            get { return personal; }
            set { personal = value; LblPersonal.Text = value; }
        }
        public string IdTarea
        {
            get { return idTarea; }
            set { idTarea = value;}
        }
        public string Inicio_real
        {
            get { return inicio_real; }
            set { inicio_real = value; LblInicioReal.Text = value.Equals("") ? "N/A": value; }
        }
        public string Fin_real
        {
            get { return fin_real; }
            set { fin_real = value; LblFinReal.Text = value.Equals("") ? "N/A" : value; }
        }
        public string DiferenciaInicios
        {
            get { return diferenciaInicios; }
            set { diferenciaInicios = value; }
        }
        public string DiferenciaFinales
        {
            get { return diferenciaFinales; }
            set { diferenciaFinales = value; }
        }
        public string DiferenciaInicioHoy
        {
            get { return diferenciaInicioHoy; }
            set { diferenciaInicioHoy = value; }
        }
            public string DiferenciaFinHoy
        {
            get { return diferenciaFinHoy; }
            set { diferenciaFinHoy = value; }
        }
    

        private void CuTarea_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void asignarFechasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFechas mFrmFechas = new FrmFechas();
            mFrmFechas.setFechas(inicio_real, fin_real, IdTarea,revisado);
            mFrmFechas.ShowDialog();
            padre.listar();
            
        }

        private void CuTarea_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            
        }

        private void BtnEditar_MouseClick(object sender, MouseEventArgs e)
        {

            CmsMenu.Show(this, 10, 90);

        }

        private void PbRetraso_MouseClick(object sender, MouseEventArgs e)
        {
           
        }
        public void Mensajes()
        {
            if (int.Parse(diferenciaFinHoy) < 0)
            {
                mensajeFin = "El fin de esta retrasado por " +  Math.Abs(int.Parse(diferenciaFinHoy)) + " Dias";
                PbRetrasoFin.Visible = true;
            }
            if (int.Parse(diferenciaInicioHoy) < 0)
            {
                mensajeInicio += "El inicio del proyecto esta retrasado por: " + Math.Abs(int.Parse(diferenciaInicioHoy)) + " Dias";
                PbRetrasoInicio.Visible = true;
            }

            if (int.Parse(diferenciaInicios) < 0)
            {
                mensajeInicio += "\n\nEsta tarea debia iniciar el " + inicio + " pero inicio el " + inicio_real ;
                PbRetrasoInicio.Visible = true;
            }

            if (int.Parse(diferenciaFinales) < 0)
            {
                mensajeFin += "\n\nEsta tarea debia terminar el " + fin + " pero termino el " + fin_real;
                PbRetrasoFin.Visible = true;
            }


            TtMensaje.SetToolTip(this.PbRetrasoInicio, mensajeInicio);
            TtMensaje.SetToolTip(this.PbRetrasoFin, mensajeFin);
        }

        private void BtnEditar_Click(object sender, EventArgs e) {

        }
    }
}
