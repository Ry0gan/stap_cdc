﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace STAP_CDC.Acceso
{
    public partial class FrmConfigConex : Form
    {
        string server = "";
        string BD = "";
        public FrmConfigConex()
        {
            InitializeComponent();
            TxtIP.Select();
        }

        private void FrmConfigConex_Load(object sender, EventArgs e)
        {
            string conexion = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
            char[] Servidor;
            
            Servidor = conexion.ToCharArray();
            int posicion;
            posicion = conexion.LastIndexOf("SERVER=");
            for (int i = posicion+7; i < Servidor.Length; i++)
            {
                if(Servidor[i] == ';')
                {
                    break;
                }
                server = server + Servidor[i];
            }


            int posicion2;
            posicion2 = conexion.LastIndexOf("DATABASE=");
            for (int i = posicion2 + 9; i < Servidor.Length; i++)
            {
                if (Servidor[i] == ';')
                {
                    break;
                }
                BD = BD + Servidor[i];
            }

            TxtIP.Text = server;
            TxtDataBase.Text = BD;
        }

        private void BtnCambiar_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            
        }

        private void FrmConfigConex_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((TxtIP.Text.Length > 0) && (TxtDataBase.Text.Length > 0)
                            && (e.KeyChar == (char)Keys.Enter))
            {
                BtnCambiar.PerformClick();
            }
        }

        private void BtnCambiar_MouseClick(object sender, MouseEventArgs e)
        {
            if (TxtIP.Text.Length > 0 && TxtDataBase.Text.Length > 0)
            {
                string NuevoServer = TxtIP.Text.Trim();
                string NuevaBD = TxtDataBase.Text.Trim();
                string NuevaConfig = "DRIVER={MySQL ODBC 5.2w Driver};SERVER=\"" + NuevoServer + "\";DATABASE=\"" + NuevaBD + "\";UID=stap_dba;PWD=P455w0rd;";
                NuevaConfig = NuevaConfig.Replace("\"", string.Empty);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

                foreach (XmlElement element in xmlDoc.DocumentElement)
                {
                    if (element.Name.Equals("connectionStrings"))
                    {
                        foreach (XmlNode node in element.ChildNodes)
                        {
                            node.Attributes[1].Value = NuevaConfig;
                        }
                    }
                }
                xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                ConfigurationManager.RefreshSection("connectionStrings");
                MessageBox.Show(this, "Conexión Modificada", "Exito!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show(this, "Ambos campos deben de estar llenos para modificar la conexión", "Error!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TxtIP.Select();
            }
        }

        private void BtnCanelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
