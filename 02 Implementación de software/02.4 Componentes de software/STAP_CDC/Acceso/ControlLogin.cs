﻿using AccesoDatos;
using CRUD_personal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC.Acceso
{
    class ControlLogin
    {
        Conexion mConexion;
        static public bool comprobar = false;
        public ControlLogin()
        {
            mConexion = new Conexion();
            try {
                mConexion.conectar();
                comprobar = true;
            } catch
            {
                comprobar = false;
            }
        }

        public bool acceder(string usuario, string contrasena)
        {
            string SQL = "select personal.idPersonal, personal.iniciales, usuario.contrasena, usuario.tipo from personal inner join usuario on personal.idPersonal = usuario.idPersonal where usuario.contrasena = BINARY '?c' and personal.iniciales = BINARY '?u' and personal.visibilidad = 1; ";
            SQL = SQL.Replace("?u", usuario);
            SQL = SQL.Replace("?c", contrasena);
            //Obtener idPersonal para usarlo en otros formularios
            DataSet res=mConexion.ejecutarConsulta(SQL);
            foreach (DataRow fila in res.Tables[0].Rows)
            {
                Global.idPersonal = Convert.ToInt32(fila["idPersonal"]);
                Global.NivelUsuario = Convert.ToInt32(fila["tipo"]);
                return true;
            }
            return false;
            //return (((DataTable)mConexion.ejecutarConsulta(SQL).Tables[0]).Rows.Count >= 1) ? true : false;
        }
        public string nombreUsuario(string usuario, string contrasena)
        {
            string SQL = "select personal.nombre_completo from personal inner join usuario on personal.idPersonal = usuario.idPersonal where usuario.contrasena = BINARY '?c' and personal.iniciales = BINARY '?u' and personal.visibilidad = 1;";
            SQL = SQL.Replace("?u", usuario);
            SQL = SQL.Replace("?c", contrasena);

            return mConexion.ejecutarConsulta(SQL).Tables[0].Rows[0]["nombre_completo"].ToString();
        }

        public bool verificar()
        {
            string SQL = "select * from usuario inner join personal on usuario.idPersonal = personal.idPersonal where personal.visibilidad = 1 and usuario.tipo = 1;";

            return ((DataTable)mConexion.ejecutarConsulta(SQL).Tables[0]).Rows.Count >=1 ? true : false;
        }

        public bool comprobarUsuario(string usuario, string contrasena) {
            string SQL = "select personal.idPersonal, personal.iniciales, usuario.contrasena, usuario.tipo from personal inner join usuario on personal.idPersonal = usuario.idPersonal where usuario.contrasena = BINARY '?c' and personal.iniciales = BINARY '?u' and personal.visibilidad = 1; ";
            SQL = SQL.Replace("?u", usuario);
            SQL = SQL.Replace("?c", contrasena);
            
            return (((DataTable)mConexion.ejecutarConsulta(SQL).Tables[0]).Rows.Count >= 1) ? true : false;
        }

    }
}
