﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace STAP_CDC
{
    public partial class FrmLoading : Form
    {
        public FrmLoading(string usuario)
        {
            InitializeComponent();
            LblUsuario.Text = LblUsuario.Text = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(usuario.ToLower());
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (ProgressBar.Value == 100)
            {
                Timer.Stop();
                this.Close();
            }
            else
            {
                ProgressBar.Value += 1;
            }
        }

        private void FrmLoading_Load(object sender, EventArgs e)
        {
            guna2ShadowForm1.SetShadowForm(this);
            Timer.Start();
        }
    }
}
