﻿namespace STAP_CDC.Acceso
{
    partial class FrmConfigConex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConfigConex));
            this.TxtIP = new Guna.UI2.WinForms.Guna2TextBox();
            this.TxtDataBase = new Guna.UI2.WinForms.Guna2TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnCanelar = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.BtnCambiar = new Guna.UI2.WinForms.Guna2Button();
            this.SuspendLayout();
            // 
            // TxtIP
            // 
            this.TxtIP.Animated = true;
            this.TxtIP.BackColor = System.Drawing.Color.White;
            this.TxtIP.BorderRadius = 10;
            this.TxtIP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtIP.DefaultText = "";
            this.TxtIP.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtIP.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtIP.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIP.DisabledState.Parent = this.TxtIP;
            this.TxtIP.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIP.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIP.FocusedState.Parent = this.TxtIP;
            this.TxtIP.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIP.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIP.HoverState.Parent = this.TxtIP;
            this.TxtIP.Location = new System.Drawing.Point(146, 49);
            this.TxtIP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIP.Name = "TxtIP";
            this.TxtIP.PasswordChar = '\0';
            this.TxtIP.PlaceholderText = "";
            this.TxtIP.SelectedText = "";
            this.TxtIP.ShadowDecoration.Parent = this.TxtIP;
            this.TxtIP.Size = new System.Drawing.Size(284, 44);
            this.TxtIP.TabIndex = 6;
            // 
            // TxtDataBase
            // 
            this.TxtDataBase.Animated = true;
            this.TxtDataBase.BackColor = System.Drawing.Color.White;
            this.TxtDataBase.BorderRadius = 10;
            this.TxtDataBase.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtDataBase.DefaultText = "";
            this.TxtDataBase.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtDataBase.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtDataBase.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtDataBase.DisabledState.Parent = this.TxtDataBase;
            this.TxtDataBase.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtDataBase.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtDataBase.FocusedState.Parent = this.TxtDataBase;
            this.TxtDataBase.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDataBase.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtDataBase.HoverState.Parent = this.TxtDataBase;
            this.TxtDataBase.Location = new System.Drawing.Point(146, 103);
            this.TxtDataBase.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDataBase.Name = "TxtDataBase";
            this.TxtDataBase.PasswordChar = '\0';
            this.TxtDataBase.PlaceholderText = "";
            this.TxtDataBase.SelectedText = "";
            this.TxtDataBase.ShadowDecoration.Parent = this.TxtDataBase;
            this.TxtDataBase.Size = new System.Drawing.Size(284, 44);
            this.TxtDataBase.TabIndex = 7;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(39, 59);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(89, 22);
            this.label24.TabIndex = 67;
            this.label24.Text = "Servidor:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(52, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 22);
            this.label1.TabIndex = 68;
            this.label1.Text = "BD:";
            // 
            // BtnCanelar
            // 
            this.BtnCanelar.Animated = true;
            this.BtnCanelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCanelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.BorderRadius = 15;
            this.BtnCanelar.BorderThickness = 1;
            this.BtnCanelar.CheckedState.Parent = this.BtnCanelar;
            this.BtnCanelar.CustomImages.Parent = this.BtnCanelar;
            this.BtnCanelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCanelar.ForeColor = System.Drawing.Color.White;
            this.BtnCanelar.HoverState.Parent = this.BtnCanelar;
            this.BtnCanelar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCanelar.Image")));
            this.BtnCanelar.ImageSize = new System.Drawing.Size(25, 25);
            this.BtnCanelar.Location = new System.Drawing.Point(268, 156);
            this.BtnCanelar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCanelar.Name = "BtnCanelar";
            this.BtnCanelar.ShadowDecoration.Parent = this.BtnCanelar;
            this.BtnCanelar.Size = new System.Drawing.Size(139, 49);
            this.BtnCanelar.TabIndex = 69;
            this.BtnCanelar.Text = "Cancelar";
            this.BtnCanelar.UseTransparentBackground = true;
            this.BtnCanelar.Click += new System.EventHandler(this.BtnCanelar_Click);
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.Location = new System.Drawing.Point(460, -1);
            this.guna2ControlBox1.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(45, 36);
            this.guna2ControlBox1.TabIndex = 70;
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.BorderColor = System.Drawing.Color.White;
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(417, -1);
            this.guna2ControlBox2.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(35, 36);
            this.guna2ControlBox2.TabIndex = 71;
            // 
            // BtnCambiar
            // 
            this.BtnCambiar.Animated = true;
            this.BtnCambiar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCambiar.BorderColor = System.Drawing.Color.DarkViolet;
            this.BtnCambiar.BorderRadius = 15;
            this.BtnCambiar.BorderThickness = 1;
            this.BtnCambiar.CheckedState.Parent = this.BtnCambiar;
            this.BtnCambiar.CustomImages.Parent = this.BtnCambiar;
            this.BtnCambiar.FillColor = System.Drawing.Color.DarkViolet;
            this.BtnCambiar.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCambiar.ForeColor = System.Drawing.Color.White;
            this.BtnCambiar.HoverState.Parent = this.BtnCambiar;
            this.BtnCambiar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCambiar.Image")));
            this.BtnCambiar.ImageSize = new System.Drawing.Size(25, 25);
            this.BtnCambiar.Location = new System.Drawing.Point(122, 156);
            this.BtnCambiar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCambiar.Name = "BtnCambiar";
            this.BtnCambiar.ShadowDecoration.Parent = this.BtnCambiar;
            this.BtnCambiar.Size = new System.Drawing.Size(138, 49);
            this.BtnCambiar.TabIndex = 72;
            this.BtnCambiar.Text = "Modificar";
            this.BtnCambiar.UseTransparentBackground = true;
            this.BtnCambiar.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BtnCambiar_MouseClick);
            // 
            // FrmConfigConex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(502, 218);
            this.Controls.Add(this.BtnCambiar);
            this.Controls.Add(this.guna2ControlBox2);
            this.Controls.Add(this.guna2ControlBox1);
            this.Controls.Add(this.BtnCanelar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.TxtDataBase);
            this.Controls.Add(this.TxtIP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmConfigConex";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmConfigConex";
            this.Load += new System.EventHandler(this.FrmConfigConex_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmConfigConex_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI2.WinForms.Guna2TextBox TxtIP;
        private Guna.UI2.WinForms.Guna2TextBox TxtDataBase;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button BtnCanelar;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2Button BtnCambiar;
    }
}