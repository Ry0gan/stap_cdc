﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using CRUD_personal;
using STAP_CDC.Acceso;
using STAP_CDC.CRUD;
using STAP_CDC.CRUD.CRUD_personal;

namespace STAP_CDC.Acceso
{
    public partial class FrmLogin : Form
    {
       ControlLogin mControlLogin;
        public FrmLogin()
        {
            InitializeComponent();
            TxtPass.PasswordChar = '\u25CF';
            TxtNewPass.PasswordChar = '\u25CF';
            TxtConfirmNewPass.PasswordChar = '\u25CF';
            Bitmap bmp = new Bitmap(Properties.Resources.favicon);
            this.Icon = Icon.FromHandle(bmp.GetHicon());
        }

        private void BtnAcceder_Click(object sender, EventArgs e)
        {
            if (TxtUsuario.Text.Trim() != "")
            {
                if (TxtPass.Text.Trim() != "")
                {
                    try
                    {
                        
                        ControlLogin mControlLogin = new ControlLogin();
                        if(ControlLogin.comprobar)
                        {
                            verificar();
                            ControlLogin.comprobar = false;
                        }
                        else
                        {
                            MessageBox.Show("ERROR al conectarse a la base de datos, verifique su configuración. \n\nSi el problema persiste por favor de comuníquese con el administrador.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        if (mControlLogin.acceder(TxtUsuario.Text, TxtPass.Text))
                        {
                            FrmLoading mFrmBienvenida = new FrmLoading(mControlLogin.nombreUsuario(TxtUsuario.Text, TxtPass.Text));
                            mFrmBienvenida.ShowDialog();
                            this.Hide();
                            

                            FrmPrincipal mFrmPrincipal = new FrmPrincipal();
                            mFrmPrincipal.ShowDialog();
                            this.Close();
                            mFrmPrincipal.FormClosed += cerrarSesion;
                            
                        } else
                        {
                            mensajeError("Usuario y/o contraseña incorrectos, inténtelo de nuevo."); ;
                            TxtUsuario.Focus();
                        }
                    }
                    catch
                    {
                    
                    }
                }
                else
                {
                    mensajeError("Por favor ingrese la contraseña...");
                    TxtPass.Focus();
                }
            }
            else
            {
                mensajeError("Por favor ingrese el usuario...");
                TxtUsuario.Focus();
            }
        }

        private void TxtPass_Enter(object sender, EventArgs e)
        {
            LblError.Visible = false;
        }
        private void TxtUsuario_Enter(object sender, EventArgs e)
        {
            LblError.Visible = false;
        }

        public void mensajeError(string mensaje)
        {
            LblError.Text = "      " + mensaje;
            LblError.Visible = true;
        }

        
        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            this.PanelLogin.Visible = false;
            this.PanelSigin.Visible = true;
            TxtNombre.Focus();
        }

        private void BtnCanelar_Click(object sender, EventArgs e)
        {
            this.PanelSigin.Visible = false;
            TxtNombre.Clear();
            TxtIdentificador.Clear();
            TxtNewPass.Clear();
            TxtConfirmNewPass.Clear();
        }

        private void BtnNewPass_CheckedChanged(object sender, EventArgs e)
        {
            switch (BtnNewPass.Checked)
            {
                case true:
                    TxtNewPass.PasswordChar = '\u0000';
                    TxtConfirmNewPass.PasswordChar = '\u0000';
                    break;

                case false:
                    TxtNewPass.PasswordChar = '\u25CF';
                    TxtConfirmNewPass.PasswordChar = '\u25CF';
                    break;
            }
        }

        private void TxtConfirmNewPass_KeyUp(object sender, KeyEventArgs e)
        {
            if (TxtNewPass.Text == TxtConfirmNewPass.Text)
            {
                EProvider.Clear();
                AProvider.SetError(TxtConfirmNewPass, "Correcto");
            }
            else
            {
                AProvider.Clear();
                EProvider.SetError(TxtConfirmNewPass, "Incorrecta");
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (TxtNombre.Text.Trim() != "")
            {
                if (TxtIdentificador.Text.Trim() != "")
                {
                    if (TxtNewPass.Text.Trim() != "" && TxtConfirmNewPass.Text.Trim() != "")
                    {
                        Usuario mUsuario = new Usuario();
                        Personal mPersonal = new Personal();

                        mPersonal.Nombre = TxtNombre.Text;
                        mPersonal.Iniciales = TxtIdentificador.Text;

                        mUsuario.contrasena = TxtConfirmNewPass.Text;
                        mUsuario.tipo = "1";

                        ControlPersonal mControlPersonal = new ControlPersonal();

                        mControlPersonal.agregarUsuarioYPass(mPersonal, mUsuario);
                        MessageBox.Show("Ahora ya puedes iniciar sesión!!","Aviso",MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.PanelSigin.Visible = false;
                        this.PanelLogin.Visible = true;
                        TxtUsuario.Focus();
                    }
                    else
                    {
                        EProvider.Clear();
                        EProvider.SetError(TxtConfirmNewPass, "Campo contraseña no puede quedar vacío.");
                        TxtConfirmNewPass.Focus();
                    }
                }
                else
                {
                    EProvider.Clear();
                    EProvider.SetError(TxtIdentificador, "Campo iniciales no puede quedar vacío.");
                    TxtIdentificador.Focus();
                }
            }
            else
            {
                EProvider.Clear();
                EProvider.SetError(TxtNombre,"Campo nombre no puede quedar vacío.");
                TxtNombre.Focus();
            }
        }

        private void TxtNombre_Leave(object sender, EventArgs e)
        {
            String iniciales = "";

            try
            {
                String[] nombreCompleto = TxtNombre.Text.Split(' ');
                foreach (String palabra in nombreCompleto)
                {
                    iniciales += palabra[0];
                }
                TxtIdentificador.Text = iniciales.ToUpper();
            }
            catch
            {

            }
        }

        private void BtnMostrar_CheckedChanged(object sender, EventArgs e)
        {
            switch (BtnMostrar.Checked)
            {
                case true:
                    TxtPass.PasswordChar = '\u0000';
                    break;

                case false:
                    TxtPass.PasswordChar = '\u25CF';
                    break;
            }
        }

        private void verificar()
        {
           ControlLogin mControlLogin = new ControlLogin();
            switch (mControlLogin.verificar())
            {
                case true:
                    this.PanelLogin.Visible = true;
                    TxtUsuario.Focus();
                    break;
                case false:
                    this.PanelLogin.Visible = false;
                    //TxtNombre.Focus();
                    break;
            }
        }


        private void cerrarSesion(object sender, FormClosedEventArgs e)
        {
            TxtUsuario.Text = "";
            TxtPass.Text = "";
            LblError.Visible = false;

            this.Show();

            TxtUsuario.Focus();
        }

        private void FrmLogin_Leave(object sender, EventArgs e) {
          
        }

        private void FrmLogin_Enter(object sender, EventArgs e) {
          
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            
        }

        private void FrmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void FrmLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ControlLogin.comprobar)
            {
                if (mControlLogin.verificar())
                {
                    if ((TxtUsuario.Text.Length > 0) && (TxtPass.Text.Length > 0)
                            && (e.KeyChar == (char)Keys.Enter))
                    {
                        BtnAcceder.PerformClick();
                    }
                }
                else
                {
                    if ((TxtNombre.Text.Length > 0) && (TxtNewPass.Text.Length > 0)
                        && (TxtConfirmNewPass.Text.Length > 0) && (e.KeyChar == (char)Keys.Enter))
                    {
                        BtnGuardar.PerformClick();
                    }
                }
            }
            else
            {
                if ((TxtUsuario.Text.Length > 0) && (TxtPass.Text.Length > 0)
                            && (e.KeyChar == (char)Keys.Enter))
                {
                    BtnAcceder.PerformClick();
                }
            }
            
        }

        private void Config_MouseClick(object sender, MouseEventArgs e)
        {
            FrmConfigConex mFrmConfig = new FrmConfigConex();
            mFrmConfig.ShowDialog();
        }

        private void Configuracion_Click(object sender, EventArgs e)
        {
            FrmConfigConex mFrmConfig = new FrmConfigConex();
            mFrmConfig.ShowDialog();
        }
    }
}
