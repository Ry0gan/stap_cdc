﻿namespace STAP_CDC.Acceso
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.PanelLogin = new System.Windows.Forms.Panel();
            this.Config = new System.Windows.Forms.PictureBox();
            this.BtnMostrar = new Guna.UI2.WinForms.Guna2Button();
            this.LblError = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.guna2ControlBox2 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox1 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TxtPass = new Guna.UI2.WinForms.Guna2TextBox();
            this.BtnAcceder = new Guna.UI2.WinForms.Guna2Button();
            this.TxtUsuario = new Guna.UI2.WinForms.Guna2TextBox();
            this.PanelSigin = new System.Windows.Forms.Panel();
            this.BtnNewPass = new Guna.UI2.WinForms.Guna2Button();
            this.TxtConfirmNewPass = new Guna.UI2.WinForms.Guna2TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtNewPass = new Guna.UI2.WinForms.Guna2TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnGuardar = new Guna.UI2.WinForms.Guna2Button();
            this.BtnCanelar = new Guna.UI2.WinForms.Guna2Button();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtNombre = new Guna.UI2.WinForms.Guna2TextBox();
            this.TxtIdentificador = new Guna.UI2.WinForms.Guna2TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.guna2ControlBox5 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox6 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnRegistrar = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ControlBox3 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.guna2ControlBox4 = new Guna.UI2.WinForms.Guna2ControlBox();
            this.EProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.AProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.sombra = new Guna.UI2.WinForms.Guna2ShadowForm(this.components);
            this.Configuracion = new System.Windows.Forms.PictureBox();
            this.PanelLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Config)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PanelSigin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Configuracion)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(7, 244);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(311, 20);
            this.label2.TabIndex = 46565465;
            this.label2.Text = "Seguimiento de Tareas y Avance de Proyectos";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-7, 140);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(339, 91);
            this.label1.TabIndex = 8948;
            this.label1.Text = "🆂🆃🅰🅿";
            // 
            // guna2DragControl1
            // 
            this.guna2DragControl1.TargetControl = this.PanelLogin;
            // 
            // PanelLogin
            // 
            this.PanelLogin.BackColor = System.Drawing.Color.White;
            this.PanelLogin.Controls.Add(this.Config);
            this.PanelLogin.Controls.Add(this.BtnMostrar);
            this.PanelLogin.Controls.Add(this.LblError);
            this.PanelLogin.Controls.Add(this.label3);
            this.PanelLogin.Controls.Add(this.guna2ControlBox2);
            this.PanelLogin.Controls.Add(this.guna2ControlBox1);
            this.PanelLogin.Controls.Add(this.guna2Separator2);
            this.PanelLogin.Controls.Add(this.guna2Separator1);
            this.PanelLogin.Controls.Add(this.pictureBox2);
            this.PanelLogin.Controls.Add(this.pictureBox1);
            this.PanelLogin.Controls.Add(this.TxtPass);
            this.PanelLogin.Controls.Add(this.BtnAcceder);
            this.PanelLogin.Controls.Add(this.TxtUsuario);
            this.PanelLogin.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelLogin.ForeColor = System.Drawing.Color.White;
            this.PanelLogin.Location = new System.Drawing.Point(356, 0);
            this.PanelLogin.Margin = new System.Windows.Forms.Padding(4);
            this.PanelLogin.Name = "PanelLogin";
            this.PanelLogin.Size = new System.Drawing.Size(663, 407);
            this.PanelLogin.TabIndex = 0;
            // 
            // Config
            // 
            this.Config.Image = global::STAP_CDC.Properties.Resources.engranaje;
            this.Config.Location = new System.Drawing.Point(605, 345);
            this.Config.Name = "Config";
            this.Config.Size = new System.Drawing.Size(46, 50);
            this.Config.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Config.TabIndex = 65;
            this.Config.TabStop = false;
            this.Config.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Config_MouseClick);
            // 
            // BtnMostrar
            // 
            this.BtnMostrar.BackColor = System.Drawing.Color.White;
            this.BtnMostrar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnMostrar.BorderRadius = 5;
            this.BtnMostrar.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.ToogleButton;
            this.BtnMostrar.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.BtnMostrar.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("BtnMostrar.CheckedState.Image")));
            this.BtnMostrar.CheckedState.Parent = this.BtnMostrar;
            this.BtnMostrar.CustomImages.Parent = this.BtnMostrar;
            this.BtnMostrar.FillColor = System.Drawing.Color.Transparent;
            this.BtnMostrar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnMostrar.ForeColor = System.Drawing.Color.White;
            this.BtnMostrar.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.BtnMostrar.HoverState.Parent = this.BtnMostrar;
            this.BtnMostrar.Image = ((System.Drawing.Image)(resources.GetObject("BtnMostrar.Image")));
            this.BtnMostrar.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnMostrar.Location = new System.Drawing.Point(539, 196);
            this.BtnMostrar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnMostrar.Name = "BtnMostrar";
            this.BtnMostrar.PressedColor = System.Drawing.Color.Transparent;
            this.BtnMostrar.ShadowDecoration.Color = System.Drawing.Color.LightGray;
            this.BtnMostrar.ShadowDecoration.Depth = 10;
            this.BtnMostrar.ShadowDecoration.Parent = this.BtnMostrar;
            this.BtnMostrar.Size = new System.Drawing.Size(55, 34);
            this.BtnMostrar.TabIndex = 2;
            this.BtnMostrar.CheckedChanged += new System.EventHandler(this.BtnMostrar_CheckedChanged);
            // 
            // LblError
            // 
            this.LblError.AutoSize = true;
            this.LblError.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblError.ForeColor = System.Drawing.Color.Red;
            this.LblError.Image = ((System.Drawing.Image)(resources.GetObject("LblError.Image")));
            this.LblError.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LblError.Location = new System.Drawing.Point(108, 287);
            this.LblError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblError.Name = "LblError";
            this.LblError.Size = new System.Drawing.Size(75, 19);
            this.LblError.TabIndex = 64;
            this.LblError.Text = "Mensage";
            this.LblError.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LblError.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.label3.Location = new System.Drawing.Point(257, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 29);
            this.label3.TabIndex = 57;
            this.label3.Text = "Iniciar Sesión";
            // 
            // guna2ControlBox2
            // 
            this.guna2ControlBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox2.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox2.FillColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox2.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox2.HoverState.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox2.Location = new System.Drawing.Point(536, 0);
            this.guna2ControlBox2.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox2.Name = "guna2ControlBox2";
            this.guna2ControlBox2.ShadowDecoration.Parent = this.guna2ControlBox2;
            this.guna2ControlBox2.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox2.TabIndex = 63;
            // 
            // guna2ControlBox1
            // 
            this.guna2ControlBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox1.FillColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox1.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox1.HoverState.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox1.Location = new System.Drawing.Point(599, 0);
            this.guna2ControlBox1.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox1.Name = "guna2ControlBox1";
            this.guna2ControlBox1.ShadowDecoration.Parent = this.guna2ControlBox1;
            this.guna2ControlBox1.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox1.TabIndex = 62;
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.BackColor = System.Drawing.Color.Transparent;
            this.guna2Separator2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.guna2Separator2.Location = new System.Drawing.Point(75, 228);
            this.guna2Separator2.Margin = new System.Windows.Forms.Padding(4);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(519, 12);
            this.guna2Separator2.TabIndex = 61;
            this.guna2Separator2.TabStop = false;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.BackColor = System.Drawing.Color.Transparent;
            this.guna2Separator1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.guna2Separator1.Location = new System.Drawing.Point(75, 145);
            this.guna2Separator1.Margin = new System.Windows.Forms.Padding(4);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(519, 12);
            this.guna2Separator1.TabIndex = 60;
            this.guna2Separator1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(75, 198);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(35, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 59;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(75, 114);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 58;
            this.pictureBox1.TabStop = false;
            // 
            // TxtPass
            // 
            this.TxtPass.BackColor = System.Drawing.Color.Transparent;
            this.TxtPass.BorderColor = System.Drawing.Color.Transparent;
            this.TxtPass.BorderRadius = 10;
            this.TxtPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtPass.DefaultText = "";
            this.TxtPass.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtPass.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtPass.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPass.DisabledState.Parent = this.TxtPass;
            this.TxtPass.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtPass.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPass.FocusedState.Parent = this.TxtPass;
            this.TxtPass.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPass.ForeColor = System.Drawing.Color.Black;
            this.TxtPass.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtPass.HoverState.Parent = this.TxtPass;
            this.TxtPass.Location = new System.Drawing.Point(124, 196);
            this.TxtPass.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPass.Name = "TxtPass";
            this.TxtPass.PasswordChar = '\0';
            this.TxtPass.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.TxtPass.PlaceholderText = "CONTRASEÑA";
            this.TxtPass.SelectedText = "";
            this.TxtPass.ShadowDecoration.Parent = this.TxtPass;
            this.TxtPass.Size = new System.Drawing.Size(413, 44);
            this.TxtPass.Style = Guna.UI2.WinForms.Enums.TextBoxStyle.Material;
            this.TxtPass.TabIndex = 1;
            this.TxtPass.Enter += new System.EventHandler(this.TxtPass_Enter);
            // 
            // BtnAcceder
            // 
            this.BtnAcceder.Animated = true;
            this.BtnAcceder.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcceder.BorderRadius = 15;
            this.BtnAcceder.BorderThickness = 1;
            this.BtnAcceder.CheckedState.Parent = this.BtnAcceder;
            this.BtnAcceder.CustomImages.Parent = this.BtnAcceder;
            this.BtnAcceder.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnAcceder.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcceder.ForeColor = System.Drawing.Color.White;
            this.BtnAcceder.HoverState.Parent = this.BtnAcceder;
            this.BtnAcceder.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcceder.Image")));
            this.BtnAcceder.Location = new System.Drawing.Point(140, 343);
            this.BtnAcceder.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAcceder.Name = "BtnAcceder";
            this.BtnAcceder.Padding = new System.Windows.Forms.Padding(27, 0, 0, 0);
            this.BtnAcceder.ShadowDecoration.BorderRadius = 15;
            this.BtnAcceder.ShadowDecoration.Depth = 10;
            this.BtnAcceder.ShadowDecoration.Enabled = true;
            this.BtnAcceder.ShadowDecoration.Parent = this.BtnAcceder;
            this.BtnAcceder.Size = new System.Drawing.Size(377, 49);
            this.BtnAcceder.TabIndex = 3;
            this.BtnAcceder.Text = "ACCEDER";
            this.BtnAcceder.UseTransparentBackground = true;
            this.BtnAcceder.Click += new System.EventHandler(this.BtnAcceder_Click);
            // 
            // TxtUsuario
            // 
            this.TxtUsuario.BackColor = System.Drawing.Color.White;
            this.TxtUsuario.BorderColor = System.Drawing.Color.Transparent;
            this.TxtUsuario.BorderRadius = 10;
            this.TxtUsuario.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtUsuario.DefaultText = "";
            this.TxtUsuario.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtUsuario.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtUsuario.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtUsuario.DisabledState.Parent = this.TxtUsuario;
            this.TxtUsuario.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtUsuario.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtUsuario.FocusedState.Parent = this.TxtUsuario;
            this.TxtUsuario.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUsuario.ForeColor = System.Drawing.Color.Black;
            this.TxtUsuario.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtUsuario.HoverState.Parent = this.TxtUsuario;
            this.TxtUsuario.Location = new System.Drawing.Point(124, 113);
            this.TxtUsuario.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUsuario.Name = "TxtUsuario";
            this.TxtUsuario.PasswordChar = '\0';
            this.TxtUsuario.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.TxtUsuario.PlaceholderText = "USUARIO";
            this.TxtUsuario.SelectedText = "";
            this.TxtUsuario.ShadowDecoration.Parent = this.TxtUsuario;
            this.TxtUsuario.Size = new System.Drawing.Size(469, 44);
            this.TxtUsuario.Style = Guna.UI2.WinForms.Enums.TextBoxStyle.Material;
            this.TxtUsuario.TabIndex = 0;
            this.TxtUsuario.MouseEnter += new System.EventHandler(this.TxtUsuario_Enter);
            // 
            // PanelSigin
            // 
            this.PanelSigin.BackColor = System.Drawing.Color.White;
            this.PanelSigin.Controls.Add(this.Configuracion);
            this.PanelSigin.Controls.Add(this.BtnNewPass);
            this.PanelSigin.Controls.Add(this.TxtConfirmNewPass);
            this.PanelSigin.Controls.Add(this.label6);
            this.PanelSigin.Controls.Add(this.TxtNewPass);
            this.PanelSigin.Controls.Add(this.label8);
            this.PanelSigin.Controls.Add(this.BtnGuardar);
            this.PanelSigin.Controls.Add(this.BtnCanelar);
            this.PanelSigin.Controls.Add(this.label24);
            this.PanelSigin.Controls.Add(this.TxtNombre);
            this.PanelSigin.Controls.Add(this.TxtIdentificador);
            this.PanelSigin.Controls.Add(this.label9);
            this.PanelSigin.Controls.Add(this.label7);
            this.PanelSigin.Controls.Add(this.guna2ControlBox5);
            this.PanelSigin.Controls.Add(this.guna2ControlBox6);
            this.PanelSigin.ForeColor = System.Drawing.Color.White;
            this.PanelSigin.Location = new System.Drawing.Point(353, 0);
            this.PanelSigin.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSigin.Name = "PanelSigin";
            this.PanelSigin.Size = new System.Drawing.Size(664, 409);
            this.PanelSigin.TabIndex = 66;
            this.PanelSigin.Visible = false;
            // 
            // BtnNewPass
            // 
            this.BtnNewPass.BackColor = System.Drawing.Color.White;
            this.BtnNewPass.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnNewPass.BorderRadius = 5;
            this.BtnNewPass.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.ToogleButton;
            this.BtnNewPass.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("BtnNewPass.CheckedState.Image")));
            this.BtnNewPass.CheckedState.Parent = this.BtnNewPass;
            this.BtnNewPass.CustomImages.Parent = this.BtnNewPass;
            this.BtnNewPass.FillColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnNewPass.ForeColor = System.Drawing.Color.White;
            this.BtnNewPass.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.HoverState.Parent = this.BtnNewPass;
            this.BtnNewPass.Image = ((System.Drawing.Image)(resources.GetObject("BtnNewPass.Image")));
            this.BtnNewPass.ImageSize = new System.Drawing.Size(30, 30);
            this.BtnNewPass.Location = new System.Drawing.Point(372, 198);
            this.BtnNewPass.Margin = new System.Windows.Forms.Padding(4);
            this.BtnNewPass.Name = "BtnNewPass";
            this.BtnNewPass.PressedColor = System.Drawing.Color.Transparent;
            this.BtnNewPass.ShadowDecoration.Color = System.Drawing.Color.LightGray;
            this.BtnNewPass.ShadowDecoration.Depth = 10;
            this.BtnNewPass.ShadowDecoration.Parent = this.BtnNewPass;
            this.BtnNewPass.Size = new System.Drawing.Size(55, 34);
            this.BtnNewPass.TabIndex = 7;
            this.BtnNewPass.Tag = "";
            this.BtnNewPass.CheckedChanged += new System.EventHandler(this.BtnNewPass_CheckedChanged);
            // 
            // TxtConfirmNewPass
            // 
            this.TxtConfirmNewPass.Animated = true;
            this.TxtConfirmNewPass.BackColor = System.Drawing.Color.White;
            this.TxtConfirmNewPass.BorderRadius = 10;
            this.TxtConfirmNewPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtConfirmNewPass.DefaultText = "";
            this.TxtConfirmNewPass.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtConfirmNewPass.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtConfirmNewPass.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtConfirmNewPass.DisabledState.Parent = this.TxtConfirmNewPass;
            this.TxtConfirmNewPass.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtConfirmNewPass.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtConfirmNewPass.FocusedState.Parent = this.TxtConfirmNewPass;
            this.TxtConfirmNewPass.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtConfirmNewPass.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtConfirmNewPass.HoverState.Parent = this.TxtConfirmNewPass;
            this.TxtConfirmNewPass.Location = new System.Drawing.Point(77, 278);
            this.TxtConfirmNewPass.Margin = new System.Windows.Forms.Padding(5, 5, 27, 5);
            this.TxtConfirmNewPass.Name = "TxtConfirmNewPass";
            this.TxtConfirmNewPass.PasswordChar = '\0';
            this.TxtConfirmNewPass.PlaceholderText = "";
            this.TxtConfirmNewPass.SelectedText = "";
            this.TxtConfirmNewPass.ShadowDecoration.Parent = this.TxtConfirmNewPass;
            this.TxtConfirmNewPass.Size = new System.Drawing.Size(284, 44);
            this.TxtConfirmNewPass.TabIndex = 8;
            this.TxtConfirmNewPass.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtConfirmNewPass_KeyUp);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(83, 252);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(225, 22);
            this.label6.TabIndex = 73;
            this.label6.Text = "*Confirmar contraseña:";
            // 
            // TxtNewPass
            // 
            this.TxtNewPass.Animated = true;
            this.TxtNewPass.BackColor = System.Drawing.Color.White;
            this.TxtNewPass.BorderRadius = 10;
            this.TxtNewPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtNewPass.DefaultText = "";
            this.TxtNewPass.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtNewPass.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtNewPass.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNewPass.DisabledState.Parent = this.TxtNewPass;
            this.TxtNewPass.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNewPass.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNewPass.FocusedState.Parent = this.TxtNewPass;
            this.TxtNewPass.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNewPass.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNewPass.HoverState.Parent = this.TxtNewPass;
            this.TxtNewPass.Location = new System.Drawing.Point(77, 194);
            this.TxtNewPass.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNewPass.Name = "TxtNewPass";
            this.TxtNewPass.PasswordChar = '\0';
            this.TxtNewPass.PlaceholderText = "";
            this.TxtNewPass.SelectedText = "";
            this.TxtNewPass.ShadowDecoration.Parent = this.TxtNewPass;
            this.TxtNewPass.Size = new System.Drawing.Size(284, 44);
            this.TxtNewPass.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(83, 165);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 22);
            this.label8.TabIndex = 71;
            this.label8.Text = "Contraseña:";
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Animated = true;
            this.BtnGuardar.BackColor = System.Drawing.Color.Transparent;
            this.BtnGuardar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.BorderRadius = 15;
            this.BtnGuardar.BorderThickness = 1;
            this.BtnGuardar.CheckedState.Parent = this.BtnGuardar;
            this.BtnGuardar.CustomImages.Parent = this.BtnGuardar;
            this.BtnGuardar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(131)))), ((int)(((byte)(189)))));
            this.BtnGuardar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F);
            this.BtnGuardar.ForeColor = System.Drawing.Color.White;
            this.BtnGuardar.HoverState.Parent = this.BtnGuardar;
            this.BtnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("BtnGuardar.Image")));
            this.BtnGuardar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnGuardar.Location = new System.Drawing.Point(163, 348);
            this.BtnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.ShadowDecoration.Parent = this.BtnGuardar;
            this.BtnGuardar.Size = new System.Drawing.Size(165, 49);
            this.BtnGuardar.TabIndex = 9;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseTransparentBackground = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnCanelar
            // 
            this.BtnCanelar.Animated = true;
            this.BtnCanelar.BackColor = System.Drawing.Color.Transparent;
            this.BtnCanelar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.BorderRadius = 15;
            this.BtnCanelar.BorderThickness = 1;
            this.BtnCanelar.CheckedState.Parent = this.BtnCanelar;
            this.BtnCanelar.CustomImages.Parent = this.BtnCanelar;
            this.BtnCanelar.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(6)))), ((int)(((byte)(5)))));
            this.BtnCanelar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCanelar.ForeColor = System.Drawing.Color.White;
            this.BtnCanelar.HoverState.Parent = this.BtnCanelar;
            this.BtnCanelar.Image = ((System.Drawing.Image)(resources.GetObject("BtnCanelar.Image")));
            this.BtnCanelar.ImageSize = new System.Drawing.Size(35, 35);
            this.BtnCanelar.Location = new System.Drawing.Point(380, 348);
            this.BtnCanelar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnCanelar.Name = "BtnCanelar";
            this.BtnCanelar.ShadowDecoration.Parent = this.BtnCanelar;
            this.BtnCanelar.Size = new System.Drawing.Size(165, 49);
            this.BtnCanelar.TabIndex = 10;
            this.BtnCanelar.Text = "Cancelar";
            this.BtnCanelar.UseTransparentBackground = true;
            this.BtnCanelar.Click += new System.EventHandler(this.BtnCanelar_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(83, 69);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(89, 22);
            this.label24.TabIndex = 66;
            this.label24.Text = "Nombre:";
            // 
            // TxtNombre
            // 
            this.TxtNombre.Animated = true;
            this.TxtNombre.BackColor = System.Drawing.Color.White;
            this.TxtNombre.BorderRadius = 10;
            this.TxtNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtNombre.DefaultText = "";
            this.TxtNombre.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtNombre.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtNombre.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNombre.DisabledState.Parent = this.TxtNombre;
            this.TxtNombre.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtNombre.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNombre.FocusedState.Parent = this.TxtNombre;
            this.TxtNombre.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNombre.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtNombre.HoverState.Parent = this.TxtNombre;
            this.TxtNombre.Location = new System.Drawing.Point(81, 94);
            this.TxtNombre.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.PasswordChar = '\0';
            this.TxtNombre.PlaceholderText = "";
            this.TxtNombre.SelectedText = "";
            this.TxtNombre.ShadowDecoration.Parent = this.TxtNombre;
            this.TxtNombre.Size = new System.Drawing.Size(284, 44);
            this.TxtNombre.TabIndex = 4;
            this.TxtNombre.Leave += new System.EventHandler(this.TxtNombre_Leave);
            // 
            // TxtIdentificador
            // 
            this.TxtIdentificador.Animated = true;
            this.TxtIdentificador.BackColor = System.Drawing.Color.White;
            this.TxtIdentificador.BorderRadius = 10;
            this.TxtIdentificador.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtIdentificador.DefaultText = "";
            this.TxtIdentificador.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TxtIdentificador.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TxtIdentificador.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIdentificador.DisabledState.Parent = this.TxtIdentificador;
            this.TxtIdentificador.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TxtIdentificador.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIdentificador.FocusedState.Parent = this.TxtIdentificador;
            this.TxtIdentificador.Font = new System.Drawing.Font("Microsoft Tai Le", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentificador.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TxtIdentificador.HoverState.Parent = this.TxtIdentificador;
            this.TxtIdentificador.Location = new System.Drawing.Point(417, 94);
            this.TxtIdentificador.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentificador.Name = "TxtIdentificador";
            this.TxtIdentificador.PasswordChar = '\0';
            this.TxtIdentificador.PlaceholderText = "";
            this.TxtIdentificador.SelectedText = "";
            this.TxtIdentificador.ShadowDecoration.Parent = this.TxtIdentificador;
            this.TxtIdentificador.Size = new System.Drawing.Size(179, 44);
            this.TxtIdentificador.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(408, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 22);
            this.label9.TabIndex = 67;
            this.label9.Text = "Iniciales:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bahnschrift Light SemiCondensed", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.label7.Location = new System.Drawing.Point(224, 4);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(227, 29);
            this.label7.TabIndex = 57;
            this.label7.Text = "Agregar Administrador";
            // 
            // guna2ControlBox5
            // 
            this.guna2ControlBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox5.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox5.FillColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox5.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox5.HoverState.Parent = this.guna2ControlBox5;
            this.guna2ControlBox5.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox5.Location = new System.Drawing.Point(537, 0);
            this.guna2ControlBox5.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox5.Name = "guna2ControlBox5";
            this.guna2ControlBox5.ShadowDecoration.Parent = this.guna2ControlBox5;
            this.guna2ControlBox5.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox5.TabIndex = 63;
            // 
            // guna2ControlBox6
            // 
            this.guna2ControlBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox6.BackColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox6.FillColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox6.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox6.HoverState.Parent = this.guna2ControlBox6;
            this.guna2ControlBox6.IconColor = System.Drawing.Color.Black;
            this.guna2ControlBox6.Location = new System.Drawing.Point(600, 0);
            this.guna2ControlBox6.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox6.Name = "guna2ControlBox6";
            this.guna2ControlBox6.ShadowDecoration.Parent = this.guna2ControlBox6;
            this.guna2ControlBox6.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox6.TabIndex = 62;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(416, 171);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(534, 23);
            this.label4.TabIndex = 31;
            this.label4.Text = "Presiona el siguiente botón para registrarte y así poder iniciar sesión.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 22F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(393, 60);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(562, 50);
            this.label5.TabIndex = 32;
            this.label5.Text = "Vemos que eres nuevo por aquí!!";
            // 
            // BtnRegistrar
            // 
            this.BtnRegistrar.Animated = true;
            this.BtnRegistrar.BackColor = System.Drawing.Color.Transparent;
            this.BtnRegistrar.BorderRadius = 15;
            this.BtnRegistrar.BorderThickness = 1;
            this.BtnRegistrar.CheckedState.Parent = this.BtnRegistrar;
            this.BtnRegistrar.CustomImages.Parent = this.BtnRegistrar;
            this.BtnRegistrar.FillColor = System.Drawing.Color.White;
            this.BtnRegistrar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRegistrar.ForeColor = System.Drawing.Color.Black;
            this.BtnRegistrar.HoverState.Parent = this.BtnRegistrar;
            this.BtnRegistrar.Location = new System.Drawing.Point(517, 257);
            this.BtnRegistrar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnRegistrar.Name = "BtnRegistrar";
            this.BtnRegistrar.Padding = new System.Windows.Forms.Padding(27, 0, 0, 0);
            this.BtnRegistrar.ShadowDecoration.BorderRadius = 15;
            this.BtnRegistrar.ShadowDecoration.Color = System.Drawing.Color.White;
            this.BtnRegistrar.ShadowDecoration.Depth = 10;
            this.BtnRegistrar.ShadowDecoration.Enabled = true;
            this.BtnRegistrar.ShadowDecoration.Parent = this.BtnRegistrar;
            this.BtnRegistrar.Size = new System.Drawing.Size(377, 49);
            this.BtnRegistrar.TabIndex = 57;
            this.BtnRegistrar.Text = "Registrar";
            this.BtnRegistrar.UseTransparentBackground = true;
            this.BtnRegistrar.Click += new System.EventHandler(this.BtnAgregar_Click);
            // 
            // guna2ControlBox3
            // 
            this.guna2ControlBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox3.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.guna2ControlBox3.FillColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox3.HoverState.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ControlBox3.HoverState.Parent = this.guna2ControlBox3;
            this.guna2ControlBox3.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox3.Location = new System.Drawing.Point(891, 0);
            this.guna2ControlBox3.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox3.Name = "guna2ControlBox3";
            this.guna2ControlBox3.ShadowDecoration.Parent = this.guna2ControlBox3;
            this.guna2ControlBox3.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox3.TabIndex = 65;
            // 
            // guna2ControlBox4
            // 
            this.guna2ControlBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2ControlBox4.BackColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox4.FillColor = System.Drawing.Color.Transparent;
            this.guna2ControlBox4.HoverState.FillColor = System.Drawing.Color.Red;
            this.guna2ControlBox4.HoverState.Parent = this.guna2ControlBox4;
            this.guna2ControlBox4.IconColor = System.Drawing.Color.White;
            this.guna2ControlBox4.Location = new System.Drawing.Point(953, 0);
            this.guna2ControlBox4.Margin = new System.Windows.Forms.Padding(4);
            this.guna2ControlBox4.Name = "guna2ControlBox4";
            this.guna2ControlBox4.ShadowDecoration.Parent = this.guna2ControlBox4;
            this.guna2ControlBox4.Size = new System.Drawing.Size(60, 36);
            this.guna2ControlBox4.TabIndex = 64;
            // 
            // EProvider
            // 
            this.EProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.EProvider.ContainerControl = this;
            this.EProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("EProvider.Icon")));
            // 
            // AProvider
            // 
            this.AProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.AProvider.ContainerControl = this.TxtConfirmNewPass;
            this.AProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("AProvider.Icon")));
            // 
            // Configuracion
            // 
            this.Configuracion.Image = global::STAP_CDC.Properties.Resources.engranaje;
            this.Configuracion.Location = new System.Drawing.Point(608, 348);
            this.Configuracion.Name = "Configuracion";
            this.Configuracion.Size = new System.Drawing.Size(46, 50);
            this.Configuracion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Configuracion.TabIndex = 74;
            this.Configuracion.TabStop = false;
            this.Configuracion.Click += new System.EventHandler(this.Configuracion_Click);
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(1019, 407);
            this.Controls.Add(this.PanelSigin);
            this.Controls.Add(this.PanelLogin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BtnRegistrar);
            this.Controls.Add(this.guna2ControlBox4);
            this.Controls.Add(this.guna2ControlBox3);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LOGIN";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.Enter += new System.EventHandler(this.FrmLogin_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmLogin_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmLogin_KeyPress);
            this.Leave += new System.EventHandler(this.FrmLogin_Leave);
            this.PanelLogin.ResumeLayout(false);
            this.PanelLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Config)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PanelSigin.ResumeLayout(false);
            this.PanelSigin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Configuracion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Panel PanelLogin;
        private Guna.UI2.WinForms.Guna2Button BtnMostrar;
        public System.Windows.Forms.Label LblError;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox2;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox1;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2TextBox TxtPass;
        private Guna.UI2.WinForms.Guna2Button BtnAcceder;
        private Guna.UI2.WinForms.Guna2TextBox TxtUsuario;
        private Guna.UI2.WinForms.Guna2Button BtnRegistrar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox3;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox4;
        private System.Windows.Forms.Panel PanelSigin;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox5;
        private Guna.UI2.WinForms.Guna2ControlBox guna2ControlBox6;
        private Guna.UI2.WinForms.Guna2TextBox TxtConfirmNewPass;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2TextBox TxtNewPass;
        private System.Windows.Forms.Label label8;
        private Guna.UI2.WinForms.Guna2Button BtnGuardar;
        private Guna.UI2.WinForms.Guna2Button BtnCanelar;
        private System.Windows.Forms.Label label24;
        private Guna.UI2.WinForms.Guna2TextBox TxtNombre;
        private Guna.UI2.WinForms.Guna2TextBox TxtIdentificador;
        private System.Windows.Forms.Label label9;
        private Guna.UI2.WinForms.Guna2Button BtnNewPass;
        private System.Windows.Forms.ErrorProvider EProvider;
        private System.Windows.Forms.ErrorProvider AProvider;
        private Guna.UI2.WinForms.Guna2ShadowForm sombra;
        private System.Windows.Forms.PictureBox Config;
        private System.Windows.Forms.PictureBox Configuracion;
    }
}