﻿namespace STAP_CDC
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.guna2CustomGradientPanel1 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.btnCerrarSesion = new Guna.UI2.WinForms.Guna2Button();
            this.BtnReportesProyecto = new Guna.UI2.WinForms.Guna2Button();
            this.btnCRUDRol = new Guna.UI2.WinForms.Guna2Button();
            this.LblHome = new System.Windows.Forms.Label();
            this.BtnCRUDPersonal = new Guna.UI2.WinForms.Guna2Button();
            this.BtnCRUDproyectos = new Guna.UI2.WinForms.Guna2Button();
            this.BtnSeguimiento = new Guna.UI2.WinForms.Guna2Button();
            this.imgSlide = new System.Windows.Forms.PictureBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.BarraTitulo = new System.Windows.Forms.Panel();
            this.BtnMin = new Guna.UI2.WinForms.Guna2ControlBox();
            this.BtnMax = new Guna.UI2.WinForms.Guna2ControlBox();
            this.BtnSalir = new Guna.UI2.WinForms.Guna2ControlBox();
            this.PanelFormulario = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolTipSideBar = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.guna2CustomGradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlide)).BeginInit();
            this.BarraTitulo.SuspendLayout();
            this.PanelFormulario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.guna2CustomGradientPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 731);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(100, 29);
            this.panel2.TabIndex = 1;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // guna2CustomGradientPanel1
            // 
            this.guna2CustomGradientPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2CustomGradientPanel1.BorderRadius = 15;
            this.guna2CustomGradientPanel1.Controls.Add(this.btnCerrarSesion);
            this.guna2CustomGradientPanel1.Controls.Add(this.BtnReportesProyecto);
            this.guna2CustomGradientPanel1.Controls.Add(this.btnCRUDRol);
            this.guna2CustomGradientPanel1.Controls.Add(this.LblHome);
            this.guna2CustomGradientPanel1.Controls.Add(this.BtnCRUDPersonal);
            this.guna2CustomGradientPanel1.Controls.Add(this.BtnCRUDproyectos);
            this.guna2CustomGradientPanel1.Controls.Add(this.BtnSeguimiento);
            this.guna2CustomGradientPanel1.Controls.Add(this.imgSlide);
            this.guna2CustomGradientPanel1.Controls.Add(this.shapeContainer1);
            this.guna2CustomGradientPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.guna2CustomGradientPanel1.Location = new System.Drawing.Point(12, 41);
            this.guna2CustomGradientPanel1.Name = "guna2CustomGradientPanel1";
            this.guna2CustomGradientPanel1.ShadowDecoration.Parent = this.guna2CustomGradientPanel1;
            this.guna2CustomGradientPanel1.Size = new System.Drawing.Size(74, 684);
            this.guna2CustomGradientPanel1.TabIndex = 0;
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCerrarSesion.BackColor = System.Drawing.Color.Transparent;
            this.btnCerrarSesion.BorderColor = System.Drawing.Color.Transparent;
            this.btnCerrarSesion.BorderRadius = 10;
            this.btnCerrarSesion.BorderThickness = 1;
            this.btnCerrarSesion.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.btnCerrarSesion.CheckedState.BorderColor = System.Drawing.Color.White;
            this.btnCerrarSesion.CheckedState.FillColor = System.Drawing.Color.White;
            this.btnCerrarSesion.CheckedState.Parent = this.btnCerrarSesion;
            this.btnCerrarSesion.CustomImages.Parent = this.btnCerrarSesion;
            this.btnCerrarSesion.FillColor = System.Drawing.Color.Transparent;
            this.btnCerrarSesion.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnCerrarSesion.ForeColor = System.Drawing.Color.White;
            this.btnCerrarSesion.HoverState.Parent = this.btnCerrarSesion;
            this.btnCerrarSesion.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarSesion.Image")));
            this.btnCerrarSesion.ImageSize = new System.Drawing.Size(30, 30);
            this.btnCerrarSesion.Location = new System.Drawing.Point(2, 640);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.ShadowDecoration.Parent = this.btnCerrarSesion;
            this.btnCerrarSesion.Size = new System.Drawing.Size(69, 38);
            this.btnCerrarSesion.TabIndex = 10;
            this.toolTipSideBar.SetToolTip(this.btnCerrarSesion, "Cerrar Sesión");
            this.btnCerrarSesion.UseTransparentBackground = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // BtnReportesProyecto
            // 
            this.BtnReportesProyecto.BackColor = System.Drawing.Color.Transparent;
            this.BtnReportesProyecto.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnReportesProyecto.BorderRadius = 24;
            this.BtnReportesProyecto.BorderThickness = 1;
            this.BtnReportesProyecto.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.BtnReportesProyecto.CheckedState.BorderColor = System.Drawing.Color.White;
            this.BtnReportesProyecto.CheckedState.FillColor = System.Drawing.Color.White;
            this.BtnReportesProyecto.CheckedState.Parent = this.BtnReportesProyecto;
            this.BtnReportesProyecto.CustomImages.Parent = this.BtnReportesProyecto;
            this.BtnReportesProyecto.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.BtnReportesProyecto.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnReportesProyecto.ForeColor = System.Drawing.Color.White;
            this.BtnReportesProyecto.HoverState.Parent = this.BtnReportesProyecto;
            this.BtnReportesProyecto.Image = ((System.Drawing.Image)(resources.GetObject("BtnReportesProyecto.Image")));
            this.BtnReportesProyecto.ImageSize = new System.Drawing.Size(40, 40);
            this.BtnReportesProyecto.Location = new System.Drawing.Point(15, 200);
            this.BtnReportesProyecto.Name = "BtnReportesProyecto";
            this.BtnReportesProyecto.ShadowDecoration.Parent = this.BtnReportesProyecto;
            this.BtnReportesProyecto.Size = new System.Drawing.Size(60, 50);
            this.BtnReportesProyecto.TabIndex = 9;
            this.toolTipSideBar.SetToolTip(this.BtnReportesProyecto, "Reportes de proyectos");
            this.BtnReportesProyecto.UseTransparentBackground = true;
            this.BtnReportesProyecto.CheckedChanged += new System.EventHandler(this.moveSideMark);
            this.BtnReportesProyecto.Click += new System.EventHandler(this.BtnReportesProyecto_Click);
            // 
            // btnCRUDRol
            // 
            this.btnCRUDRol.BackColor = System.Drawing.Color.Transparent;
            this.btnCRUDRol.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.btnCRUDRol.BorderRadius = 24;
            this.btnCRUDRol.BorderThickness = 1;
            this.btnCRUDRol.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.btnCRUDRol.CheckedState.BorderColor = System.Drawing.Color.White;
            this.btnCRUDRol.CheckedState.FillColor = System.Drawing.Color.White;
            this.btnCRUDRol.CheckedState.Parent = this.btnCRUDRol;
            this.btnCRUDRol.CustomImages.Parent = this.btnCRUDRol;
            this.btnCRUDRol.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.btnCRUDRol.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnCRUDRol.ForeColor = System.Drawing.Color.White;
            this.btnCRUDRol.HoverState.Parent = this.btnCRUDRol;
            this.btnCRUDRol.Image = ((System.Drawing.Image)(resources.GetObject("btnCRUDRol.Image")));
            this.btnCRUDRol.ImageSize = new System.Drawing.Size(40, 40);
            this.btnCRUDRol.Location = new System.Drawing.Point(15, 500);
            this.btnCRUDRol.Name = "btnCRUDRol";
            this.btnCRUDRol.ShadowDecoration.Parent = this.btnCRUDRol;
            this.btnCRUDRol.Size = new System.Drawing.Size(60, 50);
            this.btnCRUDRol.TabIndex = 8;
            this.toolTipSideBar.SetToolTip(this.btnCRUDRol, "Catálogo de roles");
            this.btnCRUDRol.UseTransparentBackground = true;
            this.btnCRUDRol.CheckedChanged += new System.EventHandler(this.moveSideMark);
            this.btnCRUDRol.Click += new System.EventHandler(this.btnCRUCRol_Click);
            // 
            // LblHome
            // 
            this.LblHome.AutoSize = true;
            this.LblHome.BackColor = System.Drawing.Color.Transparent;
            this.LblHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblHome.ForeColor = System.Drawing.Color.White;
            this.LblHome.Location = new System.Drawing.Point(-5, 36);
            this.LblHome.Name = "LblHome";
            this.LblHome.Size = new System.Drawing.Size(86, 25);
            this.LblHome.TabIndex = 4;
            this.LblHome.Text = "🆂🆃🅰🅿";
            this.LblHome.Click += new System.EventHandler(this.LblHome_Click);
            // 
            // BtnCRUDPersonal
            // 
            this.BtnCRUDPersonal.BackColor = System.Drawing.Color.Transparent;
            this.BtnCRUDPersonal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnCRUDPersonal.BorderRadius = 24;
            this.BtnCRUDPersonal.BorderThickness = 1;
            this.BtnCRUDPersonal.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.BtnCRUDPersonal.CheckedState.BorderColor = System.Drawing.Color.White;
            this.BtnCRUDPersonal.CheckedState.FillColor = System.Drawing.Color.White;
            this.BtnCRUDPersonal.CheckedState.Parent = this.BtnCRUDPersonal;
            this.BtnCRUDPersonal.CustomImages.Parent = this.BtnCRUDPersonal;
            this.BtnCRUDPersonal.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.BtnCRUDPersonal.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnCRUDPersonal.ForeColor = System.Drawing.Color.White;
            this.BtnCRUDPersonal.HoverState.Parent = this.BtnCRUDPersonal;
            this.BtnCRUDPersonal.Image = ((System.Drawing.Image)(resources.GetObject("BtnCRUDPersonal.Image")));
            this.BtnCRUDPersonal.ImageSize = new System.Drawing.Size(40, 40);
            this.BtnCRUDPersonal.Location = new System.Drawing.Point(15, 400);
            this.BtnCRUDPersonal.Name = "BtnCRUDPersonal";
            this.BtnCRUDPersonal.ShadowDecoration.Parent = this.BtnCRUDPersonal;
            this.BtnCRUDPersonal.Size = new System.Drawing.Size(60, 50);
            this.BtnCRUDPersonal.TabIndex = 0;
            this.toolTipSideBar.SetToolTip(this.BtnCRUDPersonal, "Catálogo de personal");
            this.BtnCRUDPersonal.UseTransparentBackground = true;
            this.BtnCRUDPersonal.CheckedChanged += new System.EventHandler(this.moveSideMark);
            this.BtnCRUDPersonal.Click += new System.EventHandler(this.BtnCRUDPersonal_Click);
            // 
            // BtnCRUDproyectos
            // 
            this.BtnCRUDproyectos.BackColor = System.Drawing.Color.Transparent;
            this.BtnCRUDproyectos.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnCRUDproyectos.BorderRadius = 24;
            this.BtnCRUDproyectos.BorderThickness = 1;
            this.BtnCRUDproyectos.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.BtnCRUDproyectos.CheckedState.BorderColor = System.Drawing.Color.White;
            this.BtnCRUDproyectos.CheckedState.FillColor = System.Drawing.Color.White;
            this.BtnCRUDproyectos.CheckedState.Parent = this.BtnCRUDproyectos;
            this.BtnCRUDproyectos.CustomImages.Parent = this.BtnCRUDproyectos;
            this.BtnCRUDproyectos.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.BtnCRUDproyectos.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnCRUDproyectos.ForeColor = System.Drawing.Color.White;
            this.BtnCRUDproyectos.HoverState.Parent = this.BtnCRUDproyectos;
            this.BtnCRUDproyectos.Image = ((System.Drawing.Image)(resources.GetObject("BtnCRUDproyectos.Image")));
            this.BtnCRUDproyectos.ImageSize = new System.Drawing.Size(40, 40);
            this.BtnCRUDproyectos.Location = new System.Drawing.Point(15, 300);
            this.BtnCRUDproyectos.Name = "BtnCRUDproyectos";
            this.BtnCRUDproyectos.ShadowDecoration.Parent = this.BtnCRUDproyectos;
            this.BtnCRUDproyectos.Size = new System.Drawing.Size(60, 50);
            this.BtnCRUDproyectos.TabIndex = 0;
            this.toolTipSideBar.SetToolTip(this.BtnCRUDproyectos, "Catálogo de proyectos");
            this.BtnCRUDproyectos.UseTransparentBackground = true;
            this.BtnCRUDproyectos.CheckedChanged += new System.EventHandler(this.moveSideMark);
            this.BtnCRUDproyectos.Click += new System.EventHandler(this.BtnCRUDProyectos_Click);
            // 
            // BtnSeguimiento
            // 
            this.BtnSeguimiento.BackColor = System.Drawing.Color.Transparent;
            this.BtnSeguimiento.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BtnSeguimiento.BorderRadius = 24;
            this.BtnSeguimiento.BorderThickness = 1;
            this.BtnSeguimiento.ButtonMode = Guna.UI2.WinForms.Enums.ButtonMode.RadioButton;
            this.BtnSeguimiento.CheckedState.BorderColor = System.Drawing.Color.White;
            this.BtnSeguimiento.CheckedState.FillColor = System.Drawing.Color.White;
            this.BtnSeguimiento.CheckedState.Parent = this.BtnSeguimiento;
            this.BtnSeguimiento.CustomImages.Parent = this.BtnSeguimiento;
            this.BtnSeguimiento.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(41)))), ((int)(((byte)(43)))));
            this.BtnSeguimiento.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.BtnSeguimiento.ForeColor = System.Drawing.Color.White;
            this.BtnSeguimiento.HoverState.Parent = this.BtnSeguimiento;
            this.BtnSeguimiento.Image = ((System.Drawing.Image)(resources.GetObject("BtnSeguimiento.Image")));
            this.BtnSeguimiento.ImageSize = new System.Drawing.Size(40, 40);
            this.BtnSeguimiento.Location = new System.Drawing.Point(15, 100);
            this.BtnSeguimiento.Name = "BtnSeguimiento";
            this.BtnSeguimiento.ShadowDecoration.Parent = this.BtnSeguimiento;
            this.BtnSeguimiento.Size = new System.Drawing.Size(60, 50);
            this.BtnSeguimiento.TabIndex = 0;
            this.toolTipSideBar.SetToolTip(this.BtnSeguimiento, "Seguimientos de proyectos");
            this.BtnSeguimiento.UseTransparentBackground = true;
            this.BtnSeguimiento.CheckedChanged += new System.EventHandler(this.moveSideMark);
            this.BtnSeguimiento.Click += new System.EventHandler(this.BtnSeguimiento_Click);
            // 
            // imgSlide
            // 
            this.imgSlide.Image = ((System.Drawing.Image)(resources.GetObject("imgSlide.Image")));
            this.imgSlide.Location = new System.Drawing.Point(48, 75);
            this.imgSlide.Name = "imgSlide";
            this.imgSlide.Size = new System.Drawing.Size(39, 101);
            this.imgSlide.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgSlide.TabIndex = 0;
            this.imgSlide.TabStop = false;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(74, 684);
            this.shapeContainer1.TabIndex = 7;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderColor = System.Drawing.Color.White;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = -2;
            this.lineShape2.X2 = 72;
            this.lineShape2.Y1 = 24;
            this.lineShape2.Y2 = 24;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderColor = System.Drawing.Color.White;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 0;
            this.lineShape1.X2 = 74;
            this.lineShape1.Y1 = 73;
            this.lineShape1.Y2 = 73;
            // 
            // BarraTitulo
            // 
            this.BarraTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(36)))), ((int)(((byte)(45)))));
            this.BarraTitulo.Controls.Add(this.BtnMin);
            this.BarraTitulo.Controls.Add(this.BtnMax);
            this.BarraTitulo.Controls.Add(this.BtnSalir);
            this.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarraTitulo.Location = new System.Drawing.Point(100, 0);
            this.BarraTitulo.Name = "BarraTitulo";
            this.BarraTitulo.Size = new System.Drawing.Size(944, 29);
            this.BarraTitulo.TabIndex = 4;
            this.BarraTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BarraTitulo_MouseDown);
            // 
            // BtnMin
            // 
            this.BtnMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnMin.Animated = true;
            this.BtnMin.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MinimizeBox;
            this.BtnMin.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.BtnMin.HoverState.Parent = this.BtnMin;
            this.BtnMin.IconColor = System.Drawing.Color.White;
            this.BtnMin.Location = new System.Drawing.Point(797, 0);
            this.BtnMin.Name = "BtnMin";
            this.BtnMin.ShadowDecoration.Parent = this.BtnMin;
            this.BtnMin.Size = new System.Drawing.Size(45, 29);
            this.BtnMin.TabIndex = 2;
            // 
            // BtnMax
            // 
            this.BtnMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnMax.Animated = true;
            this.BtnMax.ControlBoxType = Guna.UI2.WinForms.Enums.ControlBoxType.MaximizeBox;
            this.BtnMax.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.BtnMax.HoverState.Parent = this.BtnMax;
            this.BtnMax.IconColor = System.Drawing.Color.White;
            this.BtnMax.Location = new System.Drawing.Point(842, 0);
            this.BtnMax.Name = "BtnMax";
            this.BtnMax.ShadowDecoration.Parent = this.BtnMax;
            this.BtnMax.Size = new System.Drawing.Size(45, 29);
            this.BtnMax.TabIndex = 1;
            this.BtnMax.Click += new System.EventHandler(this.BtnMax_Click);
            // 
            // BtnSalir
            // 
            this.BtnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSalir.Animated = true;
            this.BtnSalir.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(34)))), ((int)(((byte)(43)))));
            this.BtnSalir.HoverState.FillColor = System.Drawing.Color.Red;
            this.BtnSalir.HoverState.Parent = this.BtnSalir;
            this.BtnSalir.IconColor = System.Drawing.Color.White;
            this.BtnSalir.Location = new System.Drawing.Point(887, 0);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.ShadowDecoration.Parent = this.BtnSalir;
            this.BtnSalir.Size = new System.Drawing.Size(45, 29);
            this.BtnSalir.TabIndex = 0;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // PanelFormulario
            // 
            this.PanelFormulario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelFormulario.BackColor = System.Drawing.Color.White;
            this.PanelFormulario.Controls.Add(this.label5);
            this.PanelFormulario.Controls.Add(this.label4);
            this.PanelFormulario.Controls.Add(this.pictureBox1);
            this.PanelFormulario.ForeColor = System.Drawing.Color.Silver;
            this.PanelFormulario.Location = new System.Drawing.Point(100, 29);
            this.PanelFormulario.Name = "PanelFormulario";
            this.PanelFormulario.Size = new System.Drawing.Size(948, 793);
            this.PanelFormulario.TabIndex = 3;
            this.PanelFormulario.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelFormulario_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(26)))), ((int)(((byte)(33)))));
            this.label5.Font = new System.Drawing.Font("Microsoft Tai Le", 9F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(57)))), ((int)(((byte)(67)))));
            this.label5.Location = new System.Drawing.Point(224, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(250, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Seguimiento de Tareas y Avance de Proyectos";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(26)))), ((int)(((byte)(33)))));
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 100F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(57)))), ((int)(((byte)(67)))));
            this.label4.Location = new System.Drawing.Point(55, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(573, 153);
            this.label4.TabIndex = 5;
            this.label4.Text = "🆂🆃🅰🅿";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(948, 793);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1044, 731);
            this.Controls.Add(this.BarraTitulo);
            this.Controls.Add(this.PanelFormulario);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "STAP";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.guna2CustomGradientPanel1.ResumeLayout(false);
            this.guna2CustomGradientPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlide)).EndInit();
            this.BarraTitulo.ResumeLayout(false);
            this.PanelFormulario.ResumeLayout(false);
            this.PanelFormulario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Panel BarraTitulo;
        private Guna.UI2.WinForms.Guna2ControlBox BtnMin;
        private Guna.UI2.WinForms.Guna2ControlBox BtnMax;
        private Guna.UI2.WinForms.Guna2ControlBox BtnSalir;
        private System.Windows.Forms.Panel PanelFormulario;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel1;
        private System.Windows.Forms.Label LblHome;
        private Guna.UI2.WinForms.Guna2Button BtnCRUDPersonal;
        private Guna.UI2.WinForms.Guna2Button BtnCRUDproyectos;
        private Guna.UI2.WinForms.Guna2Button BtnSeguimiento;
        private System.Windows.Forms.PictureBox imgSlide;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.ToolTip toolTipSideBar;
        private Guna.UI2.WinForms.Guna2Button btnCRUDRol;
        private Guna.UI2.WinForms.Guna2Button BtnReportesProyecto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2Button btnCerrarSesion;
    }
}

