﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STAP_CDC.Properties;
using System.Configuration;

namespace AccesoDatos
{
    public class Conexion
    {
        OdbcConnection ConexionMySQL;

      
        public void conectar()
        {
            ConexionMySQL = new OdbcConnection(ConfigurationManager.ConnectionStrings["Con"].ConnectionString);
            ConexionMySQL.Open();
        }

        public void desconectar()
        {
            ConexionMySQL.Close();
        }

        public DataSet ejecutarConsulta(String SQL)
        {
            OdbcDataAdapter AdaptadorDatos = new OdbcDataAdapter(SQL, ConexionMySQL);
            DataSet resultado = new DataSet();
            AdaptadorDatos.Fill(resultado);
            return resultado;
        }
        public bool ejecutarActualizacion(String SQL)
        {
            try
            {
                OdbcCommand Comando = new OdbcCommand(SQL, ConexionMySQL);
                Comando.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
    }
}
