USE STAP;

delete from `equipo_trabajo`;
delete from `tarea_personal`;
delete from `tarea`;
delete from `proyecto`;
delete from `rol`;
delete from `usuario`;
delete from `personal`;


INSERT INTO `proyecto` (`idProyecto`, `titulo_proyecto`, `detalles`, `inicio_proyecto`, `fin_proyecto`)
VALUES 
(1,'STAP','Realizar una plataforma para el seguimiento de tareas y avances de proyectos de trabajo del Centro de Desarrollo y Capacitación del ITSZN','2020-06-10','2020-08-10'),
(2,'Idiomas ITSZN','Facilitar la administración de la información y la generación de reportes en el centro de Idiomas del Instituto Tecnológico Superior Zacatecas Norte mediante un sistema de software que gestione los datos de los estudiantes, cursos, calificaciones, exámenes de ubicación y exámenes TOEIC','2020-09-21','2020-12-01');

INSERT INTO `personal` (`idPersonal`, `iniciales`, `nombre_completo`)
VALUES 
(1,'DAS','DANIEL ARREDONDO SALCEDO'),
(2,'VMDS','VICTOR MANUEL DELFIN SANTOS'),
(3,'IGF','IVAN GUTIERREZ FLORES'),
(4,'RDS','ROGELIO DOMINGUES SANCHEZ'),
(5,'AES','ABRAHAM ESQUIVEL SALAS'),
(6,'MISG','MANUEL IGNACIO SALAS GUZMAN'),
(7,'JMST','JOSE MARIA SALAS TORRES'); 

INSERT INTO `rol` 
VALUES 
(1,'PM','Administrador del proyecto'),
(2,'TL','Líder técnico'),
(3,'DES','Diseñador'),
(4,'PR','Desarrollador'),
(5,'AN','Analista'),
(6,'PO','Product Owner'),
(7,'CUS','Cliente');

INSERT INTO `equipo_trabajo` VALUES (1,1,1,1),(2,1,1,2),(3,1,2,3),(4,1,3,5),(5,1,4,4),(6,1,6,6),(7,1,7,7);

INSERT INTO `tarea`  (`idTarea`, `idProyecto`, `descripcion`, `inicio_plan`, `fin_plan`, `inicio_real`, `fin_real`)  VALUES 
(1,1,'Planificación del proyecto','2020-06-10','2020-06-12','2020-06-10','2020-06-12'),
(2,1,'Creación, estimación y priorización de HU','2020-06-15','2020-06-17','2020-06-15','2020-06-17'),
(3,1,'Investigación','2020-06-18','2020-06-19',NULL,NULL),
(4,1,'Planificación del sprint','2020-06-22','2020-06-22','2020-06-22','2020-06-22'),
(5,1,'HU7 - Diseño general','2020-06-22','2020-06-23','2020-06-22',NULL),
(9,1,'HU8 - Estandar GUI','2020-06-22','2020-06-23','2020-06-22',NULL),
(10,1,'HU1 - CRUD Personal','2020-06-24','2020-07-01',NULL,NULL),
(11,1,'HU2 - CRUD Roles','2020-06-24','2020-07-01',NULL,NULL),
(12,1,'HU3 - CRUD Proyectos','2020-06-24','2020-07-01',NULL,NULL);

INSERT INTO `tarea_personal` VALUES 
(1,1),(2,1),(5,1),(1,2),(2,2),(3,2),(10,2),(1,3),
(2,3),(3,3),(9,3),(11,3),(1,4),(2,4),(3,4),(9,4),(12,4);


INSERT INTO `Usuario` (`idPersonal`,`contrasena`, `tipo`)
VALUES 
(1,'DAS',1),
(2,'VMDS',0),
(3,'IGF',0),
(4,'RDS',0),
(5,'AES',0),
(6,'MISG',1),
(7,'JMST',1); 
