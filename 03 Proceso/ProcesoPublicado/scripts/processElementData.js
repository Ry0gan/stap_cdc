//------------------------------------------------------------------------------
// Copyright (c) 2005, 2006 IBM Corporation and others.
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v1.0
// which accompanies this distribution, and is available at
// http://www.eclipse.org/legal/epl-v10.html
// 
// Contributors:
// IBM Corporation - initial implementation
//------------------------------------------------------------------------------

// This file will include dynamically generated process layout related data from 
// the publishing service 
contentPage.processPage.imageFiles["_Kh_XIdSIEeeX1PFevEFi2AActivity"]="./../../Plugin_CDCITSZN/deliveryprocesses/resources/SCRUM_ISO29110S_667906B8_667906b8_Activity.jpeg"
contentPage.processPage.imageFiles["_Kh_XIdSIEeeX1PFevEFi2A,_9HgnUdrPEeecH5hx3_WldgActivity"]="./../../Plugin_CDCITSZN/deliveryprocesses/resources/Sprint_417BABE1_81be98ed_Activity.jpeg"
contentPage.processPage.imageFiles["_Kh_XIdSIEeeX1PFevEFi2A,_9HgnUdrPEeecH5hx3_WldgActivityDetail"]="./../../Plugin_CDCITSZN/deliveryprocesses/resources/Sprint_417BABE1_81be98ed_ActivityDetail.jpeg"
contentPage.processPage.imageFiles["_Kh_XIdSIEeeX1PFevEFi2A,_JAaZ4drOEeecH5hx3_WldgActivity"]="./../../Plugin_CDCITSZN/deliveryprocesses/resources/Inicio_A86A205B_e8ad0d67_Activity.jpeg"
contentPage.processPage.imageFiles["_Kh_XIdSIEeeX1PFevEFi2A,_JAaZ4drOEeecH5hx3_WldgActivityDetail"]="./../../Plugin_CDCITSZN/deliveryprocesses/resources/Inicio_A86A205B_e8ad0d67_ActivityDetail.jpeg"
contentPage.processPage.imageFiles["_Kh_XIdSIEeeX1PFevEFi2A,_X_NUcdrOEeecH5hx3_WldgActivity"]="./../../Plugin_CDCITSZN/deliveryprocesses/resources/Cierre_5F18CC0A_9f5bb916_Activity.jpeg"
contentPage.processPage.elementUrls["_TCeUs9rOEeecH5hx3_Wldg"] = ["Historias de usuario","Plugin_CDCITSZN/deliveryprocesses/historias_de_usuario_706721C0.html"];
contentPage.processPage.elementUrls["_IE2eoNrQEeecH5hx3_Wldg"] = ["Evaluar cambios","Plugin_CDCITSZN/deliveryprocesses/evaluar_cambios_B6A0F12B.html"];
contentPage.processPage.elementUrls["_C5GLc9rQEeecH5hx3_Wldg"] = ["Project Manager (Administrador de proyecto)","Plugin_CDCITSZN/deliveryprocesses/project_manager_DBCC73CA.html"];
contentPage.processPage.elementUrls["_MxI5idrOEeecH5hx3_Wldg"] = ["Enunciado de proyecto","Plugin_CDCITSZN/deliveryprocesses/enunciado_de_proyecto_323FA58D.html"];
contentPage.processPage.elementUrls["_21wzwAXBEeiL4p4195R-Ug"] = ["Technical Leader (Líder técnico)","Plugin_CDCITSZN/deliveryprocesses/technical_leader_E81B90BA.html"];
contentPage.processPage.elementUrls["_grhoYgXQEeiWD9oW6duE3w"] = ["Acta de reunión de avance","Plugin_CDCITSZN/deliveryprocesses/acta_de_reunion_de_avance_581E84C5.html"];
contentPage.processPage.elementUrls["_LHISNKdHEeiPEMncseLNVw"] = ["Diseño general","Plugin_CDCITSZN/deliveryprocesses/diseno_general_A3549F3F.html"];
contentPage.processPage.elementUrls["_wKegJAXEEeiL4p4195R-Ug"] = ["Manual de mantenimiento","Plugin_CDCITSZN/deliveryprocesses/manual_de_mantenimiento_6496F7FE.html"];
contentPage.processPage.elementUrls["_wKegLQXEEeiL4p4195R-Ug"] = ["Analyst (Analista)","Plugin_CDCITSZN/deliveryprocesses/analyst_F8589C4C.html"];
contentPage.processPage.elementUrls["_MxI5gNrOEeecH5hx3_Wldg"] = ["Planificación de proyecto","Plugin_CDCITSZN/deliveryprocesses/planificacion_de_proyecto_BA82CD39.html"];
contentPage.processPage.elementUrls["_C5GLcdrQEeecH5hx3_Wldg"] = ["Work team (Equipo de trabajo)","Plugin_CDCITSZN/deliveryprocesses/work_team_6854BDF5.html"];
contentPage.processPage.elementUrls["_IE2eo9rQEeecH5hx3_Wldg"] = ["Plan de proyecto","Plugin_CDCITSZN/deliveryprocesses/plan_de_proyecto_18B1BB16.html"];
contentPage.processPage.elementUrls["_JAaZ4drOEeecH5hx3_Wldg"] = ["Inicio","Plugin_CDCITSZN/deliveryprocesses/Inicio_A86A205B.html"];
contentPage.processPage.elementUrls["_Kh_XIdSIEeeX1PFevEFi2A"] = ["SCRUM con ISO29110","Plugin_CDCITSZN/deliveryprocesses/SCRUM_ISO29110S_667906B8.html"];
contentPage.processPage.elementUrls["_21wzwgXBEeiL4p4195R-Ug"] = ["Designer (Diseñador)","Plugin_CDCITSZN/deliveryprocesses/designer_E15284E0.html"];
contentPage.processPage.elementUrls["_MxI5jdrOEeecH5hx3_Wldg"] = ["Control de riesgos","Plugin_CDCITSZN/deliveryprocesses/control_de_riesgos_F6D187AC.html"];
contentPage.processPage.elementUrls["_oPGY1KdFEeiPEMncseLNVw"] = ["Project Manager (Administrador de proyecto)","Plugin_CDCITSZN/deliveryprocesses/project_manager_76E1FAC7.html"];
contentPage.processPage.elementUrls["_gr-UUQXQEeiWD9oW6duE3w"] = ["Acta de aceptación","Plugin_CDCITSZN/deliveryprocesses/acta_de_aceptacion_11A392B4.html"];
contentPage.processPage.elementUrls["_TCeUsNrOEeecH5hx3_Wldg"] = ["Creación de historias de usuario","Plugin_CDCITSZN/deliveryprocesses/creacion_de_historias_de_usuario_E5657D5.html"];
contentPage.processPage.elementUrls["_807_8qc7EeiPEMncseLNVw"] = ["Componente de software","Plugin_CDCITSZN/deliveryprocesses/componente_de_software_88B02265.html"];
contentPage.processPage.elementUrls["_ZTu3YNrOEeecH5hx3_Wldg"] = ["Cerrar el proyecto","Plugin_CDCITSZN/deliveryprocesses/cerrar_el_proyecto_3E4C7848.html"];
contentPage.processPage.elementUrls["_TCeUsdrOEeecH5hx3_Wldg"] = ["Analyst (Analista)","Plugin_CDCITSZN/deliveryprocesses/analyst_FCEF6BEB.html"];
contentPage.processPage.elementUrls["_C5GLcNrQEeecH5hx3_Wldg"] = ["Planificación de sprint","Plugin_CDCITSZN/deliveryprocesses/planificacion_de_sprint_79BBA9DF.html"];
contentPage.processPage.elementUrls["_80VjAac7EeiPEMncseLNVw"] = ["Customer (Cliente/Representante del cliente)","Plugin_CDCITSZN/deliveryprocesses/customer_4A1E5E38.html"];
contentPage.processPage.elementUrls["_IpHpUNrQEeecH5hx3_Wldg"] = ["Reunión de retrospectiva","Plugin_CDCITSZN/deliveryprocesses/reunion_de_retrospectiva_9BBBD625.html"];
contentPage.processPage.elementUrls["_22M4oAXBEeiL4p4195R-Ug"] = ["Diseño general","Plugin_CDCITSZN/deliveryprocesses/diseno_general_93018EE7.html"];
contentPage.processPage.elementUrls["_2wPakAXBEeiL4p4195R-Ug"] = ["Crear/Actualizar diseño general","Plugin_CDCITSZN/deliveryprocesses/crear_actualizar_diseno_general_C3CCE43A.html"];
contentPage.processPage.elementUrls["_MxI5h9rOEeecH5hx3_Wldg"] = ["Work team (Equipo de trabajo)","Plugin_CDCITSZN/deliveryprocesses/work_team_E1257943.html"];
contentPage.processPage.elementUrls["_wKegIQXEEeiL4p4195R-Ug"] = ["Technical Leader (Líder técnico)","Plugin_CDCITSZN/deliveryprocesses/technical_leader_AAA2F5EF.html"];
contentPage.processPage.elementUrls["_MxI5g9rOEeecH5hx3_Wldg"] = ["Project Manager (Administrador de proyecto)","Plugin_CDCITSZN/deliveryprocesses/project_manager_1C939724.html"];
contentPage.processPage.elementUrls["_9HgnUdrPEeecH5hx3_Wldg"] = ["Sprint","Plugin_CDCITSZN/deliveryprocesses/Sprint_417BABE1.html"];
contentPage.processPage.elementUrls["_MxI5gdrOEeecH5hx3_Wldg"] = ["Customer (Cliente/Representante del cliente)","Plugin_CDCITSZN/deliveryprocesses/customer_A91BE14F.html"];
contentPage.processPage.elementUrls["_ptU2hAXEEeiL4p4195R-Ug"] = ["Diseño detallado de historia de usuario","Plugin_CDCITSZN/deliveryprocesses/diseno_detallado_de_historia_de_usuario_B6230C0B.html"];
contentPage.processPage.elementUrls["_wKegMAXEEeiL4p4195R-Ug"] = ["Designer (Diseñador)","Plugin_CDCITSZN/deliveryprocesses/designer_B24C9E5B.html"];
contentPage.processPage.elementUrls["_IE2eodrQEeecH5hx3_Wldg"] = ["Solicitud de cambio","Plugin_CDCITSZN/deliveryprocesses/solicitud_de_cambio_A53A0541.html"];
contentPage.processPage.elementUrls["_wKegJwXEEeiL4p4195R-Ug"] = ["Manual de operación","Plugin_CDCITSZN/deliveryprocesses/manual_de_operacion_686BCC34.html"];
contentPage.processPage.elementUrls["_wKegKgXEEeiL4p4195R-Ug"] = ["Manual de usuario","Plugin_CDCITSZN/deliveryprocesses/manual_de_usuario_225FCE43.html"];
contentPage.processPage.elementUrls["_62VQwAXCEeiL4p4195R-Ug"] = ["Implementacion de historias de usuario","Plugin_CDCITSZN/deliveryprocesses/implementacion_de_historias_de_usuario_9C8090D0.html"];
contentPage.processPage.elementUrls["_LHISMadHEeiPEMncseLNVw"] = ["Diseño detallado de historia de usuario","Plugin_CDCITSZN/deliveryprocesses/diseno_detallado_de_historia_de_usuario_CD5BD136.html"];
contentPage.processPage.elementUrls["_MxI5i9rOEeecH5hx3_Wldg"] = ["Plan de proyecto","Plugin_CDCITSZN/deliveryprocesses/plan_de_proyecto_A5B75B62.html"];
contentPage.processPage.elementUrls["_grYecQXQEeiWD9oW6duE3w"] = ["Historias de usuario","Plugin_CDCITSZN/deliveryprocesses/historias_de_usuario_7C14E4CA.html"];
contentPage.processPage.elementUrls["_Zshx4NrOEeecH5hx3_Wldg"] = ["Generar los manuales","Plugin_CDCITSZN/deliveryprocesses/generar_los_manuales_770FA9E.html"];
contentPage.processPage.elementUrls["_LGicUadHEeiPEMncseLNVw"] = ["Plan de pruebas","Plugin_CDCITSZN/deliveryprocesses/plan_de_pruebas_B432D49D.html"];
contentPage.processPage.elementUrls["_egwQYQXEEeiL4p4195R-Ug"] = ["Programmer (Programador)","Plugin_CDCITSZN/deliveryprocesses/programmer_7BD17EE5.html"];
contentPage.processPage.elementUrls["_GwTGgNrQEeecH5hx3_Wldg"] = ["Revisión del Sprint","Plugin_CDCITSZN/deliveryprocesses/revision_del_sprint_A46D6227.html"];
contentPage.processPage.elementUrls["_80MZEac7EeiPEMncseLNVw"] = ["Product Backlog","Plugin_CDCITSZN/deliveryprocesses/product_backlog_5D0F414D.html"];
contentPage.processPage.elementUrls["_MxI5hdrOEeecH5hx3_Wldg"] = ["Technical Leader (Líder técnico)","Plugin_CDCITSZN/deliveryprocesses/technical_leader_6DADC36E.html"];
contentPage.processPage.elementUrls["_grYedAXQEeiWD9oW6duE3w"] = ["Componente de software","Plugin_CDCITSZN/deliveryprocesses/componente_de_software_3608E6D9.html"];
contentPage.processPage.elementUrls["_oPGY0adFEeiPEMncseLNVw"] = ["Customer (Cliente/Representante del cliente)","Plugin_CDCITSZN/deliveryprocesses/customer_A0E92CBE.html"];
contentPage.processPage.elementUrls["_LGrmQadHEeiPEMncseLNVw"] = ["Control de riesgos","Plugin_CDCITSZN/deliveryprocesses/control_de_riesgos_CF43A702.html"];
contentPage.processPage.elementUrls["_TCeUtdrOEeecH5hx3_Wldg"] = ["Product Backlog","Plugin_CDCITSZN/deliveryprocesses/product_backlog_C1814E0A.html"];
contentPage.processPage.elementUrls["_X_NUcdrOEeecH5hx3_Wldg"] = ["Cierre","Plugin_CDCITSZN/deliveryprocesses/Cierre_5F18CC0A.html"];
